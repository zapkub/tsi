### SERVICE FINDER API

#### Event
-  serviceMarkerIsClicked
return : CustomEvent Object
 Attribute

```
#!javascript

    { 
       @extends CustomEvent;

       detail: { 
         info : { ServiceObject },
         marker: { GoogleMap.Marker }
       }
     }
```


#### Example


```
#!javascript

  <script>
    // API Example
      window.addEventListener('serviceMarkerIsClicked',function(e){
        alert(e.detail.info.garage_name_th);
      });
  </script>
```


#