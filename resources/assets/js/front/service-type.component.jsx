let React = require('react');
let ReactDOM = require('react-dom');

import classNames from 'classnames';

export default class ServiceTypeSelector extends React.Component
{
  constructor(props){
    super(props);//?
  }
  selectedType(key)   {
    this.props.onChange(key);
  }
  render()
  {
    return (
      <div className="type-selector-container">
        {services.map((item,key)=>{
          let classn = classNames("type-child",{
            selected:(key == this.props.selected)
          });
          return (<div className={classn} onClick={this.selectedType.bind(this,key)} key={key}>{item.name}</div>);
        })}
      </div>
    )
  }
}
