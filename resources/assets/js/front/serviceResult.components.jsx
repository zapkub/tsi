let React = require('react');
let ReactDOM = require('react-dom');
import { _ } from 'underscore';
import CustomMarker from './customGoogleMapMarker';
import ServiceTypeSelector from './service-type.component.jsx';

class ServiceFormResult extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
          center : props.center,
          result : props.result
        }
        //?
        this.markPosition = this.markPosition.bind(this);
    }

    componentDidMount() {
        var styles = [
            {
                stylers: [
                    { hue: "#053c79" },
                    { saturation: 100 }
                ]
            }, {
                featureType: "road",
                elementType: "geometry",
                stylers: [
                    { lightness: 100 },
                    { visibility: "simplified" }
                ]
            }, {
                featureType: "road",
                elementType: "labels",
                stylers: [
                    { visibility: "off" }
                ]
            }
        ];
        this.map = new google.maps.Map(document.getElementById('service-map'), {
            center: { lat: 13.397, lng: 102.644 },
            // scrollwheel: false,
            zoom: 12,
            //draggable: false,
            disableDefaultUI: true

        });
        this.map.setOptions({ styles: styles });
        if(this.state.center)
        this.markPosition(this.state.center,this.state.result);
       
    }
    markPosition(center,result) {
        let markers = [];

        this.map.panTo(new google.maps.LatLng(center.lat,center.lng));
        _.each(result.service, (item, $index) => {
            // let marker = new google.maps.Marker({
            //     position:
            //     {
            //         lat: parseFloat(item.latitude),
            //         lng: parseFloat(item.longitude)
            //     },
            //     map: this.map,
            //     animation: google.maps.Animation.DROP,
            //     title: item.garage_name
            // })
            // console.log(item);
            let marker = new CustomMarker(
              new google.maps.LatLng(item.latitude,item.longitude),
              this.map,
              {
                marker_id:"result"+$index,
                title:item.name
              }
            )
            marker.addListener('click',function(){
                var event = new CustomEvent('serviceMarkerIsClicked', {'detail': {
                    marker: marker,
                    info: item,
                }});
                window.dispatchEvent(event);
            });
            markers.push(marker);
        })

        

        this.setState({ 'markers': markers });

        console.log(this.props.HQLocation);
        markers.push(new CustomMarker(
           new google.maps.LatLng(this.props.HQLocation.lat,this.props.HQLocation.lng),
           this.map,
           {
                marker_id:'HQ',
                title:'บริษัท ไทยเศรษฐกิจประกันภัย จำกัด มหาชน'
           }
        ));
    }
    clearMarker() {
        _.each(this.state.markers, item => {
            item.remove();
            item.setMap(null);
        })
        this.setState({ 'markers': [] })
    }
    componentWillReceiveProps(nextProps) {
        this.setState({
          center:nextProps.center,
          result:nextProps.result
        })
    }
    shouldComponentUpdate (nextProps,nextState){
      if(nextProps.center != this.props.center){
        this.clearMarker();
        
        this.markPosition(nextProps.center,nextProps.result);
      }
      return nextProps !== this.props;
    }
    render() {
        //create marker
        // console.log(this.state);
        return (
            <div className="service-result">
            <div className="map-wrap">

                {this.props.children}
                <div id="service-map"></div>
            </div>
                <div className="result-list">
                    <table className="table result-wrap">
                        <tbody>
                            <tr>
                                <td>
                                    <h2>ผลลัพธ์การค้นหา: { this.state.result.service.length}</h2>
                                </td>
                            </tr>
                            {
                                this.state.result.service.map((item, index) => (
                                    <tr key={index}>
                                        <td>
                                            <b>{item.name}</b> { (item.latitude) ? "":(<span className="latnotfound">Lat/long not found</span>)}
                                            <p>{item.address1 + item.address2 + item.zipcode}</p>
                                            <p>โทรศัพท์: {item.phone} <br/> Fax: {item.fax} </p>
                                        </td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        )

    }
}


ServiceFormResult.defaultProps = {};
export default ServiceFormResult;
