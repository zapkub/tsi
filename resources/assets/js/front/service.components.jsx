'use strict';
import React from 'react'; // React
import ReactDOM from 'react-dom';
import $ from 'jquery';
import classNames from 'classnames';
import "babel-polyfill"; // add promise library
import {_} from 'underscore';
import ServiceFormResult from "./serviceResult.components.jsx";
import linkState from 'react-link-state';
import ServiceTypeSelector from './service-type.component.jsx';

class ServiceFormDropdown extends React.Component {
  constructor() {
    super();
    this.map; //??
  }
  componentDidMount() {}
  selectedOption(event) {
    this.setState({'selected': event.target.value});
    this.props.onChange(parseInt(event.target.value));
  }
  render() {
    let spinnerClass = classNames({'spinner': true, 'active': this.props.isLoading})
    let selectClass = classNames({
      'dropdown': true,
      'hide-icon': this.props.isLoading || !this.props.choice.length,
      'col-xs-12': true,
      'col-md-7': true
    })
    let placeholder = this.props.isLoading
      ? 'กำลังโหลดข้อมูล '
      : (this.props.choice.length)
        ? 'เลือก'
        : '' + this.props.placeholder
    return (
      <div className="form-row">
        <label className={selectClass}>
          <div className="spinner-wrap">
            <div className={spinnerClass}></div>
          </div>
          <select value={this.props.selected} className='dropdown' onChange={this.selectedOption.bind(this)}>
            {(
              <option key={99999999} value={'default'}>

                {placeholder}

              </option>
            )}
            {this.props.choice.map((item, index) => {
              if (this.props.isLoading) {
                return;
              }
              return (
                <option value={index} key={index}>
                  {item.name}
                  &nbsp;
                  พบ
                  &nbsp;
                  {item.count
                    ? item.count
                    : 0 }
                  &nbsp;
                  แห่ง
                </option>
              )
            })}
          </select>
        </label>
      </div>
    )

  }
}

class ServiceComponent extends React.Component {
  constructor() {
    super();
    this.onChange = this.onChange.bind(this);
    this.state = {
      HQlocation: {lat:13.7245206,lng: 100.6331099},
      selectedArea: null,
      selectedProvince: null,
      selectedDistrict: null,
      serviceResult:{service:[]},
      areaLoading: false,
      area: [],
      province: [],
      district: [],
      mapCenter:{lat:13.7245206,lng: 100.6331099},
      locationName: '',
      isLoadingProvince: false,
      isLoadingArea: false,
      isLoadingdistrict: false,
      service: services,
      selectedService: services[0].type,
      selectedServiceIndex :0,
    };
  }
  onChange(state) {
    this.setState(state);
  }

  ajaxGet(url, callback) {
    let promise = new Promise((resolve, reject) => {
      setTimeout(() => {
        $.get(url).then((res) => {
          if (callback) {
            callback(res);
            resolve();
          } else {
            resolve(res);
          }

        })
      }, 0)
    });

    return promise;
  }
  getCurrentServiceType(index)
  {

    if(typeof index === "undefined")
      index = this.state.selectedServiceIndex;

    return services[index].type;
  }
  componentDidMount() {
    // get area first
    this.setState({'isLoadingArea': true});
    // console.log('start components');
    this.ajaxGet('service-finder/available-area' +
      '/' + services[0].type, (res) => {
      
      // this.setState({HQlocation:{
      //   lat: 13.7245206,
      //   lng: 100.6331099
      // }})
      this.setState({'isLoadingArea': false});
      this.setState({'area': res.area});
      console.log(this.state);
      //dev
      //this.getDevelopData();
    })

  }

  refreshData(index)
  {
      this.setState({'isLoadingArea': true,area: [],
      province: [],
      district: []});
      this.ajaxGet('service-finder/available-area' +
      '/' + this.getCurrentServiceType(index), (res) => {

          this.setState({'isLoadingArea': false});
          this.setState({'area': res.area,'selectedArea':null});

      });

  }

  getProvince(index) {
    let area = this.state.area[index];
    let area_id = area.id;
    this.setState({selectedArea: index, province: [], district: []})
    this.setState({'isLoadingProvince': true});
    let request = this.ajaxGet('service-finder/available-province/' + area_id + '/' + this.getCurrentServiceType(), (res) => {
      
      this.setState({'isLoadingProvince': false});
      this.setState({province: res.province, selectedProvince: 'default', selectedDistrict: 'default'})
    })
    return request;
  }
  getDistrict(index) {
    let province = this.state.province[index];
    let province_id = province.id;
    this.setState({selectedProvince: index});
    this.setState({'isLoadingdistrict': true})
    let request = this.ajaxGet('service-finder/available-district/' + province_id + '/' + this.getCurrentServiceType(), (res) => {
       console.log(res);
      this.setState({'isLoadingdistrict': false});

      let address = `${res.province_name}+Thailand`;
      this.ajaxGet('https://maps.googleapis.com/maps/api/geocode/json?address=' + address, (location_res) => {
        let location = {}
        if (location_res.results.length) {
          location = location_res.results[0].geometry.location;
        }
        this.setState({'serviceResult': res, district: res.district, mapCenter: location})
      });

      
    })
    return request;
  }
  getDataFromDistrict(index) {
    let district = this.state.district[index];
    let district_id = district.id;
    this.setState({selectedDistrict: index, isMapLoading: true});
    // get Service , Garage list gprs result here by district_id and type

    this.ajaxGet('service-finder/available-service/' + district_id + '/' + this.getCurrentServiceType(), (service_res) => {
      // console.log(service_res)
      let address = `${service_res.province_name}+${service_res.district_name}+Thailand`;
      this.ajaxGet('https://maps.googleapis.com/maps/api/geocode/json?address=' + address, (location_res) => {
        let location = {}
        if (location_res.results.length) {
          location = location_res.results[0].geometry.location;
        }
        this.setState({'serviceResult': service_res, mapCenter: location, locationName: address, isMapLoading: false});
      })
    })

  }
  selectSevice(index) {
    this.setState({'selectedServiceIndex': index});
    this.refreshData(index);
  }

  getDevelopData() {
    this.getProvince(1).then(() => {

      this.getDistrict(0).then(() => {
        this.getDataFromDistrict(14)
      });
    })
  }
  render() {
    
    if (this.state.serviceResult || true) {
      let isLoadingClass = classNames({'map-loading-wrap': true, 'active': this.state.isMapLoading})
      return (
        <div>
            <div className={isLoadingClass}>
              <div className="spinner">
            </div>
          </div>
          <ServiceFormResult HQLocation={this.state.HQlocation} result={this.state.serviceResult} center={this.state.mapCenter} name={this.state.locationName}>
            <ServiceTypeSelector onChange={this.selectSevice.bind(this)} selected={this.state.selectedServiceIndex}></ServiceTypeSelector>
            <div className="location-selected-wrap">
              <ServiceFormDropdown selected={this.state.selectedArea} isLoading={this.state.isLoadingArea} placeholder="ภาค" choice={this.state.area} onChange={this.getProvince.bind(this)}/>
              <ServiceFormDropdown selected={this.state.selectedProvince} isLoading={this.state.isLoadingProvince} placeholder="จังหวัด" choice={this.state.province} onChange={this.getDistrict.bind(this)}/>
              <ServiceFormDropdown selected={this.state.selectedDistrict} isLoading={this.state.isLoadingdistrict} placeholder="เขต / อำเภอ" choice={this.state.district} onChange={this.getDataFromDistrict.bind(this)}/>
            </div>
            </ServiceFormResult>

        </div>
      );
    } else {
      return (
        <div>
          <div id="banner">
            <div className="container service-intro">
              <div className="mascot">
                <div className="image-div">
                </div>
              </div>
              <div className="form-wrap">
                <div className="content">
                  <form className="service-finder-form">
                    <h1 className="">
                      <span>ค้นหาบริการของ TSI
                      </span>
                    </h1>
                    <h2>เลือกประเภทของบริการ</h2>
                    <div className="service-list">
                      {this.state.service.map((item, index) => {
                        return (
                          <div className="service-item" key={index} data-type={item.type} onClick={this.selectSevice.bind(this,index)}>
                            <div className="service-box"></div>
                            {item.name}
                          </div>
                        )
                      })}
                    </div>

                    <ServiceFormDropdown isLoading={this.state.isLoadingArea} placeholder="ภาค" choice={this.state.area} selected={this.state.selectedArea} onChange={this.getProvince.bind(this)}/>
                    <ServiceFormDropdown isLoading={this.state.isLoadingProvince} placeholder="จังหวัด" require="ภาค" choice={this.state.province} selected={this.state.selectedProvince} onChange={this.getDistrict.bind(this)}/>
                    <ServiceFormDropdown isLoading={this.state.isLoadingdistrict} placeholder="เขต / อำเภอ" choice={this.state.district} selected={this.state.selectedDistrict} onChange={this.getDataFromDistrict.bind(this)}/>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      )
    }
  }
}

// Start React //
/* */(function() {
  ReactDOM.render(
    <ServiceComponent/>, document.getElementById('service-finder'));
})()

export default ServiceComponent;
