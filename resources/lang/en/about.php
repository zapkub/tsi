<?php
return [
  'company_name'=>'The Thai Setakij Insurance Public Company Limited',
  'history'=>'ประวัติความเป็นมา',
  'vision'=>'วิสัยทัศน์และพันธกิจ',
  'manager'=>'คณะผู้บริหาร',
  'committee'=>'คณะกรรมการบริหาร',
  'annual_report'=>'รายงานประจำปี',
  'financial'=>'ฐานะการเงินและผลการดำเนินงาน',
  'financial_report'=>'รายงานฐานะการเงิน',
  'download'=>'Download',
  'about'=>'เกี่ยวกับบริษัท'
];
