<?php
return [
  'campaign'=>'เลือกแคมเปญที่เหมาะสมกับคุณ',
  'campaign_budget'=>'งบประมาณที่ต้องการ',
  'campaign_car'=>'ประเภทรถของคุณ',
  'btn_search'=>'ค้นหา',
  'have_accident'=>'เมื่อท่านเกิดอุบัติเหตุ ควรทำอย่างไรดี?',
  'car_insurance'=>'ประกันภัยรถยนต์แต่ละประเภทต่างกันอย่างไร.....อยากรู้ คลิ๊กเลย',
  'select_insurance'=>'เลือกประกันภัยที่คุณต้องการ',
  'insurance_type'=>'ประกันภัยแบบไหนเหมาะกับคุณ',
  'news_activity'=>'ข่าวสารและกิจกรรม',
  'read_more'=>'Read more',
  'agent'=>'Agent'

];
