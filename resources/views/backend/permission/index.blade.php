<?php
use App\Http\Controllers\Backend\PermissionController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new PermissionController(); // New Object Controller
$mainFn = new MainFunction(); // New Object Main Function

$table = $objCon->tbName;
$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$path = $objCon->path;

$orderBy = Input::get('orderBy');
$sortBy = Input::get('sortBy');
$search = Input::get('search');

$a_otherParam = Input::except(['orderBy','sortBy']);
$strParam = $mainFn->parameter($a_otherParam);

$admin_id = auth()->guard('admin')->user()->admin_id;
if(!empty($admin_id)){
    // echo $mainFn->chk_permission($admin_id,'manage_user');
}else{
    return Redirect::to('_admin/logout');
}



?>
@extends('admin')
@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>{{ $titlePage }}</h1>
                </div>
            </div>
        </div>

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->

                        <div class="portlet light">

                            <form action{{URL::to($path)}} class="form-horizontal" method="GET" >
                            <input type="hidden" name="admin_role_id" value="{{ Input::get('admin_role_id') }}" >
                            <div class="form-group">
                                <label class="control-label col-md-1">Search</label>
                                <div class="col-md-3">
                                    <input class="form-control" type="text" name="search" value="{{ $search }}" >
                                    <span class="help-block">Search by page name</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-1 col-md-3 ">
                                    <button class="btn blue btn-sm" type="submit" ><i class="fa fa-search"></i> Search</button>
                                </div>
                            </div>
                            </form>
                            <hr>

                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-database font-green-sharp"></i>
                                    <span class="caption-subject font-green-sharp bold">Found {{ $countData }} Record(s).</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('ID',$pkField,$orderBy,$sortBy,$strParam) !!} </th>
                                            <th>{!! $mainFn->sorting('Page Name','page_name',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="text-center col-md-1 font-blue">Read</th>
                                            <th class="text-center col-md-1 font-blue">Create</th>
                                            <th class="text-center col-md-1 font-blue">Update</th>
                                            <th class="text-center col-md-1 font-blue">Delete</th>
                                            <th class="text-center col-md-1 font-blue"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($countData > 0)
                                            @foreach($data as $field)
                                                <tr>
                                                    <td class="text-center">{{ $field->$pkField }}</td>
                                                    <td>{{ $field->page_name }}</td>

                                                    @if($field->r == 0)
                                                        <td><div class="text-center"><a id="r-{{$field->$pkField}}" class="change-active fa fa-close text-red" data-tb_name="{{$table}}" data-pk_field="{{$pkField}}" data-v_pk_field="{{$field->$pkField}}" data-change_field="r"  data-value="{{$field->r}}"></a></div></td>
                                                    @else
                                                        <td><div class="text-center"><a id="r-{{$field->$pkField}}" class="change-active fa fa-check text-green" data-tb_name="{{$table}}" data-pk_field="{{$pkField}}" data-v_pk_field="{{$field->$pkField}}" data-change_field="r"  data-value="{{$field->r}}"></a></div></td>
                                                    @endif

                                                    
                                                    @if($field->c == 0)
                                                        <td><div class="text-center"><a id="c-{{$field->$pkField}}" class="change-active fa fa-close text-red" data-tb_name="{{$table}}" data-pk_field="{{$pkField}}" data-v_pk_field="{{$field->$pkField}}" data-change_field="c"  data-value="{{$field->c}}"></a></div></td>
                                                    @else
                                                        <td><div class="text-center"><a id="c-{{$field->$pkField}}" class="change-active fa fa-check text-green" data-tb_name="{{$table}}" data-pk_field="{{$pkField}}" data-v_pk_field="{{$field->$pkField}}" data-change_field="c"  data-value="{{$field->c}}"></a></div></td>
                                                    @endif

                                                    

                                                    @if($field->u == 0)
                                                        <td><div class="text-center"><a id="u-{{$field->$pkField}}" class="change-active fa fa-close text-red" data-tb_name="{{$table}}" data-pk_field="{{$pkField}}" data-v_pk_field="{{$field->$pkField}}" data-change_field="u"  data-value="{{$field->u}}"></a></div></td>
                                                    @else
                                                        <td><div class="text-center"><a id="u-{{$field->$pkField}}" class="change-active fa fa-check text-green" data-tb_name="{{$table}}" data-pk_field="{{$pkField}}" data-v_pk_field="{{$field->$pkField}}" data-change_field="u"  data-value="{{$field->u}}"></a></div></td>
                                                    @endif

                                                    @if($field->d == 0)
                                                        <td><div class="text-center"><a id="d-{{$field->$pkField}}" class="change-active fa fa-close text-red" data-tb_name="{{$table}}" data-pk_field="{{$pkField}}" data-v_pk_field="{{$field->$pkField}}" data-change_field="d"  data-value="{{$field->d}}"></a></div></td>
                                                    @else
                                                        <td><div class="text-center"><a id="d-{{$field->$pkField}}" class="change-active fa fa-check text-green" data-tb_name="{{$table}}" data-pk_field="{{$pkField}}" data-v_pk_field="{{$field->$pkField}}" data-change_field="d"  data-value="{{$field->d}}"></a></div></td>
                                                    @endif

                                                    <td class="text-center">
                                                        <a class="change-crud-active btn btn-xs btn-circle green" data-tb_name="{{$table}}" data-pk_field="{{$pkField}}" data-v_pk_field="{{$field->$pkField}}"
                                                            data-change_field_c="c" data-change_field_r="r" data-change_field_u="u" data-change_field_d="d" data-value="0"> Select All</a>
                                                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr><td colspan='7' class='text-center'>No Result.</td></tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                {!! $data->appends(Input::except('page'))->render() !!}
                            </div>
                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <script src="{{ URL::asset('js/change-status.js') }}"type="text/javascript"></script>
@endsection