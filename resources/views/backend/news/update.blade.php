<?php
use App\Http\Controllers\Backend\NewsController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new NewsController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);

$admin_id = auth()->guard('admin')->user()->admin_id;
if(!empty($admin_id)){
    // echo $mainFn->chk_permission($admin_id,'about_company');
}else{
    return Redirect::to('_admin/logout');
}

?>
@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                {!! Form::open(array('url'=>$url_to , 'method' => $method , 'id' => 'form' , 'class' => 'form-horizontal','enctype'=>"multipart/form-data" )) !!}
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">


                                    {{--<div class="form-body">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label class="control-label col-md-3">Type</label>--}}
                                            {{--<div class="col-md-4">--}}
                                                {{--<select name="type" class="form-control">--}}
                                                    {{--<option value="1" @if($type==1) selected @endif>ประชุมผู้ถือหุ้น</option>--}}
                                                    {{--<option value="2" @if($type==2) selected @endif>Csr</option>--}}
                                                    {{--<option value="3" @if($type==3) selected @endif>ข่าวประกาศ</option>--}}
                                                {{--</select>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Date</label>
                                            <div class="col-md-4">
                                                <input type="text" name="news_date" class="date-picker form-control" value="{{ $news_date }}" data-date-format="yyyy-mm-dd">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Name</label>
                                            <div class="col-md-4">
                                                <input type="text" name="name_th"  class="form-control" value="{{ $name_th }}"/>
                                            </div>
                                            <div class="col-md-1">
                                                ( TH )
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Name</label>
                                            <div class="col-md-4">
                                                <input type="text" name="name_en"  class="form-control" value="{{ $name_en }}"/>
                                            </div>
                                            <div class="col-md-1">
                                                ( EN )
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Short Description ( TH )</label>
                                            <div class="col-md-9">
                                                <textarea name="short_descripion_th" id="short_descripion_th" class="form-control">{{ $short_descripion_th }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Short Description ( EN )</label>
                                            <div class="col-md-9">
                                                <textarea name="short_descripion_en" id="short_descripion_en" class="form-control">{{ $short_descripion_en }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Description ( TH )</label>
                                            <div class="col-md-9">
                                                <textarea name="description_th" id="description_th" class="form-control ckeditor">{{ $description_th }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Description ( EN )</label>
                                            <div class="col-md-9">
                                                <textarea name="description_en" id="description_en" class="form-control ckeditor">{{ $description_en }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="control-group ">
                                        <div class="controls">
                                            <div id="thumbnail" class="text-center">
                                                @if($img_path != '')
                                                    <img src="{{URL::asset('uploads/news/'.$img_path) }}" id="img_path2" style="max-width:400px;">
                                                @else
                                                    <img src="" id="img_path2" style="max-width:400px;">
                                                @endif

                                                <input type="hidden" name="img_path" id="img_path" value="<?php echo  $img_path?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Cover Image ( TH )</label>
                                            <div class="col-md-4">
                                                @if($img_path != '')
                                                    <div id="remove_img" class="btn btn-danger" >Remove</div>
                                                    <div id="upload" class="btn blue" style="display:none;"><i class="icon-picture"></i> Select File</div>

                                                @elseif ($img_path == '')
                                                    <div id="upload" class="btn blue" ><i class="icon-picture"></i> Select File</div>
                                                    <div id="remove_img" class="btn btn-danger" style="display:none;" >Remove</div>

                                                @endif

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label"> </label>
                                        <div class="col-md-4">
                                            <span style="color:#F00">* หมายเหตุ</span><br>
                                                                    <span style="">
                                                                    - ไฟล์ต้องเป็นมีขนาดกว้าง 720x382 pixel. <br>
                                                                    - นามสกุลของรูปภาพต้อง .png, .jpg เท่านั้น<br>
                                                                    </span>
                                        </div>
                                    </div>


                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">PDF</label>
                                            <div class="col-md-4">

                                                @if($pdf_path != '')
                                                    <a href="{{ '/uploads/news_pdf/'.$pdf_path }}" id="pdf_name" class="btn btn-xs btn-circle blue" target="_blank"><i class="fa fa-file"></i></a>
                                                    <button type="button" class="btn btn-xs btn-circle red" id="remove_pdf"><i class="fa  fa-trash-o"></i></button>
                                                    <input type="file" name="pdf_path" id="upload_pdf" style="display:none;" >
                                                @else
                                                    <input type="file" name="pdf_path" id="upload_pdf">

                                                    <button type="button" class="btn btn-xs btn-circle red" id="remove_pdf" style="display:none;"><i class="fa  fa-trash-o"></i></button>

                                                @endif
                                                <input type="hidden" name="pdf_path" id="pdf_path" value="<?php echo  $pdf_path?>">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="control-group ">
                                        <div class="controls">
                                            <div id="thumbnail1" class="text-center">
                                                @if($img_path_en != '')
                                                    <img src="{{URL::asset('uploads/news_en/'.$img_path_en) }}" id="img_path4" style="max-width:400px;">
                                                @else
                                                    <img src="" id="img_path4" style="max-width:400px;">
                                                @endif

                                                <input type="hidden" name="img_path_en" id="img_path_en" value="<?php echo  $img_path_en?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Cover Image ( EN )</label>
                                            <div class="col-md-4">
                                                @if($img_path_en != '')
                                                    <div id="remove_img1" class="btn btn-danger" >Remove</div>
                                                    <div id="upload1" class="btn blue" style="display:none;"><i class="icon-picture"></i> Select File</div>

                                                @elseif ($img_path_en == '')
                                                    <div id="upload1" class="btn blue" ><i class="icon-picture"></i> Select File</div>
                                                    <div id="remove_img1" class="btn btn-danger" style="display:none;" >Remove</div>

                                                @endif

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label"> </label>
                                        <div class="col-md-4">
                                            <span style="color:#F00">* หมายเหตุ</span><br>
                                                                    <span style="">
                                                                    - ไฟล์ต้องเป็นมีขนาดกว้าง 720x382 pixel. <br>
                                                                    - นามสกุลของรูปภาพต้อง .png, .jpg เท่านั้น<br>
                                                                    </span>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">PDF EN</label>
                                            <div class="col-md-4">

                                                @if($pdf_path_en != '')
                                                    <a href="{{ '/uploads/news_pdf_en/'.$pdf_path_en }}" id="pdf_name1" class="btn btn-xs btn-circle blue" target="_blank"><i class="fa fa-file"></i></a>
                                                    <button type="button" class="btn btn-xs btn-circle red" id="remove_pdf1"><i class="fa  fa-trash-o"></i></button>
                                                    <input type="file" name="pdf_path_en" id="upload_pdf1" style="display:none;" >
                                                @else
                                                    <input type="file" name="pdf_path_en" id="upload_pdf1">

                                                    <button type="button" class="btn btn-xs btn-circle red" id="remove_pdf1" style="display:none;"><i class="fa  fa-trash-o"></i></button>

                                                @endif
                                                <input type="hidden" name="pdf_path_en" id="pdf_path_en" value="<?php echo  $pdf_path_en?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                {!! Form::close() !!}

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>


@endsection
@section('js')
    <script src="{{URL::asset('js/elfinder-upload.js')}}"></script>
    <script src="{{URL::asset('assets/admin/scripts/validate/category.js')}}"></script>
@endsection