<?php
use App\Http\Controllers\Backend\NewsController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new NewsController(); // New Object Controller
$mainFn = new MainFunction(); // New Object Main Function

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$path = $objCon->path;

$orderBy = Input::get('orderBy');
$sortBy = Input::get('sortBy');
$search = Input::get('search');

$a_otherParam = Input::except(['orderBy','sortBy']);
$strParam = $mainFn->parameter($a_otherParam);

$admin_id = auth()->guard('admin')->user()->admin_id;
if(!empty($admin_id)){
    // echo $mainFn->chk_permission($admin_id,'about_company');
}else{
    return Redirect::to('_admin/logout');
}

?>
@extends('admin')
@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>{{ $titlePage }}</h1>
                </div>
            </div>
        </div>

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->

                        <div class="portlet light">

                            <form action{{URL::to($path)}} class="form-horizontal" method="GET" >
                            <div class="form-group">
                                <label class="control-label col-md-1">Search</label>
                                <div class="col-md-3">
                                    <input class="form-control" type="text" name="search" value="{{ $search }}" >
                                    <span class="help-block">Search by name</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-1 col-md-3 ">
                                    <button class="btn blue btn-sm" type="submit" ><i class="fa fa-search"></i> Search</button>
                                </div>
                            </div>
                            </form>
                            <hr>

                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-database font-green-sharp"></i>
                                    <span class="caption-subject font-green-sharp bold">Found {{ $countData }} Record(s).</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('ID',$pkField,$orderBy,$sortBy,$strParam) !!} </th>
                                            <th class="col-md-1 text-center">Images (TH)</th>
                                            <th class="col-md-1 text-center">Images (EN)</th>
                                            <th class="col-md-2 text-center">{!! $mainFn->sorting('Name (TH)',"name_th",$orderBy,$sortBy,$strParam) !!} </th>
                                            <th class="col-md-2 text-center">{!! $mainFn->sorting('Name (EN)',"name_en",$orderBy,$sortBy,$strParam) !!} </th>
                                            <th class="col-md-1 text-center">PDF (TH)</th>
                                            <th class="col-md-1 text-center">PDF (EN)</th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('Date','news_date',$orderBy,$sortBy,$strParam) !!}</th>
                                            @if($permission['c'] == '1' || $permission['u'] == '1' || $permission['d'] == '1')
                                                <th class="col-md-1 text-center">
                                                    @if($permission['c'] == '1')
                                                        <a href="{{ URL::to($path.'/create') }}" class="btn btn-circle blue btn-xs"><i class="fa fa-plus"></i> Add</a>
                                                    @endif
                                                </th>
                                            @endif
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($countData > 0)
                                            @foreach($data as $field)
                                                <tr>
                                                    <td class="text-center">{{ $field->$pkField }}</td>
                                                    <td class="text-center">
                                                        <?php
                                                        $path2 = "uploads/news/100/".$field->img_path;
                                                        $path_img = URL::asset($path2);
                                                        $check_path = public_path($path2);
                                                        ?>

                                                            @if(file_exists($check_path) and !empty($field->img_path))
                                                                <img src="{{ $path_img }}" style="max-width:400px;">
                                                            @endif
                                                    </td>
                                                    <td class="text-center">
                                                        <?php
                                                        $path2 = "uploads/news/100/".$field->img_path_en;
                                                        $path_img = URL::asset($path2);
                                                        $check_path = public_path($path2);
                                                        ?>

                                                        @if(file_exists($check_path) and !empty($field->img_path_en))
                                                            <img src="{{ $path_img }}" style="max-width:400px;">
                                                        @endif
                                                    </td>
                                                    <td>{{ $field->name_th }}</td>
                                                    <td>{{ $field->name_en }}</td>
                                                    <td class="text-center">
                                                        @if(!empty($field->pdf_path))
                                                            <a href="{{ URL::asset('uploads/news_pdf/'.$field->pdf_path) }}" class="btn btn-xs btn-circle blue" target="_blank"><i class="fa fa-file"></i></a>
                                                        @endif
                                                    </td>
                                                    <td class="text-center">
                                                        @if(!empty($field->pdf_path_en))
                                                            <a href="{{ URL::asset('uploads/news_pdf_en/'.$field->pdf_path_en) }}" class="btn btn-xs btn-circle blue" target="_blank"><i class="fa fa-file"></i></a>
                                                        @endif
                                                    </td>
                                                    <td class="text-center">{{ $mainFn->format_date_th($field->news_date,2) }}</td>
                                                    @if($permission['c'] == '1' || $permission['u'] == '1' || $permission['d'] == '1')
                                                    
                                                        <td class="text-center">
                                                            @if($permission['u'] == '1')
                                                                <a href="{{ URL::to($path.'/'.$field->$pkField.'/edit?'.$strParam) }}" class="btn btn-xs btn-circle green"><i class="fa fa-edit"></i></a>
                                                            @endif
                                                            {!! Form::open(array('url' => $path.'/'.$field->$pkField, 'method' => 'delete' , 'class' => 'frm-delete')) !!}
                                                            @if($permission['d'] == '1')
                                                                <button type="submit" class="btn btn-xs btn-circle red" ><i class="fa  fa-trash-o"></i></button>
                                                            @endif
                                                            {!! Form::close() !!}
                                                        </td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr><td colspan='9' class='text-center'>No Result.</td></tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                {!! $data->appends(Input::except('page'))->render() !!}
                            </div>
                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
@endsection