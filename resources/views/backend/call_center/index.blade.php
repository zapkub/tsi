<?php
use App\Http\Controllers\Backend\CarInsuranceController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new CarInsuranceController(); // New Object Controller
$mainFn = new MainFunction(); // New Object Main Function

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$path = $objCon->path;

$orderBy = Input::get('orderBy');
$sortBy = Input::get('sortBy');
$search = Input::get('search');
$car_name = Input::get('car_name');

$a_otherParam = Input::except(['orderBy','sortBy']);
$strParam = $mainFn->parameter($a_otherParam);


?>
@extends('admin')
@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>{{ $titlePage }}</h1>
                </div>
            </div>
        </div>

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->

                        <div class="portlet light">

                            <form action{{URL::to($path)}} class="form-horizontal" method="GET" >

                                <div class="form-group">
                                    <label class="control-label col-md-2">Car Brand</label>
                                    <div class="col-md-3">
                                        <input class="form-control" type="text" name="car_name" value="{{ $car_name }}" >
                                    </div>
                                </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Search</label>
                                <div class="col-md-3">
                                    <input class="form-control" type="text" name="search" value="{{ $search }}" >
                                    <span class="help-block">Search by Firstname , Lastname , Mobile , Number</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-1 col-md-3 ">
                                    <button class="btn blue btn-sm" type="submit" ><i class="fa fa-search"></i> Search</button>
                                </div>
                            </div>


                            </form>
                            <hr>

                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-database font-green-sharp"></i>
                                    <span class="caption-subject font-green-sharp bold">Found {{ $countData }} Record(s).</span>
                                </div>
                            </div>
                        
                            <div class="portlet-body">
                                <div class="table-responsive">

                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('ID',$pkField,$orderBy,$sortBy,$strParam) !!} </th>
                                            <th>{!! $mainFn->sorting('ใบคำขอเลขที่','app_no',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th>{!! $mainFn->sorting('ผู้ขอประกันภัย','firstname',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th>{!! $mainFn->sorting('เบอร์โทรศัพท์','mobile',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th>{!! $mainFn->sorting('ทะเบียนรถ','license_plate',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th>{!! $mainFn->sorting('วันที่ขอ','date_coverage',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th>{!! $mainFn->sorting('รหัสตัวแทน','code_agent',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th>{!! $mainFn->sorting('สาขา','service_id',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="text-center">Case</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @if($countData > 0)
                                            @foreach($data as $field)
                                                <tr>
                                                    <td class="text-center">{{ $field->$pkField }}</td>
                                                    <td>{{ $field->app_no }}</td>
                                                    <td>{{ $field->firstname }} {{ $field->lastname }}</td>
                                                    <td>{{ $field->mobile }}</td>
                                                    <td>{{ $field->license_plate }}</td>
                                                    <td>{{ $field->date_coverage }}</td>
                                                    <td>{{ $field->code_agent }}</td>
                                                    <td>{{ $field->service_id }}</td>
                                                    <td class="text-center"><a href="{{ URL::to('_admin/send_case/create?'.$pkField."=".$field->$pkField)  }}" class="btn btn-circle green btn-xs" target="_blank"><i class="fa fa-edit"></i></a></td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr><td colspan='9' class='text-center'>No Result.</td></tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                {!! $data->appends(Input::except('page'))->render() !!}
                            </div>

                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
@endsection