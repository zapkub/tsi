<?php
use App\Http\Controllers\Backend\ServiceController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new ServiceController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$geography = App\Model\Geography::whereNull('deleted_at')->get();


$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);

$admin_id = auth()->guard('admin')->user()->admin_id;
if(!empty($admin_id)){
    // echo $mainFn->chk_permission($admin_id,'service');
}else{
    return Redirect::to('_admin/logout');
}
?>
@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" class="form-horizontal">
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Branch</label>
                                            <div class="col-md-3">
                                                <input type="text" name="branch_name_th"  class="form-control" value="{{ $branch_name_th }}"/>
                                            </div>
                                            <div class="col-md-1">
                                                ( TH )
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" name="branch_name_en"  class="form-control" value="{{ $branch_name_en }}"/>
                                            </div>
                                            <div class="col-md-1">
                                                ( EN )
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Address 1</label>
                                            <div class="col-md-4">
                                                <textarea name="address1" id="address1" class="form-control">{{ $address1 }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Address 2</label>
                                            <div class="col-md-4">
                                                <textarea name="address2" id="address2" class="form-control">{{ $address2 }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">ภาค</label>
                                        <div class="col-md-4">
                                            <select name="region_id" class="form-control filter-region" data-section="address"></select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">จังหวัด</label>
                                        <div class="col-md-4">
                                            <select name="province_id" class="form-control filter-province" data-section="address"></select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">อำเภอ</label>
                                        <div class="col-md-4">
                                            <select name="district_id" class="form-control filter-district" data-section="address"></select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">รหัสไปรษณีย์</label>
                                        <div class="col-md-4">
                                            <input type="text" name="zipcode" value="" class="form-control filter-zipcode" data-section="address">
                                        </div>
                                    </div>


                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Phone</label>
                                            <div class="col-md-4">
                                                <input type="text" name="phone"  class="form-control" value="{{ $phone }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Fax</label>
                                            <div class="col-md-4">
                                                <input type="text" name="fax"  class="form-control" value="{{ $fax }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Contact Name</label>
                                            <div class="col-md-4">
                                                <input type="text" name="contact_name"  class="form-control" value="{{ $contact_name }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Email</label>
                                            <div class="col-md-4">
                                                <input type="text" name="email"  class="form-control" value="{{ $email }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Map</label>
                                            <div class="col-md-4">
                                                <input type="text" name="map"  class="form-control" value="{{ $map }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Latitude</label>
                                            <div class="col-md-4">
                                                <input type="text" name="latitude"  class="form-control" value="{{ $latitude }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Longitude</label>
                                            <div class="col-md-4">
                                                <input type="text" name="longitude"  class="form-control" value="{{ $longitude }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>

@endsection
@section('js')
    <script src="{{URL::asset('js/elfinder-upload.js')}}"></script>
    <script src="{{URL::asset('assets/admin/scripts/validate/service.js')}}"></script>
    {{ Html::script('js/filter-address.js') }}
    <script>
        $(document).ready(function() {
            filter_address('region','region','null','{{ $data->region_id or '' }}','address');
            filter_address('region','province','{{ $data->region_id or '' }}','{{ $data->province_id or '' }}','address');
            filter_address('province','district','{{ $data->province_id or '' }}','{{ $data->district_id or '' }}','address');
            filter_address('district','zipcode','{{ $data->district_id or '' }}','{{ $data->zipcode or '' }}','address');
        });
    </script>

@endsection