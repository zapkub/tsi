<?php
use App\Http\Controllers\Backend\CarInsuranceController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new CarInsuranceController(); // New Object Controller
$mainFn = new MainFunction(); // New Object Main Function

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$tb_name = $objCon->tbName;
$path = $objCon->path;

$orderBy = Input::get('orderBy');
$sortBy = Input::get('sortBy');
$search = Input::get('search');

$a_otherParam = Input::except(['orderBy','sortBy']);
$strParam = $mainFn->parameter($a_otherParam);

$admin_id = auth()->guard('admin')->user()->admin_id;
if(!empty($admin_id)){
    echo $mainFn->chk_permission($admin_id,'car_insurance');
}else{
    return Redirect::to('_admin/logout');
}

?>
@extends('admin')
@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <?php
                $db = DB::table('admin')->select('firstname','lastname')->where('admin_id',$admin_id)->first();
                ?>
                <div class="page-title">
                    <h3><a href="{{ URL::to('_admin/car_insurance_branch') }}">ใบคำขอออนไลน์ (สาขา)</a> >> {{ $titlePage }} :: {{ $db->firstname }} {{ $db->lastname }}</h3>
                </div>
            </div>
        </div>

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->

                        <div class="portlet light">

                            <form action{{URL::to($path)}} class="form-horizontal" method="GET" >
                                <div class="form-group">
                                    <label class="control-label col-md-1">Search</label>
                                    <div class="col-md-3">
                                        <input class="form-control" type="text" name="search" value="{{ $search }}" >
                                        <span class="help-block">Search by ใบคำขอเลขที่</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-1 col-md-3 ">
                                        <button class="btn blue btn-sm" type="submit" ><i class="fa fa-search"></i> Search</button>
                                    </div>
                                </div>
                            </form>
                            <hr>

                            <div class="portlet-title">
                                <div class="caption">
                                    @if($status==0)
                                    <span class="caption-subject font-green-sharp bold">สถานะใบคำขอ <li class="fa fa-edit"></li> รอการยืนยัน จำนวน {{ $countData }} ฉบับ</span>
                                        @elseif($status==1)
                                        <span class="caption-subject font-green-sharp bold">สถานะใบคำขอ <li class="fa fa-check"></li> ผ่านการยืนยัน จำนวน {{ $countData }} ฉบับ</span>
                                        @elseif($status==2)
                                        <span class="caption-subject font-green-sharp bold">สถานะใบคำขอ <li class="fa fa-close"></li> ไม่อนุมัติ จำนวน {{ $countData }} ฉบับ</span>
                                    @endif
                                </div>
                            </div>

                            <div class="portlet-body">
                                <div class="table-responsive">

                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('ID',$pkField,$orderBy,$sortBy,$strParam) !!} </th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('ใบคำขอเลขที่','no',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-4 text-center">{!! $mainFn->sorting('ผู้ขอประกันภัย','firstname',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('ทะเบียนรถ','registration',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('วันที่ขอ','created_at',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('รหัสตัวแทน','code_agent',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('สาขา','branch_id',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-2 text-center"></th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @if($countData > 0)
                                            @foreach($data as $field)
                                                <tr>
                                                    <td class="text-center">{{ $field->$pkField }}</td>
                                                    <td class="text-center">{{ $field->no }}</td>
                                                    <td>{{ $field->firstname }} {{ $field->lastname }}</td>
                                                    <td class="text-center">{{ $field->registration }}</td>
                                                    <td class="text-center">{{ $mainFn->format_date_en($field->created_at,4) }}</td>
                                                    <td class="text-center">{{ $field->code_agent }}</td>
                                                    <td class="text-center">{{ $field->Branch->name_th }}</td>
                                                    <td class="text-center">
                                                        @if($status==0)
                                                        <a href="{{ URL::to('_admin/car_insurance/'.$field->$pkField.'/edit?status='.input::get('status').$strParam) }}" target="_blank"  class="btn btn-circle blue btn-xs"><i class="fa fa-search"></i></a>
                                                        @endif

                                                        <a href="{{ URL::to('_admin/car_insurance/'.$field->$pkField.'/?'.$strParam) }}" target="_blank"  class="btn btn-circle yellow btn-xs"><i class="fa fa-print"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr><td colspan='9' class='text-center'>No Result.</td></tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                {!! $data->appends(Input::except('page'))->render() !!}
                            </div>

                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
@endsection