<?php
use App\Http\Controllers\Backend\CarInsuranceController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new CarInsuranceController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}

$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);

$admin_id = auth()->guard('admin')->user()->admin_id;
if(!empty($admin_id)){
    echo $mainFn->chk_permission($admin_id,'car_insurance');
}else{
    return Redirect::to('_admin/logout');
}
?>
<style>
    .border-table{
        border-bottom: solid 1px #000;
        border-top:solid 1px #000;
        margin-bottom: 20px;
    }
    input[type="text"]{
        font-size: 14px;
        font-weight: normal;
        color: #333;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        box-shadow: none;
        transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        -ms-border-radius: 4px;
        -o-border-radius: 4px;
        border-radius: 4px;
    }
    .t-width{
        width:50px;
    }
    .head{
        background-color: #b4c4cd;
    }
    .bg-red{
        background-color:#bf6a40;
    }
    .f-red{
        color:#b94a48;
    }
</style>

@extends('admin')
@section('content')

        <!-- BEGIN SAMPLE TABLE PORTLET-->
<div class="portlet light">

    <div class="portlet-body flip-scroll">

        <table class="table-condensed flip-content border-table">
            <thead class="flip-content">
            <tr class="head">
                <td colspan="3" class="text-center"><strong>บริษัท ไทยเศรษฐกิจประกันภัย จำกัด (มหาชน)<br>คำขอเอาประกันภัยรถยนต์</strong></td>
            </tr>
            </thead>
            <tbody>

            <tr>
                <td></td>
                <td><strong>การประกันภัยรถยนต์</strong></td>
                <td>{{ $mainFn->format_date_th(date('Y-m-d H:i:s'),3) }}</td>
            </tr>
            <?php
            $db = DB::table('agent')->select('flag','firstname','lastname','agent_no','created_at')->where('agent_id',$agent_id)->first();
            $db2 = DB::table('admin')->select('firstname','lastname','created_at')->where('admin_id',$admin_id)->first();
            ?>
            <tr>
                <td><strong>รหัส TSK</strong></td>
                <td>@if($db->flag==2) สาขา
                    <?php $branch_db = DB::table('branch')->whereNull('deleted_at')->where('branch_id',$branch_id)->first(); ?>
                    {{ $branch_db->name_th }}
                    @endif
                    เล่มที่ {{ $book }}
                </td>
                <td>ผู้ขอ {{ $db->firstname }} {{ $db->lastname }} {{ $mainFn->format_date_th($db->created_at,3) }}</td>
            </tr>
            <tr>
                <td></td>
                <td>@if($type_repair==1) ซ่อมอู่ @elseif($type_repair==2) ซ่อมห้าง @elseif($type_repair==3) T- Care @endif</td>
                <td>ผู้ยืนยัน {{ $db2->firstname }} {{ $db2->lastname }} {{ $mainFn->format_date_th($db->created_at,3) }} </td>
            </tr>
            </tbody>
        </table>

        <table class="table-condensed flip-content border-table">

            <thead class="flip-content">
            <tr class="head">
                <th colspan="3">ผู้ขอเอาประกันภัย</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>ชื่อ {{ $firstname }} {{ $lastname }}</td>
                <td>
                    @if($status_insurance==0) <i class="fa fa-check"></i> ประกันใหม่ @endif
                    @if($status_insurance==1) <i class="fa fa-check"></i> ต่ออายุกรมธรรม์ที่ กท {{ $extend }} @endif
                </td>
            </tr>
            <tr>
                <td>ที่อยู่</td>
                <td>{{ $address }}</td>
            </tr>
            <tr>
                <td>ประเภทการประกันภัย</td>
                <td>
                    @if($type_insurance==0) <i class="fa fa-check"></i> ประเภท1 @endif
                    @if($type_insurance==1) <i class="fa fa-check"></i> ประเภท2 @endif
                    @if($type_insurance==2) <i class="fa fa-check"></i> ประเภท3 @endif
                    @if($type_insurance==3) <i class="fa fa-check"></i> ประเภท2+ @endif
                    @if($type_insurance==4) <i class="fa fa-check"></i> ประเภท3+ @endif
                    @if($type_insurance==5) <i class="fa fa-check"></i> พ.ร.บ @endif
                    @if($type_insurance==6) <i class="fa fa-check"></i> อื่นๆ ระบุ <input type="text" name="type_insurance_other" value="{{ $type_insurance_other }}"> @endif
            </tr>
            <tr>
                <td>เลขที่บัตรประชาชน</td>
                <td>{{ $id_card }}</td>
            </tr>
            <tr>
                <td>อาชีพ</td>
                <td>
                    @if($type_insurance==0) <i class="fa fa-check"></i> ข้าราชการ / พนักงานรัฐวิสาหกิจ @endif
                    @if($type_insurance==1) <i class="fa fa-check"></i> รับจ้าง @endif
                    @if($type_insurance==2) <i class="fa fa-check"></i> ธุรกิจส่วนตัว @endif
                    @if($type_insurance==3) <i class="fa fa-check"></i> ไม่ได้ประกอบอาชีพ @endif
                    @if($type_insurance==4) <i class="fa fa-check"></i> อื่นๆ ระบุ {{ $occupation_other }} @endif
                </td>
            </tr>
            </tbody>
        </table>

        <table class="table-condensed flip-content border-table">

            <thead class="flip-content">
            <tr class="head">
                <th colspan="4">ประเภทการรับประกันภัยที่ต้องการ</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $year = date('Y');
            $birthday_1 = explode('-',$birthday);
            foreach ($birthday_1 as $key => $value){
                if($key==0){
                    $year = $value;
                }
                if($key==1){
                    $month = $value;
                }
                if($key==2){
                    $day = $value;
                }
            }

            $birthday_2 = explode('-',$birthday2);
            foreach ($birthday_2 as $key => $value){
                if($key==0){
                    $year2 = $value;
                }
                if($key==1){
                    $month2 = $value;
                }
                if($key==2){
                    $day2 = $value;
                }
            }
            ?>
            @if($type_insurance==0)
                <tr>
                    <td><i class="fa fa-check"></i> ไม่ระบุผู้ขับขี่</td>
                    <td></td>
                </tr>
            @endif

            @if($type_insurance==1)
                <tr>
                    <td><i class="fa fa-check"></i> ระบุชื่อผู้ขับขี่ 1.{{ $driver_name }}</td>
                    <td>
                        วัน {{ $day }}
                        เดือน {{ $month }}
                        ปีเกิด {{ $year }}
                    </td>
                    <td>อาชีพ {{ $driver_occupation }}</td>
                </tr>
                <tr>
                    <td>เลขที่ใบขับขี่ {{ $driver_license }}</td>
                    <td>จังหวัด {{ $driver_provice }}</td>
                    <td>โทร {{ $driver_mobile }}</td>
                </tr>
                <tr>
                    <td>ระบุชื่อผู้ขับขี่ 2.{{ $driver_name2 }}</td>
                    <td>
                    <td>
                        วัน {{ $day2 }}
                        เดือน {{ $month2 }}
                        ปีเกิด {{ $year2 }}
                    </td>
                    <td>อาชีพ {{ $driver_occupation2 }}</td>
                </tr>
                <tr>
                    <td>เลขที่ใบขับขี่ {{ $driver_license2 }}</td>
                    <td>จังหวัด {{ $driver_provice2 }}</td>
                    <td>โทร {{ $driver_mobile2 }}</td>
                    <td></td>
                </tr>
            @endif

            </tbody>
        </table>

        <table class="table-condensed flip-content border-table">
            <tr>
                <td>เลขเครื่องหมายตาม พ.ร.บ. {{ $number_sign }}</td>
                <td>บริษัท {{ $company }}</td>
                <td>สิ้นสุดเมื่อ {{ $end_date }}</td>
            </tr>
            <tr>
                <td>
                    การใช้รถยนต์        @if($use_car==0) <i class="fa fa-check"></i> @endif
                    ส่วนบุคคล           @if($use_car==1) <i class="fa fa-check"></i> @endif
                    เพื่อการพาณิชน์       @if($use_car==2) <i class="fa fa-check"></i> @endif
                    เพื่อการพาณิชน์พิเศษ   @if($use_car==3) <i class="fa fa-check"></i> @endif
                    รับจ้างสาธารณะ      @if($use_car==4) <i class="fa fa-check"></i> @endif
                    อื่นๆ               @if($use_car==6) <i class="fa fa-check"></i> ระบุ {{ $use_car_other }} @endif
                </td>
            </tr>
            <tr>
                <td colspan="3">ผู้รับประโยชน์ {{ $beneficiary }}</td>
            </tr>
        </table>

        <table class="table-condensed flip-content border-table">

            <thead class="flip-content">
            <tr class="head">
                <th colspan="7">รายการรถยนต์ที่เอาประกัน :</th>
            </tr>
            </thead>
            <tbody>
            <tr class="text-center">
                <td class="f-red">ชื่อรถยนต์ / รุ่น*</td>
                <td class="f-red">เลขทะเบียน*</td>
                <td class="f-red">เลขตัวถัง*</td>
                <td class="f-red">ปี รุ่น*</td>
                <td>จำนวนที่นั่ง / ขนาด / น้ำหนัก</td>
                <td>มูลค่าเต็มรวมตกแต่ง</td>
            </tr>
            <tr>
                <td>{{ $name_car }}</td>
                <td>{{ $registration }}</td>
                <td>{{ $chassis }}</td>
                <td>{{ $roon }}</td>
                <td class="text-center">
                    @if(empty($seating) and empty($size) and empty($weight))
                        <?php $s = $seating ?>

                    @else

                        <?php $s = $seating."/".$size."/".$weight ?>

                    @endif
                    {{ $s }}

                </td>
                <td>{{ $total_decorate }}</td>
            </tr>
            <tr class="text-center">
                <td>รหัสรถยนต์</td>
                <td>เลขเครื่องรถยนต์</td>
                <td colspan="4"></td>
            </tr>
            <tr>
                <td>{{ $code_car }}</td>
                <td>{{ $engine }}</td>
                <td colspan="4"></td>
            </tr>
            </tbody>
        </table>

        <table class="table-condensed flip-content border-table">

            <thead class="flip-content">
            <tr class="head">
                <th colspan="4">รายการตกแต่งเปลี่ยนแปลงรถยนต์เพิ่มเติม (โปรดระบุรายละเอียด)</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>1. {{ $decorate_car }}</td>
                <td>ราคา {{ $price }} บาท</td>
            </tr>
            <tr>
                <td>2. {{ $decorate_car2 }}</td>
                <td>ราคา {{ $price2 }} บาท</td>
            </tr>
            <tr>
                <td>3. {{ $decorate_car3 }}</td>
                <td>ราคา {{ $price3 }} บาท</td>
            </tr>
            <tr>
                <td>อุปกรณ์พิเศษ @if($equipment==0) <i class="fa fa-check"></i> @endif ไม่มี @if($equipment==1) <i class="fa fa-check"></i> @endif มีระบุ</td>
                @if($equipment==1) <td>{{ $equipment_other }}</td> @endif
                <td></td>
            </tr>
            </tbody>
        </table>

        <table class="table-condensed flip-content border-table">
            <thead class="flip-content">
            <tr class="head">
                <th>ความรับผิดต่อบุคคลภายนอก</th>
                <th>รถยนต์เสียหาย สูญหายไฟไหม้</th>
                <th>ความคุ้มครองตามเอกสารแนบท้าย</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td valign="top">1.ความเสียหายต่อชีวิต ร่างกาย หรือ อนามัย<br>เฉพาะส่วนเกินวงเงินสูงสุดตาม พ.ร.บ. {{ $person_damage }} บาท/คน {{ $person_damage2 }} บาท/ครั้ง<br>
                    2.ความเสียหายต่อทรัพย์สิน {{ $person_damage3 }} บาท/ครั้ง<br>
                    2.1ความเสียหายส่วนแรก {{ $person_damage4 }} บาท/ครั้ง
                </td>
                <td valign="top">1.ความเสียหายต่อรถยนต์ {{ $car_damage }} บาท / ครั้ง<br>
                    1.1.ความเสียหายส่วนแรก {{ $car_damage2 }} บาท / ครั้ง<br>
                    2.รถยนต์สูญหายไฟไหม้ {{ $car_damage3 }} บาท<br>

                    <span style="font-size:36px;color:red;">ไม่รวม พ.ร.บ</span>
                </td>
                <td valign="top">1.อุบัติเหตุส่วนบุคคล<br>
                    1.1.เสียชีวิต สูญเสียอวัยวะ ทุพพลภาพถาวร<br>
                    ก.ผู้ขับขี่ 1 คน {{ $document_ptt }} บาท<br>
                    ข.ผู้โดยสาร {{ $document_ptt2 }} คน<br>
                    1.2.ทุพพลภาพชั่วคราว<br>
                    ก.ผู้ขับขี่ 1 คน {{ $document_ptt3 }} บาท / สัปดาห์<br>
                    ข.ผู้โดยสาร {{ $document_ptt4 }} คน {{ $document_ptt5 }} บาท / คน / สัปดาห์<br>
                    2.ค่ารักษาพยาบาล {{ $document_ptt6 }} บาท / คน
                    3.การประกันตัวผู้ขับขี่ {{ $document_ptt7 }} บาท / ครั้ง
                </td>
            </tr>
            <tr>
                <td>เบี้ยสุทธิ {{ $net }} บาท</td>
                <td>เบี้ยรวมภาษีอากร {{ $net_tax }} บาท</td>
                <td>เบี้ย พ.ร.บ {{ $net_act }} บาท</td>
            </tr>

            </tbody>
        </table>

        <table class="table-condensed flip-content border-table">

            <thead class="flip-content">
            <tr>
                <td colspan="2">ข้าพเจ้าขอรับรองว่า คำบอกกล่าวตามรายการข้างบนเป็นความจริงและให้นับถือเป็นส่วนหนึ่งของสัญญาระหว่างข้าพเจ้ากับบริษัท</td>
            </tr>
            <tr>
                <td>โดยข้าพเจ้ามีความประสงค์ให้กรมธรรม์มีผลบังคับใช้ตั้งแต่วันที่</td>
                <td>
                    {{ $from_date }} ถึงวันที่ {{ $to_date }}
                </td>
            </tr>
            </tbody>
        </table>

        <table class="table-condensed flip-content border-table">
            <tbody>

            <tr>
                <td colspan="4">ลงชื่อผู้พิมพ์</td>
            </tr>
            <tr>
                <td>ชื่อตัวแทน</td>
                <td>{{ $agent }}</td>
                <td>ลายมือชื่อผู้ขอเอาประกัน..........................................................................</td>
                <td></td>
            </tr>
            <tr>
                <td class="f-red">ชื่อรหัสตัวแทน* กท</td>
                <td>{{ $code_agent }}</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>หมายเหตุ</td>
                <td>{{ $note }}</td>
                <td>วันที่...................เดือน......................พ.ศ......................</td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td class="f-red">เอกสารฉบับนี้เป็นเพียงการแจ้งขอเอาประกันภัยรถยนต์เท่านั้น<br>มิใช่เอกสารการแสดงการรับชำระเงินค่าเบี้ยประกันภัย</td>
                <td></td>
            </tr>

            </tbody>
        </table>

        <table class="table-condensed flip-content border-table">
            <tbody>
            <tr>
                <td colspan="4" class="f-red">คำเตือนของกรมการประกันภัย กระทรวงพาณิชย์<br>
                    ให้ตอบคำถามข้างต้นตามเป็นจริงทุกข้อ มิฉะนั้นบริษัทอาจถือเป็นเหตุปฏิเสธความรับผิดชอบตามสัญญาประกันภัยได้<br>
                    ตามประมวลกฏหมายเพ่งและพาณิชน์มาตรา 865</td>
            </tr>
            </tbody>
        </table>

        <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" class="form-horizontal">
            <input name="_method" type="hidden" value="{{$method}}">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

        <table class="table-condensed flip-content border-table">
            <tbody>
            <tr>
                <td class="text-center">
                    <button type="submit" name="check" class="btn btn-xs btn-circle green" value="1"><i class="fa  fa-check"></i> อนุมัติ</button>
                    <button type="submit" name="close" id="close" class="btn btn-xs btn-circle red" value="1"><i class="fa  fa-close"></i> ไม่อนุมัติ</button>
                </td>

            </tr>
            <tr id="reason_close" style="display: none;">
                <td class="text-center"><textarea name="reason_c" id="reason_c"></textarea></td>
            </tr>
            </tbody>
        </table>
        </form>
    </div>
</div>
<!-- END SAMPLE TABLE PORTLET-->

<script>
    $('#close').on('click',function () {
        var close_value = $('#close').val();
        var reason_c = $('#reason_c').val();
        if(close_value == 1){
            $('#reason_close').css('display','');
            if(reason_c == ''){
                $('#reason_close').attr('required','true');
                return false;
            }
        }
    });
</script>

@endsection



