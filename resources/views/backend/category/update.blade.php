<?php
use App\Http\Controllers\Backend\CategoryController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new CategoryController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);

$admin_id = auth()->guard('admin')->user()->admin_id;
if(!empty($admin_id)){
    // echo $mainFn->chk_permission($admin_id,'product');
}else{
    return Redirect::to('_admin/logout');
}

?>



@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                {!! Form::open(array('url'=>$url_to , 'method' => $method , 'id' => 'form' , 'class' => 'form-horizontal','enctype'=>"multipart/form-data" )) !!}
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Category Name</label>
                                            <div class="col-md-4">
                                                <input type='text' name="category_name_th" id="category_name_th" class="form-control" value="{{ $category_name_th }}" >
                                            </div>
                                            <div class="col-md-4">
                                                ( TH )
                                                </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Category Name</label>
                                            <div class="col-md-4">
                                                <input type='text' name="category_name_en" id="category_name_en" class="form-control" value="{{ $category_name_en }}" >
                                            </div>
                                            <div class="col-md-4">
                                                ( EN )
                                            </div>
                                        </div>
                                    </div>

                                    {{--<div class="form-body">--}}
                                    {{--<div class="form-group">--}}
                                    {{--<label class="control-label col-md-3">Parent Category</label>--}}
                                    {{--<div class="col-md-4">--}}
                                    {{--<input type='text' name="parent_category_id" id="parent_category_id" class="form-control" value="{{ $parent_category_id}}" >--}}
                                    {{--</div>--}}
                                    {{--</div>--}}
                                    {{--</div>--}}

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Parent Category</label>
                                            <div class="col-md-4">
                                                <select name="parent_category_id" class="form-control select2-container form-control select2me" >
                                                    <option value="0">/</option>
                                                    {{--@foreach($category as $field1)--}}
                                                        {{ $mainFn->search_select_category2($category_id,'',$parent_category_id) }}
                                                        {{--<option value="{{$field1->category_id}}" @if($category_id == $field1->category_id) selected @endif >{{$field1->category_name}}</option>--}}
                                                    {{--@endforeach--}}
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Icon Name</label>
                                            <div class="col-md-4">
                                                <input type='text' name="icon_name" id="icon_name" class="form-control" value="{{ $icon_name }}" >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Description ( TH )</label>
                                            <div class="col-md-4">
                                                <textarea name="description_th" id="description_th" class="form-control">{{ $description_th }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Description ( EN )</label>
                                            <div class="col-md-4">
                                                <textarea name="description_en" id="description_en" class="form-control">{{ $description_en }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="control-group ">
                                        <div class="controls">
                                            <div id="thumbnail" class="text-center">
                                                <?php
                                                $path = "uploads/category/100/".$img_name;
                                                $path_img = URL::asset($path);
                                                $check_path = public_path($path);
                                                ?>

                                                @if(file_exists($check_path) and !empty($img_name))
                                                    <img src="{{ $path_img }}" id="img_path2" style="max-width:400px;">
                                                @else
                                                    <img src="" id="img_path2" style="max-width:400px;">
                                                @endif

                                                <input type="hidden" name="img_name" id="img_path" value="<?php echo  $img_name?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Image</label>
                                            <div class="col-md-4">
                                                @if($img_name != '')
                                                    <div id="remove_img" class="btn btn-danger" >Remove</div>
                                                    <div id="upload" class="btn blue" style="display:none;"><i class="icon-picture"></i> Select File</div>

                                                @elseif ($img_name == '')
                                                    <div id="upload" class="btn blue" ><i class="icon-picture"></i> Select File</div>
                                                    <div id="remove_img" class="btn btn-danger" style="display:none;" >Remove</div>

                                                @endif

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label"> </label>
                                        <div class="col-md-4">
                                            <span style="color:#F00">* หมายเหตุ</span><br>
                                                                    <span style="">
                                                                    - ไฟล์ต้องเป็นมีขนาดกว้าง 1440x550 pixel. <br>
                                                                    - นามสกุลของรูปภาพต้อง .png, .jpg เท่านั้น<br>
                                                                    </span>
                                        </div>
                                    </div>

                                    <div class="control-group ">
                                        <div class="controls">
                                            <div id="thumbnail1" class="text-center">
                                                @if($s_img_name != '')
                                                    <img src="{{URL::asset('uploads/category_s/'.$s_img_name) }}" id="img_path4" style="max-width:400px;">
                                                @else
                                                    <img src="" id="img_path4" style="max-width:400px;">
                                                @endif

                                                <input type="hidden" name="s_img_name" id="s_img_name" value="<?php echo  $s_img_name?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Small Image</label>
                                            <div class="col-md-4">
                                                @if($s_img_name != '')
                                                    <div id="remove_img1" class="btn btn-danger" >Remove</div>
                                                    <div id="upload1" class="btn blue" style="display:none;"><i class="icon-picture"></i> Select File</div>

                                                @elseif ($s_img_name == '')
                                                    <div id="upload1" class="btn blue" ><i class="icon-picture"></i> Select File</div>
                                                    <div id="remove_img1" class="btn btn-danger" style="display:none;" >Remove</div>

                                                @endif

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label"> </label>
                                        <div class="col-md-4">
                                            <span style="color:#F00">* หมายเหตุ</span><br>
                                                                    <span style="">
                                                                    - ไฟล์ต้องเป็นมีขนาดกว้าง 720x624 pixel. <br>
                                                                    - นามสกุลของรูปภาพต้อง .png, .jpg เท่านั้น<br>
                                                                    </span>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                {!! Form::close() !!}

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>

@endsection

@section('js')
    <script src="{{URL::asset('js/category-upload.js')}}"></script>
    <script src="{{URL::asset('assets/admin/scripts/validate/category.js')}}"></script>
@endsection
