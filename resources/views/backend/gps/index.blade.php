@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>GPS</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body">

                                <form method="post" id="send_gps" class="form-horizontal" action="{{ URL::to('_admin/gps/save') }}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" >
                                    <div class="form-group">
                                        <label for="gps" class="col-sm-2 control-label">GPS</label>
                                        <div class="col-sm-10">
                                            <input type="text" id="gps" class="form-control" name="gps" >
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
@endsection
@section('js')
    <script>
        $( document ).ready(function() {
            function getLocation() {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(showPosition);
                } else {
                    x.value = "Geolocation is not supported by this browser.";
                }
            }
            function showPosition(position) {
                $("#gps").val(position.coords.latitude + "," + position.coords.longitude)
            }

            getLocation();
            setInterval( function(){ getLocation() },1000);

            setInterval( function(){
                var gps = $("#gps").val()
                if(gps != ""){ $('form').submit(); }
            },5000);
        });
    </script>
@endsection