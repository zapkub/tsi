<?php
use App\Http\Controllers\Backend\CsrController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new CsrController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);
?>
@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" class="form-horizontal">
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                    <div class="form-body">
                                        <div class="form-group">
                                        <label class="control-label col-md-3">Date</label>
                                        <div class="col-md-4">
                                            <div class="input-large date-picker input-daterange" data-date="<?php echo date("Y-m-d");?>" data-date-format="yyyy-mm-dd">
                                                <input type="text" class="form-control" name="csr_news_date" value="{{ $csr_news_date }}">
                                        </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Name TH</label>
                                            <div class="col-md-4">
                                                <input type="text" name="name_th"  class="form-control" value="{{ $name_th }}"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Name EN</label>
                                            <div class="col-md-4">
                                                <input type="text" name="name_en"  class="form-control" value="{{ $name_en }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Description TH</label>
                                            <div class="col-md-9">
                                                <textarea name="description_th" id="description_th" class="form-control">{{ $description_th }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Description EN</label>
                                            <div class="col-md-9">
                                                <textarea name="description_en" id="description_en" class="form-control">{{ $description_en }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="control-group ">
                                        <div class="controls">
                                            <div id="thumbnail" class="text-center">
                                                @if($img_path != '')
                                                    <img src="{{URL::asset('uploads/csr/'.$img_path) }}" id="img_path2" style="max-width:400px;">
                                                @else
                                                    <img src="" id="img_path2" style="max-width:400px;">
                                                @endif

                                                <input type="hidden" name="img_path" id="img_path" value="<?php echo  $img_path?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Image PDF [TH]</label>
                                            <div class="col-md-4">
                                                @if($img_path != '')
                                                    <div id="remove_img" class="btn btn-danger" >Remove</div>
                                                    <div id="upload" class="btn blue" style="display:none;"><i class="icon-picture"></i> Select File</div>

                                                @elseif ($img_path == '')
                                                    <div id="upload" class="btn blue" ><i class="icon-picture"></i> Select File</div>
                                                    <div id="remove_img" class="btn btn-danger" style="display:none;" >Remove</div>

                                                @endif

                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">PDF</label>
                                            <div class="col-md-4">

                                                @if($pdf_path != '')
                                                    <a href="{{ '/uploads/csr_pdf/'.$pdf_path }}" id="pdf_name" class="btn btn-xs btn-circle blue" target="_blank"><i class="fa fa-file"></i></a>
                                                    <button type="button" class="btn btn-xs btn-circle red" id="remove_pdf"><i class="fa  fa-trash-o"></i></button>
                                                    <input type="file" name="pdf_path" id="upload_pdf" style="display:none;" >
                                                @else
                                                    <input type="file" name="pdf_path" id="upload_pdf">

                                                    <button type="button" class="btn btn-xs btn-circle red" id="remove_pdf" style="display:none;"><i class="fa  fa-trash-o"></i></button>

                                                @endif
                                                <input type="hidden" name="pdf_path" id="pdf_path" value="<?php echo  $pdf_path?>">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="control-group ">
                                        <div class="controls">
                                            <div id="thumbnail1" class="text-center">
                                                @if($img_path_en != '')
                                                    <img src="{{URL::asset('uploads/csr_en/'.$img_path_en) }}" id="img_path3" style="max-width:400px;">
                                                @else
                                                    <img src="" id="img_path3" style="max-width:400px;">
                                                @endif

                                                <input type="hidden" name="img_path_en" id="img_path1" value="<?php echo  $img_path_en?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Image PDF [EN]</label>
                                            <div class="col-md-4">
                                                @if($img_path_en != '')
                                                    <div id="remove_img1" class="btn btn-danger" >Remove</div>
                                                    <div id="upload1" class="btn blue" style="display:none;"><i class="icon-picture"></i> Select File</div>

                                                @elseif ($img_path_en == '')
                                                    <div id="upload1" class="btn blue" ><i class="icon-picture"></i> Select File</div>
                                                    <div id="remove_img1" class="btn btn-danger" style="display:none;" >Remove</div>

                                                @endif

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">PDF EN</label>
                                            <div class="col-md-4">

                                                @if($pdf_path_en != '')
                                                    <a href="{{ '/uploads/csr_pdf_en/'.$pdf_path_en }}" id="pdf_name1" class="btn btn-xs btn-circle blue" target="_blank"><i class="fa fa-file"></i></a>
                                                    <button type="button" class="btn btn-xs btn-circle red" id="remove_pdf1"><i class="fa  fa-trash-o"></i></button>
                                                    <input type="file" name="pdf_path_en" id="upload_pdf1" style="display:none;" >
                                                @else
                                                    <input type="file" name="pdf_path_en" id="upload_pdf1">

                                                    <button type="button" class="btn btn-xs btn-circle red" id="remove_pdf1" style="display:none;"><i class="fa  fa-trash-o"></i></button>

                                                @endif
                                                <input type="hidden" name="pdf_path_en" id="pdf_path_en" value="<?php echo  $pdf_path_en?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <script src="{{URL::asset('js/elfinder-upload.js')}}"></script>
@endsection