<?php
use App\Http\Controllers\Backend\SpecialController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new SpecialController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);
?>



@extends('admin')

@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" id="form-user" class="form-horizontal">
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">



                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Product</label>
                                            <div class="col-md-3">
                                                <select name="product_id" class="form-control select2-container form-control select2me" >
                                                    @foreach($product as $field1)
                                                        <option value="{{$field1->product_id}}" @if($product_id == $field1->product_id) selected @endif >{{$field1->product_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Price</label>
                                            <div class="col-md-4">
                                                <input type='number' step="0.01" min="0" name="price" id="price" class="form-control" value="{{ $price }}">
                                            </div>
                                        </div>
                                    </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3">Start Date</label>
                                                <div class="col-md-4">
                                                    <input type='text' name="start_date" id="start_date" class="form-control" value="{{ $start_date }}" data-date-format="yyyy-mm-dd hh:ii" readonly>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3">End Date</label>
                                                <div class="col-md-4">
                                                    <input type='text' name="end_date" id="end_date" class="form-control" value="{{ $end_date }}" data-date-format="yyyy-mm-dd hh:ii" readonly>
                                                </div>
                                            </div>







                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <script src="{{URL::asset('js/elfinder-upload.js')}}"></script>

    <script>
        $(function() {
            $( "#start_date,#end_date" ).datetimepicker();

        });



    </script>

@endsection
