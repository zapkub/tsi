<?php
use App\Http\Controllers\Backend\ContactMsgController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new ContactMsgController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);

$admin_id = auth()->guard('admin')->user()->admin_id;
if(!empty($admin_id)){
    // echo $mainFn->chk_permission($admin_id,'career');
}else{
    return Redirect::to('_admin/logout');
}

?>
@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" id="form-character" class="form-horizontal">
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                    <div class="form-body">


                                        <div class="form-group">
                                            <label class="control-label col-md-3">From Name</label>
                                            <div class="col-md-4">
                                                <input type='text' name="name" id="name" class="form-control" value="Web Master">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">From Email</label>
                                            <div class="col-md-4">
                                                <input type='text' name="email" id="email" class="form-control" value="webmaster@tsi.com">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">To Name</label>
                                            <div class="col-md-4">
                                                <input type='text' name="to_name" id="to_name" class="form-control" readonly value="{{$a_use_name}}">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">To Email</label>
                                            <div class="col-md-4">
                                                <input type='text' name="to_email" id="to_email" class="form-control" readonly value={{$a_use_email}}>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Mobile</label>
                                            <div class="col-md-4">
                                                <input type='text' name="mobile" id="mobile" class="form-control" value={{$a_use_mobile}}>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Message from user </label>
                                            <div class="col-md-4"> <label class="control-label">{!! $a_use_msg !!}</label></div>
                                        </div>


                                        <div class="form-group">
                                            <label class="control-label col-md-3">Message</label>
                                            <div class="col-md-4">
                                                <textarea name="msg" id="msg" class="form-control" rows="12" cols="15" >{{ $msg }}</textarea>
                                            </div>
                                        </div>


                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <input type="hidden" name="id" value="{{Input::get('id')}}">

                                                    <input type="hidden" name="strParam" value="{{$strParam}}">
                                                    <button type="submit" class="btn blue">{{ $txt_manage }}</button>
                                                    <button type="reset" class="btn default">Reset</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <script src="{{URL::asset('js/elfinder-upload.js')}}"></script>

    <script>
        $(function() {
            $( "#post_datetime" ).datetimepicker();

        });
    </script>

@endsection
@section('js')
    <script src="{{URL::asset('assets/admin/scripts/contact_msg.js')}}"></script>
@endsection
