<?php
use App\Http\Controllers\Backend\ExecutiveCommitteeController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new ExecutiveCommitteeController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$user_title = App\Model\UserTitle::whereNull('deleted_at')->get();
$position = App\Model\Position::whereNull('deleted_at')->get();

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);

$admin_id = auth()->guard('admin')->user()->admin_id;
if(!empty($admin_id)){
    // echo $mainFn->chk_permission($admin_id,'about_company');
}else{
    return Redirect::to('_admin/logout');
}
?>
@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                {!! Form::open(array('url'=>$url_to , 'method' => $method , 'id' => 'form' , 'class' => 'form-horizontal','enctype'=>"multipart/form-data" )) !!}
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Title </label>
                                        <div class="col-md-4">
                                            <select class="form-control select2me" data-placeholder="Select..." name="user_title_id">
                                                <option value="">กรุณาเลือก</option>
                                                @foreach($user_title as $cate)
                                                    <option value="{{$cate->user_title_id}}" @if($user_title_id==$cate->user_title_id) selected @endif>{{$cate->title}} @if(!empty($cate->title_en)) ( {{ $cate->title_en }} ) @endif</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Firstname</label>
                                            <div class="col-md-3">
                                                <input type="text" name="firstname_th"  class="form-control" value="{{ $firstname_th }}"/>
                                            </div>
                                            <div class="col-md-1">
                                                ( TH )
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" name="firstname_en"  class="form-control" value="{{ $firstname_en }}"/>
                                            </div>
                                            <div class="col-md-1">
                                                ( EN )
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Lastname</label>
                                            <div class="col-md-3">
                                                <input type="text" name="lastname_th"  class="form-control" value="{{ $lastname_th }}"/>
                                            </div>
                                            <div class="col-md-1">
                                                ( TH )
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" name="lastname_en"  class="form-control" value="{{ $lastname_en}}"/>
                                            </div>
                                            <div class="col-md-1">
                                                ( EN )
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Education</label>
                                            <div class="col-md-3">
                                                <input type="text" name="education_th"  class="form-control" value="{{ $education_th }}"/>
                                            </div>
                                            <div class="col-md-1">
                                                ( TH )
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" name="education_en"  class="form-control" value="{{ $education_en }}"/>
                                            </div>
                                            <div class="col-md-1">
                                                ( EN )
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="control-label col-md-3">Position</label>
                                        <div class="col-md-4">
                                            <select class="form-control select2me" data-placeholder="Select..." name="position_id">
                                                <option value="">กรุณาเลือกหมวดหมู่</option>
                                                @foreach($position as $cate)
                                                    <option value="{{$cate->position_id}}" @if($position_id==$cate->position_id) selected @endif>{{$cate->name_th}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Age</label>
                                            <div class="col-md-4">
                                                <input type="text" name="age"  class="form-control" value="{{ $age }}"/>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Shareholding ( TH )</label>
                                            <div class="col-md-9">
                                                <textarea name="shareholding_th" id="shareholding_th" class="form-control ckeditor">{{ $shareholding_th }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Shareholding ( EN )</label>
                                            <div class="col-md-9">
                                                <textarea name="shareholding_en" id="shareholding_en" class="form-control ckeditor">{{ $shareholding_en }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Experience ( TH )</label>
                                            <div class="col-md-9">
                                                <textarea name="experience_th" id="experience_th" class="form-control ckeditor">{{ $experience_th }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Experience ( EN )</label>
                                            <div class="col-md-9">
                                                <textarea name="experience_en" id="experience_en" class="form-control ckeditor">{{ $experience_en }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="control-group ">
                                        <div class="controls">
                                            <div id="thumbnail" class="text-center">
                                                @if($img_path != '')
                                                    <img src="{{URL::asset('uploads/executive_committee/'.$img_path) }}" id="img_path2" style="max-width:400px;">
                                                @else
                                                    <img src="" id="img_path2" style="max-width:400px;">
                                                @endif

                                                <input type="hidden" name="img_path" id="img_path" value="<?php echo  $img_path?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Image</label>
                                            <div class="col-md-4">
                                                @if($img_path != '')
                                                    <div id="remove_img" class="btn btn-danger" >Remove</div>
                                                    <div id="upload" class="btn blue" style="display:none;"><i class="icon-picture"></i> Select File</div>

                                                @elseif ($img_path == '')
                                                    <div id="upload" class="btn blue" ><i class="icon-picture"></i> Select File</div>
                                                    <div id="remove_img" class="btn btn-danger" style="display:none;" >Remove</div>

                                                @endif

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label"> </label>
                                        <div class="col-md-4">
                                            <span style="color:#F00">* หมายเหตุ</span><br>
                                                                    <span style="">
                                                                    - ไฟล์ต้องเป็นมีขนาดกว้าง 210x278 pixel. <br>
                                                                    - นามสกุลของรูปภาพต้อง .png, .jpg เท่านั้น<br>
                                                                    </span>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                {!! Form::close() !!}

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>

@endsection

@section('js')
    <script src="{{URL::asset('js/elfinder-upload.js')}}"></script>
    <script src="{{URL::asset('assets/admin/scripts/validate/executive_committee.js')}}"></script>
@endsection