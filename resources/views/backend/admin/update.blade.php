<?php
use App\Http\Controllers\Backend\AdminController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new AdminController();
$mainFn = new MainFunction();

$admin_roles = App\Model\AdminRole::whereNull('deleted_at')->get();
$user_title = App\Model\UserTitle::whereNull('deleted_at')->get();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);

$admin_id = auth()->guard('admin')->user()->admin_id;
if(!empty($admin_id)){
    // echo $mainFn->chk_permission($admin_id,'manage_user');
}else{
    return Redirect::to('_admin/logout');
}
?>

@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                {!! Form::open(array('url'=>$url_to , 'method' => $method , 'id' => 'form' , 'class' => 'form-horizontal','enctype'=>"multipart/form-data" )) !!}
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Role</label>
                                            <div class="col-md-4">
                                                <select class="form-control " data-placeholder="Select..." id="role" name="role" data-type="role">
                                                    <option value="">กรุณาเลือกหมวดหมู่</option>
                                                    @foreach($admin_roles as $admin_role)
                                                        <option value="{{$admin_role->admin_role_id}}" <?php if($admin_role_id==$admin_role->admin_role_id )echo "selected"; ?>  >{{$admin_role->role_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Title</label>
                                            <div class="col-md-4">
                                                <select class="form-control " data-placeholder="Select..." id="title" name="title" data-type="title">
                                                    <option value="">กรุณาเลือกหมวดหมู่</option>
                                                    @foreach($user_title as $cate)
                                                        <option value="{{$cate->user_title_id}}" <?php if($title==$cate->user_title_id )echo "selected"; ?>  >{{$cate->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Firstname</label>
                                            <div class="col-md-4">
                                                <input type="text" name="firstname"  class="form-control" value="{{ $firstname }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Lastname</label>
                                            <div class="col-md-4">
                                                <input type="text" name="lastname"  class="form-control" value="{{ $lastname }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Address 1</label>
                                            <div class="col-md-4">
                                                <textarea name="address1" id="address1" class="form-control">{{ $address1 }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Address 2</label>
                                            <div class="col-md-4">
                                                <textarea name="address2" id="address2" class="form-control">{{ $address2 }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Position</label>
                                            <div class="col-md-4">
                                                <input type="text" name="position"  class="form-control" value="{{ $position }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Department</label>
                                            <div class="col-md-4">
                                                <input type="text" name="department"  class="form-control" value="{{ $department }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Branch</label>
                                            <div class="col-md-4">
                                                <input type="text" name="branch"  class="form-control" value="{{ $branch }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Telephone</label>
                                            <div class="col-md-4">
                                                <input type="text" name="telephone"  class="form-control" value="{{ $telephone }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Fax</label>
                                            <div class="col-md-4">
                                                <input type="text" name="fax"  class="form-control" value="{{ $fax }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Ext</label>
                                            <div class="col-md-4">
                                                <input type="text" name="ext"  class="form-control" value="{{ $ext }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Mobile</label>
                                            <div class="col-md-4">
                                                <input type="text" name="mobile"  class="form-control" value="{{ $mobile }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Email</label>
                                            <div class="col-md-4">
                                                <input type="text" name="email"  class="form-control" value="{{ $email }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Line ID</label>
                                            <div class="col-md-4">
                                                <input type="text" name="line_id"  class="form-control" value="{{ $line_id }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Map</label>
                                            <div class="col-md-4">
                                                <input type="text" name="map"  class="form-control" value="{{ $map }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Username</label>
                                            <div class="col-md-4">
                                                <input type="text" name="username"  class="form-control" value="{{ $username }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Password</label>
                                            <div class="col-md-4">
                                                @if($method=="PUT")
                                                    <input type="password" name="new_password"  class="form-control" value=""/>
                                                    <input type="hidden" name="password"  class="form-control" value="{{ $password }}"/>
                                                @else
                                                    <input type="password" name="password"  class="form-control" value=""/>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <hr>
                                    <h3>Set Permission</h3>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3"></label>
                                            <div class="col-md-9">
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="banner" id="banner" value="1" @if($banner==1) checked @endif> Banner
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="product" id="product" value="1" @if($product==1) checked @endif> ผลิตภัณฑ์
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3"></label>
                                            <div class="col-md-9">
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="service" id="service" value="1" @if($service==1) checked @endif> บริการลูกค้า
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="about_company" id="about_company" value="1" @if($about_company==1) checked @endif> เกี่ยวกับบริษัท
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3"></label>
                                            <div class="col-md-9">
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="career" id="career" value="1" @if($career==1) checked @endif> ติดต่อบริษัท
                                                </label>
                                                    <label class="checkbox-inline">
                                                    <input type="checkbox" name="car_insurance" id="car_insurance" value="1" @if($car_insurance==1) checked @endif> ใบคำขอประกัน
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3"></label>
                                            <div class="col-md-9">
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="car_insurance" id="car_insurance" value="1" @if($car_insurance==1) checked @endif> ใบคำขอประกัน
                                                    </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="manage_user" id="manage_user" value="1" @if($manage_user==1) checked @endif> จัดการผู้ใช้
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                {!! Form::close() !!}

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
@endsection
@section('js')
    <script src="{{URL::asset('assets/admin/scripts/validate/admin.js')}}"></script>
@endsection