<?php
use App\Http\Controllers\Backend\BannerController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new BannerController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);

$admin_id = auth()->guard('admin')->user()->admin_id;
if(!empty($admin_id)){
    // echo $mainFn->chk_permission($admin_id,'banner');
}else{
    return Redirect::to('_admin/logout');
}
?>
@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" class="form-horizontal">
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3" for="status">Status</label>
                                            <div class="col-md-4">
                                                <select name="status"  class="form-control select2me">
                                                    <option value="1" @if ($status=="1") selected @endif>Open</option>
                                                    <option value="0" @if ($status=="0") selected @endif >Close</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">URL</label>
                                            <div class="col-md-4">
                                                <input type="text" name="url"  class="form-control" value="{{ $url }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <?php
                                    $path = "uploads/banner/".$img_path;
                                    $path_img = URL::asset($path);
                                    $check_path = public_path($path);
                                    ?>

                                    <div class="control-group ">
                                        <div class="controls">
                                            <div id="thumbnail" class="text-center">
                                                @if(file_exists($check_path) and !empty($img_path))
                                                    <img src="{{ $path_img }}" id="img_path2" style="max-width:400px;">
                                                @else
                                                    <img src="" id="img_path2" style="max-width:400px;">
                                                @endif

                                                <input type="hidden" name="img_path" id="img_path" value="<?php echo  $img_path?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Image</label>
                                            <div class="col-md-9">
                                                <div id="upload" class="btn blue"><i class="icon-picture"></i> Select File</div>
                                                <div id="remove_img" class="btn btn-danger" style="display:none;">Remove</div>
                                                {{--<input type="file" name="img_path">--}}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label"> </label>
                                        <div class="col-md-4">
                                            <span style="color:#F00">* หมายเหตุ</span><br>
                                                                    <span style="">
                                                                    - ไฟล์ต้องเป็นมีขนาดกว้าง 1140x464 pixel. <br>
                                                                    - นามสกุลของรูปภาพต้อง .png, .jpg เท่านั้น<br>
                                                                    </span>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <script src="{{URL::asset('js/elfinder-upload.js')}}"></script>

@endsection