<?php
use App\Http\Controllers\Backend\BannerFooterController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new BannerFooterController(); // New Object Controller
$mainFn = new MainFunction(); // New Object Main Function

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$path = $objCon->path;
$tb_name = $objCon->tbName;

$orderBy = Input::get('orderBy');
$sortBy = Input::get('sortBy');
$search = Input::get('search');

$a_otherParam = Input::except(['orderBy','sortBy']);
$strParam = $mainFn->parameter($a_otherParam);

$admin_id = auth()->guard('admin')->user()->admin_id;
if(!empty($admin_id)){
    // echo $mainFn->chk_permission($admin_id,'banner');
}else{
    return Redirect::to('_admin/logout');
}
?>
@extends('admin')
@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>{{ $titlePage }}</h1>
                </div>
            </div>
        </div>

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->

                        <div class="portlet light">

                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-database font-green-sharp"></i>
                                    <span class="caption-subject font-green-sharp bold">Found {{ $countData }} Record(s).</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('ID',$pkField,$orderBy,$sortBy,$strParam) !!} </th>
                                            <th class="col-md-1 text-center">Images</th>
                                            <th class="col-md-2 text-center">{!! $mainFn->sorting('URL','url',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('Status','status',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-2 text-center">{!! $mainFn->sorting('Sequence','sequence',$orderBy,$sortBy,$strParam) !!}</th>
                                            @if($permission['c'] == '1' || $permission['u'] == '1' || $permission['d'] == '1')
                                                <th class="col-md-1 text-center">
                                                    @if($permission['c'] == '1')
                                                        <a href="{{ URL::to($path.'/create') }}" class="btn btn-circle blue btn-xs"><i class="fa fa-plus"></i> Add</a>
                                                    @endif
                                                </th>
                                            @endif
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($countData > 0)
                                            @foreach($data as $field)
                                                <tr>
                                                    <td class="text-center">{{ $field->$pkField }}</td>
                                                    <td class="text-center">
                                                        <?php
                                                        $path2 = "uploads/banner_footer/100/".$field->img_path;
                                                        $path_img = URL::asset($path2);
                                                        $check_path = public_path($path2);
                                                        ?>

                                                        @if(file_exists($check_path) and !empty($field->img_path))
                                                            <img src="{{ $path_img }}" width="100">
                                                        @endif
                                                    </td>
                                                    <td><a href="{{ URL::to($field->url) }}"></a></td>

                                                    @if($field->status == 0)
                                                        <td><div class="text-center"><a id="status-{{$field->banner_footer_id}}" class=" change-active fa fa-close text-red" data-tb_name="{{$tb_name}}" data-pk_field="{{$pkField}}" data-v_pk_field="{{$field->$pkField}}" data-change_field="status"  data-value="{{$field->status}}"></a></div></td>
                                                    @else
                                                        <td><div class="text-center"><a id="status-{{$field->banner_footer_id}}" class=" change-active fa fa-check text-green" data-tb_name="{{$tb_name}}" data-pk_field="{{$pkField}}" data-v_pk_field="{{$field->$pkField}}" data-change_field="status"  data-value="{{$field->status}}"></a></div></td>
                                                    @endif

                                                    <td>
                                                        {!! Form::open(array('url'=>'_admin/sequence/banner' , 'method' => 'post' , 'id' => 'form' , 'class' => 'form-horizontal'  )) !!}

                                                        <div class="input-group">
                                                            <input type="text" class="form-control" name="new_sorting" value="{{$field->sequence}}">
                                                            <div class="input-group-btn">
                                                                <input type="hidden" name="{{$pkField}}" value="{{$field->$pkField}}">
                                                                <input type="hidden" name="old_sorting" value="{{$field->sequence}}">;
                                                                <button class="btn" type="submit"><i class="glyphicon glyphicon-resize-horizontal"></i></button>
                                                            </div>
                                                        </div>
                                                        {!! Form::close() !!}
                                                    </td>

                                                    @if($permission['c'] == '1' || $permission['u'] == '1' || $permission['d'] == '1')
                                                        <td class="text-center">
                                                            @if($permission['u'] == '1')
                                                                <a href="{{ URL::to($path.'/'.$field->$pkField.'/edit?'.$strParam) }}" class="btn btn-xs btn-circle green"><i class="fa fa-edit"></i></a>
                                                            @endif
                                                            @if($permission['d'] == '1')
                                                                <form action="{{URL::to($path.'/'.$field->$pkField)}}" method="post" class="frm-delete">
                                                                    <input name="_method" type="hidden" value="delete">
                                                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                                    <button type="submit" class="btn btn-xs btn-circle red" ><i class="fa  fa-trash-o"></i></button>
                                                                </form>
                                                            @endif
                                                        </td>
                                                    @endif 
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr><td colspan='7' class='text-center'>No Result.</td></tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                {!! $data->appends(Input::except('page'))->render() !!}
                            </div>
                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>

    <script src="{{ URL::asset('js/change-status.js') }}"type="text/javascript"></script>
@endsection