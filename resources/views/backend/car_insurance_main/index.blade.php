<?php
use App\Http\Controllers\Backend\CarInsuranceMainController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new CarInsuranceMainController(); // New Object Controller
$mainFn = new MainFunction(); // New Object Main Function

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$tb_name = $objCon->tbName;
$path = $objCon->path;

$orderBy = Input::get('orderBy');
$sortBy = Input::get('sortBy');
$search = Input::get('search');

$a_otherParam = Input::except(['orderBy','sortBy']);
$strParam = $mainFn->parameter($a_otherParam);

$admin_id = auth()->guard('admin')->user()->admin_id;
$db = DB::table('admin')->select('firstname','lastname')->where('admin_id',$admin_id)->first();
if(!empty($admin_id)){
    // echo $mainFn->chk_permission($admin_id,'car_insurance');
}else{
    return Redirect::to('_admin/logout');
}

?>
@extends('admin')
@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>{{ $titlePage }} :: {{ $db->firstname }} {{ $db->lastname }}</h1>
                </div>
            </div>
        </div>

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->

                        <div class="portlet light">

                            <form action{{URL::to($path)}} class="form-horizontal" method="GET" >
                                <div class="form-group">
                                    <label class="control-label col-md-1"></label>
                                    <div class="col-md-4">
                                        <div class="input-group input-large date-picker input-daterange" data-date="<?php echo date("Y-m-d");?>" data-date-format="yyyy-mm-dd">
                                            <input type="text" class="form-control" name="from_date" value="">
												<span class="input-group-addon">
												to </span>
                                            <input type="text" class="form-control" name="to_date" value="">
                                        </div>
                                        <span class="help-block">
											Select date range </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-1 col-md-3 ">
                                        <button class="btn blue btn-sm" type="submit" ><i class="fa fa-search"></i> Search</button>
                                    </div>
                                </div>
                            </form>
                            <hr>

                            <div class="portlet light">

                                <div class="col-md-3 text-right">
                                    ใบคำขอประกันรถยนต์
                                </div>
                                    <div class="col-md-3 text-left">
                                        <a href="{{ URL::to('_admin/car_insurance_agent?status=0') }}" class="btn btn-info"><li class="fa fa-edit"></li> รอการยืนยัน <span class="btn btn-circle btn-xs">{{ $count_1 }}</span>
                                        </a>
                                    </div>
                                    <div class="col-md-3 text-left">
                                        <a href="{{ URL::to('_admin/car_insurance_agent?status=1') }}" class="btn btn-success"><li class="fa fa-check"></li> ผ่านการยืนยัน <span class="btn btn-circle btn-xs">{{ $count_2 }}</span>
                                        </a>
                                    </div>
                                    <div class="col-md-3 text-left">
                                        <a href="{{ URL::to('_admin/car_insurance_agent?status=2') }}" class="btn btn-danger"><li class="fa fa-close"></li> ไม่อนุมัติ <span class="btn btn-circle btn-xs">{{ $count_3 }}</span>
                                        </a>
                                    </div>
                            </div>

                            <div class="portlet light">

                                <div class="col-md-3 text-right">
                                    ใบสลักหลัง
                                </div>
                                <div class="col-md-3 text-left">
                                    <a href="{{ URL::to('_admin/form_insurance_agent?status=0') }}" class="btn btn-info"><li class="fa fa-edit"></li> รอการยืนยัน <span class="btn btn-circle btn-xs">{{ $count_s_1 }}</span>
                                    </a>
                                </div>
                                <div class="col-md-3 text-left">
                                    <a href="{{ URL::to('_admin/form_insurance_agent?status=1') }}" class="btn btn-success"><li class="fa fa-check"></li> ผ่านการยืนยัน <span class="btn btn-circle btn-xs">{{ $count_s_2 }}</span>
                                    </a>
                                </div>
                                <div class="col-md-3 text-left">
                                    <a href="{{ URL::to('_admin/form_insurance_agent?status=2') }}" class="btn btn-danger"><li class="fa fa-close"></li> ไม่อนุมัติ <span class="btn btn-circle btn-xs">{{ $count_s_3 }}</span>
                                    </a>
                                </div>
                            </div>

                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
@endsection