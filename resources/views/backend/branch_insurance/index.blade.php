<?php
use App\Http\Controllers\Backend\BranchInsuranceController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new BranchInsuranceController(); // New Object Controller
$mainFn = new MainFunction(); // New Object Main Function

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$path = $objCon->path;

$orderBy = Input::get('orderBy');
$sortBy = Input::get('sortBy');
$search = Input::get('search');

$a_otherParam = Input::except(['orderBy','sortBy']);
$strParam = $mainFn->parameter($a_otherParam);


?>
@extends('admin')
@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>{{ $titlePage }}</h1>
                </div>
            </div>
        </div>

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->

                        <div class="portlet light">

                            <form action{{URL::to($path)}} class="form-horizontal" method="GET" >
                            <div class="form-group">
                                <label class="control-label col-md-1">Search</label>
                                <div class="col-md-3">
                                    <input class="form-control" type="text" name="search" value="{{ $search }}" >
                                    <span class="help-block">Search by name</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-1 col-md-3 ">
                                    <button class="btn blue btn-sm" type="submit" ><i class="fa fa-search"></i> Search</button>
                                </div>
                            </div>
                            </form>
                            <hr>

                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-database font-green-sharp"></i>
                                    <span class="caption-subject font-green-sharp bold">Found {{ $countData }} Record(s).</span>
                                </div>
                            </div>
                        
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('ID',$pkField,$orderBy,$sortBy,$strParam) !!} </th>
                                            <th>{!! $mainFn->sorting('เลขที่ใบคำร้อง','service_id',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th>{!! $mainFn->sorting('ทะเบียนรถ','policy_no',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th>{!! $mainFn->sorting('เบี้ยสุทธิ','policy_no',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th>{!! $mainFn->sorting('เบี้ยรวมภาษี','policy_no',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th>{!! $mainFn->sorting('เบี้ยพ.ร.บ','policy_no',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th>{!! $mainFn->sorting('วันที่เริ่มคุ้มครอง','policy_no',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th>{!! $mainFn->sorting('ตำแทน','policy_no',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th>{!! $mainFn->sorting('สาขา','policy_no',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th>{!! $mainFn->sorting('ผู้ยืนยัน','policy_no',$orderBy,$sortBy,$strParam) !!}</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @if($countData > 0)
                                            @foreach($data as $field)
                                                <tr>
                                                    <td class="text-center">{{ $field->$pkField }}</td>
                                                    <td>{{ $field->policy_no }}</td>
                                                    <td>{{ $field->policy_no }}</td>
                                                    <td>{{ $field->policy_no }}</td>
                                                    <td>{{ $field->policy_no }}</td>
                                                    <td>{{ $field->policy_no }}</td>
                                                    <td>{{ $field->policy_no }}</td>
                                                    <td>{{ $field->policy_no }}</td>
                                                    <td>{{ $field->policy_no }}</td>
                                                    <td>{{ $field->policy_no }}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr><td colspan='9' class='text-center'>No Result.</td></tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                {!! $data->appends(Input::except('page'))->render() !!}
                            </div>

                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
@endsection