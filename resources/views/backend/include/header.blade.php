<?php
    use App\Library\MainFunction;
    $objFn = new MainFunction();
?>
<div class="page-header">
    <!-- BEGIN HEADER TOP -->
    <div class="page-header-top">
        <div class="container">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                <a href="{{URL::to('_admin')}}"><img src="{{ URL::asset('images/logo-blue.png') }}" alt="logo" class="logo-default" height="68"></a>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler"></a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu hidden-xs">
                <ul class="nav navbar-nav pull-right">
                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="javascript:;" >
                            <span class="username username-hide-mobile">Hello, {{auth()->guard('admin')->user()->firstname}}</span>
                        </a>
                    </li>
                    <li class="droddown dropdown-separator">
                        <span class="separator"></span>
                    </li>
                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="{{URL::to('_admin/logout')}}" >
                            <span class="username username-hide-mobile"><i class="fa fa-sign-out"></i> Logout</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
    </div>
    <!-- END HEADER TOP -->
    <!-- BEGIN HEADER MENU -->
    <?php
    $data = DB::table('admin')->select('intro_page','product','service','news','about_company','career','car_insurance','surveyor','admin','agent')->where('admin_id',auth()->guard('admin')->user()->admin_id)->first();
    ?>
    <div class="page-header-menu">
        <div class="container">

            <!-- BEGIN MEGA MENU -->
            <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
            <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
            <div class="hor-menu ">
                <ul class="nav navbar-nav">


                    <!-- <li class="menu-dropdown classic-menu-dropdown">
                        <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
                            Banner <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li class=""><a href="{{ URL::to('_admin/banner') }}">Banner Top</a></li>
                            <li class=""><a href="{{ URL::to('_admin/banner_claim') }}">Banner Claim</a></li>
                            <li class=""><a href="{{ URL::to('_admin/banner_footer') }}">Banner Footer</a></li>
                        </ul>
                    </li> -->

                    <!-- {{-- @if($data->product==1) --}} -->
                        <!-- <li class="menu-dropdown classic-menu-dropdown">
                            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
                                ผลิตภัณฑ์ <i class="fa fa-angle-down"></i>
                            </a>

                            <ul class="dropdown-menu pull-left">
                                <li class=""><a href="{{ URL::to('_admin/category/') }}">หมวดหมู่</a></li>
                                <li class=""><a href="{{ URL::to('_admin/product/') }}">ผลิตภัณฑ์</a></li>
                            </ul>
                        </li> -->
                    <!-- {{-- @endif --}} -->

                    <!-- {{-- @if($data->service==1) --}}
                        <li class="menu-dropdown classic-menu-dropdown">
                            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
                                บริการลูกค้า<i class="fa fa-angle-down"></i>
                            </a>

                            <ul class="dropdown-menu pull-left">
                                <li class=""><a href="{{ URL::to('_admin/garage/') }}">ค้นหาอู่</a></li>
                                <li class=""><a href="{{ URL::to('_admin/repair') }}">ค้นหาศูนย์ซ่อม</a></li>
                                <li class=""><a href="{{ URL::to('_admin/service/') }}">ค้นหาศูนย์บริการ TSI</a></li>
                                @if($data->surveyor==1)
                                    <li><a href="{{ URL::to('_admin/surveyor') }}">ตัวแทนประกัน</a></li>
                                @endif
                            </ul>
                        </li>
                    {{-- @endif --}} -->

                    <!-- {{-- @if($data->about_company==1) --}}
                        <li class="menu-dropdown classic-menu-dropdown">
                            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
                                เกี่ยวกับบริษัท<i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-left">
                                <li class=""><a href="{{ URL::to('_admin/history/1/edit/') }}">ประวัติบริษัท</a></li>
                                <li class=""><a href="{{ URL::to('_admin/vision_and_mission/1/edit/') }}">วิสัยทัศน์และพันธกิจ</a></li>
                                <li class=""><a href="{{ URL::to('_admin/position/') }}">ตำแหน่ง</a></li>
                                <li class=""><a href="{{ URL::to('_admin/executive_committee/') }}">คณะผู้บริหาร</a></li>
                                <li class=""><a href="{{ URL::to('_admin/yearbook/') }}">รายงานประจำปี</a></li>
                                <li class=""><a href="{{ URL::to('_admin/financial/') }}">ฐานะการเงิน และผลการดำเนินงาน</a></li>
                                @if($data->news==1)
                                    <li><a href="{{ URL::to('_admin/news/') }}">ข่าวประกาศต่างๆ</a></li>
                                @endif
                            </ul>
                        </li>
                    {{-- @endif --}} -->

                    <!-- {{-- @if($data->career==1) --}}
                        <li class="menu-dropdown classic-menu-dropdown">
                            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
                                ติดต่อบริษัท <i class="fa fa-angle-down"></i>
                            </a>

                            <ul class="dropdown-menu pull-left">
                                <li class=""><a href="{{ URL::to('_admin/career') }}">ร่วมงานกับเรา</a></li>
                                <li><a href="{{ URL::to('_admin/contact_msg') }}">ติดต่อขอซื้อประกัน</a></li>
                                <li><a href="{{ URL::to('_admin/petition') }}">ส่งเรื่องร้องเรียน</a></li>
                            </ul>
                        </li>
                    {{-- @endif --}} -->

                    <!-- {{-- @if($data->car_insurance==1) --}}
                        <li class="menu-dropdown classic-menu-dropdown">
                            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
                                ใบคำขอประกัน<i class="fa fa-angle-down"></i>
                            </a>

                            <ul class="dropdown-menu pull-left">
                                <li class=""><a href="{{ URL::to('_admin/car_insurance_branch') }}">ใบคำขอ / ใบสลักหลัง (สาขา)</a></li>
                                {{--                            <li class=""><a href="{{ URL::to('_admin/form_insurance') }}"><i class="fa fa-file"></i> ยืนยันสลักหลัง (สาขา)</a></li>--}}
                                <li class=""><a href="{{ URL::to('_admin/car_insurance_main') }}">ใบคำขอ / ใบสลักหลัง (ตัวแทน)</a></li>
                                {{--                            <li class=""><a href="{{ URL::to('_admin/form_insurance_agent') }}"><i class="fa fa-file"></i> ยืนยันสลักหลัง (ตัวแทน)</a></li>--}}
                            </ul>
                        </li>
                    {{-- @endif --}} -->

                    <!-- <li class="menu-dropdown classic-menu-dropdown">
                        <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
                            จัดการผู้ใช้ <i class="fa fa-angle-down"></i>
                        </a>

                        <ul class="dropdown-menu pull-left">
                            @if($data->admin==1)
                                <li><a href="{{ URL::to('_admin/admin') }}">รายชื่อบุคลากร</a></li>
                            @endif
                            @if($data->agent==1)
                                <li><a href="{{ URL::to('_admin/agent') }}">ตัวแทน</a></li>
                            @endif
                            <li><a href="{{ URL::to('_admin/admin_role') }}">บทบาทผู้ใช้</a></li>
                            <li><a href="{{ URL::to('_admin/page') }}">หน้า</a></li>
                        </ul>
                    </li>
                    <li class="visible-xs-block"><a href="logout"><i class="fa fa-sign-out"></i> Logout</a></li> -->









                    @if($objFn->can_access_page('Banner') || $objFn->can_access_page('BannerClaim') || $objFn->can_access_page('Banner Footer'))
                        <li class="menu-dropdown classic-menu-dropdown">
                            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
                                Banner <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-left">
                                @if($objFn->can_access_page('Banner'))
                                    <li class=""><a href="{{ URL::to('_admin/banner') }}">Banner Top</a></li>
                                @endif
                                @if($objFn->can_access_page('BannerClaim'))
                                    <li class=""><a href="{{ URL::to('_admin/banner_claim') }}">Banner Claim</a></li>
                                @endif
                                @if($objFn->can_access_page('Banner Footer'))
                                    <li class=""><a href="{{ URL::to('_admin/banner_footer') }}">Banner Footer</a></li>
                                @endif
                            </ul>
                        </li>
                    @endif

                    @if($objFn->can_access_page('Category') || $objFn->can_access_page('Product'))
                        <li class="menu-dropdown classic-menu-dropdown">
                            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
                                ผลิตภัณฑ์ <i class="fa fa-angle-down"></i>
                            </a>

                            <ul class="dropdown-menu pull-left">
                                @if($objFn->can_access_page('Category'))
                                    <li class=""><a href="{{ URL::to('_admin/category/') }}">หมวดหมู่</a></li>
                                @endif
                                @if($objFn->can_access_page('Product'))
                                    <li class=""><a href="{{ URL::to('_admin/product/') }}">ผลิตภัณฑ์</a></li>
                                @endif
                            </ul>
                        </li>
                    @endif

                    @if($objFn->can_access_page('Category') || $objFn->can_access_page('Compensation'))
                        <li class="menu-dropdown classic-menu-dropdown">
                            <a href="{{ URL::to('_admin/compensation/') }}">บริการด้านสินไหม</a>

                        </li>
                    @endif

                    @if($objFn->can_access_page('Garage') || $objFn->can_access_page('Repair') || $objFn->can_access_page('Service') || $objFn->can_access_page('Surveyor'))
                        <li class="menu-dropdown classic-menu-dropdown">
                            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
                                บริการลูกค้า<i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-left">
                                @if($objFn->can_access_page('Garage'))
                                    <li class=""><a href="{{ URL::to('_admin/garage/') }}">ค้นหาอู่</a></li>
                                @endif
                                @if($objFn->can_access_page('Repair'))
                                    <li class=""><a href="{{ URL::to('_admin/repair') }}">ค้นหาศูนย์ซ่อม</a></li>
                                @endif
                                @if($objFn->can_access_page('Service'))
                                    <li class=""><a href="{{ URL::to('_admin/service/') }}">ค้นหาศูนย์บริการ TSI</a></li>
                                @endif
                                @if($objFn->can_access_page('Surveyor'))
                                    <li><a href="{{ URL::to('_admin/surveyor') }}">ตัวแทนประกัน</a></li>
                                @endif
                            </ul>
                        </li>
                    @endif


                    @if($objFn->can_access_page('History') || $objFn->can_access_page('Vision And Mission') || $objFn->can_access_page('Positio') || $objFn->can_access_page('Executive Committee') || $objFn->can_access_page('Year Book') || $objFn->can_access_page('Financial') || $objFn->can_access_page('News'))
                        <li class="menu-dropdown classic-menu-dropdown">
                            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
                                เกี่ยวกับบริษัท<i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-left">
                                <li class=""><a href="{{ URL::to('_admin/history/1/edit/') }}">ประวัติบริษัท</a></li>
                                <li class=""><a href="{{ URL::to('_admin/vision_and_mission/1/edit/') }}">วิสัยทัศน์และพันธกิจ</a></li>
                                <li class=""><a href="{{ URL::to('_admin/position/') }}">ตำแหน่ง</a></li>
                                <li class=""><a href="{{ URL::to('_admin/executive_committee/') }}">คณะผู้บริหาร</a></li>
                                <li class=""><a href="{{ URL::to('_admin/yearbook/') }}">รายงานประจำปี</a></li>
                                <li class=""><a href="{{ URL::to('_admin/financial/') }}">ฐานะการเงิน และผลการดำเนินงาน</a></li>
                                @if($data->news==1)
                                    <li><a href="{{ URL::to('_admin/news/') }}">ข่าวประกาศต่างๆ</a></li>
                                @endif
                            </ul>
                        </li>
                    @endif

                    @if($objFn->can_access_page('Career') || $objFn->can_access_page('Contact Message') || $objFn->can_access_page('Petition'))
                        <li class="menu-dropdown classic-menu-dropdown">
                            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
                                ติดต่อบริษัท <i class="fa fa-angle-down"></i>
                            </a>

                            <ul class="dropdown-menu pull-left">
                                @if($objFn->can_access_page('Career'))
                                    <li class=""><a href="{{ URL::to('_admin/career') }}">ร่วมงานกับเรา</a></li>
                                @endif
                                @if($objFn->can_access_page('Contact Message'))
                                    <li><a href="{{ URL::to('_admin/contact_msg') }}">ติดต่อขอซื้อประกัน</a></li>
                                @endif
                                @if($objFn->can_access_page('Petition'))
                                    <li><a href="{{ URL::to('_admin/petition') }}">ส่งเรื่องร้องเรียน</a></li>
                                @endif
                            </ul>
                        </li>
                    @endif

                    @if($objFn->can_access_page('การตรวจยืนยันใบคำขอออนไลน์') || $objFn->can_access_page('การตรวจยืนยันใบสลักหลัง'))
                        <li class="menu-dropdown classic-menu-dropdown">
                            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
                                ใบคำขอประกัน<i class="fa fa-angle-down"></i>
                            </a>

                            <ul class="dropdown-menu pull-left">
                                @if($objFn->can_access_page('การตรวจยืนยันใบคำขอออนไลน์'))
                                    <li class=""><a href="{{ URL::to('_admin/car_insurance_branch') }}">ใบคำขอ / ใบสลักหลัง (สาขา)</a></li>
                                @endif
                                @if($objFn->can_access_page('การตรวจยืนยันใบสลักหลัง'))
                                    <li class=""><a href="{{ URL::to('_admin/car_insurance_main') }}">ใบคำขอ / ใบสลักหลัง (ตัวแทน)</a></li>
                                @endif

                                {{--<li class=""><a href="{{ URL::to('_admin/form_insurance') }}"><i class="fa fa-file"></i> ยืนยันสลักหลัง (สาขา)</a></li>--}}
                                {{--<li class=""><a href="{{ URL::to('_admin/form_insurance_agent') }}"><i class="fa fa-file"></i> ยืนยันสลักหลัง (ตัวแทน)</a></li>--}}
                            </ul>
                        </li>
                    @endif

                    @if($objFn->can_access_page('Staff') || $objFn->can_access_page('User') || $objFn->can_access_page('AdminRole') || $objFn->can_access_page('Page'))
                        <li class="menu-dropdown classic-menu-dropdown">
                            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
                                จัดการผู้ใช้ <i class="fa fa-angle-down"></i>
                            </a>

                            <ul class="dropdown-menu pull-left">
                                @if($objFn->can_access_page('Staff'))
                                    <li><a href="{{ URL::to('_admin/admin') }}">รายชื่อบุคลากร</a></li>
                                @endif
                                @if($objFn->can_access_page('User'))
                                    <li><a href="{{ URL::to('_admin/agent') }}">ตัวแทน</a></li>
                                @endif
                                @if($objFn->can_access_page('AdminRole'))
                                    <li><a href="{{ URL::to('_admin/admin_role') }}">บทบาทผู้ใช้</a></li>
                                @endif
                                @if($objFn->can_access_page('Page'))
                                    <li><a href="{{ URL::to('_admin/page') }}">หน้า</a></li>
                                @endif
                            </ul>
                        </li>
                    @endif

                    <li class="visible-xs-block"><a href="logout"><i class="fa fa-sign-out"></i> Logout</a></li>



                </ul>
            </div>
            <!-- END MEGA MENU -->
        </div>
    </div>
    <!-- END HEADER MENU -->
</div>
