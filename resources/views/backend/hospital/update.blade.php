<?php
use App\Http\Controllers\Backend\HospitalController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new HospitalController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$title = App\Model\Title::whereNull('deleted_at')->get();
$brand = App\Model\Brand::whereNull('deleted_at')->get();

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);
?>
@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" class="form-horizontal">
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Title</label>
                                            <div class="col-md-9">
                                                <select class="form-control " data-placeholder="Select..." id="title_id" name="title_id" data-type="title_id">
                                                    <option value="">กรุณาเลือกหมวดหมู่</option>
                                                    @foreach($title as $cate)
                                                        <option value="{{$cate->title_id}}" <?php if($title_id==$cate->title_id )echo "selected"; ?>  >{{$cate->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Brand</label>
                                            <div class="col-md-4">
                                                <select class="form-control " data-placeholder="Select..." id="brand_id" name="brand_id" data-type="brand_id">
                                                    <option value="">กรุณาเลือกหมวดหมู่</option>
                                                    @foreach($brand as $cate)
                                                        <option value="{{$cate->brand_id}}" <?php if($brand_id==$cate->brand_id)echo "selected"; ?>  >{{$cate->name_th}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">region</label>
                                            <div class="col-md-4">
                                                <input type="text" name="region"  class="form-control" value="{{ $region }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Name</label>
                                            <div class="col-md-4">
                                                <input type="text" name="name"  class="form-control" value="{{ $name }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Address 1</label>
                                            <div class="col-md-9">
                                                <textarea name="address1" id="address1" class="form-control">{{ $address1 }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Address 2</label>
                                            <div class="col-md-9">
                                                <textarea name="address2" id="address2" class="form-control">{{ $address2 }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">District</label>
                                            <div class="col-md-4">
                                                <input type="text" name="district_id"  class="form-control" value="{{ $district_id }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Zip</label>
                                            <div class="col-md-4">
                                                <input type="text" name="zip"  class="form-control" value="{{ $zip }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Zone</label>
                                            <div class="col-md-4">
                                                <input type="text" name="zone"  class="form-control" value="{{ $zone }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Contact Name</label>
                                            <div class="col-md-4">
                                                <input type="text" name="contact"  class="form-control" value="{{ $contact }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Phone</label>
                                            <div class="col-md-4">
                                                <input type="text" name="phone"  class="form-control" value="{{ $phone }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Mobile</label>
                                            <div class="col-md-4">
                                                <input type="text" name="mobile"  class="form-control" value="{{ $mobile }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Fax</label>
                                            <div class="col-md-4">
                                                <input type="text" name="fax"  class="form-control" value="{{ $fax }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Email</label>
                                            <div class="col-md-4">
                                                <input type="text" name="email"  class="form-control" value="{{ $email }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <script src="{{URL::asset('js/elfinder-upload.js')}}"></script>
@endsection