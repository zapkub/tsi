<?php
use App\Http\Controllers\Backend\SurveyorController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new SurveyorController(); // New Object Controller
$mainFn = new MainFunction(); // New Object Main Function

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$path = $objCon->path;

$orderBy = Input::get('orderBy');
$sortBy = Input::get('sortBy');
$search = Input::get('search');
$region_id = input::get('region_id');
$province_id = input::get('province_id');
$district_id = input::get('district_id');

$region = App\Model\Region::whereNull('deleted_at')->get();
$province = App\Model\Province::whereNull('deleted_at')->get();
$district = App\Model\District::whereNull('deleted_at')->get();

$a_otherParam = Input::except(['orderBy','sortBy']);
$strParam = $mainFn->parameter($a_otherParam);

$admin_id = auth()->guard('admin')->user()->admin_id;
if(!empty($admin_id)){
    // echo $mainFn->chk_permission($admin_id,'service');
}else{
    return Redirect::to('_admin/logout');
}

?>
@extends('admin')
@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>{{ $titlePage }}</h1>
                </div>
            </div>
        </div>

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->

                        <div class="portlet light">

                            <form action{{URL::to($path)}} class="form-horizontal" method="GET" >
                                <div class="form-group">
                                    <label class="control-label col-md-1">Search</label>
                                    <div class="col-md-3">
                                        <input class="form-control" type="text" name="search" value="{{ $search }}" >
                                        <span class="help-block">Search by name</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-1"></label>

                                    <div class="col-md-3">
                                        <select name="region_id" class="form-control filter-region" data-section="address"></select>
                                        <span class="help-block">* ภาค</span>
                                    </div>

                                    <div class="col-md-3">
                                        <select name="province_id" class="form-control filter-province" data-section="address"></select>
                                        <span class="help-block">* จังหวัด</span>
                                    </div>

                                    <div class="col-md-3">
                                        <select name="district_id" class="form-control filter-district" data-section="address"></select>
                                        <span class="help-block">* อำเภอ</span>
                                    </div>
                                </div>



                                <div class="form-group">
                                    <div class="col-md-offset-1 col-md-1 ">
                                        <button class="btn blue btn-sm" type="submit" ><i class="fa fa-search"></i> Search</button>
                                    </div>
                                    <div class="col-md-3 ">
                                        <button class="btn red btn-sm" type="reset" ><i class="fa fa-refresh"></i> Reset</button>
                                    </div>
                                </div>
                            </form>
                            <hr>

                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-database font-green-sharp"></i>
                                    <span class="caption-subject font-green-sharp bold">Found {{ $countData }} Record(s).</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('ID',$pkField,$orderBy,$sortBy,$strParam) !!} </th>
                                            <th>{!! $mainFn->sorting('Company Name','company_name',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th>{!! $mainFn->sorting('Branch','company_branch',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th>{!! $mainFn->sorting('Phone / Mobile / Fax','phone',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th>{!! $mainFn->sorting('Region','region_id',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th>{!! $mainFn->sorting('Province','province_id',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th>{!! $mainFn->sorting('District','district_id',$orderBy,$sortBy,$strParam) !!}</th>
                                            @if($permission['c'] == '1' || $permission['u'] == '1' || $permission['d'] == '1')
                                                <th class="col-md-1 text-center">
                                                    @if($permission['c'] == '1')
                                                        <a href="{{ URL::to($path.'/create') }}" class="btn btn-circle blue btn-xs"><i class="fa fa-plus"></i> Add</a>
                                                    @endif
                                                </th>
                                            @endif
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($countData > 0)
                                            @foreach($data as $field)
                                                <tr>
                                                    <td class="text-center">{{ $field->$pkField }}</td>
                                                    <td>{{ $field->company_title }} {{ $field->company_name }}</td>
                                                    <td>{{ $field->company_branch }}</td>
                                                    <td>Phone : {{ $field->phone }}<br>Mobile : {{ $field->mobile }}<br>Fax : {{ $field->fax }}</td>
                                                    <td>
                                                        @if(!empty($field->region_id))
                                                            {{ $field->Region->name_th }}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if(!empty($field->province_id))
                                                            {{ $field->Province->province_name_th }}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if(!empty($field->district_id))
                                                            {{ $field->District->name }}
                                                        @endif
                                                    </td>
                                                    @if($permission['c'] == '1' || $permission['u'] == '1' || $permission['d'] == '1')
                                                        <td class="text-center">
                                                            @if($permission['u'] == '1')
                                                                <a href="{{ URL::to($path.'/'.$field->$pkField.'/edit?'.$strParam) }}" class="btn btn-xs btn-circle green"><i class="fa fa-edit"></i></a>
                                                            @endif
                                                            @if($permission['d'] == '1')
                                                                <form action="{{URL::to($path.'/'.$field->$pkField)}}" method="post" class="frm-delete">
                                                                    <input name="_method" type="hidden" value="delete">
                                                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                                    <button type="submit" class="btn btn-xs btn-circle red" ><i class="fa  fa-trash-o"></i></button>
                                                                </form>
                                                            @endif
                                                        </td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr><td colspan='8' class='text-center'>No Result.</td></tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                {!! $data->appends(Input::except('page'))->render() !!}
                            </div>
                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
@endsection

@section('js')
    {{ Html::script('js/filter-address.js') }}
    <script>
        $(document).ready(function() {
            filter_address('region','region','null','{{ $data->region_id or '' }}','address');
            filter_address('region','province','{{ $data->region_id or '' }}','{{ $data->province_id or '' }}','address');
            filter_address('province','district','{{ $data->province_id or '' }}','{{ $data->district_id or '' }}','address');
            filter_address('district','zipcode','{{ $data->district_id or '' }}','{{ $data->zipcode or '' }}','address');
        });
    </script>

@endsection