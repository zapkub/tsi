<?php
use App\Http\Controllers\Backend\SurveyorController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new SurveyorController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$title = App\Model\Title::whereNull('deleted_at')->get();
$brand = App\Model\Brand::whereNull('deleted_at')->get();

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);

$admin_id = auth()->guard('admin')->user()->admin_id;
if(!empty($admin_id)){
    // echo $mainFn->chk_permission($admin_id,'service');
}else{
    return Redirect::to('_admin/logout');
}
?>
@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                {!! Form::open(array('url'=>$url_to , 'method' => $method , 'id' => 'form' , 'class' => 'form-horizontal','enctype'=>"multipart/form-data" )) !!}
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Brand</label>
                                            <div class="col-md-4">
                                                <select class="form-control " data-placeholder="Select..." id="brand_id" name="brand_id" data-type="brand_id">
                                                    <option value="">กรุณาเลือกหมวดหมู่</option>
                                                    @foreach($brand as $cate)
                                                        <option value="{{$cate->brand_id}}" <?php if($brand_id==$cate->brand_id)echo "selected"; ?>  >{{$cate->name_th}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Company Title</label>
                                            <div class="col-md-4">
                                                <select class="form-control " data-placeholder="Select..." id="company_title" name="company_title" data-type="company_title">
                                                    <option value="">กรุณาเลือกหมวดหมู่</option>
                                                    @foreach($title as $cate)
                                                        <option value="{{$cate->name_th}}" <?php if($company_title==$cate->name_th )echo "selected"; ?>  >{{$cate->name_th}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Name</label>
                                            <div class="col-md-4">
                                                <input type="text" name="company_name"  class="form-control" value="{{ $company_name }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Company Branch</label>
                                            <div class="col-md-4">
                                                <input type="text" name="company_branch"  class="form-control" value="{{ $company_branch }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Address 1</label>
                                            <div class="col-md-9">
                                                <textarea name="address1" id="address1" class="form-control">{{ $address1 }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Address 2</label>
                                            <div class="col-md-9">
                                                <textarea name="address2" id="address2" class="form-control">{{ $address2 }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">ภาค</label>
                                        <div class="col-md-4">
                                            <select name="region_id" class="form-control filter-region" data-section="address"></select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">จังหวัด</label>
                                        <div class="col-md-4">
                                            <select name="province_id" class="form-control filter-province" data-section="address"></select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">อำเภอ</label>
                                        <div class="col-md-4">
                                            <select name="district_id" class="form-control filter-district" data-section="address"></select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">รหัสไปรษณีย์</label>
                                        <div class="col-md-4">
                                            <input type="text" name="zipcode" value="" class="form-control filter-zipcode" data-section="address">
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Contact Name</label>
                                            <div class="col-md-4">
                                                <input type="text" name="contact_name"  class="form-control" value="{{ $contact_name }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Phone</label>
                                            <div class="col-md-4">
                                                <input type="text" name="phone"  class="form-control" value="{{ $phone }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Mobile</label>
                                            <div class="col-md-4">
                                                <input type="text" name="mobile"  class="form-control" value="{{ $mobile }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Fax</label>
                                            <div class="col-md-4">
                                                <input type="text" name="fax"  class="form-control" value="{{ $fax }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Grade</label>
                                            <div class="col-md-4">
                                                <input type="text" name="grade"  class="form-control" value="{{ $grade }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Share Location</label>
                                            <div class="col-md-4">
                                                <input type="text" name="share_location"  class="form-control" value="{{ $share_location }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Survey Type</label>
                                            <div class="col-md-4">
                                                <input type="text" name="survey_type"  class="form-control" value="{{ $survey_type }}"/>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="control-label col-md-3">Date</label>
                                        <div class="col-md-4">
                                            <div class="input-group input-large date-picker input-daterange" data-date="<?php echo date("Y-m-d");?>" data-date-format="yyyy-mm-dd">
                                                <input type="text" class="form-control" name="start_date" value="{{ $start_date }}">
												<span class="input-group-addon">
												to </span>
                                                <input type="text" class="form-control" name="end_date" value="{{ $end_date }}">
                                            </div>
                                            <!-- /input-group -->
											<span class="help-block">
											Select date range </span>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Status</label>
                                            <div class="col-md-4">
                                                <input type="text" name="status"  class="form-control" value="{{ $status }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Notice</label>
                                            <div class="col-md-4">
                                                <textarea name="notice" id="notice" class="form-control">{{ $notice }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Map Code</label>
                                            <div class="col-md-4">
                                                <input type="text" name="map_code"  class="form-control" value="{{ $map_code }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                {!! Form::close() !!}

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>

@endsection
@section('js')
    <script src="{{URL::asset('js/elfinder-upload.js')}}"></script>
    <script src="{{URL::asset('assets/admin/scripts/validate/surveyor.js')}}"></script>
    {{ Html::script('js/filter-address.js') }}
    <script>
        $(document).ready(function() {
            filter_address('region','region','null','{{ $data->region_id or '' }}','address');
            filter_address('region','province','{{ $data->region_id or '' }}','{{ $data->province_id or '' }}','address');
            filter_address('province','district','{{ $data->province_id or '' }}','{{ $data->district_id or '' }}','address');
            filter_address('district','zipcode','{{ $data->district_id or '' }}','{{ $data->zipcode or '' }}','address');
        });
    </script>

@endsection