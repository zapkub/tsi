<?php
use App\Http\Controllers\Backend\GarageController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;
use App\Model\District;
use App\Model\Region;
use App\Model\Province;
use App\Model\Amphur;

$objCon = new GarageController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$title = App\Model\Title::whereNull('deleted_at')->get();
$gtype = App\Model\Gtype::whereNull('deleted_at')->get();
$garage_type = App\Model\GarageType::whereNull('deleted_at')->get();
$garage_status = App\Model\GarageStatus::whereNull('deleted_at')->get();
$brand = App\Model\Brand::whereNull('deleted_at')->get();

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);

$admin_id = auth()->guard('admin')->user()->admin_id;
if(!empty($admin_id)){
    // echo $mainFn->chk_permission($admin_id,'service');
}else{
    return Redirect::to('_admin/logout');
}
?>
@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                {!! Form::open(array('url'=>$url_to , 'method' => $method , 'id' => 'form' , 'class' => 'form-horizontal','enctype'=>"multipart/form-data" )) !!}
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Status</label>
                                            <div class="col-md-4">
                                                <select class="form-control " data-placeholder="Select..." id="garage_status_id" name="garage_status_id" data-type="garage_status_id	">
                                                    @foreach($garage_status as $cate)
                                                        <option value="{{$cate->garage_status_id}}" <?php if($garage_status_id==$cate->garage_status_id	 )echo "selected"; ?>  >{{$cate->name_th}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Garage Code</label>
                                            <div class="col-md-4">
                                                <input type="text" name="garage_code"  class="form-control" value="{{ $garage_code }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Title</label>
                                            <div class="col-md-4">
                                                <select class="form-control " data-placeholder="Select..." id="title_id" name="title_id" data-type="title_id">
                                                    {{--<option value="">กรุณาเลือกหมวดหมู่</option>--}}
                                                    @foreach($title as $cate)
                                                        <option value="{{$cate->title_id}}" <?php if($title_id==$cate->title_id )echo "selected"; ?>  >{{$cate->name_th}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Name</label>
                                            <div class="col-md-3">
                                                <input type="text" name="garage_name_th"  class="form-control" value="{{ $garage_name_th }}"/>
                                            </div>
                                            <div class="col-md-1">
                                                ( TH )
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" name="garage_name_en"  class="form-control" value="{{ $garage_name_en }}"/>
                                            </div>
                                            <div class="col-md-1">
                                                ( EN )
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Repair Type</label>
                                            <div class="col-md-4">
                                                <select class="form-control " data-placeholder="Select..." id="gtype_id" name="gtype_id" data-type="gtype_id">
                                                    {{--<option value="">กรุณาเลือกการซ่อม</option>--}}
                                                    @foreach($gtype as $cate)
                                                        <option value="{{$cate->gtype_id}}" <?php if($gtype_id==$cate->gtype_id) echo "selected"; ?>  >{{$cate->name_th}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Garage Type</label>
                                            <div class="col-md-4">
                                                <select class="form-control " data-placeholder="Select..." id="garage_type_id" name="garage_type_id" data-type="garage_type_id">
                                                    {{--<option value="">กรุณาเลือกประเภทอู่</option>--}}
                                                    @foreach($garage_type as $cate)
                                                        <option value="{{$cate->garage_type_id}}" <?php if($garage_type_id==$cate->garage_type_id) echo "selected"; ?>  >{{$cate->name_th}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Branch</label>
                                            <div class="col-md-3">
                                                <input type="text" name="garage_branch_th"  class="form-control" value="{{ $garage_branch_th }}"/>
                                            </div>
                                            <div class="col-md-1">
                                                ( TH )
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" name="garage_branch_en"  class="form-control" value="{{ $garage_branch_en }}"/>
                                            </div>
                                            <div class="col-md-1">
                                                ( EN )
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Address 1</label>
                                            <div class="col-md-4">
                                                <textarea name="address1" id="address1" class="form-control">{{ $address1 }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Address 2</label>
                                            <div class="col-md-4">
                                                <textarea name="address2" id="address2" class="form-control">{{ $address2 }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">ภาค</label>
                                        <div class="col-md-4">
                                            <select name="region_id" class="form-control filter-region" data-section="address"></select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">จังหวัด</label>
                                        <div class="col-md-4">
                                            <select name="province_id" class="form-control filter-province" data-section="address"></select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">อำเภอ</label>
                                        <div class="col-md-4">
                                            <select name="district_id" class="form-control filter-district" data-section="address"></select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">รหัสไปรษณีย์</label>
                                        <div class="col-md-4">
                                            <input type="text" name="zipcode" value="" class="form-control filter-zipcode" data-section="address">
                                        </div>
                                    </div>
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Warantee Color</label>
                                            <div class="col-md-4">
                                                <input type="checkbox" name="warantee_color"  class="form-control" value="1"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Warantee Fix</label>
                                            <div class="col-md-4">
                                                <input type="checkbox" name="warantee_fix"  class="form-control" value="1"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Latitude</label>
                                            <div class="col-md-4">
                                                <input type="text" name="latitude"  class="form-control" value="{{ $latitude }}"/>
                                            </div>
                                            <label class="control-label col-md-1">Longitude</label>
                                            <div class="col-md-4">
                                                <input type="text" name="longitude"  class="form-control" value="{{ $longitude }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Garage Grade</label>
                                            <div class="col-md-4">
                                                <input type="text" name="garage_grade"  class="form-control" value="{{ $garage_grade }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Discount Wages</label>
                                            <div class="col-md-4">
                                                <input type="text" name="discount_wages"  class="form-control" value="{{ $discount_wages }}"/>
                                            </div>
                                            <label class="control-label col-md-1">Discount Part</label>
                                            <div class="col-md-4">
                                                <input type="text" name="discount_part"  class="form-control" value="{{ $discount_part }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Brand</label>
                                            <div class="col-md-4">
                                                <select class="form-control " data-placeholder="Select..." id="brand_id" name="brand_id" data-type="brand_id">
                                                    {{--<option value="">กรุณาเลือกแบนด์</option>--}}
                                                    @foreach($brand as $cate)
                                                        <option value="{{$cate->brand_id}}" <?php if($brand_id==$cate->brand_id	 )echo "selected"; ?>  >{{$cate->name_th}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Date</label>
                                        <div class="col-md-4">
                                            <div class="input-group input-large date-picker input-daterange" data-date="<?php echo date("Y-m-d");?>" data-date-format="dd-mm-yyyy">
                                                <input type="text" class="form-control" name="start_date" value="{{ $start_date }}">
												<span class="input-group-addon">
												to </span>
                                                <input type="text" class="form-control" name="end_date" value="{{ $end_date }}">
                                            </div>
                                            <!-- /input-group -->
											<span class="help-block">
											Select date range </span>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Map Code</label>
                                            <div class="col-md-4">
                                                <input type="text" name="map_code_insure"  class="form-control" value="{{ $map_code_insure }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Service Type</label>
                                            <div class="col-md-4">
                                                <input type="text" name="service_type"  class="form-control" value="{{ $service_type }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Facility</label>
                                            <div class="col-md-4">
                                                <input type="text" name="facility"  class="form-control" value="{{ $facility }}"/>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Contact Name</label>
                                            <div class="col-md-4">
                                                <input type="text" name="contact_name"  class="form-control" value="{{ $contact_name }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Phone</label>
                                            <div class="col-md-4">
                                                <input type="text" name="phone"  class="form-control" value="{{ $phone }}"/>
                                            </div>

                                            <label class="control-label col-md-1">Mobile</label>
                                            <div class="col-md-4">
                                                <input type="text" name="mobile"  class="form-control" value="{{ $mobile }}"/>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Fax</label>
                                            <div class="col-md-4">
                                                <input type="text" name="fax"  class="form-control" value="{{ $fax }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Email</label>
                                            <div class="col-md-4">
                                                <input type="text" name="email"  class="form-control" value="{{ $email }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Notice</label>
                                            <div class="col-md-4">
                                                <input type="text" name="notice"  class="form-control" value="{{ $notice }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    {{--<div class="form-body">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label class="control-label col-md-3">Photo</label>--}}
                                            {{--<div class="col-md-4">--}}
                                                {{--<input type="text" name="photo"  class="form-control" value="{{ $photo }}"/>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                {!! Form::close() !!}

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>

@endsection
@section('js')
    <script src="{{URL::asset('js/elfinder-upload.js')}}"></script>
    <script src="{{URL::asset('assets/admin/scripts/validate/garage.js')}}"></script>
    {{ Html::script('js/filter-address.js') }}
    <script>
        $(document).ready(function() {
            filter_address('region','region','null','{{ $data->region_id or '' }}','address');
            filter_address('region','province','{{ $data->region_id or '' }}','{{ $data->province_id or '' }}','address');
            filter_address('province','district','{{ $data->province_id or '' }}','{{ $data->district_id or '' }}','address');
            filter_address('district','zipcode','{{ $data->district_id or '' }}','{{ $data->zipcode or '' }}','address');
        });
    </script>

@endsection