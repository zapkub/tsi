<?php
use App\Http\Controllers\Backend\CarInsuranceBranchSController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new CarInsuranceBranchSController(); // New Object Controller
$mainFn = new MainFunction(); // New Object Main Function

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$tb_name = $objCon->tbName;
$path = $objCon->path;

$orderBy = Input::get('orderBy');
$sortBy = Input::get('sortBy');
$search = Input::get('search');

$a_otherParam = Input::except(['orderBy','sortBy']);
$strParam = $mainFn->parameter($a_otherParam);

$admin_id = auth()->guard('admin')->user()->admin_id;
$db = DB::table('admin')->select('firstname','lastname')->where('admin_id',$admin_id)->first();
if(!empty($admin_id)){
    echo $mainFn->chk_permission($admin_id,'car_insurance');
}else{
    return Redirect::to('_admin/logout');
}

?>
@extends('admin')
@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h3><a href="{{ URL::to('_admin/car_insurance_branch') }}">ใบคำขอออนไลน์</a> >> {{ $titlePage }} :: {{ $db->firstname }} {{ $db->lastname }}</h3>
                </div>
            </div>
        </div>

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->

                        <div class="portlet light">

                            <form action{{URL::to($path)}} class="form-horizontal" method="GET" >
                                <div class="form-group">
                                    <label class="control-label col-md-1"></label>
                                    <div class="col-md-4">
                                        <div class="input-group input-large date-picker input-daterange" data-date="<?php echo date("Y-m-d");?>" data-date-format="yyyy-mm-dd">
                                            <input type="text" class="form-control" name="from_date" value="">
												<span class="input-group-addon">
												to </span>
                                            <input type="text" class="form-control" name="to_date" value="">
                                        </div>
                                        <span class="help-block">
											Select date range </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-1 col-md-3 ">
                                        <button class="btn blue btn-sm" type="submit" ><i class="fa fa-search"></i> Search</button>
                                    </div>
                                </div>
                            </form>
                            <hr>

                            <div class="portlet-body">
                                <div class="table-responsive">

                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th class="col-md-12 text-center" colspan="10"><span class="caption-subject font-green-sharp bold">รายงานแสดงใบคำขอออนไลน์ สาขา วันที่บันทึกแจ้งงาน @if(!empty(input::get('from_date') or input::get('to_date'))) {{ $mainFn->format_date_th(input::get('from_date'),2) }} ถึง {{ $mainFn->format_date_th(input::get('to_date'),2) }} @endif</span></th>
                                        </tr>
                                        <tr>
                                            <th class="col-md-2 text-center">{!! $mainFn->sorting('เลขที่ใบคำร้อง','no',$orderBy,$sortBy,$strParam) !!} </th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('ทะเบียนรถ','registration',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('เบี้ยสุทธิ','net',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('เบี้ยรวมภาษี','net_tax',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('เบี้ยพ.ร.บ.','net_act',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center" >{!! $mainFn->sorting('วันที่เริ่มคุ้มครอง','from_date',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center" >{!! $mainFn->sorting('วันที่สิ้นสุดคุ้มครอง','to_date',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center" >{!! $mainFn->sorting('ตัวแทน','agent',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center" >{!! $mainFn->sorting('สาขา','name_th',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center" >{!! $mainFn->sorting('ผู้ยืนยัน','staff_accept',$orderBy,$sortBy,$strParam) !!}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $total_net_e = 0;
                                        $total_net_tax_e = 0;
                                        $total_net_act_e = 0;
                                        ?>
                                        @if($countData > 0)
                                            @foreach($data as $field)
                                                <tr>
                                                    <td>{{ $field->no }}</td>
                                                    <td>{{ $field->registration }}</td>
                                                    <td class="text-center">{{ number_format($field->net) }}</td>
                                                    <td class="text-center">{{ number_format($field->net_tax) }}</td>
                                                    <td class="text-center">{{ number_format($field->net_act) }}</td>
                                                    <td>{{ $mainFn->format_date_th($field->from_date,2) }}</td>
                                                    <td>{{ $mainFn->format_date_th($field->to_date,2) }}</td>
                                                    <td>{{ $field->agent }}</td>
                                                    <td>{{ $field->name_th }}</td>
                                                    <td>{{ $field->staff_accept }}</td>
                                                </tr>
                                                <?php
                                                $total_net_e += $field->net;
                                                $total_net_tax_e += $field->net_tax;
                                                $total_net_act_e += $field->net_act;
                                                ?>
                                            @endforeach
                                            <tr colspan="10">
                                                <td class="text-right" colspan="10">
                                                    <strong>จำนวนใบคำขอรวม</strong> {{ $countData }}<br>
                                                    <strong>เบี้ยสุทธิรวมทั้งสิ้น</strong> {{ number_format($total_net_e) }}<br>
                                                    <strong>เบี้ยรวมภาษีอากรรวมทั้งสิ้น</strong> {{ number_format($total_net_tax_e) }}<br>
                                                    <strong>เบี้ย พ.ร.บ. รวมทั้งสิ้น</strong> {{ number_format($total_net_act_e) }}
                                                </td>
                                            </tr>
                                        @else
                                            <tr><td colspan='9' class='text-center'>No Result.</td></tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                {!! $data->appends(Input::except('page'))->render() !!}
                            </div>

                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
@endsection