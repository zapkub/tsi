<?php

$filename  ="excel_report.xls";
header("Content-Type: application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=report_stock.xls");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);

$mainFn = new MainFunction(); // New Object Main Function
$flag_ar = array('1' => 'ตัวแทน'  , '2' => 'สาขา');
?>
<html >
<head>
    <meta charset="UTF-8">
</head>


<body>
<TABLE BORDER="1" width="100%">
    <tr>
        <th><div align="left">ID</div></th>
        <th><div align="left">ใบคำขอเลขที่</div></th>
        <th><div align="center">ผู้ขอประกันภัย</div></th>
        <th><div align="center">ทะเบียนรถ</div></th>
        @if($status==0)
            <th><div align="center">วันที่ขอ</div></th>
        @endif
        @if($status==1)
            <th><div align="center">วันที่ยืนยัน</div></th>
        @endif

        <th><div align="center">รหัสตัวแทน</div></th>
        <th><div align="center">ประเภท</div></th>
    </tr>
    @if($countData > 0)
        @foreach($data as $field)
                <tr>
                    <td><div align="center">{{$field->car_insurance_id}}</div></td>
                    <td>{{$field->no }}</td>
                    <td>{{$field->firstname }} {{$field->lastname }}</td>
                    <td>{{$field->registration }}</td>
                    <td class="text-center">{{ $mainFn->format_date_en($field->created_at,4) }}</td>
                    <td class="text-center">{{ $field->code_agent }}</td>
                    <td class="text-center">{{ $flag_ar[$field->flag] }}</td>
                </tr>

        @endforeach
    @else
        <tr><td colspan='7' class='text-center'>No Result.</td></tr>
    @endif
</TABLE>
</body>
</html>