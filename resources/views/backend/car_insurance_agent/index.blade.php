<?php
use App\Http\Controllers\Backend\CarInsuranceAgentController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new CarInsuranceAgentController(); // New Object Controller
$mainFn = new MainFunction(); // New Object Main Function

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$tb_name = $objCon->tbName;
$path = $objCon->path;

$orderBy = Input::get('orderBy');
$sortBy = Input::get('sortBy');
$search = Input::get('search');

$a_otherParam = Input::except(['orderBy','sortBy']);
$strParam = $mainFn->parameter($a_otherParam);

$admin_id = auth()->guard('admin')->user()->admin_id;
$db = DB::table('admin')->select('firstname','lastname')->where('admin_id',$admin_id)->first();
$flag_ar = array('1' => 'ตัวแทน'  , '2' => 'สาขา');
$status = input::get('status');
$flag = input::get('flag');


?>
@extends('admin')
@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1><a href="{{ URL::to('_admin/car_insurance_main') }}">{{ $titlePage }}</a> >> {{ $db->firstname }} {{ $db->lastname }}</h1>
                </div>
            </div>
        </div>

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->

                        <div class="portlet light">

                            <form action{{URL::to($path)}} class="form-horizontal" method="GET" >
                                <div class="form-group">
                                    <label class="control-label col-md-1">Search</label>
                                    <div class="col-md-3">
                                        <input class="form-control" type="text" name="search" value="{{ $search }}" >
                                        <span class="help-block">Search by ใบคำขอเลขที่ , รหัสตัวแทน</span>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="input-group input-large date-picker input-daterange" data-date="<?php echo date("Y-m-d");?>" data-date-format="yyyy-mm-dd">
                                            <input type="text" class="form-control" name="from_date" value="">
												<span class="input-group-addon">
												to </span>
                                            <input type="text" class="form-control" name="to_date" value="">
                                        </div>
                                        <span class="help-block">
											Select date range </span>
                                    </div>

                                    <div class="col-md-4">
                                        <select name="flag" class="form-control">
                                            <option value="">กรุณาเลือกประเภท</option>
                                            <option value="1" @if($flag==1) selected @endif>ตัวแทน</option>
                                            <option value="2" @if($flag==2) selected @endif>สาขา</option>
                                        </select>
                                    </div>
                                    <input type="hidden" class="form-control" name="status" value="{{ $status }}">
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-1 col-md-3 ">
                                        <button class="btn blue btn-sm" type="submit" ><i class="fa fa-search"></i> Search</button>
                                    </div>
                                </div>
                            </form>
                            <hr>

                            <div class="portlet-title">
                                <div class="caption">
                                    @if($status==0)
                                        <span class="caption-subject font-green-sharp bold">สถานะใบคำขอ <li class="fa fa-edit"></li> รอการยืนยัน จำนวน {{ $countData }} ฉบับ</span>
                                    @elseif($status==1)
                                        <span class="caption-subject font-green-sharp bold">สถานะใบคำขอ <li class="fa fa-check"></li> ผ่านการยืนยัน จำนวน {{ $countData }} ฉบับ</span>
                                    @elseif($status==2)
                                        <span class="caption-subject font-green-sharp bold">สถานะใบคำขอ <li class="fa fa-close"></li> ไม่อนุมัติ จำนวน {{ $countData }} ฉบับ</span>
                                    @endif
                                </div>
                                <div class="text-right"><a href="{{URL::to('_admin/car_insurance_agent?export=true'.$strParam)}}"><button class="btn blue btn-sm" type="submit" ><i class="fa fa-file"></i> Excel</button></a></div>
                            </div>

                            <div class="portlet-body">
                                <div class="table-responsive">

                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('ใบคำขอเลขที่','no',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-4 text-center">{!! $mainFn->sorting('ผู้ขอประกันภัย','firstname',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('ทะเบียนรถ','registration',$orderBy,$sortBy,$strParam) !!}</th>
                                            @if($status==1)
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('ผู้ยืนยัน','staff_accept',$orderBy,$sortBy,$strParam) !!}</th>
                                            @endif
                                            @if($status==0)
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('วันที่ขอ','created_at',$orderBy,$sortBy,$strParam) !!}</th>
                                            @endif
                                            @if($status==1)
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('วันที่ยืนยัน','staff_accept_date',$orderBy,$sortBy,$strParam) !!}</th>
                                            @endif
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('รหัสตัวแทน','code_agent',$orderBy,$sortBy,$strParam) !!}</th>
                                            @if($status==1)
                                                <th class="col-md-1 text-center">{!! $mainFn->sorting('สาเหตุ','reason_c',$orderBy,$sortBy,$strParam) !!}</th>
                                            @endif
                                            <th class="col-md-1 text-center"></th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @if($countData > 0)
                                            @foreach($data as $field)
                                                <tr>
                                                    <td class="text-center">{{ $field->no }}</td>
                                                    <td>{{ $field->firstname }} {{ $field->lastname }}</td>
                                                    <td class="text-center">{{ $field->registration }}</td>
                                                    @if($status==1)
                                                    <td class="text-center">{{ $field->staff_accept }}</td>
                                                    @endif
                                                    @if($status==0)
                                                    <td class="text-center">{{ $mainFn->format_date_en($field->created_at,4) }}</td>
                                                    @endif
                                                    @if($status==1)
                                                    <td class="text-center">{{ $mainFn->format_date_en($field->staff_accept_date,4) }}</td>
                                                    @endif

                                                    <td class="text-center">{{ $field->code_agent }}</td>
                                                    @if($status==1)
                                                    <td class="text-center">{{ $field->reason_c }}</td>
                                                    @endif
                                                    <td class="text-center">
                                                        @if($status==0)
                                                        <a href="{{ URL::to('_admin/car_insurance_agent/'.$field->$pkField.'/edit?status='.input::get('status')) }}" target="_blank"  class="btn btn-circle blue btn-xs"><i class="fa fa-search"></i></a>
                                                        @endif

                                                        @if($status==1 || $status==2)
                                                        <a href="{{ URL::to('_admin/car_insurance_agent/'.$field->$pkField.$strParam) }}" target="_blank"  class="btn btn-circle yellow btn-xs"><i class="fa fa-print"></i></a>
                                                            @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr><td colspan='9' class='text-center'>No Result.</td></tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                {!! $data->appends(Input::except('page'))->render() !!}
                            </div>

                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
@endsection