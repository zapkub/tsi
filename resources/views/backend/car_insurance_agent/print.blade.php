<?php
use App\Http\Controllers\Backend\CarInsuranceAgentController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new CarInsuranceAgentController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}

$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);
?>

<style>

    * {
        font-size: 11px;
        font-weight: bold;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }

    /*@page {*/
    /*size: A4;*/
    /*margin: 0;*/
    /*}*/

    /*@page {*/
    /*size: 6.3in 9.25in;*/
    /*!*margin: 27mm 16mm 27mm 16mm;*!*/
    /*margin: 0;*/
    /*}*/

    body {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
        background-color: #FAFAFA;
    }

    .page {
        width: 210mm;
        min-height: 297mm;
        padding: 5mm 20mm 20mm 20mm;
        margin: 10mm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }

    /*@media print {*/
        /*html, body {*/
            /*width: 200mm;*/
            /*height: 297mm;*/
        /*}*/
    /*}*/

    tr{
        line-height: 15px;
    }

    .head{
        border: solid 1px;
    }
    .f-red{
        color:#b94a48;
    }
</style>

@extends('print')
@section('content')
        <!-- BEGIN SAMPLE TABLE PORTLET-->
<div class="portlet light" class="page">

    <div class="portlet-body flip-scroll">

        <table>
            <thead class="flip-content">
            <tr>
                <td width="10%">
                    <img src="{{ URL::asset('images/logo_tsi.png') }}" alt="logo" class="logo-default" height="68;">
                </td>
                <td colspan="6" class="text-center">
                    <strong>บริษัท ไทยเศรษฐกิจประกันภัย จำกัด (มหาชน)<br>คำขอเอาประกันภัยรถยนต์</strong>
                </td>
            </tr>
            </thead>
        </table>


        <table>
            <thead class="flip-content">
            <tr>
                <td width="20%"><strong>รหัส TSK</strong></td>
                <td width="30%"></td>
                <td width="30%">เลขที่ใบคำขอ {{ $no }}</td>
            </tr>
            <tr>
                <td width="20%"></td>
                <td width="30%"><strong>การประกันภัยรถยนต์</strong></td>
                <td width="40%">{{ $mainFn->format_date_th(date('Y-m-d H:i:s'),3) }}</td>
            </tr>
            <tr>
                <td width="20%"></td>
                <td width="30%">
                    เล่มที่ {{ $book }}
                </td>
                <?php

                    if($data->flag==1){
                        $db = DB::table('agent')->select('firstname','lastname')->where('agent_id',$agent_id)->first();
                    }else if($data->flag==2){
                        $db = DB::table('admin')->select('firstname','lastname')->where('admin_id',$agent_id)->first();
                    }

                    ?>
                <td width="40%">ผู้แจ้ง {{ $db->firstname }} {{ $db->lastname }} ( {{ $mainFn->format_date_th($data->created_at,4) }} )</td>
            </tr>
            <tr>
                <td width="20%">ประเภทการซ่อม</td>
                <td width="30%">@if($type_repair==1) ซ่อมอู่ @elseif($type_repair==2) ซ่อมห้าง @elseif($type_repair==3) T- Care @endif</td>
                <td width="40%">@if(!empty($staff_accept)) ผู้ยืนยัน  {{ $staff_accept }} @endif @if($staff_accept_date != '0000-00-00 00:00:00')( {{ $mainFn->format_date_th($staff_accept_date,4) }} ) @endif</td>
            </tr>

        </table>

        <table>

            <tr class="head">
                <th colspan="7">&nbspผู้ขอเอาประกันภัย</th>
            </tr>

            <tr>
                <td width="30%">ชื่อ</td>
                <td width="40%">{{ $firstname }} {{ $lastname }}</td>
                <td width="30%" colspan="4">
                    @if($status_insurance==0) <i class="fa fa-check"></i> ประกันใหม่ @endif
                    @if($status_insurance==1) <i class="fa fa-check"></i> ต่ออายุกรมธรรม์ที่ {{ $extend }} @endif
                </td>
            </tr>
            <tr>
                <td width="30%">ที่อยู่</td>
                <td width="70%" colspan="6">{{ $address }}</td>
            </tr>
            <tr>
                <td width="30%">ประเภทการประกันภัย</td>
                <td width="70%" colspan="6">
                    @if($type_insurance==0) <i class="fa fa-check"></i> ประเภท1 @endif
                    @if($type_insurance==1) <i class="fa fa-check"></i> ประเภท2 @endif
                    @if($type_insurance==2) <i class="fa fa-check"></i> ประเภท3 @endif
                    @if($type_insurance==3) <i class="fa fa-check"></i> ประเภท2+ @endif
                    @if($type_insurance==4) <i class="fa fa-check"></i> ประเภท3+ @endif
                    @if($type_insurance==5) <i class="fa fa-check"></i> พ.ร.บ @endif
                    @if($type_insurance==6) <i class="fa fa-check"></i> อื่นๆ ระบุ {{ $type_insurance_other }} @endif
            </tr>
            <tr>
                <td width="30%">เลขที่บัตรประชาชน</td>
                <td width="70%" colspan="6">{{ $id_card }}</td>
            </tr>
            <tr>
                <td width="30%">อาชีพ</td>
                <td width="70%" colspan="6">
                    @if($occupation==0) <i class="fa fa-check"></i> ข้าราชการ / พนักงานรัฐวิสาหกิจ @endif
                    @if($occupation==1) <i class="fa fa-check"></i> รับจ้าง @endif
                    @if($occupation==2) <i class="fa fa-check"></i> ธุรกิจส่วนตัว @endif
                    @if($occupation==3) <i class="fa fa-check"></i> ไม่ได้ประกอบอาชีพ @endif
                    @if($occupation==4) <i class="fa fa-check"></i> อื่นๆ ระบุ {{ $occupation_other }} @endif
                </td>
            </tr>

        </table>

        <table>

            <tr class="head">
                <th colspan="7">&nbspประเภทการรับประกันภัยที่ต้องการ</th>
            </tr>

            <?php
            //            $year = date('Y');
            $birthday_1 = explode('-',$birthday);
            foreach ($birthday_1 as $key => $value){
                if($key==0){
                    $year = $value-543;
                }
                if($key==1){
                    $month = $value;
                }
                if($key==2){
                    $day = $value;
                }
            }

            $birthday_2 = explode('-',$birthday2);
            foreach ($birthday_2 as $key => $value){
                if($key==0){
                    $year2 = $value-543;
                }
                if($key==1){
                    $month2 = $value;
                }
                if($key==2){
                    $day2 = $value;
                }
            }
            ?>
            {{--@if($type_insurance==0)--}}
            <tr>
                <td width="20%" colspan="7">@if($driver_type==0)<i class="fa fa-check"></i>@endif ไม่ระบุผู้ขับขี่</td>
            </tr>
            {{--@endif--}}

            {{--@if($type_insurance==1)--}}
            <tr>
                <td width="40%" >@if($driver_type==1)<i class="fa fa-check"></i>@endif ระบุชื่อผู้ขับขี่ 1.{{ $driver_name }}</td>
                <td width="30%" > {{ $mainFn->format_date_th($year."-".$month."-".$day,1) }}
                    {{--วัน {{ $day }}--}}
                    {{--เดือน {{ $month }}--}}
                    {{--ปีเกิด {{ $year }}--}}
                </td>
                <td width="30%" colspan="5">อาชีพ {{ $driver_occupation }}</td>
            </tr>
            <tr>
                <td width="40%" >เลขที่ใบขับขี่ {{ $driver_license }}</td>
                <td width="30%" >จังหวัด {{ $driver_provice }}</td>
                <td width="30%" colspan="5">โทร {{ $driver_mobile }}</td>
            </tr>
            <tr>
                <td width="40%" >ระบุชื่อผู้ขับขี่ 2.{{ $driver_name2 }}</td>
                <td width="30%" > {{ $mainFn->format_date_th($year2."-".$month2."-".$day2,1) }}
                    {{--วัน {{ $day2 }}--}}
                    {{--เดือน {{ $month2 }}--}}
                    {{--ปีเกิด {{ $year2 }}--}}
                </td>
                <td width="30%" colspan="5">อาชีพ {{ $driver_occupation2 }}</td>
            </tr>
            <tr>
                <td width="40%" >เลขที่ใบขับขี่ {{ $driver_license2 }}</td>
                <td width="30%" >จังหวัด {{ $driver_provice2 }}</td>
                <td width="30%" colspan="5">โทร {{ $driver_mobile2 }}</td>
            </tr>
            {{--@endif--}}

            <tr>
                <td width="40%"><strong>เลขเครื่องหมายตาม พ.ร.บ.</strong> {{ $number_sign }}</td>
                <td width="30%"><strong>บริษัท</strong> {{ $company }}</td>
                <td width="30%"><strong>สิ้นสุดเมื่อ</strong> {{ $mainFn->format_date_th($end_date,2) }}</td>
            </tr>
            <tr>
                <td width="40%"><strong>การใช้รถยนต์</strong>        @if($use_car==0) <i class="fa fa-check"></i>
                    ส่วนบุคคล           @elseif($use_car==1) <i class="fa fa-check"></i>
                    เพื่อการพาณิชน์       @elseif($use_car==2) <i class="fa fa-check"></i>
                    เพื่อการพาณิชน์พิเศษ   @elseif($use_car==3) <i class="fa fa-check"></i>
                    รับจ้างสาธารณะ      @elseif($use_car==4) <i class="fa fa-check"></i>
                    อื่นๆ               @elseif($use_car==6) <i class="fa fa-check"></i> ระบุ {{ $use_car_other }} @endif</td>
                <td width="30%" colspan="6"><strong>ผู้รับประโยชน์</strong> {{ $beneficiary }}</td>
            </tr>



            </tbody>
        </table>


        <table>
            <thead class="flip-content">
            <tr class="head">
                <th colspan="7">&nbspรายการรถยนต์ที่เอาประกัน</th>
            </tr>
            <tr class="text-left">
                <td width="15%"><strong>ชื่อรถยนต์ / รุ่น</strong></td>
                <td width="15%"><strong>เลขทะเบียน</strong></td>
                <td width="25%"><strong>เลขตัวถัง</strong></td>
                <td width="5%"><strong>ปี รุ่น</strong></td>
                <td width="30%" class="text-center"><strong>จำนวนที่นั่ง / ขนาด / น้ำหนัก</strong></td>

            </tr>
            <tr>
                <td align="left">{{ $name_car }}</td>
                <td align="left">{{ $registration }}</td>
                <td align="left">{{ $chassis }}</td>
                <td align="left">{{ $roon }}</td>
                <td class="text-center">
                    <?php
                    if(empty($seating)){
                        $seating = "-";
                    }
                    if(empty($size)){
                        $size = "-";
                    }
                    if(empty($weight)){
                        $weight = "-";
                    }


                    ?>

                    <?php $s = $seating."/".$size."/".$weight ?>

                    {{ $s }}
                </td>

            </tr>
            <tr class="text-left">
                <td><strong>รหัสรถยนต์</strong></td>
                <td><strong>เลขเครื่องรถยนต์</strong></td>
                <td><strong>มูลค่าเต็มรวมตกแต่ง</strong></td>
                <td colspan="4"></td>
            </tr>
            <tr>
                <td align="left">{{ $code_car }}</td>
                <td align="left">{{ $engine }}</td>
                <td align="left">{{ $total_decorate }}</td>
                <td colspan="4"></td>
            </tr>

            </tbody>
        </table>

        <table>

            <tr class="head">
                <th colspan="7">&nbspรายการตกแต่งเปลี่ยนแปลงรถยนต์เพิ่มเติม (โปรดระบุรายละเอียด)</th>
            </tr>

            <tr>
                <td width="60%">1. {{ $decorate_car }}</td>
                <td width="10%">ราคา</td>
                <td width="10%">{{ number_format($price) }}</td>
                <td width="10%">บาท</td>
            </tr>
            <tr>
                <td width="60%">2. {{ $decorate_car2 }}</td>
                <td width="10%">ราคา</td>
                <td width="10%">{{ number_format($price2) }}</td>
                <td width="10%">บาท</td>
            </tr>
            <tr>
                <td width="60%">3. {{ $decorate_car3 }}</td>
                <td width="10%">ราคา</td>
                <td width="10%">{{ number_format($price3) }}</td>
                <td width="10%">บาท</td>
            </tr>
            <tr>
                <td>อุปกรณ์พิเศษ @if($equipment==0) <i class="fa fa-check"></i> @endif
                    ไม่มี @if($equipment==1) <i class="fa fa-check"></i>
                    มีระบุ @endif</td>
                @if($equipment==1) <td>{{ $equipment_other }}</td> @endif
                <td></td>
            </tr>
            </tbody>
        </table>

        <table>
            <tr class="head">
                <td width="30%" style="border-right:solid 1px;"><strong>&nbspความรับผิดต่อบุคคลภายนอก</strong></td>
                <td width="30%" style="border-right:solid 1px;"><strong>&nbspรถยนต์เสียหาย สูญหายไฟไหม้</strong></td>
                <td width="45%" style="border-right:solid 1px;" colspan="2"><strong>&nbspความคุ้มครองตามเอกสารแนบท้าย</strong></td>
            </tr>

            <tr>
                <td valign="top" width="35%" style="border:solid 1px;" align="center"><span style="margin-left: -50px;">1.ความเสียหายต่อชีวิต ร่างกาย หรือ อนามัย<br></span>
                    <span style="margin-left: -50px;">เฉพาะส่วนเกินวงเงินสูงสุดตาม พ.ร.บ. </span><br>{{ number_format($person_damage) }} บาท/คน <br>{{ number_format($person_damage2) }} บาท/ครั้ง<br>
                    2.ความเสียหายต่อทรัพย์สิน<br> {{ number_format($person_damage3) }} บาท/ครั้ง<br>
                    2.1ความเสียหายส่วนแรก <br>{{ number_format($person_damage4) }} บาท/ครั้ง
                </td>
                <td valign="top" width="30%" style="border:solid 1px;" align="center"><span style="margin-left: -80px;">1.ความเสียหายต่อรถยนต์</span> <br>{{ number_format($car_damage) }} บาท / ครั้ง<br>
                    1.1.ความเสียหายส่วนแรก <br>{{ number_format($car_damage2) }} บาท / ครั้ง<br>
                    2.รถยนต์สูญหายไฟไหม้ <br>{{ number_format($car_damage3) }} บาท<br>
                    <br>
                    <span style="font-size:36px;color:red;">ไม่รวม พ.ร.บ</span>
                </td>
                <td valign="top" width="45%" style="border:solid 1px;" colspan="2" align="left">1.อุบัติเหตุส่วนบุคคล<br>
                    &nbsp1.1.เสียชีวิต สูญเสียอวัยวะ ทุพพลภาพถาวร<br>
                    &nbsp&nbsp&nbspก.ผู้ขับขี่ 1 คน {{ number_format($document_ptt) }} บาท<br>
                    &nbsp&nbsp&nbspข.ผู้โดยสาร {{ number_format($document_ptt2) }} คน<br>
                    &nbsp1.2.ทุพพลภาพชั่วคราว<br>
                    &nbsp&nbsp&nbspก.ผู้ขับขี่ 1 คน {{ number_format($document_ptt3) }} บาท / สัปดาห์<br>
                    &nbsp&nbsp&nbspข.ผู้โดยสาร {{ number_format($document_ptt4) }} คน {{ number_format($document_ptt5) }} บาท / คน / สัปดาห์<br>
                    &nbsp2.ค่ารักษาพยาบาล <br>{{ number_format($document_ptt6) }} บาท / คน<br>
                    &nbsp3.การประกันตัวผู้ขับขี่ <br>{{ number_format($document_ptt7) }} บาท / ครั้ง
                </td>
            </tr>
            <tr>
                <td width="30%" style="border:solid 1px;">&nbspเบี้ยสุทธิ {{ number_format($net,2) }} บาท</td>
                <td width="30%" style="border:solid 1px;">&nbspเบี้ยรวมภาษีอากร {{ number_format($net_tax,2) }} บาท</td>
                <td colspan="2" width="45%" style="border:solid 1px;">&nbspเบี้ย พ.ร.บ {{ number_format($net_act,2) }} บาท</td>
            </tr>

        </table>

        <table>
            <tr>
                <td colspan="3">ข้าพเจ้าขอรับรองว่า คำบอกกล่าวตามรายการข้างบนเป็นความจริงและให้นับถือเป็นส่วนหนึ่งของสัญญาระหว่างข้าพเจ้ากับบริษัท</td>
            </tr>
            <tr>
                <td colspan="3">โดยข้าพเจ้ามีความประสงค์ให้กรมธรรม์มีผลบังคับใช้ตั้งแต่วันที่ {{ $mainFn->format_date_th($from_date,2) }} ถึงวันที่ {{ $mainFn->format_date_th($to_date,2) }}</td>
            </tr>
            <tr>
                <td width="10%">ชื่อผู้พิมพ์ </td>
                <td width="90%" colspan="2">...{{ $db->firstname }} {{ $db->lastname }}...</td>
            </tr>
            <tr>
                <td width="10%">ชื่อตัวแทน</td>
                <td width="50%">{{ $agent }}</td>
                <td width="40%">ลายมือชื่อผู้ขอเอาประกัน...........................................</td>
            </tr>
            <tr>
                <td width="10%"><span class="f-red">ชื่อรหัสตัวแทน*</span> </td>
                <td width="90%" colspan="2">{{ $code_agent }}</td>
            </tr>
            <tr>
                <td width="10%">หมายเหตุ</td>
                <td width="50%">{{ $note }}</td>
                <td width="40%">วันที่...................เดือน......................พ.ศ......................</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td class="f-red">เอกสารฉบับนี้เป็นเพียงการแจ้งขอเอาประกันภัยรถยนต์เท่านั้น<br>มิใช่เอกสารการแสดงการรับชำระเงินค่าเบี้ยประกันภัย</td>
            </tr>

            <tr>
                <td colspan="3" class="f-red">คำเตือนของกรมการประกันภัย กระทรวงพาณิชย์<br>
                    ให้ตอบคำถามข้างต้นตามเป็นจริงทุกข้อ มิฉะนั้นบริษัทอาจถือเป็นเหตุปฏิเสธความรับผิดชอบตามสัญญาประกันภัยได้<br>
                    ตามประมวลกฏหมายเพ่งและพาณิชน์มาตรา 865</td>
            </tr>

            <tr align="right">
                <td colspan="3">ลิขสิทธิ์ &copy; 2548-2559 บริษัท ไทยเศรษฐกิจประกันภัย จำกัด(มหาชน)</td>
            </tr>

            </tbody>
        </table>

    </div>
</div>

<!-- END SAMPLE TABLE PORTLET-->
<script>
    $(document).ready(function(){
        window.print();
    });
</script>

@endsection