<?php
use App\Http\Controllers\Backend\BlogController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new BlogController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);

?>
@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" id="form-character" class="form-horizontal">
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">


                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Title [TH]</label>
                                            <div class="col-md-4">
                                                <input type="text" name="title_th"  class="form-control" value="{{ $title_th }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Title [EN]</label>
                                            <div class="col-md-4">
                                                <input type="text" name="title_en"  class="form-control" value="{{ $title_th }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Author ID</label>
                                            <div class="col-md-4">
                                                <input type="text" name="author_id"  class="form-control" value="{{ $author_id }}"/>
                                            </div>
                                        </div>
                                    </div>

{{--                                    <div class="form-group">
                                        <label class="control-label col-md-3">Post Datetime</label>
                                        <div class="col-md-4">
                                            <input type='text' name="post_datetime" id="post_datetime" class="form-control" value="{{ $post_datetime }}" data-date-format="yyyy-mm-dd hh:ii" readonly>
                                        </div>
                                    </div>--}}




                                    {{--<div class="form-body">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label class="control-label col-md-3">Image Icon</label>--}}
                                            {{--<div class="col-md-4">--}}
                                                {{--<input type="text" name="img_icon"  class="form-control" value="{{ $img_icon }}"/>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{---- img icon--}}
                                    <div class="control-group ">
                                        <div class="controls">
                                            <div id="thumbnail" class="text-center">
                                                @if($img_name != '')
                                                    <img src="{{URL::asset('uploads/blog_img_name/'.$img_name) }}" id="img_path2" style="max-width:400px;">
                                                @else
                                                    <img src="" id="img_path2" style="max-width:400px;">
                                                @endif

                                                <input type="hidden" name="img_name" id="img_path" value="<?php echo  $img_name?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Image Name</label>
                                            <div class="col-md-4">
                                                @if($img_name != '')
                                                    <div id="remove_img" class="btn btn-danger" >Remove</div>
                                                    <div id="upload" class="btn blue" style="display:none;"><i class="icon-picture"></i> Select File</div>

                                                @elseif ($img_name == '')
                                                    <div id="upload" class="btn blue" ><i class="icon-picture"></i> Select File</div>
                                                    <div id="remove_img" class="btn btn-danger" style="display:none;" >Remove</div>

                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    {{--img icon hover--}}
                                    <div class="control-group ">
                                        <div class="controls">
                                            <div id="thumbnail1" class="text-center">
                                                @if($thumb_img_name != '')
                                                    <img src="{{URL::asset('uploads/blog_thumb/'.$thumb_img_name) }}" id="img_path3" style="max-width:400px;">
                                                @else
                                                    <img src="" id="img_path3" style="max-width:400px;">
                                                @endif

                                                <input type="hidden" name="thumb_img_name" id="img_path1" value="<?php echo  $thumb_img_name?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Thumb Image Name</label>
                                            <div class="col-md-4">
                                                @if($thumb_img_name != '')
                                                    <div id="remove_img1" class="btn btn-danger" >Remove</div>
                                                    <div id="upload1" class="btn blue" style="display:none;"><i class="icon-picture"></i> Select File</div>

                                                @elseif ($thumb_img_name == '')
                                                    <div id="upload1" class="btn blue" ><i class="icon-picture"></i> Select File</div>
                                                    <div id="remove_img1" class="btn btn-danger" style="display:none;" >Remove</div>

                                                @endif

                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Message [TH]</label>
                                            <div class="col-md-9">
                                                <textarea name="msg_th" id="msg_th" class="form-control ckeditor">{{ $msg_th }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Message [EN]</label>
                                            <div class="col-md-9">
                                                <textarea name="msg_en" id="msg_en" class="form-control ckeditor">{{ $msg_en }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <script src="{{URL::asset('js/elfinder-upload.js')}}"></script>

    <script>
        $(function() {
            $( "#post_datetime" ).datetimepicker();

        });
    </script>

@endsection
