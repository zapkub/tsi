<?php
use App\Http\Controllers\Backend\FormInsuranceAgentController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new FormInsuranceAgentController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}

$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);
?>
<style>
    * {
        font-weight: bold;
    }
    .border-table{
        border-bottom: solid 1px #000;
        border-top:solid 1px #000;
        margin-bottom: 20px;
    }
    .t-width{
        width:50px;
    }
    .head{
        background-color: #b4c4cd;
    }
    .bg-red{
        background-color:#bf6a40;
    }
    .f-red{
        color:#b94a48;
    }
</style>

@extends('print')
@section('content')

        <!-- BEGIN SAMPLE TABLE PORTLET-->
<div class="portlet light">

    <div class="portlet-body flip-scroll">

        <table class="table-condensed flip-content border-table">
            <thead class="flip-content">
            <tr class="head">
                <td colspan="4" class="text-center"><strong>บริษัท ไทยเศรษฐกิจประกันภัย จำกัด (มหาชน)<br>คำขอแก้ไขข้อความในตารางกรมธรรม</strong></td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><strong>การประกันรถยนต์</strong></td>
                <td colspan="3">วัน {{ $mainFn->format_date_th(date('Y-m-d H:i:s'),4) }}</td>
            </tr>
            <tr>
                <td></td>
                <td>เล่มที่ {{ $book }}</td>
                <td>เลขที่ {{ $no }}</td>
                <td>
                    <?php

                    if($flag==1){
                        $db = DB::table('agent')->select('firstname','lastname')->where('agent_id',$agent_id)->first();
                    }else if($flag==2){
                        $db = DB::table('admin')->select('firstname','lastname')->where('admin_id',$agent_id)->first();
                    }

                    ?>
                    ชื่อ {{ $db->firstname }} {{ $db->lastname }}
                </td>
            </tr>
            <tr>
                <td colspan="4"><strong>รหัส TSK</strong></td>
            </tr>

            </tbody>
        </table>

        <table class="table-condensed">
            <thead class="flip-content">
            <tr class="head">
                <td colspan="4" class="text-center"><strong>รายละเอียดคำร้องขอสลักหลังข้อความในตารางกรมธรรม์</strong></td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td width="20%">
                    @if($status_endorse==0)
                        ยกเลิก
                    @elseif($status_endorse==1)
                        สลักหลัง
                    @endif
                </td>
                <td width="30%"></td>
                <td width="20%">กรมธรรม์เลขที่</td>
                <td width="30%">{{ $request_no }}</td>
            </tr>
            <tr>
                <td width="20%">ชื่อผู้เอาประกัน</td>
                <td width="30%">{{ $firstname }} {{ $lastname }}</td>
                <td width="20%">วันที่คุ้มครอง</td>
                <td width="30%">{{ $mainFn->format_date_th($date_coverage,2) }}</td>
            </tr>
            <tr>
                <td width="20%">ชื่อรถยนต์</td>
                <td width="30%">{{ $car_name }}</td>
                <td width="20%">เลขทะเบียน</td>
                <td width="30%">{{ $license_plate }}</td>
            </tr>
            <tr>
                @if($status_endorse==0)
                    <td width="20%">สาเหตุการยกเลิก</td>
                    <td width="30%" colspan="3">{{ $reason }}</td>
                @elseif($status_endorse==1)
                    <td width="20%">รายละเอียดการเปลี่ยนแปลง</td>
                    <td width="30%" colspan="3">{{ $detail }}</td>
                @endif
            </tr>
            </tbody>
        </table>

        <table class="table-condensed flip-content border-table">
            <tr>
                <td colspan="4" width="20%">ลงชื่อผู้พิมพ์</td>
            </tr>
            <tr>
                <td width="20%">ชื่อตัวแทน</td>
                <td width="30%">{{ $name_agent }}</td>
                <td></td>
            </tr>
            <tr>
                <td width="20%">ชื่อรหัสตัวแทน</td>
                <td width="30%" colspan="3">{{ $code_agent }}</td>
            </tr>
            <tr>
                <td width="20%">หมายเหตุ</td>
                <td width="30%" colspan="3">{{ $note }}</td>
            </tr>
            </tbody>
        </table>

    </div>
</div>
<!-- END SAMPLE TABLE PORTLET-->

<script>
    $(document).ready(function(){
        window.print();
    });
</script>

@endsection