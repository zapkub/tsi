<?php
use App\Http\Controllers\Backend\AgentController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new AgentController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);

$admin_id = auth()->guard('admin')->user()->admin_id;
if(!empty($admin_id)){
    // echo $mainFn->chk_permission($admin_id,'manage_user');
}else{
    return Redirect::to('_admin/logout');
}

?>
@extends('admin')
@section('css')

@endsection
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                {!! Form::open(array('url'=>$url_to , 'method' => $method , 'id' => 'form' , 'class' => 'form-horizontal','enctype'=>"multipart/form-data" )) !!}

                                <input name="_method" type="hidden" value="{{$method}}">
                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">ประเภท</label>
                                        <div class="col-md-4">
                                            <select name="flag" class="form-control">
                                                <option value="1" @if($flag==1) selected @endif>ตัวแทน</option>
                                                <option value="2" @if($flag==2) selected @endif>สาขา</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Company Name</label>
                                        <div class="col-md-4">
                                            <input type="text" name="company_name"  class="form-control" value="{{ $company_name }}"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Agent No.</label>
                                        <div class="col-md-4">
                                            <input type="text" name="agent_no"  class="form-control" value="{{ $agent_no }}"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Firstname</label>
                                        <div class="col-md-4">
                                            <input type="text" name="firstname"  class="form-control" value="{{ $firstname }}"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Lastname</label>
                                        <div class="col-md-4">
                                            <input type="text" name="lastname"  class="form-control" value="{{ $lastname }}"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Email</label>
                                        <div class="col-md-4">
                                            <input type="text" name="email"  class="form-control" value="{{ $email }}"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Username</label>
                                        <div class="col-md-4">
                                            <input type="text" name="username"  class="form-control" value="{{ $username }}"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Password</label>
                                        <div class="col-md-4">
                                                <input type="password" name="new_password"  class="form-control" value=""/>
                                                <input type="hidden" name="password"  class="form-control" value="{{ $password }}"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <input type="hidden" name="strParam" value="{{$strParam}}">
                                            <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                            <button type="reset" class="btn default">Reset</button>
                                        </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
   
@endsection

@section('js')
    <script src="{{URL::asset('assets/admin/scripts/validate/agent.js')}}"></script>
@endsection