<?php
use App\Http\Controllers\Backend\CareerController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new CareerController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);

$admin_id = auth()->guard('admin')->user()->admin_id;
if(!empty($admin_id)){
    // echo $mainFn->chk_permission($admin_id,'career');
}else{
    return Redirect::to('_admin/logout');
}
?>
@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                {!! Form::open(array('url'=>$url_to , 'method' => $method , 'id' => 'form' , 'class' => 'form-horizontal','enctype'=>"multipart/form-data" )) !!}
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">สถานะ</label>
                                            <div class="col-md-4">
                                                <select class="form-control select2me" data-placeholder="Select..." name="status">
                                                    <option value="0" @if($status=='0') selected @endif>ปิด</option>
                                                    <option value="1" @if($status=='1') selected @endif>เปิด</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">ชื่อ</label>
                                            <div class="col-md-3">
                                                <input type="text" name="name_th"  class="form-control" value="{{ $name_th }}"/>
                                            </div>
                                            <div class="col-md-1">
                                                ( TH )
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" name="name_en"  class="form-control" value="{{ $name_en }}"/>
                                            </div>
                                            <div class="col-md-1">
                                                ( EN )
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">คุณสมบัติ ( TH )</label>
                                            <div class="col-md-9">
                                                <textarea name="property_th" id="property_th" class="form-control ckeditor">{{ $property_th }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">คุณสมบัติ ( EN )</label>
                                            <div class="col-md-9">
                                                <textarea name="property_en" id="property_en" class="form-control ckeditor">{{ $property_en }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">รายละเอียด ( TH )</label>
                                            <div class="col-md-9">
                                                <textarea name="detail_th" id="detail_th" class="form-control ckeditor">{{ $detail_th }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">รายละเอียด ( EN )</label>
                                            <div class="col-md-9">
                                                <textarea name="detail_en" id="detail_en" class="form-control ckeditor">{{ $detail_en }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">จำนวน</label>
                                            <div class="col-md-1">
                                                <input type="number" name="qty"  class="form-control" value="{{ $qty }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <input type="hidden" name="sequence"  class="form-control" value="{{ $sequence }}"/>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                {!! Form::close() !!}

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
@endsection

@section('js')
    <script src="{{URL::asset('assets/admin/scripts/validate/career.js')}}"></script>
@endsection