<?php
use App\Http\Controllers\Backend\CarInsuranceBranchController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new CarInsuranceBranchController(); // New Object Controller
$mainFn = new MainFunction(); // New Object Main Function

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$tb_name = $objCon->tbName;
$path = $objCon->path;

$orderBy = Input::get('orderBy');
$sortBy = Input::get('sortBy');
$search = Input::get('search');

$a_otherParam = Input::except(['orderBy','sortBy']);
$strParam = $mainFn->parameter($a_otherParam);

$admin_id = auth()->guard('admin')->user()->admin_id;
$db = DB::table('admin')->select('firstname','lastname')->where('admin_id',$admin_id)->first();
if(!empty($admin_id)){
    // echo $mainFn->chk_permission($admin_id,'car_insurance');
}else{
    return Redirect::to('_admin/logout');
}

?>
@extends('admin')
@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>{{ $titlePage }} :: {{ $db->firstname }} {{ $db->lastname }}</h1>
                </div>
            </div>
        </div>

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->

                        <div class="portlet light">

                            <form action{{URL::to($path)}} class="form-horizontal" method="GET" >
                                <div class="form-group">
                                    <label class="control-label col-md-1"></label>
                                    <div class="col-md-4">
                                        <div class="input-group input-large date-picker input-daterange" data-date="<?php echo date("Y-m-d");?>" data-date-format="yyyy-mm-dd">
                                            <input type="text" class="form-control" name="from_date" value="">
												<span class="input-group-addon">
												to </span>
                                            <input type="text" class="form-control" name="to_date" value="">
                                        </div>
                                        <span class="help-block">
											Select date range </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-1 col-md-3 ">
                                        <button class="btn blue btn-sm" type="submit" ><i class="fa fa-search"></i> Search</button>
                                    </div>
                                </div>
                            </form>
                            <hr>

                            <div class="portlet light">

                                <div class="col-md-3 text-right">
                                    ใบคำขอประกันรถยนต์
                                </div>
                                    <div class="col-md-3 text-left">
                                        <a href="{{ URL::to('_admin/car_insurance?status=0') }}" class="btn btn-info"><li class="fa fa-edit"></li> รอการยืนยัน <span class="btn btn-circle btn-xs">{{ $count_1 }}</span>
                                        </a>
                                    </div>
                                    <div class="col-md-3 text-left">
                                        <a href="{{ URL::to('_admin/car_insurance?status=1') }}" class="btn btn-success"><li class="fa fa-check"></li> ผ่านการยืนยัน <span class="btn btn-circle btn-xs">{{ $count_2 }}</span>
                                        </a>
                                    </div>
                                    <div class="col-md-3 text-left">
                                        <a href="{{ URL::to('_admin/car_insurance?status=2') }}" class="btn btn-danger"><li class="fa fa-close"></li> ไม่อนุมัติ <span class="btn btn-circle btn-xs">{{ $count_3 }}</span>
                                        </a>
                                    </div>
                            </div>

                            <div class="portlet light">

                                <div class="col-md-3 text-right">
                                    ใบสลักหลัง
                                </div>
                                <div class="col-md-3 text-left">
                                    <a href="{{ URL::to('_admin/form_insurance?status=0') }}" class="btn btn-info"><li class="fa fa-edit"></li> รอการยืนยัน <span class="btn btn-circle btn-xs">{{ $count_s_1 }}</span>
                                    </a>
                                </div>
                                <div class="col-md-3 text-left">
                                    <a href="{{ URL::to('_admin/form_insurance?status=1') }}" class="btn btn-success"><li class="fa fa-check"></li> ผ่านการยืนยัน <span class="btn btn-circle btn-xs">{{ $count_s_2 }}</span>
                                    </a>
                                </div>
                                <div class="col-md-3 text-left">
                                    <a href="{{ URL::to('_admin/form_insurance?status=2') }}" class="btn btn-danger"><li class="fa fa-close"></li> ไม่อนุมัติ <span class="btn btn-circle btn-xs">{{ $count_s_3 }}</span>
                                    </a>
                                </div>
                            </div>


                            <div class="portlet-body">
                                <div class="table-responsive">

                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th class="col-md-12 text-center" colspan="6"><span class="caption-subject font-green-sharp bold">รายงานแสดงใบคำขอออนไลน์ สาขา วันที่บันทึกแจ้งงาน @if(!empty(input::get('from_date') or input::get('to_date'))) {{ $mainFn->format_date_th(input::get('from_date'),2) }} ถึง {{ $mainFn->format_date_th(input::get('to_date'),2) }} @endif</span></th>
                                        </tr>
                                        <tr>
                                            <th class="col-md-2 text-center">{!! $mainFn->sorting('สาขา','name_th',$orderBy,$sortBy,$strParam) !!} </th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('จำนวนงานที่สั่ง สนญ.','no',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('เบี้ยสุทธิ','total_net',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('เบี้ยรวม','total_net_tax',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('เบี้ยพ.ร.บ.','total_net_act',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center" >{!! $mainFn->sorting('เบี้ยรวม+พ.ร.บ.','total',$orderBy,$sortBy,$strParam) !!}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                            $total_net_e = 0;
                                            $total_net_tax_e = 0;
                                            $total_net_act_e = 0;
                                            $total_e = 0;
                                        ?>

                                        @if($countData > 0)
                                            @foreach($data as $field)
                                                <tr>
                                                    <td><a href="{{ URL::to('_admin/car_insurance_branch_s?branch_id='.$field->branch_id) }}">{{ $field->code }} {{ $field->name_th }}</a></td>
                                                    <?php
                                                        $count = DB::table('car_insurance')->where('branch_id',$field->branch_id)->where('status',2)->count();
                                                    ?>
                                                    <td>{{ $count }} กรมธรรม์</td>
                                                    <td class="text-center">{{ number_format($field->total_net) }}</td>
                                                    <td class="text-center">{{ number_format($field->total_net_tax) }}</td>
                                                    <td class="text-center">{{ number_format($field->total_net_act) }}</td>
                                                    <td class="text-center">
                                                        <?php
                                                            $total = $field->total_net_tax+$field->total_net_act;
                                                            echo number_format($total);
                                                        ?>
                                                    </td>
                                                </tr>
                                                <?php
                                                    $total_net_e += $field->total_net;
                                                    $total_net_tax_e += $field->total_net_tax;
                                                    $total_net_act_e += $field->total_net_act;
                                                    $total_e += $total;
                                                ?>
                                            @endforeach
                                            <tr>
                                                <td class="text-center">รวมทั้งหมด</td>
                                                <td> กรมธรรม์</td>
                                                <td class="text-center">{{ number_format($total_net_e) }}</td>
                                                <td class="text-center">{{ number_format($total_net_tax_e) }}</td>
                                                <td class="text-center">{{ number_format($total_net_act_e) }}</td>
                                                <td class="text-center">{{ number_format($total_e) }}</td>
                                            </tr>
                                        @else
                                            <tr><td colspan='9' class='text-center'>No Result.</td></tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                {!! $data->appends(Input::except('page'))->render() !!}
                            </div>

                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
@endsection