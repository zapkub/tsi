<?php
use App\Http\Controllers\Backend\DocumentController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new DocumentController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);

$admin_id = auth()->guard('admin')->user()->admin_id;
if(!empty($admin_id)){
    echo $mainFn->chk_permission($admin_id,'about_company');
}else{
    return Redirect::to('_admin/logout');
}
?>
@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                {!! Form::open(array('url'=>$url_to , 'method' => $method , 'id' => 'form' , 'class' => 'form-horizontal','enctype'=>"multipart/form-data" )) !!}
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Name</label>
                                            <div class="col-md-3">
                                                <input type="text" name="document_name_th"  class="form-control" value="{{ $document_name_th }}"/>
                                            </div>
                                            <div class="col-md-1">
                                                ( TH )
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" name="document_name_en"  class="form-control" value="{{ $document_name_en }}"/>
                                            </div>
                                            <div class="col-md-1">
                                                ( EN )
                                            </div>
                                        </div>
                                    </div>

                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Date</label>
                                        <div class="col-md-3">
                                            <input type="text" name="document_date" class="date-picker form-control" value="{{ $document_date }}" data-date-format="yyyy-mm-dd">
                                        </div>
                                    </div>
                                </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">File</label>
                                            <div class="col-md-4">

                                                @if($file_name != '')
                                                    <a href="{{ '/uploads/document/'.$file_name }}" id="pdf_name" class="btn btn-xs btn-circle blue" target="_blank"><i class="fa fa-file"></i></a>
                                                    <button type="button" class="btn btn-xs btn-circle red" id="remove_pdf"><i class="fa  fa-trash-o"></i></button>
                                                    <input type="file" name="file_name" id="upload_pdf" style="display:none;" >
                                                @else
                                                    <input type="file" name="file_name" id="upload_pdf">

                                                    <button type="button" class="btn btn-xs btn-circle red" id="remove_pdf" style="display:none;"><i class="fa  fa-trash-o"></i></button>

                                                @endif
                                                <input type="hidden" name="file_name" id="pdf_path" value="<?php echo  $file_name ?>">
                                            </div>
                                        </div>
                                    </div>

                                <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="document_category_id" value="{{input::get('document_category_id')}}">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                {!! Form::close() !!}

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>

@endsection

@section('js')
    <script src="{{URL::asset('js/elfinder-upload.js')}}"></script>
    <script src="{{URL::asset('assets/admin/scripts/validate/document.js')}}"></script>
@endsection