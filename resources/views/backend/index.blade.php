<?php
use App\Http\Controllers\Backend\TelephoneController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;
use App\Model\Telephone;

$objCon = new TelephoneController(); // New Object Controller
$mainFn = new MainFunction(); // New Object Main Function
$parent_name = new Telephone();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$path = $objCon->path;
$tb_name = $objCon->tbName;

$orderBy = Input::get('orderBy');
$sortBy = Input::get('sortBy');
$search = Input::get('search');
$departments = Input::get('department');
$floor = Input::get('floor');



$a_otherParam = Input::except(['orderBy','sortBy']);
$strParam = $mainFn->parameter($a_otherParam);

$admin_id = auth()->guard('admin')->user()->admin_id;
if(!empty($admin_id)){
    // echo $mainFn->chk_permission($admin_id,'product');
}else{
    return Redirect::to('_admin/logout');
}

?>
@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1><a href="{{ URL::to('http://www1.oic.or.th/th/search/companies2.php?') }}" target="_blank">ข้อมูลรายชื่อและที่ตั้งของบริษัทประกันวินาศภัย</a></h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <?php $admin = auth()->guard('admin')->user();?>
                        <!-- BEGIN SAMPLE TABLE PORTLET-->
                        <div class="portlet light">
                            <form action{{URL::to($path)}} class="form-horizontal" method="GET" >
                                <div class="form-group">
                                    <label class="control-label col-md-1">Search</label>
                                    <div class="col-md-3">
                                        <input class="form-control" type="text" name="search" value="{{ $search }}" >
                                        <span class="help-block">Search by Name</span>
                                    </div>

                                    <label class="control-label col-md-1">FLoor</label>
                                    <div class="col-md-3">
                                        <select class="form-control select2me" data-placeholder="Select..." name="floor">
                                            <option value="" ></option>
                                            <option value="1" >1</option>
                                            <option value="2" >2</option>
                                            <option value="3" >3</option>
                                            <option value="4" >4</option>
                                            <option value="5" >5</option>
                                            <option value="6" >6</option>
                                        </select>
                                    </div>

                                    <label class="control-label col-md-1">Department</label>
                                    <div class="col-md-3">
                                        <select class="form-control select2me" data-placeholder="Select..." name="departments">
                                            <option value="" ></option>
                                            <option value="รับแจ้งอุบัติเหตุ" >รับแจ้งอุบัติเหตุ</option>
                                            <option value="สินไหมรถยนต์" >สินไหมรถยนต์</option>
                                            <option value="ประเมินราคา" >ประเมินราคา</option>
                                            <option value="สารบรรณ" >สารบรรณ</option>
                                            <option value="ฝ่ายเรียกร้อง F.5" >ฝ่ายเรียกร้อง F.5</option>
                                            <option value="กิจการพิเศษ และเรียกร้อง" >กิจการพิเศษ และเรียกร้อง</option>
                                            <option value="ฝ่ายต่างประเทศ" >ฝ่ายต่างประเทศ</option>
                                            <option value="รับประกันอัคคีภัย" >รับประกันอัคคีภัย</option>
                                            <option value="รับประกันรถยนต์" >รับประกันรถยนต์</option>
                                            <option value="รับประกันเบ็ดเตล็ด" >รับประกันเบ็ดเตล็ด</option>
                                            <option value="บ. ESCO ที่ชั้น 3" >บ. ESCO ที่ชั้น 3</option>
                                            <option value="คอมพิวเตอร์" >คอมพิวเตอร์</option>
                                            <option value="ประเมินราคาซ่อม" >ประเมินราคาซ่อม</option>
                                            <option value="ฝ่ายการเงินบัญชี" >ฝ่ายการเงินบัญชี</option>
                                            <option value="ฝ่ายทรัพยากรมนุษย์" >ฝ่ายทรัพยากรมนุษย์</option>
                                            <option value="ฝ่ายเร่งรัดหนี้สิน" >ฝ่ายเร่งรัดหนี้สิน</option>
                                            <option value="ฝ่ายกฏหมาย" >ฝ่ายกฏหมาย</option>
                                            <option value="ขยายงานการตลาด" >ขยายงานการตลาด</option>
                                            <option value="ฝ่ายการเงินและบัญชี" >ฝ่ายการเงินและบัญชี</option>
                                            <option value="ฝ่ายกรรมการบริหาร" >ฝ่ายกรรมการบริหาร</option>
                                        </select>
                                    </div>




                                </div>

                                <div class="form-group">
                                    <div class="col-md-offset-1 col-md-3 ">
                                        <button class="btn blue btn-sm" type="submit" ><i class="fa fa-search"></i> Search</button>
                                    </div>
                                </div>
                            </form>
                            <hr>

                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-database font-green-sharp"></i>
                                    <span class="caption-subject font-green-sharp bold">Found {{ $countData }} Record(s).</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr >
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('ID',$pkField,$orderBy,$sortBy,$strParam) !!} </th>
                                            <th class="text-center">{!! $mainFn->sorting('Name','name',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="text-center">{!! $mainFn->sorting('Floor','floor',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="text-center">{!! $mainFn->sorting('Department','department',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="text-center">{!! $mainFn->sorting('Telephone','telephone',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="text-center">{!! $mainFn->sorting('Fax','fax',$orderBy,$sortBy,$strParam) !!}</th>
                                            @if($permission['c'] == '1' || $permission['u'] == '1' || $permission['d'] == '1')
                                                <th class="col-md-1 text-center">
                                                  @if($permission['c'] == '1')
                                                        <a href="{{ URL::to($path.'telephone'.'/create') }}" class="btn btn-circle blue btn-xs"><i class="fa fa-plus"></i> Add</a>
                                                  @endif
                                                </th>
                                            @endif
                                        </tr>
                                        </thead>
                                        <tbody>
                                          @if($countData > 0)
                                              @foreach($data as $field)
                                          <tr>
                                            <td class="text-center">{{$field->$pkField}}</td>
                                            <td>{{$field->name}}</td>
                                            <td class="text-center">{{$field->floor}}</td>
                                            <td>{{$field->department}}</td>
                                            <td class="text-center">{{$field->telephone}}</td>
                                            <td>{{$field->fax}}</td>
                                            @if($permission['c'] == '1' || $permission['u'] == '1' || $permission['d'] == '1')
                                              <td class="text-center">
                                                      @if($permission['u'] == '1')
                                                        <a href="{{ URL::to($path.'telephone'.'/'.$field->$pkField.'/edit?'.$strParam) }}" class="btn btn-xs btn-circle green"><i class="fa fa-edit"></i></a>
                                                      @endif
                                                      @if($permission['d'] == '1')
                                                      <form action="{{URL::to($path.'telephone'.'/'.$field->$pkField)}}" method="post" class="frm-delete">
                                                          <input name="_method" type="hidden" value="delete">
                                                          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                          <button type="submit" class="btn btn-xs btn-circle red" ><i class="fa  fa-trash-o"></i></button>
                                                      </form>
                                                      @endif

                                              </td>
                                            @endif
                                          </tr>
                                              @endforeach
                                          @endif
                                        </tbody>

                                    </table>
                                </div>
                                {!! $data->appends(Input::except('page'))->render() !!}
                            </div>
                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
@endsection
