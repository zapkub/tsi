<?php
use App\Http\Controllers\Backend\ProductController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new ProductController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$category = App\Model\Category::whereNull('deleted_at')->get();

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);

$admin_id = auth()->guard('admin')->user()->admin_id;
if(!empty($admin_id)){
    // echo $mainFn->chk_permission($admin_id,'product');
}else{
    return Redirect::to('_admin/logout');
}

?>
@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                {!! Form::open(array('url'=>$url_to , 'method' => $method , 'id' => 'form' , 'class' => 'form-horizontal','enctype'=>"multipart/form-data" )) !!}
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Category</label>
                                            <div class="col-md-4">

                                                <?php
                                                if($method=="PUT"){
                                                    $category = DB::table('product_category')->where('product_id',$id)->first();
                                                    $category_id = $category->category_id;

                                                }else{
                                                    $category_id = '';
                                                }
                                                ?>

                                                <select name="category_id" class="form-control select2-container form-control select2me" >
                                                    <option value="0">/</option>

                                                    {{ $mainFn->search_select_category2($category_id,'','') }}

                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Status</label>
                                            <div class="col-md-4">
                                                <select name="status" class="form-control select2-container select2me">
                                                    <option value="0" @if($status == 0) selected @endif >Disabled</option>
                                                    <option value="1"  @if($status == 1) selected @endif>Enabled</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Product</label>
                                            <div class="col-md-4">
                                                <input type="text" name="product_name_th"  class="form-control" value="{{ $product_name_th}}"/>
                                            </div>
                                            <div class="col-md-1">
                                                ( TH )
                                                </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Product</label>
                                            <div class="col-md-4">
                                                <input type="text" name="product_name_en"  class="form-control" value="{{ $product_name_en}}"/>
                                            </div>
                                            <div class="col-md-1">
                                                ( EN )
                                                </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">คำโฆษณาผลิตภัณฑ์ ( TH )</label>
                                            <div class="col-md-9">
                                                <textarea name="short_description_th" id="short_description_th" class="form-control">{{ $short_description_th }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">คำโฆษณาผลิตภัณฑ์ ( EN )</label>
                                            <div class="col-md-9">
                                                <textarea name="short_description_en" id="short_description_en" class="form-control">{{ $short_description_en }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">คำโปรยผลิตภัณฑ์  ( TH )</label>
                                            <div class="col-md-9">
                                                <textarea name="description_th" id="description_th" class="form-control ckeditor">{{ $description_th }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">คำโปรยผลิตภัณฑ์  ( EN )</label>
                                            <div class="col-md-9">
                                                <textarea name="description_en" id="description_en" class="form-control ckeditor">{{ $description_en }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">รายละเอียดความคุ้มครอง ( TH )</label>
                                            <div class="col-md-9">
                                                <textarea name="protection_th" id="protection_th" class="form-control ckeditor">{{ $protection_th }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">รายละเอียดความคุ้มครอง ( EN )</label>
                                            <div class="col-md-9">
                                                <textarea name="protection_en" id="protection_en" class="form-control ckeditor">{{ $protection_en }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">เงื่อนไขในการรับประกันภัย ( TH )</label>
                                            <div class="col-md-9">
                                                <textarea name="warranty_th" id="warranty_th" class="form-control ckeditor">{{ $warranty_th }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">เงื่อนไขในการรับประกันภัย ( EN )</label>
                                            <div class="col-md-9">
                                                <textarea name="warranty_en" id="warranty_en" class="form-control ckeditor">{{ $warranty_en }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">สิทธิประโยชน์ ( TH )</label>
                                            <div class="col-md-9">
                                                <textarea name="privilege_th" id="privilege_th" class="form-control ckeditor">{{ $privilege_th }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">สิทธิประโยชน์ ( EN )</label>
                                            <div class="col-md-9">
                                                <textarea name="privilege_en" id="privilege_en" class="form-control ckeditor">{{ $privilege_en }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">สิทธิพิเศษ ( TH )</label>
                                            <div class="col-md-9">
                                                <textarea name="franchise_th" id="franchise_th" class="form-control ckeditor">{{ $franchise_th }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">สิทธิพิเศษ ( EN )</label>
                                            <div class="col-md-9">
                                                <textarea name="franchise_en" id="franchise_en" class="form-control ckeditor">{{ $franchise_en }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">สอบถามรายละเอียดเพิ่มเติม ( TH )</label>
                                            <div class="col-md-9">
                                                <textarea name="inquiry_th" id="inquiry_th" class="form-control ckeditor">{{ $inquiry_th }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">สอบถามรายละเอียดเพิ่มเติม ( EN )</label>
                                            <div class="col-md-9">
                                                <textarea name="inquiry_en" id="inquiry_en" class="form-control ckeditor">{{ $inquiry_en }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="control-group ">
                                        <div class="controls">
                                            <div id="thumbnail" class="text-center">
                                                <?php
                                                $path = "uploads/product/100/".$img_name;
                                                $path_img = URL::asset($path);
                                                $check_path = public_path($path);
                                                ?>

                                                @if(file_exists($check_path) and !empty($img_name))
                                                    <img src="{{ $path_img }}" id="img_path2" style="max-width:400px;">
                                                @else
                                                    <img src="" id="img_path2" style="max-width:400px;">
                                                @endif

                                                <input type="hidden" name="img_name" id="img_name" value="<?php echo  $img_name?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Image</label>
                                            <div class="col-md-4">
                                                @if($img_name != '')
                                                    <div id="remove_img" class="btn btn-danger" >Remove</div>
                                                    <div id="upload" class="btn blue" style="display:none;"><i class="icon-picture"></i> Select File</div>

                                                @elseif ($img_name == '')
                                                    <div id="upload" class="btn blue" ><i class="icon-picture"></i> Select File</div>
                                                    <div id="remove_img" class="btn btn-danger" style="display:none;" >Remove</div>

                                                @endif

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label"> </label>
                                        <div class="col-md-4">
                                            <span style="color:#F00">* หมายเหตุ</span><br>
                                                                    <span style="">
                                                                    - ไฟล์ต้องเป็นมีขนาดกว้าง 1440x550 pixel. <br>
                                                                    - นามสกุลของรูปภาพต้อง .png, .jpg เท่านั้น<br>
                                                                    </span>
                                        </div>
                                    </div>

                                    <div class="control-group ">
                                        <div class="controls">
                                            <div id="thumbnail1" class="text-center">
                                                <?php
                                                $path = "uploads/product_s/100/".$s_img_name;
                                                $path_img = URL::asset($path);
                                                $check_path = public_path($path);
                                                ?>

                                                @if(file_exists($check_path) and !empty($s_img_name))
                                                    <img src="{{ $path_img }}" id="img_path4" style="max-width:400px;">
                                                @else
                                                    <img src="" id="img_path4" style="max-width:400px;">
                                                @endif

                                                <input type="hidden" name="s_img_name" id="s_img_name" value="<?php echo  $s_img_name?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Small Image</label>
                                            <div class="col-md-4">
                                                @if($s_img_name != '')
                                                    <div id="remove_img1" class="btn btn-danger" >Remove</div>
                                                    <div id="upload1" class="btn blue" style="display:none;"><i class="icon-picture"></i> Select File</div>

                                                @elseif ($s_img_name == '')
                                                    <div id="upload1" class="btn blue" ><i class="icon-picture"></i> Select File</div>
                                                    <div id="remove_img1" class="btn btn-danger" style="display:none;" >Remove</div>

                                                @endif

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label"> </label>
                                        <div class="col-md-4">
                                            <span style="color:#F00">* หมายเหตุ</span><br>
                                                                    <span style="">
                                                                    - ไฟล์ต้องเป็นมีขนาดกว้าง 720x624 pixel. <br>
                                                                    - นามสกุลของรูปภาพต้อง .png, .jpg เท่านั้น<br>
                                                                    </span>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                {!! Form::close() !!}

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>

@endsection

@section('js')
    <script src="{{URL::asset('js/category-upload.js')}}"></script>
    <script src="{{URL::asset('assets/admin/scripts/validate/product.js')}}"></script>
@endsection