<?php
use App\Http\Controllers\Backend\FormInsuranceController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new FormInsuranceController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}

$data_car = DB::table('car_insurance')->where('no',$request_no)->first();

$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);

$admin_id = auth()->guard('admin')->user()->admin_id;
if(!empty($admin_id)){
    echo $mainFn->chk_permission($admin_id,'car_insurance');
}else{
    return Redirect::to('_admin/logout');
}
?>
<style>
    .border-table{
        border-bottom: solid 1px #000;
        border-top:solid 1px #000;
        margin-bottom: 20px;
    }
    .t-width{
        width:50px;
    }
    .head{
        background-color: #b4c4cd;
    }
    .bg-red{
        background-color:#bf6a40;
    }
    .f-red{
        color:#b94a48;
    }
</style>

@extends('admin')
@section('content')

        <!-- BEGIN SAMPLE TABLE PORTLET-->
<div class="portlet light">



    <div class="portlet-body flip-scroll">


        <div>

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#data" aria-controls="data" role="tab" data-toggle="tab">ใบสลักหลัง</a></li>
                <li role="presentation" ><a href="#data2" aria-controls="data2" role="tab" data-toggle="tab">อ้างอิงใบคำขอประกันรถยนต์</a></li>

            </ul>


            <div class="tab-content">


                <div role="tabpanel" class="tab-pane active" id="data">


                    <table class="table-condensed flip-content border-table">
                        <thead class="flip-content">
                        <tr class="head">
                            <td colspan="4" class="text-center"><strong>บริษัท ไทยเศรษฐกิจประกันภัย จำกัด (มหาชน)<br>คำขอแก้ไขข้อความในตารางกรมธรรม</strong></td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><strong>การประกันรถยนต์</strong></td>
                            <td colspan="3">วัน {{ $mainFn->format_date_th(date('Y-m-d H:i:s'),4) }}</td>
                        </tr>
                        <tr>
                            <td>สาขา*
                                <?php $branch_db = DB::table('branch')->whereNull('deleted_at')->where('branch_id',$data_car->branch_id)->first(); ?>
                                {{ $branch_db->name_th }}
                            </td>
                            <td>เล่มที่ {{ $book }}</td>
                            <td>เลขที่ {{ $no }}</td>
                            <td>
                                <?php $db = DB::table('agent')->select('firstname','lastname')->where('agent_id',$agent_id)->first(); ?>
                                ชื่อ {{ $db->firstname }} {{ $db->lastname }}
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4"><strong>รหัส TSK</strong></td>
                        </tr>

                        </tbody>
                    </table>

                    <table class="table-condensed flip-content border-table">
                        <thead class="flip-content">
                        <tr class="head">
                            <td colspan="3" class="text-center"><strong>รายละเอียดคำร้องขอสลักหลังข้อความในตารางกรมธรรม์</strong></td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                @if($status_endorse==0)
                                    ยกเลิก
                                @elseif($status_endorse==1)
                                    สลักหลัง
                                @endif
                            </td>
                            <td>กรมธรรม์เลขที่ {{ $request_no }}</td>
                        </tr>
                        <tr>
                            <td>ชื่อผู้เอาประกัน {{ $firstname }} {{ $lastname }}</td>
                            <td>วันที่คุ้มครอง {{ $mainFn->format_date_th($date_coverage,2) }}</td>
                        </tr>
                        <tr>
                            <td>ชื่อรถยนต์ {{ $car_name }}</td>
                            <td>เลขทะเบียน {{ $license_plate }}</td>
                        </tr>
                        <tr>
                            @if($status_endorse==0)
                                <td>สาเหตุการยกเลิก</td>
                                <td>{{ $reason }}</td>
                            @elseif($status_endorse==1)
                                <td>รายละเอียดการเปลี่ยนแปลง</td>
                                <td>{{ $detail }}</td>
                            @endif
                        </tr>
                        </tbody>
                    </table>

                    <table class="table-condensed flip-content border-table">
                        <tr>
                            <td colspan="2">ลงชื่อผู้พิมพ์</td>
                        </tr>
                        <tr>
                            <td>ชื่อตัวแทน</td>
                            <td>{{ $name_agent }}</td>
                        </tr>
                        <tr>
                            <td>ชื่อรหัสตัวแทน</td>
                            <td>{{ $code_agent }}</td>
                        </tr>
                        <tr>
                            <td>หมายเหตุ</td>
                            <td>{{ $reason }}</td>
                        </tr>
                        </tbody>
                    </table>

                    <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" class="form-horizontal">
                        <input name="_method" type="hidden" value="{{$method}}">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                        <table class="table-condensed flip-content border-table">
                            <tbody>
                            <tr>
                                <td class="text-center">
                                    <button type="submit" name="check" class="btn btn-xs btn-circle green" value="1"><i class="fa  fa-check"></i> อนุมัติ</button>
                                    <button type="submit" name="close" id="close" class="btn btn-xs btn-circle red" value="1"><i class="fa  fa-close"></i> ไม่อนุมัติ</button>
                                </td>

                            </tr>
                            <tr id="reason_close" style="display: none;">
                                <td class="text-center"><textarea name="reason_c" id="reason_c"></textarea></td>
                            </tr>
                            </tbody>
                        </table>
                    </form>


                </div>


                <div role="tabpanel" class="tab-pane" id="data2">

                    <table class="table-condensed flip-content border-table">
                        <thead class="flip-content">
                        <tr class="head">
                            <td colspan="3" class="text-center"><strong>บริษัท ไทยเศรษฐกิจประกันภัย จำกัด (มหาชน)<br>คำขอเอาประกันภัยรถยนต์</strong></td>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <td></td>
                            <td><strong>การประกันภัยรถยนต์</strong></td>
                            <td>{{ $mainFn->format_date_th(date('Y-m-d H:i:s'),3) }}</td>
                        </tr>
                        <?php
                        $db = DB::table('agent')->select('flag','firstname','lastname','agent_no','created_at')->where('agent_id',$agent_id)->first();
                        $db2 = DB::table('admin')->select('firstname','lastname','created_at')->where('admin_id',$admin_id)->first();
                        ?>
                        <tr>
                            <td><strong>รหัส TSK</strong></td>
                            <td>@if($db->flag==2) สาขา
                                <?php $branch_db = DB::table('branch')->whereNull('deleted_at')->where('branch_id',$data_car->branch_id)->first(); ?>
                                {{ $branch_db->name_th }}
                                @endif
                                เล่มที่ {{ $data_car->book }}
                            </td>
                            <td>ผู้ขอ {{ $db->firstname }} {{ $db->lastname }} {{ $mainFn->format_date_th($db->created_at,3) }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>@if($data_car->type_repair==1) ซ่อมอู่ @elseif($data_car->type_repair==2) ซ่อมห้าง @elseif($data_car->type_repair==3) T- Care @endif</td>
                            <td>ผู้ยืนยัน {{ $db2->firstname }} {{ $db2->lastname }} {{ $mainFn->format_date_th($db->created_at,3) }} </td>
                        </tr>
                        </tbody>
                    </table>

                    <table class="table-condensed flip-content border-table">

                        <thead class="flip-content">
                        <tr class="head">
                            <th colspan="3">ผู้ขอเอาประกันภัย</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>ชื่อ {{ $data_car->firstname }} {{ $data_car->lastname }}</td>
                            <td>
                                @if($data_car->status_insurance==0) <i class="fa fa-check"></i> ประกันใหม่ @endif
                                @if($data_car->status_insurance==1) <i class="fa fa-check"></i> ต่ออายุกรมธรรม์ที่ กท {{ $data_car->extend }} @endif
                            </td>
                        </tr>
                        <tr>
                            <td>ที่อยู่</td>
                            <td>{{ $data_car->address }}</td>
                        </tr>
                        <tr>
                            <td>ประเภทการประกันภัย</td>
                            <td>
                                @if($data_car->type_insurance==0) <i class="fa fa-check"></i> ประเภท1 @endif
                                @if($data_car->type_insurance==1) <i class="fa fa-check"></i> ประเภท2 @endif
                                @if($data_car->type_insurance==2) <i class="fa fa-check"></i> ประเภท3 @endif
                                @if($data_car->type_insurance==3) <i class="fa fa-check"></i> ประเภท2+ @endif
                                @if($data_car->type_insurance==4) <i class="fa fa-check"></i> ประเภท3+ @endif
                                @if($data_car->type_insurance==5) <i class="fa fa-check"></i> พ.ร.บ @endif
                                @if($data_car->type_insurance==6) <i class="fa fa-check"></i> อื่นๆ ระบุ <input type="text" name="type_insurance_other" value="{{ $data_car->type_insurance_other }}"> @endif
                        </tr>
                        <tr>
                            <td>เลขที่บัตรประชาชน</td>
                            <td>{{ $data_car->id_card }}</td>
                        </tr>
                        <tr>
                            <td>อาชีพ</td>
                            <td>
                                @if($data_car->type_insurance==0) <i class="fa fa-check"></i> ข้าราชการ / พนักงานรัฐวิสาหกิจ @endif
                                @if($data_car->type_insurance==1) <i class="fa fa-check"></i> รับจ้าง @endif
                                @if($data_car->type_insurance==2) <i class="fa fa-check"></i> ธุรกิจส่วนตัว @endif
                                @if($data_car->type_insurance==3) <i class="fa fa-check"></i> ไม่ได้ประกอบอาชีพ @endif
                                @if($data_car->type_insurance==4) <i class="fa fa-check"></i> อื่นๆ ระบุ {{ $data_car->occupation_other }} @endif
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <table class="table-condensed flip-content border-table">

                        <thead class="flip-content">
                        <tr class="head">
                            <th colspan="4">ประเภทการรับประกันภัยที่ต้องการ</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $year = date('Y');
                        $birthday_1 = explode('-',$data_car->birthday);
                        foreach ($birthday_1 as $key => $value){
                            if($key==0){
                                $year = $value;
                            }
                            if($key==1){
                                $month = $value;
                            }
                            if($key==2){
                                $day = $value;
                            }
                        }

                        $birthday_2 = explode('-',$data_car->birthday2);
                        foreach ($birthday_2 as $key => $value){
                            if($key==0){
                                $year2 = $value;
                            }
                            if($key==1){
                                $month2 = $value;
                            }
                            if($key==2){
                                $day2 = $value;
                            }
                        }
                        ?>
                        @if($data_car->type_insurance==0)
                            <tr>
                                <td><i class="fa fa-check"></i> ไม่ระบุผู้ขับขี่</td>
                                <td></td>
                            </tr>
                        @endif

                        @if($data_car->type_insurance==1)
                            <tr>
                                <td><i class="fa fa-check"></i> ระบุชื่อผู้ขับขี่ 1.{{ $data_car->driver_name }}</td>
                                <td>
                                    วัน {{ $day }}
                                    เดือน {{ $month }}
                                    ปีเกิด {{ $year }}
                                </td>
                                <td>อาชีพ {{ $data_car->driver_occupation }}</td>
                            </tr>
                            <tr>
                                <td>เลขที่ใบขับขี่ {{ $data_car->driver_license }}</td>
                                <td>จังหวัด {{ $data_car->driver_provice }}</td>
                                <td>โทร {{ $data_car->driver_mobile }}</td>
                            </tr>
                            <tr>
                                <td>ระบุชื่อผู้ขับขี่ 2.{{ $data_car->driver_name2 }}</td>
                                <td>
                                <td>
                                    วัน {{ $day2 }}
                                    เดือน {{ $month2 }}
                                    ปีเกิด {{ $year2 }}
                                </td>
                                <td>อาชีพ {{ $data_car->driver_occupation2 }}</td>
                            </tr>
                            <tr>
                                <td>เลขที่ใบขับขี่ {{ $data_car->driver_license2 }}</td>
                                <td>จังหวัด {{ $data_car->driver_provice2 }}</td>
                                <td>โทร {{ $data_car->driver_mobile2 }}</td>
                                <td></td>
                            </tr>
                        @endif

                        </tbody>
                    </table>

                    <table class="table-condensed flip-content border-table">
                        <tr>
                            <td>เลขเครื่องหมายตาม พ.ร.บ. {{ $data_car->number_sign }}</td>
                            <td>บริษัท {{ $data_car->company }}</td>
                            <td>สิ้นสุดเมื่อ {{ $mainFn->format_date_th($data_car->end_date,2) }}</td>
                        </tr>
                        <tr>
                            <td>
                                การใช้รถยนต์        @if($data_car->use_car==0) <i class="fa fa-check"></i> @endif
                                ส่วนบุคคล           @if($data_car->use_car==1) <i class="fa fa-check"></i> @endif
                                เพื่อการพาณิชน์       @if($data_car->use_car==2) <i class="fa fa-check"></i> @endif
                                เพื่อการพาณิชน์พิเศษ   @if($data_car->use_car==3) <i class="fa fa-check"></i> @endif
                                รับจ้างสาธารณะ      @if($data_car->use_car==4) <i class="fa fa-check"></i> @endif
                                อื่นๆ               @if($data_car->use_car==6) <i class="fa fa-check"></i> ระบุ {{ $data_car->use_car_other }} @endif
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">ผู้รับประโยชน์ {{ $data_car->beneficiary }}</td>
                        </tr>
                    </table>

                    <table class="table-condensed flip-content border-table">

                        <thead class="flip-content">
                        <tr class="head">
                            <th colspan="7">รายการรถยนต์ที่เอาประกัน :</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="text-center">
                            <td class="f-red">ชื่อรถยนต์ / รุ่น*</td>
                            <td class="f-red">เลขทะเบียน*</td>
                            <td class="f-red">เลขตัวถัง*</td>
                            <td class="f-red">ปี รุ่น*</td>
                            <td>จำนวนที่นั่ง / ขนาด / น้ำหนัก</td>
                            <td>มูลค่าเต็มรวมตกแต่ง</td>
                        </tr>
                        <tr>
                            <td class="text-center">{{ $data_car->name_car }}</td>
                            <td class="text-center">{{ $data_car->registration }}</td>
                            <td class="text-center">{{ $data_car->chassis }}</td>
                            <td class="text-center">{{ $data_car->roon }}</td>
                            <td class="text-center">
                                @if(empty($data_car->seating) and empty($data_car->size) and empty($data_car->weight))
                                    <?php $s = $data_car->seating ?>

                                @else

                                    <?php $s = $data_car->seating."/".$data_car->size."/".$data_car->weight ?>

                                @endif
                                {{ $s }}

                            </td>
                            <td class="text-center">{{ $data_car->total_decorate }}</td>
                        </tr>
                        <tr class="text-center">
                            <td>รหัสรถยนต์</td>
                            <td>เลขเครื่องรถยนต์</td>
                            <td colspan="4"></td>
                        </tr>
                        <tr>
                            <td class="text-center">{{ $data_car->code_car }}</td>
                            <td class="text-center">{{ $data_car->engine }}</td>
                            <td colspan="4"></td>
                        </tr>
                        </tbody>
                    </table>

                    <table class="table-condensed flip-content border-table">

                        <thead class="flip-content">
                        <tr class="head">
                            <th colspan="4">รายการตกแต่งเปลี่ยนแปลงรถยนต์เพิ่มเติม (โปรดระบุรายละเอียด)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1. {{ $data_car->decorate_car }}</td>
                            <td>ราคา {{ $data_car->price }} บาท</td>
                        </tr>
                        <tr>
                            <td>2. {{ $data_car->decorate_car2 }}</td>
                            <td>ราคา {{ $data_car->price2 }} บาท</td>
                        </tr>
                        <tr>
                            <td>3. {{ $data_car->decorate_car3 }}</td>
                            <td>ราคา {{ $data_car->price3 }} บาท</td>
                        </tr>
                        <tr>
                            <td>อุปกรณ์พิเศษ @if($data_car->equipment==0) <i class="fa fa-check"></i> @endif ไม่มี @if($data_car->equipment==1) <i class="fa fa-check"></i> @endif มีระบุ</td>
                            @if($data_car->equipment==1) <td>{{ $data_car->equipment_other }}</td> @endif
                            <td></td>
                        </tr>
                        </tbody>
                    </table>

                    <table class="table-condensed flip-content border-table">
                        <thead class="flip-content">
                        <tr class="head">
                            <th>ความรับผิดต่อบุคคลภายนอก</th>
                            <th>รถยนต์เสียหาย สูญหายไฟไหม้</th>
                            <th>ความคุ้มครองตามเอกสารแนบท้าย</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td valign="top">1.ความเสียหายต่อชีวิต ร่างกาย หรือ อนามัย<br>เฉพาะส่วนเกินวงเงินสูงสุดตาม พ.ร.บ. {{ $data_car->person_damage }} บาท/คน {{ $data_car->person_damage2 }} บาท/ครั้ง<br>
                                2.ความเสียหายต่อทรัพย์สิน {{ $data_car->person_damage3 }} บาท/ครั้ง<br>
                                2.1ความเสียหายส่วนแรก {{ $data_car->person_damage4 }} บาท/ครั้ง
                            </td>
                            <td valign="top">1.ความเสียหายต่อรถยนต์ {{ $data_car->car_damage }} บาท / ครั้ง<br>
                                1.1.ความเสียหายส่วนแรก {{ $data_car->car_damage2 }} บาท / ครั้ง<br>
                                2.รถยนต์สูญหายไฟไหม้ {{ $data_car->car_damage3 }} บาท<br>

                                <span style="font-size:36px;color:red;">ไม่รวม พ.ร.บ</span>
                            </td>
                            <td valign="top">1.อุบัติเหตุส่วนบุคคล<br>
                                1.1.เสียชีวิต สูญเสียอวัยวะ ทุพพลภาพถาวร<br>
                                ก.ผู้ขับขี่ 1 คน {{ $data_car->document_ptt }} บาท<br>
                                ข.ผู้โดยสาร {{ $data_car->document_ptt2 }} คน<br>
                                1.2.ทุพพลภาพชั่วคราว<br>
                                ก.ผู้ขับขี่ 1 คน {{ $data_car->document_ptt3 }} บาท / สัปดาห์<br>
                                ข.ผู้โดยสาร {{ $data_car->document_ptt4 }} คน {{ $data_car->document_ptt5 }} บาท / คน / สัปดาห์<br>
                                2.ค่ารักษาพยาบาล {{ $data_car->document_ptt6 }} บาท / คน
                                3.การประกันตัวผู้ขับขี่ {{ $data_car->document_ptt7 }} บาท / ครั้ง
                            </td>
                        </tr>
                        <tr>
                            <td>เบี้ยสุทธิ {{ $data_car->net }} บาท</td>
                            <td>เบี้ยรวมภาษีอากร {{ $data_car->net_tax }} บาท</td>
                            <td>เบี้ย พ.ร.บ {{ $data_car->net_act }} บาท</td>
                        </tr>

                        </tbody>
                    </table>

                    <table class="table-condensed flip-content border-table">

                        <thead class="flip-content">
                        <tr>
                            <td colspan="2">ข้าพเจ้าขอรับรองว่า คำบอกกล่าวตามรายการข้างบนเป็นความจริงและให้นับถือเป็นส่วนหนึ่งของสัญญาระหว่างข้าพเจ้ากับบริษัท</td>
                        </tr>
                        <tr>
                            <td colspan="2">โดยข้าพเจ้ามีความประสงค์ให้กรมธรรม์มีผลบังคับใช้ตั้งแต่วันที่
                                {{ $mainFn->format_date_th($data_car->from_date,2) }} ถึงวันที่ {{ $mainFn->format_date_th($data_car->to_date,2) }}
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <table class="table-condensed flip-content border-table">
                        <tbody>

                        <tr>
                            <td colspan="4">ลงชื่อผู้พิมพ์</td>
                        </tr>
                        <tr>
                            <td>ชื่อตัวแทน</td>
                            <td>{{ $data_car->agent }}</td>
                            <td>ลายมือชื่อผู้ขอเอาประกัน..........................................................................</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="f-red">ชื่อรหัสตัวแทน* กท</td>
                            <td>{{ $data_car->code_agent }}</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>หมายเหตุ</td>
                            <td>{{ $data_car->note }}</td>
                            <td>วันที่...................เดือน......................พ.ศ......................</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td class="f-red">เอกสารฉบับนี้เป็นเพียงการแจ้งขอเอาประกันภัยรถยนต์เท่านั้น<br>มิใช่เอกสารการแสดงการรับชำระเงินค่าเบี้ยประกันภัย</td>
                            <td></td>
                        </tr>

                        </tbody>
                    </table>

                    <table class="table-condensed flip-content border-table">
                        <tbody>
                        <tr>
                            <td colspan="4" class="f-red">คำเตือนของกรมการประกันภัย กระทรวงพาณิชย์<br>
                                ให้ตอบคำถามข้างต้นตามเป็นจริงทุกข้อ มิฉะนั้นบริษัทอาจถือเป็นเหตุปฏิเสธความรับผิดชอบตามสัญญาประกันภัยได้<br>
                                ตามประมวลกฏหมายเพ่งและพาณิชน์มาตรา 865</td>
                        </tr>
                        </tbody>
                    </table>


                </div>

            </div>

        </div>



    </div>
</div>
<!-- END SAMPLE TABLE PORTLET-->
<script>
    $('#close').on('click',function () {
        var close_value = $('#close').val();
        var reason_c = $('#reason_c').val();
        if(close_value == 1){
            $('#reason_close').css('display','');
            if(reason_c == ''){
                $('#reason_close').attr('required','true');
                return false;
            }
        }
    });
</script>
@endsection