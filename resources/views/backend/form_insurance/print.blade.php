<?php
use App\Http\Controllers\Backend\FormInsuranceController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new FormInsuranceController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}

$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);

$admin_id = auth()->guard('admin')->user()->admin_id;
if(!empty($admin_id)){
    echo $mainFn->chk_permission($admin_id,'car_insurance');
}else{
    return Redirect::to('_admin/logout');
}
?>
<style>
    .border-table{
        border-bottom: solid 1px #000;
        border-top:solid 1px #000;
        margin-bottom: 20px;
    }
    .t-width{
        width:50px;
    }
    .head{
        background-color: #b4c4cd;
    }
    .bg-red{
        background-color:#bf6a40;
    }
    .f-red{
        color:#b94a48;
    }
</style>

@extends('admin')
@section('content')

        <!-- BEGIN SAMPLE TABLE PORTLET-->
<div class="portlet light">

    <div class="portlet-body flip-scroll">

        <table class="table-condensed flip-content border-table">
            <thead class="flip-content">
            <tr class="head">
                <td colspan="4" class="text-center"><strong>บริษัท ไทยเศรษฐกิจประกันภัย จำกัด (มหาชน)<br>คำขอแก้ไขข้อความในตารางกรมธรรม</strong></td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><strong>การประกันรถยนต์</strong></td>
                <td colspan="3">วัน {{ $mainFn->format_date_th(date('Y-m-d H:i:s'),4) }}</td>
            </tr>
            <tr>
                <td>สาขา*
                    <?php $branch_db = DB::table('branch')->whereNull('deleted_at')->where('branch_id',$branch_id)->first(); ?>
                    {{ $branch_db->name_th }}
                </td>
                <td>เล่มที่ {{ $book }}</td>
                <td>เลขที่ {{ $no }}</td>
                <td>
                    <?php $db = DB::table('agent')->select('firstname','lastname')->where('agent_id',$agent_id)->first(); ?>
                    ชื่อ {{ $db->firstname }} {{ $db->lastname }}
                </td>
            </tr>
            <tr>
                <td colspan="4"><strong>รหัส TSK</strong></td>
            </tr>

            </tbody>
        </table>

        <table class="table-condensed flip-content border-table">
            <thead class="flip-content">
            <tr class="head">
                <td colspan="3" class="text-center"><strong>รายละเอียดคำร้องขอสลักหลังข้อความในตารางกรมธรรม์</strong></td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    @if($status_endorse==0)
                        ยกเลิก
                    @elseif($status_endorse==1)
                        สลักหลัง
                    @endif
                </td>
                <td>กรมธรรม์เลขที่ {{ $request_no }}</td>
            </tr>
            <tr>
                <td>ชื่อผู้เอาประกัน {{ $firstname }} {{ $lastname }}</td>
                <td>วันที่คุ้มครอง {{ $mainFn->format_date_th($date_coverage,2) }}</td>
            </tr>
            <tr>
                <td>ชื่อรถยนต์ {{ $car_name }}</td>
                <td>เลขทะเบียน {{ $license_plate }}</td>
            </tr>
            <tr>
                @if($status_endorse==0)
                    <td>สาเหตุการยกเลิก</td>
                    <td>{{ $reason }}</td>
                @elseif($status_endorse==1)
                    <td>รายละเอียดการเปลี่ยนแปลง</td>
                    <td>{{ $detail }}</td>
                @endif
            </tr>
            </tbody>
        </table>

        <table class="table-condensed flip-content border-table">
            <tr>
                <td colspan="2">ลงชื่อผู้พิมพ์</td>
            </tr>
            <tr>
                <td>ชื่อตัวแทน</td>
                <td>{{ $name_agent }}</td>
            </tr>
            <tr>
                <td>ชื่อรหัสตัวแทน</td>
                <td>{{ $code_agent }}</td>
            </tr>
            <tr>
                <td>หมายเหตุ</td>
                <td>{{ $reason }}</td>
            </tr>
            </tbody>
        </table>

    </div>
</div>
<!-- END SAMPLE TABLE PORTLET-->

<script>
    $(document).ready(function(){
        window.print();
    });
</script>

@endsection