<?php
use App\Http\Controllers\Backend\CompanyInsuranceController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new CompanyInsuranceController(); // New Object Controller
$mainFn = new MainFunction(); // New Object Main Function

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$path = $objCon->path;

$orderBy = Input::get('orderBy');
$sortBy = Input::get('sortBy');
$search = Input::get('search');

$a_otherParam = Input::except(['orderBy','sortBy']);
$strParam = $mainFn->parameter($a_otherParam);


?>
@extends('admin')
@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>{{ $titlePage }}</h1>
                </div>
            </div>
        </div>

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->

                        <div class="portlet light">

                            <form action{{URL::to($path)}} class="form-horizontal" method="GET" >
                            <div class="form-group">
                                <label class="control-label col-md-1">Search</label>
                                <div class="col-md-3">
                                    <input class="form-control" type="text" name="search" value="{{ $search }}" >
                                    <span class="help-block">Search by name</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-1 col-md-3 ">
                                    <button class="btn blue btn-sm" type="submit" ><i class="fa fa-search"></i> Search</button>
                                </div>
                            </div>
                            </form>
                            <hr>

                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-green-sharp bold">จำนวนใบคำขอที่เข้ามาในระบบทั้งหมดที่ <li class="fa fa-edit"></li> รอการยืนยัน จำนวน {{ $countData }} ฉบับ</span>
                                </div>
                            </div>
                        
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('ID',$pkField,$orderBy,$sortBy,$strParam) !!} </th>
                                            <th>{!! $mainFn->sorting('ใบคำขอเลขที่','policy_no',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th>{!! $mainFn->sorting('ผู้ขอประกันภัย','insurance_name',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th>{!! $mainFn->sorting('เบี้ยประกัน','insurance_premium',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th>{!! $mainFn->sorting('วันที่ขอ','date_coverage',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th>{!! $mainFn->sorting('รหัสตัวแทน','code_agent',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th>{!! $mainFn->sorting('สาขา','service_id',$orderBy,$sortBy,$strParam) !!}</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @if($countData > 0)
                                            @foreach($data as $field)
                                                <tr>
                                                    <td class="text-center">{{ $field->$pkField }}</td>
                                                    <td>{{ $field->policy_no }}</td>
                                                    <td>{{ $field->insurance_name }}</td>
                                                    <td>{{ $field->insurance_premium }}</td>
                                                    <td>{{ $field->date_coverage }}</td>
                                                    <td>{{ $field->code_agent }}</td>
                                                    <td>{{ $field->service_id }}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr><td colspan='9' class='text-center'>No Result.</td></tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                {!! $data->appends(Input::except('page'))->render() !!}
                            </div>

                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-green-sharp bold">จำนวนใบคำขอที่ <li class="fa fa-check"></li> ยืนยันแล้ว</span>
                                </div>
                            </div>

                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('ID',$pkField,$orderBy,$sortBy,$strParam) !!} </th>
                                            <th>{!! $mainFn->sorting('ใบคำขอเลขที่','no',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th>{!! $mainFn->sorting('ผู้ขอประกันภัย','insurance_name',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th>{!! $mainFn->sorting('ทะเบียนรถ','registration',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th>{!! $mainFn->sorting('วันที่ยืนยัน','date_coverage',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th>{!! $mainFn->sorting('รหัสตัวแทน','code_agent',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th>{!! $mainFn->sorting('สาขา','service_id',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th>{!! $mainFn->sorting('สลักหลัง','status',$orderBy,$sortBy,$strParam) !!}</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @if($countData2 > 0)
                                            @foreach($data2 as $field)
                                                <tr>
                                                    <td class="text-center">{{ $field->$pkField }}</td>
                                                    <td>{{ $field->no }}</td>
                                                    <td>{{ $field->insurance_name }}</td>
                                                    <td>{{ $field->registration }}</td>
                                                    <td>{{ $field->date_coverage }}</td>
                                                    <td>{{ $field->code_agent }}</td>
                                                    <td>{{ $field->service_id }}</td>
                                                    <td>{{ $field->status }}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr><td colspan='9' class='text-center'>No Result.</td></tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                {!! $data->appends(Input::except('page'))->render() !!}
                            </div>

                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
@endsection