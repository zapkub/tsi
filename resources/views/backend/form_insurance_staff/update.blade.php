<?php
use App\Http\Controllers\Backend\FormInsuranceController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new FormInsuranceController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$service = App\Model\Service::whereNull('deleted_at')->get();

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);


?>
<style>
    .f-red{
        color:#b94a48;
    }
</style>
@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" class="form-horizontal">
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                    <div class="portlet box yellow">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                    บริษัท ไทยเศรษฐกิจประกันภัย จำกัด (มหาชน)<br>
                                                    คำร้องขอแก้ไขข้อความในตารางกรมธรรม์
                                            </div>
                                        </div>
                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->
                                            <div class="form-body">
                                                <div class="form-group">
                                                    {{--<div class="col-md-1">--}}
                                                        {{--<label class="control-label">สาขา *</label>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="col-md-3 text-left">--}}
                                                        {{--<select class="form-control " data-placeholder="Select..." name="service_id" id="service_id" data-type="service_id">--}}
                                                            {{--<option value="">กรุณาเลือกหมวดหมู่</option>--}}
                                                            {{--@foreach($service as $cate)--}}
                                                                {{--<option value="{{$cate->service_id}}" @if($service_id==$cate->service_id) selected @endif >{{$cate->branch_name}}</option>--}}
                                                            {{--@endforeach--}}
                                                        {{--</select>--}}
                                                    {{--</div>--}}

                                                    <div class="col-md-1">
                                                        <label class="control-label">การประกันภัยรถยนต์</label>
                                                    </div>

                                                    <div class="col-md-1">
                                                        <label class="control-label">{{ $mainFn->format_date_th(date("Y-m-d H:i:s"),4) }}</label>
                                                    </div>

                                                    <div class="col-md-1">
                                                        <label class="control-label">เล่มที่</label>
                                                    </div>

                                                    <div class="col-md-2 text-left">
                                                        <input type="text" name="book" class="form-control" value="{{ $book }}">
                                                    </div>

                                                    <div class="col-md-1">
                                                        <label class="control-label">เลขที่</label>
                                                    </div>

                                                    <div class="col-md-2 text-left">
                                                        <input type="text" name="no" class="form-control" value="{{ $no }}">
                                                    </div>

                                                    <div class="col-md-2">
                                                        <label class="col-md-2 control-label">ในใบแก้ไข</label>
                                                    </div>

                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-1">
                                                        <label class="control-label">รหัส</label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label class="control-label">TSK</label>
                                                    </div>

                                                    <label class="col-md-1 control-label">ชื่อ</label>
                                                    <div class="col-md-4">
                                                        <label class="control-label"></label>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>


                                    <div class="portlet box yellow">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-file-o"></i>รายละเอียดคำร้อง
                                            </div>
                                        </div>
                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->
                                                <div class="form-body">
                                                    <div class="form-group">

                                                        <div class="col-md-3 text-right">
                                                            <div class="radio-list">
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="status" id="status" value="1" @if($status==1) checked @endif> สลักหลัง</label>
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="status" id="status" value="0" @if($status==0) checked @endif> ยกเลิก</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 text-left">
                                                            <label class="control-label">กรมธรรม์เลขที่ / ใบคำขอเลขที่ *</label>
                                                            </div>
                                                        <div class="col-md-2 text-left">
                                                            <input type="text" name="app_no" id="app_no" class="form-control" value="{{ $request_no }}">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label class="control-label">(หากแก้ไขข้อมูลในใบคำขอออนไลน์ให้ใส่เลขที่ใบคำขอนั้น)</label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">ชื่อผู้เอาประกัน</label>
                                                        <div class="col-md-3">
                                                            <input type="text" name="firstname" class="form-control"  value="{{ $firstname }}">
                                                        </div>

                                                        <div class="col-md-3">
                                                            <div class="col-md-12">
                                                                <input type="text" name="lastname" class="form-control"  value="{{ $lastname }}">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">วันคุ้มครอง</label>
                                                        <div class="col-md-3">
                                                            <div class="input-group input-large date-picker input-daterange" data-date="<?php echo date("Y-m-d");?>" data-date-format="yyyy-mm-dd">
                                                                <input type="text" class="form-control" name="date_coverage" value="{{ $date_coverage }}">

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">ชื่อรถยนต์</label>
                                                        <div class="col-md-3">
                                                            <input type="text" name="car_name" class="form-control" value="{{ $car_name }}">
                                                        </div>

                                                        <label class="col-md-2 control-label">เลขที่ทะเบียน</label>
                                                        <div class="col-md-3">
                                                            <input type="text" name="license_plate" class="form-control" value="{{ $license_plate }}" >
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">รายละเอียดการเปลี่ยนแปลง</label>
                                                        <div class="col-md-4">
                                                            <textarea name="detail" class="form-control" rows="3">{{ $detail }}</textarea>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">สาเหตุการยกเลิก</label>
                                                        <div class="col-md-4">
                                                            <textarea name="reason" class="form-control" rows="3">{{ $reason }}</textarea>
                                                        </div>
                                                    </div>

                                                </div>
                                        </div>
                                    </div>

                                    <div class="portlet box yellow">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-child"></i>ลงชื่อผู้แก้ไข
                                            </div>
                                        </div>
                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">ชื่อตัวแทน</label>
                                                    <div class="col-md-4">
                                                        <input type="text" name="name_agent" class="form-control" value="{{ $name_agent }}">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">ชื่อรหัสตัวแทน* กห</label>
                                                    <div class="col-md-4">
                                                        <input type="text" name="code_agent"  class="form-control" value="{{ $code_agent }}">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">หมายเหตุ</label>
                                                    <div class="col-md-4">
                                                        <textarea name="note"  class="form-control" rows="3">{{ $note }}</textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-actions fluid">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <input name="check_data" type="checkbox"> * รายละเอียดครบถ้วนถูกต้อง
                                                    </div>

                                                    <div class="col-md-offset-3 col-md-9">
                                                        <input type="hidden" name="strParam" value="{{$strParam}}">
                                                        <button type="submit" class="btn green">ส่งคำร้องเข้าบริษัท ไทยเศรษฐกิจประกันภัย จำกัด(มหาชน)</button>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
@endsection