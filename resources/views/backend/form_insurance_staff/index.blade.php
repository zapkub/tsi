<?php
use App\Http\Controllers\Backend\FormInsuranceStaffController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new FormInsuranceStaffController(); // New Object Controller
$mainFn = new MainFunction(); // New Object Main Function

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$tb_name = $objCon->tbName;
$path = $objCon->path;

$orderBy = Input::get('orderBy');
$sortBy = Input::get('sortBy');
$search = Input::get('search');

$a_otherParam = Input::except(['orderBy','sortBy']);
$strParam = $mainFn->parameter($a_otherParam);


?>
@extends('admin')
@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>{{ $titlePage }}</h1>
                </div>
            </div>
        </div>

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->

                        <div class="portlet light">

                            <form action{{URL::to($path)}} class="form-horizontal" method="GET" >
                                <div class="form-group">
                                    <label class="control-label col-md-1">Search</label>
                                    <div class="col-md-3">
                                        <input class="form-control" type="text" name="search" value="{{ $search }}" >
                                        <span class="help-block">Search by name</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-1 col-md-3 ">
                                        <button class="btn blue btn-sm" type="submit" ><i class="fa fa-search"></i> Search</button>
                                    </div>
                                </div>
                            </form>
                            <hr>

                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-green-sharp bold">สถานะใบแก้ไข <li class="fa fa-edit"></li> รอการยืนยัน จำนวน {{ $countData }} ฉบับ</span>
                                </div>
                            </div>

                            <div class="portlet-body">
                                <div class="table-responsive">

                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('ID',$pkField,$orderBy,$sortBy,$strParam) !!} </th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('ใบคำขอเลขที่','no',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('กรมธรรม์เลขที่','firstname',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-3 text-center">{!! $mainFn->sorting('รายละเอียด / สาเหตุ','registration',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('วันที่ขอ','created_at',$orderBy,$sortBy,$strParam) !!}</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @if($countData > 0)
                                            @foreach($data as $field)
                                                <tr>
                                                    <td class="text-center">{{ $field->$pkField }}</td>
                                                    <td class="text-center">{{ $field->no }}</td>
                                                    <td>{{ $field->firstname }}</td>
                                                    <td class="text-center">{{ $field->registration }}</td>
                                                    <td class="text-center">{{ $mainFn->format_date_th($field->created_at,4) }}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr><td colspan='9' class='text-center'>No Result.</td></tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                {!! $data->appends(Input::except('page'))->render() !!}
                            </div>

                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-green-sharp bold">สถานะใบแก้ไข <li class="fa fa-check"></li> ผ่านการยืนยัน จำนวน {{ $countData2 }} ฉบับ</span>
                                </div>
                            </div>

                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('ID',$pkField,$orderBy,$sortBy,$strParam) !!} </th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('ใบคำขอเลขที่','no',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('กรมธรรม์เลขที่','firstname',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-3 text-center">{!! $mainFn->sorting('รายละเอียด / สาเหตุ','license_plate',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('วันที่ยืนยัน','created_at',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('ผู้ยืนยัน','status',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center">Print</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @if($countData2 > 0)
                                            @foreach($data2 as $field)
                                                <tr>
                                                    <td class="text-center">{{ $field->$pkField }}</td>
                                                    <td class="text-center">{{ $field->no }}</td>
                                                    <td>{{ $field->firstname }}</td>
                                                    <td class="text-center">{{ $field->license_plate }}</td>
                                                    <td class="text-center">{{ $mainFn->format_date_th($field->created_at,4) }}</td>
                                                    <td>{{ $field->firstname }}</td>
                                                    <td class="text-center"> <a href="{{ URL::to('_admin/send_insurance/'.$field->$pkField.'/'.$strParam) }}" target="_blank"  class="btn btn-circle yellow btn-xs"><i class="fa fa-print"></i></a></td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr><td colspan='9' class='text-center'>No Result.</td></tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                {!! $data->appends(Input::except('page'))->render() !!}
                            </div>


                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-green-sharp bold">สถานะใบแก้ไข <li class="fa fa-close"></li> ไม่อนุมัติ จำนวน {{ $countData3 }} ฉบับ</span>
                                </div>
                            </div>

                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('ID',$pkField,$orderBy,$sortBy,$strParam) !!} </th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('ใบคำขอเลขที่','no',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('กรมธรรม์เลขที่','firstname',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-3 text-center">{!! $mainFn->sorting('รายละเอียด / สาเหตุ','license_plate',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('วันที่ยืนยัน','created_at',$orderBy,$sortBy,$strParam) !!}</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @if($countData3 > 0)
                                            @foreach($data3 as $field)
                                                <tr>
                                                    <td class="text-center">{{ $field->$pkField }}</td>
                                                    <td class="text-center">{{ $field->no }}</td>
                                                    <td>{{ $field->firstname }}</td>
                                                    <td class="text-center">{{ $field->license_plate }}</td>
                                                    <td class="text-center">{{ $mainFn->format_date_th($field->created_at,4) }}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr><td colspan='9' class='text-center'>No Result.</td></tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                {!! $data->appends(Input::except('page'))->render() !!}
                            </div>
                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
@endsection