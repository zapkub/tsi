<?php
use App\Http\Controllers\Backend\CompensationDetailController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new CompensationDetailController(); // New Object Controller
$mainFn = new MainFunction(); // New Object Main Function

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$path = $objCon->path;
$tb_name = $objCon->tbName;

$orderBy = Input::get('orderBy');
$sortBy = Input::get('sortBy');
$search = Input::get('search');
$cid = input::get('cid');

$a_otherParam = Input::except(['orderBy','sortBy']);
$strParam = $mainFn->parameter($a_otherParam);

$admin_id = auth()->guard('admin')->user()->admin_id;
if(!empty($admin_id)){
    // echo $mainFn->chk_permission($admin_id,'career');
}else{
    return Redirect::to('_admin/logout');
}
$compensation = App\Model\Compensation::where('compensation_id',$cid)->get();
foreach ($compensation as $temp) {
    $comp = $temp->title_th;
}
?>

@extends('admin')
@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>{{ $comp }}</h1>
                </div>
            </div>
        </div>

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->

                        <div class="portlet light">

                            <form action{{URL::to($path)}} class="form-horizontal" method="GET" >
                            <div class="form-group">
                                <label class="control-label col-md-1">Search</label>
                                <div class="col-md-3">
                                    <input class="form-control" type="text" name="search" value="{{ $search }}" >
                                    <span class="help-block">Search by title</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-1 col-md-3 ">
                                    <button class="btn blue btn-sm" type="submit" ><i class="fa fa-search"></i> Search</button>
                                </div>
                            </div>
                            </form>
                            <hr>

                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-database font-green-sharp"></i>
                                    <span class="caption-subject font-green-sharp bold">Found {{ $countData }} Record(s).</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('ID',$pkField,$orderBy,$sortBy,$strParam) !!} </th>

                                            <th class="col-md-2">{!! $mainFn->sorting('title_th','title_th',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-2">{!! $mainFn->sorting('title_en','title_en',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-2">{!! $mainFn->sorting('Sequence','sequence',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-2 text-center">
                                                <a href="{{ URL::to($path.'/create?cid='.$cid) }}" class="btn btn-circle blue btn-xs"><i class="fa fa-plus"></i> Add</a>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($countData > 0)
                                            @foreach($data as $field)
                                                <tr>
                                                    <td class="text-center">{{ $field->$pkField }}</td>
                                                    <td>{{ $field->title_th }}</td>
                                                    <td>{{ $field->title_en }}</td>
                                                    <td>
                                                        {!! Form::open(array('url'=>'_admin/sequence/compensationdetail' , 'method' => 'post' , 'id' => 'form' , 'class' => 'form-horizontal'  )) !!}

                                                        <div class="input-group">
                                                            <input type="text" class="form-control" name="new_sorting" value="{{$field->sequence}}">
                                                            <div class="input-group-btn">
                                                                <input type="hidden" name="{{$pkField}}" value="{{$field->$pkField}}">
                                                                <input type="hidden" name="old_sorting" value="{{$field->sequence}}">;
                                                                <button class="btn" type="submit"><i class="glyphicon glyphicon-resize-horizontal"></i></button>
                                                            </div>
                                                        </div>
                                                        {!! Form::close() !!}
                                                    </td>
                                                    <td class="text-center">
                                                        <a href="{{ URL::to($path.'/'.$field->$pkField.'/edit?'.$strParam) }}" class="btn btn-xs btn-circle green"><i class="fa fa-edit"></i></a>
                                                        <form action="{{URL::to($path.'/'.$field->$pkField)}}" method="post" class="frm-delete">
                                                            <input name="_method" type="hidden" value="delete">
                                                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                            <button type="submit" class="btn btn-xs btn-circle red" ><i class="fa  fa-trash-o"></i></button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr><td colspan='6' class='text-center'>No Result.</td></tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                {!! $data->appends(Input::except('page'))->render() !!}
                            </div>
                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <script src="{{ URL::asset('js/change-status.js') }}"type="text/javascript"></script>
@endsection
