<?php
use App\Http\Controllers\Backend\BannerClaimController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new BannerClaimController(); // New Object Controller
$mainFn = new MainFunction(); // New Object Main Function

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$path = $objCon->path;
$tb_name = $objCon->tbName;

$orderBy = Input::get('orderBy');
$sortBy = Input::get('sortBy');
$search = Input::get('search');

$a_otherParam = Input::except(['orderBy','sortBy']);
$strParam = $mainFn->parameter($a_otherParam);

$admin_id = auth()->guard('admin')->user()->admin_id;
if(!empty($admin_id)){
    // echo $mainFn->chk_permission($admin_id,'banner');
}else{
    return Redirect::to('_admin/logout');
}
?>
@extends('admin')
@section('content')
<div class="page-container">
    <!-- BEGIN PAGE HEAD -->
    <div class="page-head">
        <div class="container">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>{{ $titlePage }}</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
    </div>
    <!-- END PAGE HEAD -->

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container">
            <!-- BEGIN PAGE CONTENT INNER -->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light">
                        <div class="portlet-body form">

                          <?php
                            // $url_to = $this->path.'/'.$id;
                            $x = 2;
                          ?>
                            <form action="{{URL::to('_admin/banner_claim/')}}" enctype="multipart/form-data" method="POST" class="form-horizontal">
                                <input name="_method" type="hidden" value="POST">
                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                @foreach($data as $key => $claim)
                                <div class="form-body">
                                  @if($key == 0)
                                    <div class="form-group text-left" style="margin-left:20px;">
                                        <h3>Banner Claim</h3>
                                    </div>
                                  @elseif($key == 1)
                                    <div class="form-group text-left" style="margin-left:20px;">
                                        <h3>Banner E-Claim</h3>
                                    </div>
                                  @endif
                                </div>

                                <!-- <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3" for="status">Status</label>
                                        <div class="col-md-4">
                                            <select name="status[]"  class="form-control select2me">
                                                <option value="1" @if ($claim['status']=="1") selected @endif>Open</option>
                                                <option value="0" @if ($claim['status']=="0") selected @endif >Close</option>
                                            </select>
                                        </div>
                                    </div>
                                </div> -->

                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">URL</label>
                                        <div class="col-md-4">
                                            <input type="text" name="url[]"  class="form-control" value="{{$claim['url']}}"/>
                                        </div>
                                    </div>
                                </div>

                                <?php
                                $path = "uploads/banner_claim/".$claim['img_path'];
                                $path_img = URL::asset($path);
                                $check_path = public_path($path);

                                ?>

                                <div class="control-group ">
                                    <div class="controls">
                                      @if($key == 0)
                                        <div id="thumbnail" class="text-center">
                                          @elseif($key == 1)
                                          <div id="thumbnail2" class="text-center">
                                          @endif

                                            @if(file_exists($check_path) and !empty($claim['img_path']))
                                                <img src="{{ $path_img }}" id="img_path{{$x}}" style="max-width:400px;">
                                            @else
                                                <img src="" id="img_path{{$x}}" style="max-width:400px;">
                                            @endif

                                              @if($key == 0)
                                                <input type="hidden" name="img_path" id="img_path{{$key+1}}" value="<?php echo  $claim['img_path']?>">
                                              @elseif($key == 1)
                                                <input type="hidden" name="img_path_1" id="img_path{{$key+1}}" value="<?php echo  $claim['img_path']?>">
                                              @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Image</label>
                                        <div class="col-md-9">
                                          @if($key == 0)
                                            <div id="upload" class="btn blue"><i class="icon-picture"></i> Select File</div>
                                            <div id="remove_img" class="btn btn-danger" style="display:none;">Remove</div>
                                          @elseif($key == 1)
                                          <div id="upload2" class="btn blue"><i class="icon-picture"></i> Select File</div>
                                          <div id="remove_img" class="btn btn-danger" style="display:none;">Remove</div>
                                          @endif
                                            {{--<input type="file" name="img_path">--}}
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label"> </label>
                                    <div class="col-md-4">
                                        <span style="color:#F00">* หมายเหตุ</span><br>
                                                                <span style="">
                                                                - ไฟล์ต้องเป็นมีขนาดกว้าง 360x141 pixel. <br>
                                                                - นามสกุลของรูปภาพต้อง .png, .jpg เท่านั้น<br>
                                                                </span>
                                    </div>
                                </div>
                                <?php
                                $x =$x + 2;
                                ?>
                                @endforeach
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <input type="hidden" name="strParam" value="{{$strParam}}">
                                            <button type="submit" class="btn green">Update</button>
                                            <button type="reset" class="btn default">Reset</button>
                                        </div>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>

            <!-- END PAGE CONTENT INNER -->
        </div>
    </div>
    <!-- END PAGE CONTENT -->
</div>
<script src="{{URL::asset('js/elfinder-upload.js')}}"></script>

@endsection
