<?php
use App\Http\Controllers\Backend\TelephoneController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new TelephoneController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);

$admin_id = auth()->guard('admin')->user()->admin_id;
if(!empty($admin_id)){
    // echo $mainFn->chk_permission($admin_id,'product');
}else{
    return Redirect::to('_admin/logout');
}

?>



@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                {!! Form::open(array('url'=>$url_to , 'method' => $method , 'id' => 'form' , 'class' => 'form-horizontal','enctype'=>"multipart/form-data" )) !!}
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Name</label>
                                            <div class="col-md-4">
                                                <input type='text' name="name" id="name" class="form-control" value="{{$name}}" >
                                            </div>
                                            <div class="col-md-4">
                                                ( TH )
                                                </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Floor</label>
                                            <div class="col-md-4">
                                                <!-- <input type='text' name="floor" id="floor" class="form-control" value="{{$floor}}" > -->
                                                <select class="form-control select2me" data-placeholder="Select..." name="floor">
                                                    <option value="{{$floor}}" >{{$floor}}</option>
                                                    @for($i=0;$i<5;$i++)
                                                      @if($i+1 == $floor)

                                                      @else
                                                        <option value="{{$i+1}}" >{{$i+1}}</option>
                                                      @endif
                                                    @endfor
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <!-- ( EN ) -->
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Department</label>
                                            <div class="col-md-4">
                                                <!-- <input type='text' name="department" id="department" class="form-control" value="{{$department}}" > -->
                                                <select class="form-control select2me" data-placeholder="Select..." name="department">
                                                  <option value="{{$department}}" >{{$department}}</option>
                                                  <option value="รับแจ้งอุบัติเหตุ" >รับแจ้งอุบัติเหตุ</option>
                                                  <option value="สินไหมรถยนต์" >สินไหมรถยนต์</option>
                                                  <option value="ประเมินราคา" >ประเมินราคา</option>
                                                  <option value="สารบรรณ" >สารบรรณ</option>
                                                  <option value="ฝ่ายเรียกร้อง F.5" >ฝ่ายเรียกร้อง F.5</option>
                                                  <option value="กิจการพิเศษ และเรียกร้อง" >กิจการพิเศษ และเรียกร้อง</option>
                                                  <option value="ฝ่ายต่างประเทศ" >ฝ่ายต่างประเทศ</option>
                                                  <option value="รับประกันอัคคีภัย" >รับประกันอัคคีภัย</option>
                                                  <option value="รับประกันรถยนต์" >รับประกันรถยนต์</option>
                                                  <option value="รับประกันเบ็ดเตล็ด" >รับประกันเบ็ดเตล็ด</option>
                                                  <option value="บ. ESCO ที่ชั้น 3" >บ. ESCO ที่ชั้น 3</option>
                                                  <option value="คอมพิวเตอร์" >คอมพิวเตอร์</option>
                                                  <option value="ประเมินราคาซ่อม" >ประเมินราคาซ่อม</option>
                                                  <option value="ฝ่ายการเงินบัญชี" >ฝ่ายการเงินบัญชี</option>
                                                  <option value="ฝ่ายทรัพยากรมนุษย์" >ฝ่ายทรัพยากรมนุษย์</option>
                                                  <option value="ฝ่ายเร่งรัดหนี้สิน" >ฝ่ายเร่งรัดหนี้สิน</option>
                                                  <option value="ฝ่ายกฏหมาย" >ฝ่ายกฏหมาย</option>
                                                  <option value="ขยายงานการตลาด" >ขยายงานการตลาด</option>
                                                  <option value="ฝ่ายการเงินและบัญชี" >ฝ่ายการเงินและบัญชี</option>
                                                  <option value="ฝ่ายกรรมการบริหาร" >ฝ่ายกรรมการบริหาร</option>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <!-- ( TH ) -->
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Telephone</label>
                                            <div class="col-md-4">
                                                <input type='text' name="telephone" id="telephone" class="form-control" value="{{$telephone}}" >
                                            </div>
                                            <div class="col-md-4">
                                                <!-- ( TH ) -->
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Fax</label>
                                            <div class="col-md-4">
                                                <input type='text' name="fax" id="fax" class="form-control" value="{{$fax}}" >
                                            </div>
                                            <div class="col-md-4">
                                                <!-- ( EN ) -->
                                            </div>
                                        </div>
                                    </div>




                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                {!! Form::close() !!}

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>

@endsection

@section('js')
    <script src="{{URL::asset('js/category-upload.js')}}"></script>
    <script src="{{URL::asset('assets/admin/scripts/validate/telephone.js')}}"></script>
@endsection
