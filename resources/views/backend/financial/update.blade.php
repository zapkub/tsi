<?php
use App\Http\Controllers\Backend\FinancialController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new FinancialController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);

$admin_id = auth()->guard('admin')->user()->admin_id;
if(!empty($admin_id)){
    // echo $mainFn->chk_permission($admin_id,'about_company');
}else{
    return Redirect::to('_admin/logout');
}
?>
@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                {!! Form::open(array('url'=>$url_to , 'method' => $method , 'id' => 'form' , 'class' => 'form-horizontal','enctype'=>"multipart/form-data" )) !!}
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">ปี</label>
                                            <div class="col-md-4">
                                                <select class="form-control " data-placeholder="Select..." id="financial_years" name="financial_years" data-type="financial_years">
                                                    <option value="">เลือกปี</option>
                                                    <?PHP
                                                    $xyear = date("Y")+543;
                                                    $xyear = $xyear - 5;
                                                    ?>
                                                    @for($i=date("Y")+543; $i>=$xyear; $i--)
                                                        <option value="{{ $i }}" @if($i==$financial_years) selected @endif >{{ $i }}</option>
                                                    @endfor
                                                </select>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">ไตรมาส</label>
                                            <div class="col-md-4">
                                                <select class="form-control " data-placeholder="Select..." id="quarter" name="quarter" data-type="quarter">
                                                    <option value="">เลือกไตรมาส</option>
                                                    <option value="1" @if($quarter==1) selected @endif>1</option>
                                                    <option value="2" @if($quarter==2) selected @endif>2</option>
                                                    <option value="3" @if($quarter==3) selected @endif>3</option>
                                                    <option value="4" @if($quarter==4) selected @endif>4</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Name ( TH )</label>
                                            <div class="col-md-4">
                                                <input type="text" name="name_th"  class="form-control" value="{{ $name_th }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Name ( EN )</label>
                                            <div class="col-md-4">
                                                <input type="text" name="name_en"  class="form-control" value="{{ $name_en }}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">PDF ( TH )</label>
                                            <div class="col-md-4">

                                                @if($pdf_path != '')
                                                    <a href="{{ '/uploads/financial_pdf/'.$pdf_path }}" id="pdf_name" class="btn btn-xs btn-circle blue" target="_blank"><i class="fa fa-file"></i></a>
                                                    <button type="button" class="btn btn-xs btn-circle red" id="remove_pdf"><i class="fa  fa-trash-o"></i></button>
                                                    <input type="file" name="pdf_path" id="upload_pdf" style="display:none;" >
                                                @else
                                                    <input type="file" name="pdf_path" id="upload_pdf">

                                                    <button type="button" class="btn btn-xs btn-circle red" id="remove_pdf" style="display:none;"><i class="fa  fa-trash-o"></i></button>

                                                @endif
                                                <input type="hidden" name="pdf_path" id="pdf_path" value="<?php echo  $pdf_path?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">PDF ( EN )</label>
                                            <div class="col-md-4">

                                                @if($pdf_path_en != '')
                                                    <a href="{{ '/uploads/financial_pdf_en/'.$pdf_path_en }}" id="pdf_name1" class="btn btn-xs btn-circle blue" target="_blank"><i class="fa fa-file"></i></a>
                                                    <button type="button" class="btn btn-xs btn-circle red" id="remove_pdf1"><i class="fa  fa-trash-o"></i></button>
                                                    <input type="file" name="pdf_path_en" id="upload_pdf1" style="display:none;" >
                                                @else
                                                    <input type="file" name="pdf_path_en" id="upload_pdf1">

                                                    <button type="button" class="btn btn-xs btn-circle red" id="remove_pdf1" style="display:none;"><i class="fa  fa-trash-o"></i></button>

                                                @endif
                                                <input type="hidden" name="pdf_path_en" id="pdf_path_en" value="<?php echo  $pdf_path_en?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                {!! Form::close() !!}

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>

@endsection

@section('js')
    <script src="{{URL::asset('js/elfinder-upload.js')}}"></script>
    <script src="{{URL::asset('assets/admin/scripts/validate/financial.js')}}"></script>
@endsection