<?php
use App\Http\Controllers\Backend\StaffController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new StaffController();
$mainFn = new MainFunction();

$user_title = App\Model\UserTitle::whereNull('deleted_at')->get();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);
?>

@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" class="form-horizontal">
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Title</label>
                                            <div class="col-md-4">
                                                <select class="form-control " data-placeholder="Select..." id="title" name="title" data-type="title">
                                                    <option value="">กรุณาเลือกหมวดหมู่</option>
                                                    @foreach($user_title as $cate)
                                                        <option value="{{$cate->user_title_id}}" <?php if($title==$cate->user_title_id )echo "selected"; ?>  >{{$cate->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Firstname</label>
                                            <div class="col-md-4">
                                                <input type="text" name="firstname"  class="form-control" value="{{ $firstname }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Lastname</label>
                                            <div class="col-md-4">
                                                <input type="text" name="lastname"  class="form-control" value="{{ $lastname }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Address 1</label>
                                            <div class="col-md-4">
                                                <textarea name="address1" id="address1" class="form-control">{{ $address1 }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Address 2</label>
                                            <div class="col-md-4">
                                                <textarea name="address2" id="address2" class="form-control">{{ $address2 }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Position</label>
                                            <div class="col-md-4">
                                                <input type="text" name="position"  class="form-control" value="{{ $position }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Department</label>
                                            <div class="col-md-4">
                                                <input type="text" name="department"  class="form-control" value="{{ $department }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Branch</label>
                                            <div class="col-md-4">
                                                <input type="text" name="branch"  class="form-control" value="{{ $branch }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Phone</label>
                                            <div class="col-md-4">
                                                <input type="text" name="phone"  class="form-control" value="{{ $phone }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Fax</label>
                                            <div class="col-md-4">
                                                <input type="text" name="fax"  class="form-control" value="{{ $fax }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Ext</label>
                                            <div class="col-md-4">
                                                <input type="text" name="ext"  class="form-control" value="{{ $ext }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Mobile</label>
                                            <div class="col-md-4">
                                                <input type="text" name="mobile"  class="form-control" value="{{ $mobile }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Email</label>
                                            <div class="col-md-4">
                                                <input type="text" name="email"  class="form-control" value="{{ $email }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Line ID</label>
                                            <div class="col-md-4">
                                                <input type="text" name="line_id"  class="form-control" value="{{ $line_id }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Map</label>
                                            <div class="col-md-4">
                                                <input type="text" name="map"  class="form-control" value="{{ $map }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Username</label>
                                            <div class="col-md-4">
                                                <input type="text" name="username"  class="form-control" value="{{ $username }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Password</label>
                                            <div class="col-md-4">
                                                @if($method=="PUT")
                                                    <input type="password" name="new_password"  class="form-control" value=""/>
                                                    <input type="hidden" name="password"  class="form-control" value="{{ $password }}"/>
                                                @else
                                                    <input type="password" name="password"  class="form-control" value=""/>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <script src="{{URL::asset('js/elfinder-upload.js')}}"></script>
@endsection