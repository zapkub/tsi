<?php
use App\Http\Controllers\Backend\CsrImgController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new CsrImgController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}


$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);

?>

@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                {!! Form::open(array('url'=>$url_to , 'method' => $method , 'id' => 'form' , 'class' => 'form-horizontal','enctype'=>"multipart/form-data" )) !!}

                                <div class="control-group ">
                                    <div class="controls">
                                        <div id="thumbnail" class="text-center">
                                            @if($img_path != '')
                                                <img src="{{URL::asset('uploads/csr_img/'.$img_path) }}" id="img_path2" style="max-width:400px;">
                                            @else
                                                <img src="" id="img_path2" style="max-width:400px;">
                                            @endif

                                            <input type="hidden" name="img_path" id="img_path" value="<?php echo  $img_path?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Image</label>
                                        <div class="col-md-4">
                                            @if($img_path != '')
                                                <div id="remove_img" class="btn btn-danger" >Remove</div>
                                                <div id="upload" class="btn blue" style="display:none;"><i class="icon-picture"></i> Select File</div>

                                            @elseif ($img_path == '')
                                                <div id="upload" class="btn blue" ><i class="icon-picture"></i> Select File</div>
                                                <div id="remove_img" class="btn btn-danger" style="display:none;" >Remove</div>

                                            @endif

                                        </div>
                                    </div>
                                </div>

                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <input type="hidden" name="csr_id" value="{{Input::get('csr_id')}}">
                                            <input type="hidden" name="strParam" value="{{$strParam}}">
                                            <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                            <button type="reset" class="btn default">Reset</button>
                                        </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <script src="{{URL::asset('js/elfinder-upload.js')}}"></script>
@endsection