<?php
use App\Http\Controllers\Backend\CsrImgController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new CsrImgController(); // New Object Controller
$mainFn = new MainFunction(); // New Object Main Function

$tbName = $objCon->tbName;
$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$path = $objCon->path;

$orderBy = Input::get('orderBy');
$sortBy = Input::get('sortBy');
$search = Input::get('search');

$a_otherParam = Input::except(['orderBy','sortBy']);
$strParam = $mainFn->parameter($a_otherParam);

$csr_id = Input::get('csr_id');

?>
@extends('admin')
@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>{{ $titlePage }}</h1>
                </div>
            </div>
        </div>

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->

                        <div class="portlet light">

                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-database font-green-sharp"></i>
                                    <span class="caption-subject font-green-sharp bold">Found {{ $countData }} Record(s).</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('ID',$pkField,$orderBy,$sortBy,$strParam) !!} </th>
                                            <th class="col-md-6">{!! $mainFn->sorting('Img Path','img_path',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-3 text-center">{!! $mainFn->sorting('Sorting','sorting',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-2 text-center">
                                                <a href="{{ URL::to($path.'/create?&csr_id='.$csr_id) }}" class="btn btn-circle blue btn-xs"><i class="fa fa-plus"></i> Add</a>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($countData > 0)
                                            @foreach($data as $field)
                                                <tr>
                                                    <td class="text-center">{{ $field->$pkField }}</td>
                                                    <td><img src="{{URL::asset('uploads/'.$tbName.'/100/'.$field->img_path)}}" ></td>
                                                    <td>
                                                        {!! Form::open(array('url'=>'_admin/sequence/csrsequence' , 'method' => 'post' , 'id' => 'form' , 'class' => 'form-horizontal'  )) !!}

                                                        <div class="input-group">
                                                            <input type="text" class="form-control " name="new_sequence" value="{{$field->sequence}}">
                                                            <div class="input-group-btn">
                                                                <input type="hidden" name="{{$pkField}}" value="{{$field->$pkField}}">
                                                                <input type="hidden" name="csr_id" value="{{$field->csr_id}}">
                                                                <input type="hidden" name="old_sequence" value="{{$field->sequence}}">;
                                                                <button class="btn" type="submit"><i class="glyphicon glyphicon-resize-horizontal"></i></button>
                                                            </div>
                                                        </div>
                                                        {!! Form::close() !!}
                                                    </td>
                                                    <td class="text-center">
                                                        <a href="{{ URL::to($path.'/'.$field->$pkField.'/edit?'.$strParam) }}" class="btn btn-xs btn-circle green"><i class="fa fa-edit"></i></a>
                                                        {!! Form::open(array('url' => $path.'/'.$field->$pkField, 'method' => 'delete' , 'class' => 'frm-delete')) !!}
                                                        <button type="submit" class="btn btn-xs btn-circle red" ><i class="fa  fa-trash-o"></i></button>
                                                        {!! Form::close() !!}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr><td colspan='5' class='text-center'>No Result.</td></tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                {!! $data->appends(Input::except('page'))->render() !!}
                            </div>
                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
@endsection