<?php
use App\Http\Controllers\Backend\PetitionController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new PetitionController(); // New Object Controller
$mainFn = new MainFunction(); // New Object Main Function
$r_time = new \App\Model\ContactMsg();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$path = $objCon->path;
$tb_name = $objCon->tbName;


$orderBy = Input::get('orderBy');
$sortBy = Input::get('sortBy');
$search = Input::get('search');

$a_otherParam = Input::except(['orderBy','sortBy']);
$strParam = $mainFn->parameter($a_otherParam);
$a_status = Config::get('menuList.status');
$i = 1;

$admin_id = auth()->guard('admin')->user()->admin_id;
if(!empty($admin_id)){
    // echo $mainFn->chk_permission($admin_id,'career');
}else{
    return Redirect::to('_admin/logout');
}
?>
@extends('admin')
@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>{{ $titlePage }}</h1>
                </div>
            </div>
        </div>

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->

                        <div class="portlet light">

                            <form action{{URL::to($path)}} class="form-horizontal" method="GET" >
                                <div class="form-group">
                                    <label class="control-label col-md-1">Search</label>
                                    <div class="col-md-3">
                                        <input class="form-control" type="text" name="search" value="{{ $search }}" >
                                        <span class="help-block">Search by Name , Email</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-1 col-md-3 ">
                                        <button class="btn blue btn-sm" type="submit" ><i class="fa fa-search"></i> Search</button>
                                    </div>
                                </div>
                            </form>
                            <hr>

                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-database font-green-sharp"></i>
                                    <span class="caption-subject font-green-sharp bold">Found {{ $countData }} Record(s).</span>
                                </div>
                            </div>
                            <div class="portlet-body">

                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>

                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('ID',$pkField,$orderBy,$sortBy,$strParam) !!} </th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('Name','name',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('E-mail','email',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-3 text-center">{!! $mainFn->sorting('Message','msg',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-2 text-center">{!! $mainFn->sorting('Reply Time','reply_time',$orderBy,$sortBy,$strParam) !!}</th>

                                            <th class="col-md-1 text-center font-blue-sharp">
                                                Reply
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($countData > 0)
                                            @foreach($data as $field)
                                                <tr>
                                                    <td class="text-center">{{ $field->$pkField}}</td>
                                                    <td>{{ $field->name }}</td>
                                                    <td>{{ $field->email }}</td>
                                                    <td>{!! $field->msg !!}</td>
                                                    @if($field->status !='0')
                                                        <td class="text-center">{{$r_time->ReplyTime($field->contact_msg_id)}}</td>

                                                    @else
                                                        <td><?php echo' '?></td>
                                                    @endif
                                                    @if($field->status == 0)
                                                        <td class="text-center"><a href="{{ URL::to($path.'/create?'.'id'.'='.$field->$pkField.$strParam) }}" class="btn btn-xs btn-circle blue"><i class="fa fa-share"></i></a></td>
                                                    @else
                                                        <td class="text-center"><a href="{{URL::to($path.'/'.$field->$pkField.$strParam)}}" class="btn btn-xs btn-circle green" ><i class="fa fa-file-text-o"></i></a></td>

                                                    @endif
                                                </tr>



                                            @endforeach
                                        @else
                                            <tr><td colspan='7' class='text-center'>No Result.</td></tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                {!! $data->appends(Input::except('page'))->render() !!}
                            </div>
                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>

@endsection

