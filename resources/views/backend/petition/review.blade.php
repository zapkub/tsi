<?php
use App\Http\Controllers\Backend\PetitionController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new PetitionController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$path = $objCon->path;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);

$admin_id = auth()->guard('admin')->user()->admin_id;
if(!empty($admin_id)){
    // echo $mainFn->chk_permission($admin_id,'career');
}else{
    return Redirect::to('_admin/logout');
}

?>
@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" id="form-character" class="form-horizontal">
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                    <div class="portlet light">
                                        <div class="portlet-body">
                                            <div class="invoice">
                                                <div class="row invoice-logo">
                                                    <div class="col-xs-6 invoice-logo-space">
                                                        <img src="{{ URL::asset('assets/images/logo.gif') }}" class="img-responsive" alt=""/>
                                                    </div>
                                                    <br><br><hr>
                                                    <div class="text-right bold" >Time reply message : {{ $r_time_send }}</div>

                                                    <div class="row">
                                                        <div class="col-xs-4">
                                                            <h2><strong>Contact Message</strong></h2>
                                                            <ul class="list-unstyled">
                                                                <li>
                                                                    Name  :  {{ $c_name }}
                                                                </li>

                                                                <li>
                                                                    E-mail :  {{ $c_email }}
                                                                </li>
                                                                <li>
                                                                    Mobile :  {{ $c_mobile }}
                                                                </li>
                                                                <li>
                                                                    Contact Message  :  {{ $c_msg }}
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="col-xs-4">

                                                        </div>
                                                        <div class="col-xs-4 invoice-payment">
                                                            <h2><strong>Reply Message</strong></h2>
                                                            <ul class="list-unstyled">
                                                                <li>
                                                                    Name  :  {{ $r_name }}
                                                                </li>

                                                                <li>
                                                                    E-mail :  {{ $r_email }}
                                                                </li>
                                                                <li>
                                                                    Mobile :  {{ $r_mobile }}
                                                                </li>
                                                                <li>
                                                                    Reply Message  :  {{ $r_msg }}
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-11">
                                                <input type="hidden" name="id" value="{{Input::get('id')}}">
                                                <a href="{{ URL::to($path. '?' . $strParam) }}" class="btn blue">Back</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <script src="{{URL::asset('js/elfinder-upload.js')}}"></script>

    <script>
        $(function() {
            $( "#post_datetime" ).datetimepicker();

        });
    </script>

@endsection

