<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Potters</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{URL::asset('bootstrap-scolling/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{URL::asset('bootstrap-scolling/css/scrolling-nav.css')}}" rel="stylesheet">

    <link href="{{URL::asset('css/mainStyle.css')}}" rel="stylesheet">
    <link rel="shortcut icon" href="{{ url()->asset('images/favicon.ico') }}" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery -->
    <script src="{{URL::asset('bootstrap-scolling/js/jquery.js')}}"></script>

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

@include('frontend.include.header')

@yield('content')

@include('frontend.include.footer')


<!-- Bootstrap Core JavaScript -->
<script src="{{URL::asset('bootstrap-scolling/js/bootstrap.min.js')}}"></script>

<!-- Scrolling Nav JavaScript -->
<script src="{{URL::asset('bootstrap-scolling/js/jquery.easing.min.js')}}"></script>
<script src="{{URL::asset('bootstrap-scolling/js/scrolling-nav.js')}}"></script>


</body>

</html>
