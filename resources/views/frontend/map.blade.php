<html>
  <head>
    <title>Surveyors</title>
    <style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #map {
        height: 100%;
      }
    </style>
  </head>

  <body>
    <div id="map"></div>
  </body>

  <script>
  function initMap() {
      var centerLatLng = { lat: 13.719272, lng: 100.517900 };

      var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        center: centerLatLng
      });

      @foreach ($surveyors as $surveyor)
      var marker = new google.maps.Marker({
        position: { lat: <?php echo $surveyor->latitude; ?>, lng: <?php echo $surveyor->longitude ?> },
        map: map,
        title: '{{ $surveyor->contact_name }}'
      });
      @endforeach
  }
  </script>
  <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDSHM_Qe7iA9JxQaB6jePBAg_43V-Tzw3U&callback=initMap">
    </script>
</html>
