<header>
    <div class="container">
      <a href="{{ URL::route('home') }}" id="logo">
        <img src="{{ URL::asset('images/logo4.png') }}" class="image" />
        <span class="text visible-lg">
          บริษัท ไทยเศรษฐกิจประกันภัย จำกัด (มหาชน) <br>
          <div class="sub-text">The Thai Setakij Insurance Public Company Limited</div>
        </span>
      </a>

      <div id="nav-bar">
        <i class="glyphicon glyphicon-menu-hamburger"></i>

        <nav>
          <div class="nav-item">
            <a href="#">{{trans('navbar.product')}}</a>

            <div class="sub-nav">
              @foreach ($global_categories as $cat)
              <a href="{{ URL::route('product.category-detail', $cat->category_id) }}">{{ $cat['category_name_' . session()->get('locale')] }}</a>
              @endforeach
            </div>
          </div>

          <div class="nav-item">
            <a href="#">{{ trans('navbar.claiming') }}</a>

            <div class="sub-nav">
              <a href="{{ URL::to('motor_claims') }}">สินไหมรถยนต์</a>
              <a href="{{ URL::to('other_claims') }}">สินไหมอื่นๆ</a>
              <!-- <a href="{{ URL::to('what-to-do') }}">ขั้นตอนการแจ้งอุบัติเหตุ</a> -->
              <!-- <a href="{{ URL::to('claim') }}">ขั้นตอนการเคลมประกัน</a> -->
              <!-- <a href="{{ URL::to('compensation') }}">ขั้นตอนการเรียกร้องค่าสินไหมทดแทน</a> -->
            </div>
          </div>

          <div class="nav-item">
            <a href="{{ URL::to('service') }}">{{ trans('navbar.service') }}</a>

            <!-- <div class="sub-nav">
              <a href="{{ URL::to('service') }}">ค้นหาศูนย์บริการ</a>
              <a href="{{ URL::to('service') }}">ค้นหาศูนย์ซ่อม</a>
              <a href="{{ URL::to('service') }}">ค้นหาอู่ซ่อม</a>
            </div> -->
          </div>

          <a href="{{ URL::route('about_us.history') }}" class="nav-item another-nav-item">เกี่ยวกับบริษัท</a>
          <div class="nav-item visible-xs">
            <a href="#">ติดต่อบริษัท</a>
            <div class="sub-nav">
              <a href="{{ URL::to('contact') }}">สำนักงานใหญ่</a>
              <a href="{{ url()->to('petition') }}">ส่งเรื่องร้องเรียน</a>
              <a href="{{ URL::route('career.list') }}">สมัครงาน</a>
            </div>
          </div>
          <div class="nav-item visible-xs">
            <a href="#">ข่าวสาร</a>
            <div class="sub-nav">
              <a href="{{ URL::to('news') }}">ข่าวประกาศต่างๆ</a>
              <a href="{{ URL::to('download') }}">ดาวน์โหลดเอกสาร</a>
            </div>
          </div>


        </nav>
      </div>

      <div id="lang-bar" class="language-container ">
          <!-- <a class="langChange {{session()->get('locale') == 'en'? 'active':''}}" href="{{URL::to('lang/en')}}" >EN</a> | <a href="{{URL::to('lang/th')}}" class="langChange {{session()->get('locale') == 'th'? 'active':''}}">TH</a> -->
      </div>

      <a href="{{ url()->to('contact') }}" id="emergency_button" class="button -default hidden-xs hidden-sm">
          <i class="glyphicon glyphicon-earphone"></i> แจ้งอุบัติเหตุ 1352
      </a>
    </div>
</header>
