
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>TSI Insurance -  @yield('title')</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <!-- Sass compile result-->
      <link href="{{URL::asset('css/front/app.css')}}" rel="stylesheet">
    <link rel="shortcut icon" href="{{ url()->asset('images/favicon.ico') }}" />

      @yield('more-stylesheet')
    <!-- END Sass -->

    {{-- Fonts --}}
      <link href="{{URL::asset('fonts/helvethaica/stylesheet.css')}}" rel="stylesheet">
      <link href="{{URL::asset('fonts/sarabun/fontface.css')}}" rel="stylesheet">
    {{-- END Fonts --}}

  </head>

  <body>
    @include('frontend.layouts.header')

    <div id="ui-main">
      @yield('content')
    </div>

    @include('frontend.layouts.footer')
  </body>

  <!-- Import jQuery -->
  <script src="{{ URL::asset('js/jquery.min.js') }}"></script>

  <!-- Import script from browserify -->

  <!-- END -->

  <script type="text/javascript">
    $(function(){
      $('#nav-bar i.glyphicon-menu-hamburger').on('click', function(){
        if ($('#nav-bar').hasClass('focused'))
          $('#nav-bar').blur().removeClass('focused');
        else
          $('#nav-bar').focus().addClass('focused');
      });
    });
  </script>
  @yield('more-script')

</html>
