<footer>
    <div id="full-footer" class="container hidden-xs hidden-sm">
        <div class="col-md-2">
            <div class="headline">ผลิตภัณฑ์</div>
            <div class="list">
                @foreach ($global_categories as $cat)
                <a href="{{ URL::route('product.category-detail', $cat->category_id) }}" class="item">{{ $cat['category_name_' . session()->get('locale')] }}</a>
                @endforeach
            </div>
        </div>
        <div class="col-md-2">
            <div class="headline">บริการด้านสินไหม</div>
            <div class="list">
                <a href="{{ URL::to('motor_claims') }}" class="item">สินไหมรถยนต์</a>
                <a href="{{ URL::to('other_claims')}}" class="item">สินไหมอื่นๆ</a>
            </div>
        </div>

        <div class="col-md-2">
          <div class="headline">บริการลูกค้าของเรา</div>
          <div class="list">
              <a href="{{ URL::to('service') }}" class="item">ค้นหาศูนย์บริการ</a>
              <a href="{{ URL::to('service') }}" class="item">ค้นหาศูนย์ซ่อม</a>
              <a href="{{ URL::to('service') }}" class="item">ค้นหาอู่ซ่อม</a>
          </div>
          <div class="headline">ติดต่อบริษัท</div>
          <div class="list">
              <a href="{{ URL::to('contact') }}" class="item">สำนักงานใหญ่</a>
              <a href="{{ url()->to('petition') }}" class="item">ส่งเรื่องร้องเรียน</a>
              <a href="{{ URL::route('career.list') }}" class="item">สมัครงาน</a>
          </div>
        </div>

        <div class="col-md-2">
            <div class="headline">เกี่ยวกับบริษัท</div>
            <div class="list">
                <a href="{{ URL::route('about_us.history') }}" class="item">ประวัติบริษัท</a>
                <a href="{{ URL::route('about_us.vision') }}" class="item">วิสัยทัศน์และพันธกิจ</a>
                {{--<a href="{{ URL::route('about_us.manager') }}" class="item">คณะกรรมการบริหาร</a>--}}
                <a href="{{ URL::route('about_us.manager') }}" class="item">คณะผู้บริหาร</a>
                <a href="{{ URL::route('about_us.annual_report') }}" class="item">รายงานประจำปี</a>
                <a href="{{ URL::route('about_us.overall_operation') }}" class="item">ฐานะการเงินและผลการดำเนินงาน</a>

            </div>
        </div>

        <div class="col-md-2">
            <div class="headline">ข่าวสารและกิจกรรม</div>
            <div class="list">
                <a href="{{ URL::to('news') }}" class="item">ข่าวประกาศต่างๆ</a>
                <a href="{{ URL::to('download') }}" class="item">ดาวน์โหลดเอกสาร</a>
            </div>
            <div class="headline">สำหรับพนักงาน</div>
            <div class="list">
                <!-- <a href="{{ URL::route('career.list') }}" class="item">สมัครงาน</a> -->
                <a href="{{ URL::to('_admin/login') }}" class="item">ระบบอินทราเน็ต</a>
                <a href="{{ URL::to('_agent') }}" class="item">{{trans('index.agent')}}</a>
            </div>


        </div>

        <div class="col-md-2">
          <div class="social">
            <a href="https://www.facebook.com/Thaisetakijinsurance" class="" target="_blank">
              <img class="facebook" src="{{URL::asset('images/dd_ff.png')}}" ></img>
            </a>
            <a href="#" class="" target="_blank">
              <img class="" src="{{URL::asset('images/dd-tw.png')}}" ></img>
            </a>
            <a href="#" class="" target="_blank">
              <img class="facebook" src="{{URL::asset('images/dd-ig.png')}}" ></img>
            </a>
            <a href="#" class="" target="_blank">
              <img class="facebook" src="{{URL::asset('images/dd-gg.png')}}" ></img>
            </a>
          </div>




            <a href="{{ url()->to('contact') }}" class="button -default">
                <i class="glyphicon glyphicon-earphone"></i> แจ้งอุบัติเหตุ 1352
            </a>
            @foreach($bannerClaim as $key => $claim)
              @if($key == 0)
                <a href="{{$claim['url']}}" class="button" target="_blank">
                    <img class="img-responsive" src="{{URL::asset('uploads/banner_claim/' . $claim['img_path'])}}">
                  <!-- <img src="{{URL::asset('images/E-survey.png')}}"> -->

                </a>
              @elseif($key == 1)
                <a href="{{$claim['url']}}" class="button" target="_blank">
                    <!-- <img src="{{URL::asset('images/E-claim.png')}}"> -->
                    <img class="img-responsive" src="{{URL::asset('uploads/banner_claim/' . $claim['img_path'])}}">
                </a>
              @endif
            @endforeach
        </div>
    </div>

    <div id="wrap-footer" class="container hidden-md hidden-lg">
        บริษัท ไทยเศรษฐกิจประกันภัย จำกัด มหาชน <br>
        <div class="sub-text">The Thai Setakij Insurance Public Company Limited</div>
    </div>
</footer>
