<?php
$lang = session()->get('locale');
?>
@extends('frontend.layouts/main')

@section('title','Home')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('tools/bxslider/jquery.bxslider.css') }}"/>
  <link rel="stylesheet" href="{{ URL::asset('css/front/home.css') }}"/>
@endsection

@section('content')
<div id="banner-top">

  <div class="container">
    <ul>
        @foreach ($banners as $banner)
        <li>
            <a href="{{ $banner->url }}" target="_blank">

                <img class="hidden-xs" src="{{ URL::asset('uploads/banner/' . $banner->img_path) }}">
                <img class="visible-xs" src="{{ URL::asset('uploads/banner/' . $banner->img_path_mobile) }}">

            </a>
        </li>
        @endforeach
    </ul>
  </div>
</div>
<!-- <div id="banner">
  <div class="container">
    <div class="col-xs-12 col-sm-6 col-md-offset-1 col-lg-offset-2">
      <div class="content">
        {{ Form::open(array('url' => '')) }}
        <div class="instruction">
          {{ trans('index.campaign') }}
        </div>

        <div class="form-row">
          <label class="dropdown col-xs-12 col-md-7">
            <select name="budget" id="search-budget" class="select">
              <option value="0" disabled selected>{{ trans('index.campaign_budget') }}</option>
              <option value="1">0 - 1,000 บาท</option>
              <option value="2">1,001 - 2,000 บาท</option>
              <option value="3">2,001 - 5,000 บาท</option>
              <option value="4">5,001 - 10,000 บาท</option>
            </select>
          </label>
        </div>

        <div class="form-row">
          <label class="dropdown col-xs-12 col-md-7">
            <select name="car-type" id="search-car-type" class="select">
              <option value="0" disabled selected>{{ trans('index.campaign_car') }}</option>
              <option value="1">รถยนต์ส่วนตัว</option>
              <option value="2">รถยนต์รับจ้าง</option>
              <option value="3">มอเตอร์ไซค์</option>
            </select>
          </label>
        </div>

        <div class="form-row">
          <button type="submit" class="button -default col-xs-12 col-md-7">{{ trans('index.btn_search') }} <i class="glyphicon glyphicon-chevron-right"></i></button>
        </div>
        {{ Form::close() }}
      </div>
    </div>
    <div class="col-xs-12 col-sm-4 col-sm-offset-1 col-md-offset-0 col-lg-1 hidden-xs">
      <div class="image-div">
        <img src="{{ URL::asset('images/home-banner-01.png') }}" />
      </div>
    </div>
  </div>
</div> -->

<a href="{{ URL::to('what-to-do') }}" id="quick-faq-bar" class="button -default">
  <i class="glyphicon glyphicon-comment"></i> {{ trans('index.have_accident') }}
</a>

<div id="products" class="container">
  <div class="headline">
    <div class="main">Get Insurance for Anything You Need</div>
    <div class="sub">{{ trans('index.select_insurance') }}</div>
  </div>

  <div class="list">
    @foreach ($global_categories as $key => $cat)
    <div class="product col-xs-6 col-sm-4 col-md-2">
      <a href="{{ URL::route('product.category-detail', $cat->category_id) }}" class="icon square button -default">
        <img src="{{ URL::asset('images/home-icon-' . $cat->icon_name . '-hover.png') }}" alt="{{ $cat['category_name_' . session()->get('locale')] }}" class="img-hover" />
        <img src="{{ URL::asset('images/home-icon-' . $cat->icon_name . '.png') }}" alt="{{ $cat['category_name_' . session()->get('locale')] }}" class="img-normal" />
      </a>

      <div class="info">
        <div class="arrow"></div>

        <a href="{{ URL::route('product.category-detail', $cat->category_id) }}" class="name">{{ $cat['category_name_' . session()->get('locale')] }}</a>
      </div>
    </div>
    @if (($key+1)%2 == 0)
        <div class="clearfix visible-xs-block"></div>
    @endif
    @if (($key+1)%3 == 0)
        <div class="clearfix visible-sm-block"></div>
    @endif
    @endforeach
  </div>
</div>

@if (count($intro_banners) > 0)
<div id="highlight">
  <div class="container">
    <div class="headline">
      {{ trans('index.insurance_type') }}
    </div>

    <div class="banner bxSlider">
      @foreach ($intro_banners as $banner)
      <a href="{{ $banner->url }}"><img src="{{ URL::asset('uploads/intro_page/' . $banner->img_path) }}" /></a>
      @endforeach
    </div>
  </div>
</div>
@endif

<div id="news">
  <div class="container">
    <div class="headline">{{ trans('index.news_activity') }} <span class="view-all"><a href="{{ url()->to('news') }}">ดูทั้งหมด</a></span></div>

    <div class="list">
      <div class="row">
        @foreach ($news as $key => $news_item)
          <?php
            $news_name = $news_item->name_th;
            $news_des = $news_item->short_description_th;
            $img_path = $news_item->img_path;
            $url_detail = URL::to('news/'.$news_item->news_id);
            if($lang == 'en'){
              if($news_item->name_en != '') $news_name = $news_item->name_en;
              if($news_item->short_description_en != '') $news_des = $news_item->short_description_en;
            }
            if($img_path != '' && file_exists('uploads/news/'.$img_path)){
              $img_path = URL::asset('uploads/news/'.$img_path);
            }else{
              $img_path = 'http://dummyimage.com/300x140/f0f0f0/d9d9d9.jpg&text=TSI';
            }
          ?>
          <div class="news-item col-sm-4">
            <a href="{{ $url_detail }}" title="{{ $news_name }}" >
              <div class="image" style="background-image: url('{{ $img_path }}')" >
            </div>
            </a>
            <div class="title"><a href="{{ $url_detail }}" title="{{ $news_name }}">{{ $news_name }}</a></div>
            <div class="description">
              {{ $news_des }}... <a href="{{ $url_detail }}" class="read-more">{{ trans('index.read_more') }}</a>
            </div>
          </div>

          @if (($key+1)%3==0)
        </div>
        <div class="row">
          @endif
        @endforeach
      </div>
    </div>
  </div>
</div>

<div id="footer-banners">
  <div class="container">
    <div class="row">
      @if(count($banners_footer) > 0)
        @foreach($banners_footer as $bf)
          <?php
          $img_path = $bf->img_path;
          if($img_path != '' && file_exists('uploads/banner_footer/'.$img_path)){
            $img_path = URL::asset('uploads/banner_footer/'.$img_path);
          }else{
            $img_path = 'http://dummyimage.com/262x103/f0f0f0/d9d9d9.jpg&text=TSI';
          }
          ?>

          <div class="col-sm-3">
            <a href="{{ $bf->url }}"><img src="{{ $img_path }}"></a>
          </div>
        @endforeach
      @endif
    </div>
  </div>
</div>
@endsection

@section('more-script')
  <script src="{{ URL::asset('tools/bxslider/jquery.bxslider.min.js') }}"></script>
  <script type="text/javascript">
    $(function(){
        @if (count($banners) > 1)
            $('#banner-top ul').bxSlider();
        @else
            $('#banner-top ul').bxSlider({
              'auto': false,
              'pager': false,
              'controls': false
            });
        @endif

        $('.bxSlider').bxSlider({
          'auto': true,
          'controls': true,
          'pause': 8000
        });
    });
  </script>
@endsection
