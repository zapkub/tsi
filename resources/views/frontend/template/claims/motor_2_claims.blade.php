<?php
$lang = session()->get('locale');
?>
@extends('frontend.layouts/main')

@section('title','What to do after a car accident?')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/claim.css') }}"/>
@endsection

@section('content')
<div id="content">
  <div class="container">
    <div class="col-sm-10 col-sm-offset-1">
      <h1 class="headline">{!!$compensation['title_' . session()->get('locale')]!!}</h1>
      @foreach($motor as $key => $claim)
        @if($key == 0)
          <div class="box active">
            <div class="topic">{!!$claim['title_' . session()->get('locale')]!!}</div>

            <div class="detail" style="display:block;">
              {!!$claim['detail_' . session()->get('locale')]!!}

            </div>
          </div>
        @else
          <div class="box">
            <div class="topic">{!!$claim['title_' . session()->get('locale')]!!}</div>

            <div class="detail">
              {!!$claim['detail_' . session()->get('locale')]!!}

            </div>
          </div>
        @endif
      @endforeach
      <!-- <div class="box">
        <div class="topic">กรณีลูกค้าซื้อกรมธรรม์คุ้มครองความเสียหายตัวรถ ประเภทการจัดซ่อมเป็นซ่อมอู่</div>

        <div class="detail">
          <ol>
            <li>บริษัทฯแนะนำลูกค้านำรถซ่อมที่อู่ในเครือของบริษัทฯ ที่ลูกค้าสะดวกที่สุด(สามารถดูรายชื่ออู่ได้ที่ www.tsi.co.th )</li>
            <li>เอกสารที่ใช้ในการติดต่อเจ้าหน้าที่ของอู่ซ่อม มีดังนี้<br/>
              2.1 ใบรับรองความเสียหาย<br/>
              2.2 สำเนาใบขับขี่<br/>
              2.3 สำเนาทะเบียนรถ
            </li>
            <li>เจ้าหน้าที่ของอู่ซ่อมเมื่อได้รับเอกสารทั้งหมด จะออกใบรับรถให้กับทางลูกค้า</li>
            <li>เจ้าหน้าที่ของอู่ซ่อม ดำเนินการประเมินราคาค่าซ่อม นำเสนอบริษัทฯ เพื่อขออนุมัติการจัดซ่อม</li>
            <li>เจ้าหน้าที่ประเมินราคาของบริษัทฯ พิจารณาอนุมัติจัดซ่อมให้อู่ซ่อม ดำเนินการจัดซ่อมต่อไป ภายใน 3 วัน</li>
            <li>เมื่อทางอู่ซ่อม ดำเนินการซ่อมรถให้กับลูกค้าเสร็จ เมื่อถึงกำหนดวัน เวลาที่อู่ซ่อมนัดหมายรับรถ ให้ลูกค้านำใบรับรถติดต่ออู่ซ่อมเพื่อนำรถออก </li>
          </ol>
        </div>
      </div> -->

      <!-- <div class="box">
        <div class="topic">กรณีลูกค้าประสงค์นำรถไปซ่อมเอง ซึ่งไม่ใช่อู่ในเครือของบริษัทฯ(อู่นอกเครือ)</div>

        <div class="detail">
          <ol>
            <li>เมื่อลูกค้าติดต่ออู่ซ่อมที่จะนำรถเข้าซ่อมได้แล้ว ให้อู่ซ่อมนั้นๆ เร่งดำเนินการจัดทำใบประเมินราคา เพื่อนำเสนอบริษัทฯขออนุมัติจัดซ่อม </li>
            <li>เอกสารที่ใช้ในการนำเสนอบริษัทฯขออนุมัติจัดซ่อม มีดังนี้
              <ul>
                <li>ใบรับรองความเสียหาย</li>
                <li>ใบประเมินราคาค่าซ่อม</li>
                <li>ภาพถ่ายความเสียหาย ทุกรายการ กรุณาถ่ายภาพให้ชัดเจน</li>
              </ul>
            </li>
            <li>ช่องทางการนำเสนอเอกสารเพื่อขออนุมัติจัดซ่อม มีดังนี้
              <ul>
                <li>ลูกค้าแฟกซ์เอกสารเพื่อนำเสนอขออนุมัติ มาที่ เบอร์ 02-2360318</li>
                <li>ลูกค้าอีเมล์พร้อมแนบเอกสารเพื่อนำเสนอขออนุมัติ มาที่ claims.ev@tsi.co.th  </li>
                <li>ลูกค้านำเอกสารเพื่อนำเสนอขออนุมัติ มาที่บริษัทฯด้วยตนเอง</li>
              </ul>

              *** บางกรณีอู่ซ่อมจะดำเนินขั้นตอนนี้แทนลูกค้า เพื่ออำนวยความสะดวกให้
            </li>
            <li>เจ้าหน้าที่ประเมินราคาของบริษัทฯ พิจารณาอนุมัติจัดซ่อมให้อู่ซ่อม ดำเนินการจัดซ่อมต่อไป</li>
            <li>เมื่อทางอู่ซ่อม ดำเนินการซ่อมรถให้กับลูกค้าเสร็จ เมื่อถึงกำหนดวัน เวลาที่อู่ซ่อมนัดหมายรับรถ ให้ลูกค้านำใบรับรถติดต่ออู่ซ่อมเพื่อนำรถออก </li>
            <li>ขั้นตอนการเบิกค่าใช้จ่ายในการจัดซ่อม ให้จัดเตรียมเอกสาร ดังนี้
              <ul>
                <li>ใบรับรองความเสียหาย(ฉบับจริง)</li>
                <li>ใบประเมินราคาค่าซ่อม ที่ได้รับการอนุมัติจัดซ่อม โดยมีประทับตราบริษัทฯ</li>
                <li>สำเนาใบขับขี่</li>
                <li>สำเนาทะเบียนรถ</li>
                <li>ใบเสร็จรับเงินฉบับจริง</li>
                <li>ภาพถ่ายในการจัดซ่อม : ก่อนซ่อม ขณะซ่อม หลังซ่อม</li>
                <li>สำเนาบัญชีธนาคารของผู้ครอบครองรถ</li>
              </ul>
            </li>
          </ol>
        </div>
      </div> -->
    </div>
  </div>
</div>

  <!-- <div id="emergency">
    <div class="container">
      <div class="headline">เบอร์ฉุกเฉิน อีกหนึ่งความห่วงใย จาก ที เอส ไอ</div>
      <div class="row item">
        <div class="col-sm-4">
          <div class="icon"><img src="{{ URL::asset('images/what-to-do-icon-01.png') }}"></div>
          <div class="name">แจ้งปัญหาจราจร</div>
          <div class="number">1137 / 1644</div>
          <div class="detail">
            เมื่อพบเห็นอุบัติเหตุ หรือต้องการขอความช่วยเหลืออื่นๆ<br>
            ติดต่อ จ.ส.100 โทร.1137, สวพ.91 โทร.1644
          </div>
        </div>
        <div class="col-sm-4">
          <div class="icon"><img src="{{ URL::asset('images/what-to-do-icon-02.png') }}"></div>
          <div class="name">ศูนย์กู้ชีพนเรนทร</div>
          <div class="number">1669</div>
          <div class="detail">
            กรณีมีผู้บาดเจ็บ เจ็บป่วยฉุกเฉิน หรือมีผู้เสียชีวิต<br>
            ติดต่อศูนย์กู้ชีพนเรนทร โทร.1669
          </div>
        </div>
        <div class="col-sm-4">
          <div class="icon"><img src="{{ URL::asset('images/what-to-do-icon-03.png') }}"></div>
          <div class="name">ตำรวจทางหลวง</div>
          <div class="number">1193</div>
          <div class="detail">
            เดินทางอุ่นใจ ปลอดภัยไปกับกรมทางหลวง<br>
            ติดต่อตำรวจทางหลวง โทร.1193
          </div>
        </div>
      </div>
    </div>
  </div> -->
  </div>



@endsection

@section('more-script')
  {{ Html::script('js/front/what-to-do.js') }}

  <script>
    $(function(){
      // $('.box.active .detail').parent().find('.detail').slideDown();
      $('.box .topic').on('click', function(){
        if ($(this).parent().hasClass('active')){
          $(this).parent().find('.detail').slideUp();
          $(this).parent().removeClass('active');
        }
        else{
          $('.box.active .detail').slideUp();
          $(this).parent().find('.detail').slideDown();
          $('.box.active').removeClass('active');
          $(this).parent().addClass('active');
        }
      });
    });
  </script>

@endsection
