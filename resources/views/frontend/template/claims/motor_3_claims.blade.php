<?php
$lang = session()->get('locale');
?>
@extends('frontend.layouts/main')

@section('title','What to do after a car accident?')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/claim.css') }}"/>
@endsection

@section('content')
<div id="content">
  <div class="container">
    <div class="col-sm-10 col-sm-offset-1">
      <h1 class="headline">ขั้นตอนการพิจารณาสินไหม</h1>

      <div class="box active">
        <div class="topic">ขั้นตอนและวิธีดำเนินการเรียกร้องค่าสินไหมทดแทน</div>

        <div class="detail" style="display:block;">
          
        </div>
      </div>

    </div>
  </div>
</div>

  <div id="emergency">
    <div class="container">
      <div class="headline">เบอร์ฉุกเฉิน อีกหนึ่งความห่วงใย จาก ที เอส ไอ</div>
      <div class="row item">
        <div class="col-sm-4">
          <div class="icon"><img src="{{ URL::asset('images/what-to-do-icon-01.png') }}"></div>
          <div class="name">แจ้งปัญหาจราจร</div>
          <div class="number">1137 / 1644</div>
          <div class="detail">
            เมื่อพบเห็นอุบัติเหตุ หรือต้องการขอความช่วยเหลืออื่นๆ<br>
            ติดต่อ จ.ส.100 โทร.1137, สวพ.91 โทร.1644
          </div>
        </div>
        <div class="col-sm-4">
          <div class="icon"><img src="{{ URL::asset('images/what-to-do-icon-02.png') }}"></div>
          <div class="name">ศูนย์กู้ชีพนเรนทร</div>
          <div class="number">1669</div>
          <div class="detail">
            กรณีมีผู้บาดเจ็บ เจ็บป่วยฉุกเฉิน หรือมีผู้เสียชีวิต<br>
            ติดต่อศูนย์กู้ชีพนเรนทร โทร.1669
          </div>
        </div>
        <div class="col-sm-4">
          <div class="icon"><img src="{{ URL::asset('images/what-to-do-icon-03.png') }}"></div>
          <div class="name">ตำรวจทางหลวง</div>
          <div class="number">1193</div>
          <div class="detail">
            เดินทางอุ่นใจ ปลอดภัยไปกับกรมทางหลวง<br>
            ติดต่อตำรวจทางหลวง โทร.1193
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>



@endsection

@section('more-script')
  {{ Html::script('js/front/what-to-do.js') }}

  <script>
    $(function(){
      // $('.box.active .detail').parent().find('.detail').slideDown();
      $('.box .topic').on('click', function(){
        if ($(this).parent().hasClass('active')){
          $(this).parent().find('.detail').slideUp();
          $(this).parent().removeClass('active');
        }
        else{
          $('.box.active .detail').slideUp();
          $(this).parent().find('.detail').slideDown();
          $('.box.active').removeClass('active');
          $(this).parent().addClass('active');
        }
      });
    });
  </script>

@endsection
