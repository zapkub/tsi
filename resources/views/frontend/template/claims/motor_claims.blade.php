<?php
$lang = session()->get('locale');
?>
@extends('frontend.layouts/main')

@section('title','What to do after a car accident?')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/motor_claims.css') }}"/>
@endsection

@section('content')

  <div id="what-to-do">
    <div class="container">
      <div class="headline">
        สินไหมรถยนต์
      </div>
      <div class="headline-sub">
        <!-- อุบัติเหตุเป็นเรื่องที่เกิดขึ้นได้ตลอดเวลา มาเรืียนรู้ขั้นตอนง่ายๆเมื่อเกิดอุบัติเหตุกันเถอะ -->
      </div>

      <div class="to-do-list row">
        @foreach($motor as $key => $claim)
        <?php
          $num = 2;
          $url_motor_detail = URL::to('motor_claims/'.$num.'/'. $claim['compensation_id']);
          $img_name = $claim->img_name;

          if (!empty($img_name) && file_exists('uploads/compensation/' . $img_name))
            $img_path = URL::asset('uploads/compensation/' . $claim->img_name);
          else
            $img_path = 'http://dummyimage.com/600x400/DDDDDD/BBBBBB&text=TSI';
        ?>
            @if($key == 0)
              <?php
                $num = 1;
                $url_motor_detail = URL::to('motor_claims/'.$num.'/'. $claim['compensation_id']);
                $img_name = $claim->img_name;

                if (!empty($img_name) && file_exists('uploads/compensation/' . $img_name))
                  $img_path = URL::asset('uploads/compensation/' . $claim->img_name);
                else
                  $img_path = 'http://dummyimage.com/600x400/DDDDDD/BBBBBB&text=TSI';
              ?>
              <div class="claim col-md-4">
                <a href="{{ $url_motor_detail }}" class="img-responsive text-center"><img src="{{ $img_path }}" />{{$claim['title_th']}}</a>
              </div>
            @else
              <div class="claim col-md-4">
                <a href="{{ $url_motor_detail }}" class="img-responsive text-center"><img src="{{ $img_path }}" />{{$claim['title_th']}}</a>
              </div>
            @endif
          @endforeach
          <!-- <div class="claim col-xs-4 col-md-4">

            <a href="{{ URL::to('motor_2_claims') }}" class="img-responsive " style="background-color:green;"><img>ชั้นตอนการนำรถยนต์เข้าซ่อมศูนย์หรืออู่</a>
          </div>
          <div class="claim col-xs-4 col-md-4">
            <a href="{{URL::to('motor_3_claims')}}" class="img-responsive c" style="background-color:blue;"><img>ขั้นตอนการพิจารณาการจ่ายสินไหม</a>
          </div> -->

    </div>
  </div>

  <div id="emergency">
    <div class="container">
      <div class="headline">เบอร์ฉุกเฉิน อีกหนึ่งความห่วงใย จาก ที เอส ไอ</div>
      <div class="row item">
        <div class="col-sm-4">
          <div class="icon"><img src="{{ URL::asset('images/what-to-do-icon-01.png') }}"></div>
          <div class="name">แจ้งปัญหาจราจร</div>
          <div class="number">1137 / 1644</div>
          <div class="detail">
            เมื่อพบเห็นอุบัติเหตุ หรือต้องการขอความช่วยเหลืออื่นๆ<br>
            ติดต่อ จ.ส.100 โทร.1137, สวพ.91 โทร.1644
          </div>
        </div>
        <div class="col-sm-4">
          <div class="icon"><img src="{{ URL::asset('images/what-to-do-icon-02.png') }}"></div>
          <div class="name">ศูนย์กู้ชีพนเรนทร</div>
          <div class="number">1669</div>
          <div class="detail">
            กรณีมีผู้บาดเจ็บ เจ็บป่วยฉุกเฉิน หรือมีผู้เสียชีวิต<br>
            ติดต่อศูนย์กู้ชีพนเรนทร โทร.1669
          </div>
        </div>
        <div class="col-sm-4">
          <div class="icon"><img src="{{ URL::asset('images/what-to-do-icon-03.png') }}"></div>
          <div class="name">ตำรวจทางหลวง</div>
          <div class="number">1193</div>
          <div class="detail">
            เดินทางอุ่นใจ ปลอดภัยไปกับกรมทางหลวง<br>
            ติดต่อตำรวจทางหลวง โทร.1193
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>



@endsection

@section('more-script')
  {{ Html::script('js/front/what-to-do.js') }}
@endsection
