<?php
$lang = session()->get('locale');
?>
@extends('frontend.layouts/main')

@section('title','What to do after a car accident?')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/claim.css') }}"/>
@endsection

@section('content')
<div id="content">
  <div class="container">
    <div class="col-sm-10 col-sm-offset-1">
      <h1 class="headline">เอกสารประกอบการพิจารณาสินไหม</h1>

      <div class="box active">
        <div class="topic">เอกสารประกอบการเรียกร้องค่าสินไหมทดแทน</div>

        <div class="detail" style="display:block;">
            <strong>1. การประกันอัคคีภัย, ประกันภัยทรัพย์สิน และ การประกันภัยเบ็ดเตล็ด</strong><br/>
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1.1 หนังสือการแจ้งเหตุ<br/>
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1.2 ภาพถ่ายความเสียหาย<br/>
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1.3 ใบเสนอราคาค่าซ่อมแซม/ใบเสร็จรับเงิน<br/>
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1.4 สำเนาบันทึกประจำวัน (ถ้ามี)<br/>
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1.5 เอกสารประกอบเพิ่มเติมอื่น ๆ ซึ่งจะแจ้งขอเป็นกรณีไป<br/><br/>

            <strong>2. การประกันภัยอุบัติเหตุ</strong><br/>
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.1 ค่ารักษาพยาบาล<br/>
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.1.1  แบบฟอร์มการเรียกร้องค่าสินไหมทดแทน<br/>
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.1.2  สำเนาบัตรประชาชน พร้อมรับรองสำเนาถูกต้อง<br/>
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.1.3  ใบรับรองแพทย์ (ฉบับจริง)<br/>
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.1.4  ใบเสร็จรับเงิน (ฉบับจริง)<br/>
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.1.5  เอกสารประกอบเพิ่มเติมอื่น ๆ ซึ่งจะแจ้งขอเป็นกรณีไป<br/><br/>
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.2 กรณีการเสียชีวิต<br/>
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.2.1  แบบฟอร์มการเรียกร้องค่าสินไหมทดแทน<br/>
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.2.2  สำเนาบัตรประชาชน พร้อมรับรองสำเนาถูกต้อง<br/>
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.2.3  สำเนาใบมรณบัตร<br/>
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.2.4  สำเนารายงานชันสูตรพลิกศพ <br/>
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.2.5  สำเนาบันทึกประจำวันตำรวจ<br/>
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.2.6  สำเนาผลสรุปคดีของตำรวจ<br/>
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.2.7  สำเนาทะเบียนบ้าน  สำเนาบัตรประชาชน  (ผู้เสียชีวิต)<br/>
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.2.8  สำเนาบัตรผู้รับผลประโยชน์<br/>
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.2.9  หนังสือแต่งตั้งผู้จัดการมรดก      <br/>
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.2.10เอกสารประกอบเพิ่มเติมอื่น ๆ ซึ่งจะแจ้งขอเป็นกรณีไป<br/><br/>
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.3 กรณีสูญเสียอวัยวะ หรือสายตา และทุพพลภาพถาวร<br/>
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.3.1  แบบฟอร์มการเรียกร้องค่าสินไหมทดแทน<br/>
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.3.2  สำเนาบัตรประชาชน พร้อมรับรองสำเนาถูกต้อง<br/>
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.3.3  ใบรายงานทางการแพทย์ (ระบุอาการ)<br/>
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.3.4  ใบรับรองแพทย์ (ฉบับจริง)<br/>
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.3.5  สำเนาบัตรประชาชนผู้พิการ<br/>
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.3.6  เอกสารประกอบเพิ่มเติมอื่น ๆ ซึ่งจะแจ้งขอเป็นกรณีไป<br/><br/>

             <strong>3. สำหรับประกันภัยทางทะเลและขนส่ง</strong><br/>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3.1 กรมธรรม์ฉบับจริง<br/>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3.2 ใบกำกับสินค้า (INVOICE)<br/>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3.3 ใบกำกับหีบห่อสินค้า (PACKING LIST)<br/>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3.4 ใบตราส่งสินค้า (BILL OF LADING) ในกรณีทางเรือ  และ (AIR WAYBILL) ในกรณีทางอากาศ<br/>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3.5 ภาพถ่ายของสินค้าที่เสียหาย<br/>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3.6 หนังสือเรียกร้องค่าสินไหมไปยังผู้รับประกันภัย<br/>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3.7 หนังสือเรียกร้องค่าสินไหมไปยังผู้ขนส่ง<br/>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3.8 เอกสารอื่น ๆ ที่แสดงความเสียหาย หรือสูญหาย ที่ออกโดยผู้ขนส่ง / การท่าเรือ / การท่าอากาศยาน เช่น WHARF SURVEY NOTE , DMC , ใบรับสินค้าที่มีลายเซ็นต์พนักงาน ขับรถ  หนังสือตอบรับจากผู้ขนส่ง<br/>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3.9 เอกสารประกอบเพิ่มเติมอื่น ๆ ซึ่งจะแจ้งขอเป็นกรณีไป
            </li>
          </ol>
        </div>
      </div>


    </div>
  </div>
</div>

  <div id="emergency">
    <div class="container">
      <div class="headline">เบอร์ฉุกเฉิน อีกหนึ่งความห่วงใย จาก ที เอส ไอ</div>
      <div class="row item">
        <div class="col-sm-4">
          <div class="icon"><img src="{{ URL::asset('images/what-to-do-icon-01.png') }}"></div>
          <div class="name">แจ้งปัญหาจราจร</div>
          <div class="number">1137 / 1644</div>
          <div class="detail">
            เมื่อพบเห็นอุบัติเหตุ หรือต้องการขอความช่วยเหลืออื่นๆ<br>
            ติดต่อ จ.ส.100 โทร.1137, สวพ.91 โทร.1644
          </div>
        </div>
        <div class="col-sm-4">
          <div class="icon"><img src="{{ URL::asset('images/what-to-do-icon-02.png') }}"></div>
          <div class="name">ศูนย์กู้ชีพนเรนทร</div>
          <div class="number">1669</div>
          <div class="detail">
            กรณีมีผู้บาดเจ็บ เจ็บป่วยฉุกเฉิน หรือมีผู้เสียชีวิต<br>
            ติดต่อศูนย์กู้ชีพนเรนทร โทร.1669
          </div>
        </div>
        <div class="col-sm-4">
          <div class="icon"><img src="{{ URL::asset('images/what-to-do-icon-03.png') }}"></div>
          <div class="name">ตำรวจทางหลวง</div>
          <div class="number">1193</div>
          <div class="detail">
            เดินทางอุ่นใจ ปลอดภัยไปกับกรมทางหลวง<br>
            ติดต่อตำรวจทางหลวง โทร.1193
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>



@endsection

@section('more-script')
  {{ Html::script('js/front/what-to-do.js') }}

  <script>
    $(function(){
      // $('.box.active .detail').parent().find('.detail').slideDown();
      $('.box .topic').on('click', function(){
        if ($(this).parent().hasClass('active')){
          $(this).parent().find('.detail').slideUp();
          $(this).parent().removeClass('active');
        }
        else{
          $('.box.active .detail').slideUp();
          $(this).parent().find('.detail').slideDown();
          $('.box.active').removeClass('active');
          $(this).parent().addClass('active');
        }
      });
    });
  </script>

@endsection
