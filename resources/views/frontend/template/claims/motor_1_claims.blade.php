<?php
$lang = session()->get('locale');
?>
@extends('frontend.layouts/main')

@section('title','What to do after a car accident?')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/motor_claims.css') }}"/>
@endsection

@section('content')
<div id="what-to-do">
  <div class="container">
    <div class="headline">
      How to do when you have the accident?<br>เมื่อท่านเกิดอุบัติเหตุ ควรทำอย่างไรดี?
    </div>
    <div class="headline-sub">
      อุบัติเหตุเป็นเรื่องที่เกิดขึ้นได้ตลอดเวลา มาเรืียนรู้ขั้นตอนง่ายๆเมื่อเกิดอุบัติเหตุกันเถอะ
    </div>
    <div class="to-do-list row">
      <div class="col-md-4 hidden-xs hidden-sm image">
        <img src="{{ URL::asset('images/what-to-do-01.png') }}">
      </div>


      <div class="list-item col-md-8">
        @foreach($motor as $key => $claim)
          <div class="item" data-id="item-0{{$key}}">
            <div class="number">{{$key+1}}</div>
            <div class="text">{!!$claim['title_' . session()->get('locale')]!!}</div>
          </div>

          <div id="item-0{{$key}}" class="sub-item">
            {!!$claim['detail_' . session()->get('locale')]!!}

          </div>
        @endforeach



        <!-- <div class="item" data-id="item-02">
          <div class="number">2</div>
          <div class="text">รอเจ้าหน้าที่ ณ จุดเกิดเหตุ</div>
        </div>
        <div id="item-02" class="sub-item">
          หลังจากโทรแจ้ง 1352 ของเราแล้ว ภายใน 10 นาที จะมีเจ้าหน้าที่ของทีเอสไอ ติดต่อกลับมาหาท่าน เพื่อแจ้ง ชื่อและเบอร์โทรศัพท์ ของเจ้าหน้าที่ ที่จะมาให้บริการ ณ จุดเกิดเหตุ
        </div>
        <div class="item" data-id="item-03">
          <div class="number">3</div>
          <div class="text">ถ่ายรูปไว้ก่อนแยกย้ายทุกครั้ง</div>
        </div>
        <div id="item-03" class="sub-item">
          หากต้องการแยกย้ายรถ  ถ้าลักษณะเหตุชัดเจนฝ่ายหนึ่งฝ่ายใดยอมรับผิดให้ถ่ายรูปไว้และหลบเข้าข้างทางและหากไม่สามารถตกลงกันได้ให้แจ้งตำรวจมาฉีดพ่นสเปรย์ก่อนหรือหากไม่สะดวกรอตำรวจให้ถ่ายรูปไว้ก่อนแยกย้ายทุกครั้ง
        </div>
        <div class="item" data-id="item-04">
          <div class="number">4</div>
          <div class="text">เตรียมเอกสารไว้รอเจ้าหน้าที่</div>
        </div>
        <div id="item-04" class="sub-item">
          ระหว่างรอเจ้าหน้าที่ เดินทางมาให้บริการ ท่านสามารถเตรียมเอกสารไว้รอก่อนก็ได้คะ เพื่อความรวดเร็ว เอกสารที่ต้องเตรียม ได้แก่ ใบขับขี่  สำเนาทะเบียนรถ  หน้าตารางกรมธรรม์(ถ้ามี) สิ่งที่สำคัญในระหว่างรอท่านควรยืนหรือนั่งรอในที่ที่ปลอดภัยด้วยนะคะ
        </div>
        <div class="item" data-id="item-05">
          <div class="number">5</div>
          <div class="text">เมื่อพนักงานมาถึง ท่านจะได้รับบริการดังนี้</div>
        </div>
        <div id="item-05" class="sub-item">
          เมื่อพนักงานมาถึง ท่านจะได้รับการบริการ ดังนี้
          <ul>
            <li>เจ้าหน้าที่แนะนำตัว</li>
            <li>ตรวจสอบเอกสารที่ท่านได้เตรียมไว้</li>
            <li>เริ่มดำเนินการตรวจสอบอุบัติเหตุ เมื่อเสร็จสิ้น ท่านจะได้รับเอกสาร 1 ฉบับ เราเรียกว่า <a href="{{ URL::asset('uploads/ใบรับรองความเสียหาย.pdf') }}" target="_blank">“ใบรับรองความเสียหาย”</a> พร้อมแนะนำศูนย์บริการหรืออู่ในเครือของทีเอสไอ ที่ท่านสามารถนำรถเข้าซ่อม</li>
            <li>หากมีข้อสงสัยให้สอบถามเจ้าหน้าที่ตรวจสอบอุบัติเหตุได้ ก่อนรับเอกสารทุกครั้ง</li>
          </ul>
        </div>
        <div class="item" data-id="item-06">
          <div class="number">6</div>
          <div class="text">นำเอกสารไปที่ศูนย์บริการหรืออู่เมื่อนำรถเข้ามาซ่อม</div>
        </div> -->
        <!-- <div id="item-06" class="sub-item">
          เมื่อท่านนำรถเข้าซ่อมให้นำเอกสาร ไปที่ศูนย์บริการหรืออู่ ดังนี้
          <ul>
            <li>ใบรับรองความเสียหาย</li>
            <li>สำเนาใบขับขี่</li>
            <li>สำเนาทะเบียนรถ</li>
          </ul>
        </div> -->

      </div>
    </div>
  </div>
</div>

  <!-- <div id="emergency">
    <div class="container">
      <div class="headline">เบอร์ฉุกเฉิน อีกหนึ่งความห่วงใย จาก ที เอส ไอ</div>
      <div class="row item">
        <div class="col-sm-4">
          <div class="icon"><img src="{{ URL::asset('images/what-to-do-icon-01.png') }}"></div>
          <div class="name">แจ้งปัญหาจราจร</div>
          <div class="number">1137 / 1644</div>
          <div class="detail">
            เมื่อพบเห็นอุบัติเหตุ หรือต้องการขอความช่วยเหลืออื่นๆ<br>
            ติดต่อ จ.ส.100 โทร.1137, สวพ.91 โทร.1644
          </div>
        </div>
        <div class="col-sm-4">
          <div class="icon"><img src="{{ URL::asset('images/what-to-do-icon-02.png') }}"></div>
          <div class="name">ศูนย์กู้ชีพนเรนทร</div>
          <div class="number">1669</div>
          <div class="detail">
            กรณีมีผู้บาดเจ็บ เจ็บป่วยฉุกเฉิน หรือมีผู้เสียชีวิต<br>
            ติดต่อศูนย์กู้ชีพนเรนทร โทร.1669
          </div>
        </div>
        <div class="col-sm-4">
          <div class="icon"><img src="{{ URL::asset('images/what-to-do-icon-03.png') }}"></div>
          <div class="name">ตำรวจทางหลวง</div>
          <div class="number">1193</div>
          <div class="detail">
            เดินทางอุ่นใจ ปลอดภัยไปกับกรมทางหลวง<br>
            ติดต่อตำรวจทางหลวง โทร.1193
          </div>
        </div>
      </div>
    </div>
  </div> -->
  </div>



@endsection

@section('more-script')
  {{ Html::script('js/front/what-to-do.js') }}
@endsection
