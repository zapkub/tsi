<?php
$lang = session()->get('locale');
?>
@extends('frontend.layouts/main')

@section('title','Download')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/download.css') }}"/>
@endsection

@section('content')
<div id="download">
  <div class="container">
    <div class="headline">ดาวน์โหลดเอกสาร</div>

    @foreach($docs as $key => $doc)
      <?php
      $category_id = $doc->document_category_id;
      ?>
      <div class="panel-download">
        <div class="panel-heading">
          <a class="panel-link" href="#body-{{ $category_id }}">{{ $doc->category_name }} <span class="icon-plus"><i class="glyphicon glyphicon-plus"></i></span></a>
        </div>
        <div id="body-{{ $category_id }}" class="panel-body @if($key == 0) active @endif">
          <div class="download-list">
            @if(count($doc->documents) > 0)
              @foreach($doc->documents as $key2 => $value)
                <?php
                  $dir = 'uploads/document/';
                  $file_name = $value->file_name;
                  $dir_file_name = $dir.$file_name;
                  $path = url()->asset($dir_file_name);
                  if($file_name == '' || !file_exists($dir_file_name)){
                    $path = '#';
                  }
                ?>
                <div class="col-md-4">{{ $key2+1 .'. '.$value->document_name }} </div>
                <div class="col-md-4 text-center" >@if($value->document_date != '0000-00-00' ){{ $objFn->format_date_th($value->document_date,2) }} @else - @endif</div>
                <div class="col-md-4 text-center" ><a href="{{ $path }}" target="_blank" @if($path == '#') class="unlink" @endif>ดาวน์โหลด</a></div>
                <div class="clearfix"></div>
              @endforeach
            @else
              <div class="col-md-12 text-center">ไม่พบข้อมูล</div>
            @endif
          </div>
        </div>
      </div>
    @endforeach
    {{--<div class="panel-download">--}}
      {{--<div class="panel-heading">--}}
        {{--<a class="panel-link" href="#body-2">รายชื่อสมาชิกอู่กลาง <span class="icon-plus"><i class="glyphicon glyphicon-plus"></i></span></a>--}}
      {{--</div>--}}
      {{--<div id="body-2" class="panel-body">--}}
        {{--<div class="col-md-5">1. xxxx sdsdfdsfdsf sdfsdfsdf sdfsdfsdf sdfsdfsdfsdfsfsdfsdfsdf</div>--}}
        {{--<div class="col-md-7"><a href="#">ดาวน์โหลด</a></div>--}}
        {{--<div class="clearfix"></div>--}}
      {{--</div>--}}
    {{--</div>--}}

  </div>
</div>
@endsection
@section('more-script')
  <script>
    $(function(){
      $('.panel-link').on('click',function(e){
        e.preventDefault();
        href = $(this).attr('href');
        $('.panel-body').slideUp();
        $(href).slideToggle();
      });
    });
  </script>
@endsection
