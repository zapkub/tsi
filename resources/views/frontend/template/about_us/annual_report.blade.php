<?php
$lang = session()->get('locale');
?>
@extends('frontend.layouts/main')

@section('title','About Us')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/about_us.css') }}"/>
@endsection

@section('content')
  <div id="about_us">
    <div class="container">
      <div class="col-sm-3">
        @include('frontend.template.about_us.sidebar')
      </div>

      <div class="col-sm-9">
        <div id="annual-report">
          <div class="headline">{{ trans('about.annual_report') }}</div>
          <div class="list row">
            <?php
            $img_dir['th'] = 'uploads/yearbook/';
            $img_dir['en'] = 'uploads/yearbook_en/';

            $pdf_dir['th'] = 'uploads/yearbook_pdf/';
            $pdf_dir['en'] = 'uploads/yearbook_pdf_en/';
            ?>
            @foreach($reports as $key => $report)
              <?php
                $name = $report->name_th;
                $img_name = $report->img_path;
                $pdf_name = $report->pdf_path;

                if ($lang == 'en'){
                  $name = $report->name_en;
                  $img_name = $report->img_path_en;
                  $pdf_name = $report->pdf_path_en;
                }

                $img_path = $img_dir[$lang] . $img_name;
                $pdf_path = $pdf_dir[$lang] . $pdf_name;

                // if($lang == 'en'){
                //   if($report->name_en != '') $name = $report->name_en;
                //   if($report->img_path_en != '') {
                //     $img_name = $report->img_path_en;
                //     $img_path = $dir.'_en/'.$img_name;
                //   }
                //   if($report->pdf_path_en != '') {
                //     $pdf_name = $report->pdf_path_en;
                //     $pdf_path = $dir_pdf.'_en/'.$pdf_name;
                //   }
                // }

                if($img_name != '' && file_exists($img_path)){
                  $img_path = URL::asset($img_path);
                }else{
                  $img_path = 'http://dummyimage.com/116x163/EEE/d9d9d9.jpg&text=TSI';
                }

                ?>
              <div class="report col-xs-6 col-md-3">
                <div class="title"><i class="glyphicon glyphicon-asterisk"></i> {{ $name }}</div>
                <div class="image">
                  <img src="{{ $img_path }}" />
                </div>
                <div class="download">
                  <div class="doc-icon"><img src="{{ URL::asset('images/aboutus-icon-report.png') }}" /></div>
                  <div class="download-icon">
                    <span class="hidden-xs">{{ trans('about.download') }}</span>

                    &nbsp;&nbsp;

                    @if($pdf_name != '' && file_exists($pdf_path))
                      <a href="{{ URL::asset($pdf_path) }}" target="_blank"><i class="glyphicon glyphicon-download-alt"></i></a>
                    @else
                     {{ '-' }}
                    @endif
                  </div>
                </div>
              </div>
              @if (($key+1)%2 == 0)
                <div class="clearfix visible-xs-block visible-sm-block"></div>
              @endif
              @if (($key+1)%4 == 0)
                <div class="clearfix visible-md-block"></div>
              @endif
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
