@extends('frontend.layouts/main')

@section('title','About Us')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/about_us.css') }}"/>
@endsection

@section('content')
  <div id="about_us">
    <div class="container">
      <div class="col-sm-3">
        @include('frontend.template.about_us.sidebar')
      </div>

      <div class="col-sm-9">
        <div id="executive-committee">
          <div class="headline">{{ trans('about.committee') }}</div>

          <div class="list row">
            @foreach($committees as $committee)
              <?php
                $img_path = $committee->img_path;
                if($img_path != '' && file_exists('uploads/executive_committee/'.$img_path)){
                  $img_path = URL::asset('uploads/executive_committee/'.$img_path);
                }else{
                  $img_path = 'http://dummyimage.com/300x400/f0f0f0/d9d9d9.jpg&text=TSI';
                }

              ?>
              <div class="committee col-xs-6 col-md-4">
                <div class="avatar">
                  <img src="{{ $img_path }}" />
                </div>
              </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
