<?php
$lang = session()->get('locale');
?>

@extends('frontend.layouts/main')

@section('title','About Us')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/about_us.css') }}"/>
@endsection

@section('content')
  <div id="about_us">
    <div class="container">
      <div class="col-sm-3">
        @include('frontend.template.about_us.sidebar')
      </div>

      <div class="col-sm-9">
        <div id="vision">
          <div class="headline">{{ trans('about.vision') }}</div>

          <div class="content">
            <?php
            $vision_txt = $vision->detail_th;
            if($lang == 'en'){
              $vision_txt = $vision->detail_en;
              if($vision_txt == '') $vision_txt = $vision->detail_th;
            }
            ?>
            {!! Html::decode($vision_txt) !!}

          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
