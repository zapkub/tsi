<div class="sidebar">
    <div class="headline">{{ trans('about.about') }}</div>

    <div class="list">
        <a href="{{ URL::route('about_us.history') }}" class="menu-item{{ Request::is('about_us/history')?' active':'' }}">{{ trans('about.history') }}</a>
        <a href="{{ URL::route('about_us.vision') }}" class="menu-item{{ Request::is('about_us/vision')?' active':'' }}">{{ trans('about.vision') }}</a>
        <a href="{{ URL::route('about_us.manager') }}" class="menu-item{{ Request::is('about_us/manager')?' active':'' }}">{{ trans('about.manager') }}</a>
        {{--<a href="{{ URL::route('about_us.executive_committee') }}" class="menu-item{{ Request::is('about_us/executive_committee')?' active':'' }}">{{ trans('about.committee') }}</a>--}}
        <a href="{{ URL::route('about_us.annual_report') }}" class="menu-item{{ Request::is('about_us/annual_report')?' active':'' }}">{{ trans('about.annual_report') }}</a>
        <a href="{{ URL::route('about_us.overall_operation') }}" class="menu-item{{ Request::is('about_us/overall_operation')?' active':'' }}">{{ trans('about.financial') }}</a>
    </div>
</div>