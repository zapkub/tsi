<?php
$lang = session()->get('locale');
?>

@extends('frontend.layouts/main')

@section('title','About Us')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/about_us.css') }}"/>
@endsection

@section('content')
  <div id="about_us">
    <div class="container">
      <div class="col-sm-3">
        @include('frontend.template.about_us.sidebar')
      </div>

      <div class="col-sm-9">
        <div id="manager">
          <div class="headline">{{ trans('about.manager') }}</div>

          <div class="list row">
            @foreach($managers as $manager)
              <?php
                  $title_name = $manager->title_th;
                  $firstname = $manager->firstname_th;
                  $lastname = $manager->lastname_th;
                  $position = $manager->position_th;
                  $img_path = $manager->img_path;
                  if($lang == 'en'){
                    if($manager->title_en != '') $title_name = $manager->title_en;
                    if($manager->firstname_en != '') $firstname = $manager->firstname_en;
                    if($manager->lastname_en != '') $lastname = $manager->lastname_en;
                    if($manager->position_en != '') $position = $manager->position_en;
                  }

                  if($img_path != '' && file_exists('uploads/executive_committee/'.$img_path)){
                    $img_path = URL::asset('uploads/executive_committee/'.$img_path);
                  }else{
                    $img_path = 'http://dummyimage.com/300x400/f0f0f0/d9d9d9.jpg&text=TSI';
                  }
              ?>
              <div class="manager row col-lg-6 @if($manager->sequence < 4) col-lg-offset-3 @endif">
                <div class="avatar col-xs-4">
                  <img src={{ $img_path }} />
                </div>

                <div class="info col-xs-8">
                  <div class="name">{{ $title_name.' '.$firstname.' '.$lastname }}</div>
                  <div class="position">{{ $position }}</div>
                </div>
              </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
