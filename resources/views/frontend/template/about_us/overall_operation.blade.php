<?php
$lang = session()->get('locale');
?>

@extends('frontend.layouts/main')

@section('title','About Us')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/about_us.css') }}"/>
@endsection

@section('content')
  <div id="about_us">
    <div class="container">
      <div class="col-sm-3">
        @include('frontend.template.about_us.sidebar')
      </div>

      <div class="col-sm-9">
        <div id="overall-operation">
          <div class="headline">{{ trans('about.financial') }}</div>

          <div class="content">
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>{{ trans('about.financial_report') }}</th>
                    <th class="text-center">Q1</th>
                    <th class="text-center">Q2</th>
                    <th class="text-center">Q3</th>
                    <th class="text-center">Q4</th>
                  </tr>
                </thead>

                <tbody>
                  @foreach($items as $key => $item)
                  <tr>
                    <td>ข้อมูลฐานะการเงินและผลการดำเนินงานประจำปี {{ $key }}</td>
                    @for($i=1; $i<=4; $i++)
                    <td class="text-center">
                      @if (!empty($item[$i]))
                      <?php
                        $field = $item[$i];

                        $dir_pdf = 'uploads/financial_pdf';
                        $name = $field->name_th;
                        $pdf_name = $field->pdf_path;
                        $pdf_path = $dir_pdf.'/'.$pdf_name;

                        if($lang == 'en'){
                          if($field->name_en) $name = $field->name_en;
                          if($field->pdf_path_en != '') {
                            $pdf_name = $field->pdf_path_en;
                            $pdf_path = $dir_pdf.'_en/'.$pdf_name;
                          }
                        }
                      ?>
                        @if($pdf_name != '' && file_exists($pdf_path))
                          <a href="{{ URL::asset($pdf_path) }}">
                            <img src="{{ URL::asset('images/aboutus-icon-report.png') }}" />
                          </a>
                        @else
                          -
                        @endif
                      @else
                        -
                      @endif
                    </td>
                    @endfor
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
