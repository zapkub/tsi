@extends('frontend.layouts/main')

@section('title','Home')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/home.css') }}"/>
  <link rel="stylesheet" href="{{ URL::asset('css/front/service-tsi.css') }}"/>
@endsection

@section('content')

<div id="service-finder" class="service-finder">
    <div class="filter-map pull-left hidden-md hidden-lg">
        <div class="filter">
            <div class="filter-service">
                @foreach($services as $key => $ser)
                    <a href="#" class="btn btn-service @if($key == 0) active @endif" data-type="{{ $ser['type'] }}">{{ $ser['name'] }}</a>
                @endforeach
            </div>
            <div class="filter-location">
                <select class="form-control area">
                    <option>ภาค</option>
                </select>
                <select class="form-control province">
                    <option>จังหวัด</option>
                </select>
                <select class="form-control district">
                    <option>เขต/อำเภอ</option>
                </select>
                <select class="form-control brand">
                    <option>ศูนย์</option>
                </select>
            </div>
        </div>

    </div>
    <div class="result pull-left">
        <h1>ผลลัพธ์การค้นหา</h1>
        <div class="result-list">
            {{--<div class="list">--}}
                {{--<p class="name"> <span class="latlong"></span></p>--}}
                {{--<p class="address"></p>--}}
                {{--<p class="tel-fax">--}}
                    {{--<span class="tel"></span>--}}
                    {{--<span class="fax"></span>--}}
                {{--</p>--}}
            {{--</div>--}}

        </div>
    </div>
    <div class="filter-map pull-right hidden-sm hidden-xs">
        <div class="filter">
            <div class="filter-service">
                @foreach($services as $key => $ser)
                    <a href="#" class="btn btn-service @if($key == 0) active @endif" data-type="{{ $ser['type'] }}">{{ $ser['name'] }}</a>
                @endforeach
            </div>
            <div class="filter-location">
                <select class="form-control area">
                    <option>ภาค</option>
                </select>
                <select class="form-control province">
                    <option>จังหวัด</option>
                </select>
                <select class="form-control district">
                    <option>เขต/อำเภอ</option>
                </select>
                <select class="form-control brand">
                    <option>ศูนย์</option>
                </select>
            </div>
        </div>

        <div id="map" class="show-map"></div>
    </div>
    <div class="clearfix"></div>
</div>
@endsection

@section('more-script')
    <script>
        var rootPath = "{{ URL::to('/') }}";
        var type = 'garage';
        var map;
        var markers_arr = [];
        var geocoder;
        var infoWindow;
        var infoWindowMain;
        var zoomLevel;
        var maxZoom = 14;
        var iconMarkerPath = rootPath + '/images/icon-pin.png';
        var iconBrandPath = '/uploads/brand/35/';

        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {
                    lat: 13.71900428832684,
                    lng: 100.51861926913261
                },
                zoom: 15
            });
            geocoder = new google.maps.Geocoder();
            infoWindow = new google.maps.InfoWindow();
            var marker = new google.maps.Marker({
                position: {
                    lat: 13.71900428832684,
                    lng: 100.51861926913261
                },
                map: map,
                visible: true,
                icon: iconMarkerPath
            });
            var html = '<div style="color:#1b1862">บริษัท ไทยเศรษฐกิจประกันภัย จำกัด มหาชน</div>';
            infoWindowMain = new google.maps.InfoWindow();
            bindInfoWindow(marker, map, infoWindowMain, html);

            map.addListener('zoom_changed', function() {
                zoomLevel = map.getZoom();
                limitZoomMarker(maxZoom, markers_arr, zoomLevel);
            });
        }
        $(function() {
            function getArea(type) {
                $.get('service/available-area/' + type, {}, function(data) {
                    option = '<option value="">เลือก</option>';
                    $.each(data.area, function(index, value) {
                        option += '<option value="' + value.id + '">' + value.name + ' พบ ' + value.count + ' แห่ง</option>';
                    });
                    $('.area').html(option);
                    $('.province').html('<option>จังหวัด</option>');
                    $('.district').html('<option>เขต/อำเภอ</option>');
                    $('.brand').html('<option>ศูนย์</option>')
                    $('.result-list').html('');
                });
            }

            function getResult(data) {
                // get service
                list = '';
                latlong = '';
                let i = 0;
                $.each(data.service, function(index, value) {
                    latlong = '';
                    moveToLatLng1 = 'onclick="moveToLatLng(' + i + ',\'' + value.name + '\')"';
                    if (value.latitude == '' || value.longitude == '') {
                        latlong = '<span class="latlong">Lat/Long Not found</span>';
                        moveToLatLng1 = '';
                        i--;
                    }
                    list += '<div class="list"><p class="name"  ' + moveToLatLng1 + '>' + value.name + ' ' + latlong + '</p><p class="address">' + value.address1 + ' ' + value.address2 + ' ' + value.district_name + ' ' + data.province_name + ' ' + value.zipcode + '</p><p class="tel-fax"><span class="tel">โทรศัพท์: ' + value.phone + '</span><span class="fax">Fax: ' + value.fax + '</span></p></div>';
                    i++;
                });
                $('.result-list').html(list);
            }

            // On load get area
            getArea(type);
            // On click service btn get area
            $('.btn-service').on('click', function() {
                $(this).attr('data-type') == 'officialGarage' ? $('.brand').show() : $('.brand').hide();
                obj = $(this);
                type = obj.data('type');
                $('.btn-service').removeClass('active');
                obj.addClass('active');
                getArea(type);
            });

            // On change area get province
            $('.area').on('change', function() {
                obj = $(this);
                geo_id = obj.val();
                if (geo_id != '') {
                    $.get('service/available-province/' + geo_id + '/' + type, {}, function(data) {
                        option = '<option value="">เลือก</option>';
                        $.each(data.province, function(index, value) {
                            option += '<option value="' + value.id + '">' + value.name + ' พบ ' + value.count + ' แห่ง</option>';
                        });
                        $('.province').html(option);
                        $('.district').html('<option>เขต/อำเภอ</option>');
                        $('.result-list').html('');
                    });
                } else {
                    $('.province').html('<option>จังหวัด</option>');
                    $('.district').html('<option>เขต/อำเภอ</option>');
                    $('.result-list').html('');
                }
            });

            // On change province get district and result
            $('.province').on('change', function() {
                obj = $(this);
                province_id = obj.val();
                if (province_id != '') {
                    $.get('service/available-district/' + province_id + '/' + type, {}, function(data) {
                        // get district
                        option = '<option value="">เลือก</option>';
                        $.each(data.district, function(index, value) {
                            option += '<option value="' + value.id + '">' + value.name + ' พบ ' + value.count + ' แห่ง</option>';
                        });
                        $('.district').html(option);
                        getResult(data);
                        //add maker
                        addMarkerToMap(map, data, type);
                    });
                } else {
                    $('.district').html('<option>เขต/อำเภอ</option>');
                    $('.result-list').html('');
                }
            });
            // On change district get result
            $('.district').on('change', function() {
                obj = $(this);
                district_id = obj.val();
                if (district_id != '') {
                    $.get('service/available-service/' + district_id + '/' + type, {}, function(data) {
                        // get district
                        if ($('.brand').css('display') == 'block') {
                            getResult(data);
                            //add maker
                            addMarkerToMap(map, data, type);
                            $.get('service/available-brand', {}, function(data) {
                                option = '<option value="">เลือก</option>';
                                $.each(data, function(index, value) {
                                    option += '<option value="' + value.brand_id + '">' + value.name + '</option>';
                                });
                                $('.brand').html(option);
                            });
                        } else {
                            getResult(data);
                            addMarkerToMap(map, data, type);
                        }
                    });
                } else {
                    $('.brand').html('<option>ศูนย์</option>');
                    $('.result-list').html('');
                }
            });
            $('.brand').on('change', function() {
                let district_id = $(this).siblings('.district').val();
                let brand_id = $(this).val();
                if (district_id != '') {
                    $.get('service/available-service-brand/' + district_id + '/' + brand_id + '/' + type, {}, function(data) {
                        if (data.service.length == 0) {
                            $('.result-list').html('<div class="list"><p class="name">ไม่พบข้อมูล</p></div>');
                            deleteMarker();
                            return;
                        }
                        getResult(data);
                        addMarkerToMap(map, data, type);
                    });
                } else {
                    $('.result-list').html('');
                }
            });
        });

        function moveToLatLng(markerNum, name) {
            let html = name;
            let marker = markers_arr[markerNum];
            map.setZoom(15);
            map.setCenter(marker.position);
            infoWindow.setContent(html);
            infoWindow.open(map, marker);
        }

        function addMarkerToMap(map, json, type) {
            deleteMarker();
            let iconLocation = iconMarkerPath ;
            for (var i = 0, length = json.service.length; i < length; i++) {
                var data = json.service[i];
                if (data.latitude != 0) {
                    latLng = new google.maps.LatLng(data.latitude, data.longitude);
                    var html = '<div style="color:#1b1862">' + data.name + '</div>';
                    if (type == 'officialGarage') {
                        if(data.hasOwnProperty('img_name')){
                          if(data.img_name!=""){
                              iconLocation = iconBrandPath + data.img_name ;
                          }
                        }
                    }
                    var marker = new google.maps.Marker({
                        position: latLng,
                        map: map,
                        title: data.name,
                        icon: iconLocation
                    });
                    markers_arr.push(marker);
                    bindInfoWindow(marker, map, infoWindow, html);

                }
            }
            google.maps.event.trigger(map, 'resize');
            if (markers_arr.length > 0) {
                latLng2 = markers_arr[markers_arr.length - 1].position;
                map.setZoom(15);
                map.setCenter(latLng2);
            } else {
                if (json.district_name != null) {
                    var address = json.district_name + ' ' + json.province_name;
                    address = address.replace("ฯ", "");
                } else {
                    var address = json.province_name;
                }
                geocodeAddress(address, geocoder, map);
            }
        }

        function geocodeAddress(address, geocoder, resultsMap) {
            deleteMarker();
            geocoder.geocode({
                'address': address
            }, function(results, status) {
                if (status === 'OK') {
                    map.setZoom(15);
                    resultsMap.setCenter(results[0].geometry.location);
                } else {
                    alert('Geocode was not successful for the following reason: ' + status);
                }
            });
        }

        function bindInfoWindow(marker, map, infoWindow, html) {
            google.maps.event.addListener(marker, 'click', function() {
                map.setZoom(15);
                map.setCenter(marker.position);
                google.maps.event.trigger(map, 'resize');
                infoWindow.setContent(html);
                infoWindow.open(map, marker);
            });
            infoWindow.setContent(html);
            infoWindow.open(map, marker);
        }

        function deleteMarker() {
            for (var i = 0; i < markers_arr.length; i++) {
                markers_arr[i].setMap(null);
            }
            markers_arr = [];
        }

        function limitZoomMarker(max, markers_arr, zoomLevel) {
            if (zoomLevel < max) {
                for (i = 0; i < markers_arr.length; i++) {
                    markers_arr[i].setVisible(false);
                    infoWindow.close();
                }
            } else if (zoomLevel >= max) {
                for (i = 0; i < markers_arr.length; i++) {
                    markers_arr[i].setVisible(true);
                }
            }
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDSHM_Qe7iA9JxQaB6jePBAg_43V-Tzw3U&callback=initMap"
    async defer></script>

@endsection
