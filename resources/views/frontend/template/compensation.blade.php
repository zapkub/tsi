<?php
$lang = session()->get('locale');
?>
@extends('frontend.layouts/main')

@section('title','Requesting for Compensation')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/claim.css') }}"/>
@endsection

@section('content')
  <div id="content">
    <div class="container">
      <div class="col-sm-10 col-sm-offset-1">
        <h1 class="headline">การเรียกร้องค่าสินไหมทดแทน</h1>

        <div class="box">
          <div class="topic">เอกสารประกอบการเรียกร้องค่าสินไหมทดแทน</div>

          <div class="detail">
              <strong>1. การประกันอัคคีภัย, ประกันภัยทรัพย์สิน และ การประกันภัยเบ็ดเตล็ด</strong><br/>
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1.1 หนังสือการแจ้งเหตุ<br/>
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1.2 ภาพถ่ายความเสียหาย<br/>
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1.3 ใบเสนอราคาค่าซ่อมแซม/ใบเสร็จรับเงิน<br/>
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1.4 สำเนาบันทึกประจำวัน (ถ้ามี)<br/>
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1.5 เอกสารประกอบเพิ่มเติมอื่น ๆ ซึ่งจะแจ้งขอเป็นกรณีไป<br/><br/>

              <strong>2. การประกันภัยอุบัติเหตุ</strong><br/>
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.1 ค่ารักษาพยาบาล<br/>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.1.1  แบบฟอร์มการเรียกร้องค่าสินไหมทดแทน<br/>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.1.2  สำเนาบัตรประชาชน พร้อมรับรองสำเนาถูกต้อง<br/>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.1.3  ใบรับรองแพทย์ (ฉบับจริง)<br/>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.1.4  ใบเสร็จรับเงิน (ฉบับจริง)<br/>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.1.5  เอกสารประกอบเพิ่มเติมอื่น ๆ ซึ่งจะแจ้งขอเป็นกรณีไป<br/><br/>
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.2 กรณีการเสียชีวิต<br/>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.2.1  แบบฟอร์มการเรียกร้องค่าสินไหมทดแทน<br/>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.2.2  สำเนาบัตรประชาชน พร้อมรับรองสำเนาถูกต้อง<br/>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.2.3  สำเนาใบมรณบัตร<br/>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.2.4  สำเนารายงานชันสูตรพลิกศพ <br/>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.2.5  สำเนาบันทึกประจำวันตำรวจ<br/>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.2.6  สำเนาผลสรุปคดีของตำรวจ<br/>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.2.7  สำเนาทะเบียนบ้าน  สำเนาบัตรประชาชน  (ผู้เสียชีวิต)<br/>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.2.8  สำเนาบัตรผู้รับผลประโยชน์<br/>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.2.9  หนังสือแต่งตั้งผู้จัดการมรดก      <br/>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.2.10เอกสารประกอบเพิ่มเติมอื่น ๆ ซึ่งจะแจ้งขอเป็นกรณีไป<br/><br/>
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.3 กรณีสูญเสียอวัยวะ หรือสายตา และทุพพลภาพถาวร<br/>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.3.1  แบบฟอร์มการเรียกร้องค่าสินไหมทดแทน<br/>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.3.2  สำเนาบัตรประชาชน พร้อมรับรองสำเนาถูกต้อง<br/>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.3.3  ใบรายงานทางการแพทย์ (ระบุอาการ)<br/>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.3.4  ใบรับรองแพทย์ (ฉบับจริง)<br/>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.3.5  สำเนาบัตรประชาชนผู้พิการ<br/>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.3.6  เอกสารประกอบเพิ่มเติมอื่น ๆ ซึ่งจะแจ้งขอเป็นกรณีไป<br/><br/>

               <strong>3. สำหรับประกันภัยทางทะเลและขนส่ง</strong><br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3.1 กรมธรรม์ฉบับจริง<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3.2 ใบกำกับสินค้า (INVOICE)<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3.3 ใบกำกับหีบห่อสินค้า (PACKING LIST)<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3.4 ใบตราส่งสินค้า (BILL OF LADING) ในกรณีทางเรือ  และ (AIR WAYBILL) ในกรณีทางอากาศ<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3.5 ภาพถ่ายของสินค้าที่เสียหาย<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3.6 หนังสือเรียกร้องค่าสินไหมไปยังผู้รับประกันภัย<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3.7 หนังสือเรียกร้องค่าสินไหมไปยังผู้ขนส่ง<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3.8 เอกสารอื่น ๆ ที่แสดงความเสียหาย หรือสูญหาย ที่ออกโดยผู้ขนส่ง / การท่าเรือ / การท่าอากาศยาน เช่น WHARF SURVEY NOTE , DMC , ใบรับสินค้าที่มีลายเซ็นต์พนักงาน ขับรถ  หนังสือตอบรับจากผู้ขนส่ง<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3.9 เอกสารประกอบเพิ่มเติมอื่น ๆ ซึ่งจะแจ้งขอเป็นกรณีไป
              </li>
            </ol>
          </div>
        </div>

        <div class="box">
          <div class="topic">ขั้นตอนและวิธีดำเนินการเรียกร้องค่าสินไหมทดแทน</div>

          <div class="detail">
            เมื่อเกิดความเสียหายขึ้น ผู้เอาประกันภัยมีหน้าที่ดังนี้<br/><br/>

            <strong>1. รีบดำเนินการแจ้งความเสียหายให้บริษัททราบทันทีดังนี้</strong><br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1.1 แจ้งทางโทรศัพท์,  โทรสาร,   อีเมล<br>

            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>ติดต่อเจ้าหน้าที่</th>
                            <th>ชื่อ-สกุล</th>
                            <th>โทรศัพท์</th>
                            <th>มือถือ</th>
                            <th>อีเมล</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td>ส่วนสินไหมทดแทนอัคคีภัย / ทรัพย์สิน</td>
                            <td>
                                1.  นายสาโรจน์ พิเชษฐศิริพร<br>
                                2.  น.ส.อลิสา อิงควณิช<br>
                                3.  นางสุภาพร นุกาศรัมย์
                            </td>
                            <td>
                                02-6309055 ต่อ 312<br>
                                02-6309055 ต่อ 540
                            </td>
                            <td>
                                081-4955091<br>
                                081-4204879<br>
                                089-7704967
                            </td>
                            <td>
                                <a href="mailto:claimfire@tsi.co.th">claimfire@tsi.co.th</a><br>
                                <a href="mailto:Alisa.Ing@tsi.co.th">Alisa.Ing@tsi.co.th</a><br>
                                <a href="mailto:marine.dep@tsi.co.th">marine.dep@tsi.co.th</a>
                            </td>
                        </tr>
                        <tr>
                            <td>ส่วนสินไหมทดแทนอุบัติเหตุ / นักเรียน</td>
                            <td>
                                1.  น.ส.อลิสา อิงควณิช<br>
                                2.  นางสุภาพร นุกาศรัมย์
                            </td>
                            <td>
                                02-6309055 ต่อ 540
                            </td>
                            <td>
                                081-4204879<br>
                                089-7704967
                            </td>
                            <td>
                                <a href="mailto:Alisa.Ing@tsi.co.th">Alisa.Ing@tsi.co.th</a><br>
                                <a href="mailto:marine.dep@tsi.co.th">marine.dep@tsi.co.th</a>
                            </td>
                        </tr>
                        <tr>
                            <td>ส่วนสินไหมทดแทนประกันภัยทางทะเลและขนส่ง</td>
                            <td>
                                1.  นางสุภาพร นุกาศรัมย์<br>
                                2.  นายสาโรจน์ พิเชษฐศิริพร
                            </td>
                            <td>
                                02-6309055 ต่อ 540<br>
                                02-6309055 ต่อ 312
                            </td>
                            <td>
                                089-7704967<br>
                                081-4955091
                            </td>
                            <td>
                                <a href="mailto:marine.dep@tsi.co.th">marine.dep@tsi.co.th</a><br>
                                <a href="mailto:claimfire@tsi.co.th">claimfire@tsi.co.th</a>
                            </td>
                        </tr>
                        <tr>
                            <td>ส่วนสินไหมทดแทนเบ็ดเตล็ด</td>
                            <td>
                                1. น.ส.อลิสา อิงควณิช<br>
                                2. นางสุภาพร นุกาศรัมย์
                            </td>
                            <td>
                                02-6309055 ต่อ 540
                            </td>
                            <td>
                                081-4204879<br>
                                089-7704967
                            </td>
                            <td>
                                <a href="mailto:Alisa.Ing@tsi.co.th">Alisa.Ing@tsi.co.th</a><br>
                                <a href="mailto:marine.dep@tsi.co.th">marine.dep@tsi.co.th</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1.2  ระบุชื่อของบุคคลผู้ประสานงานที่ต้องการให้บริษัทติดต่อ<br><br>
            <strong>2.  ดำเนินการบรรเทาความเสียหาย ไม่ให้ได้รับความเสียหายเพิ่มขึ้น</strong><br><br>
            <strong>3.  เก็บรักษาทรัพย์สินที่เสียหาย หรือถ่ายรูปไว้เพื่อให้บริษัทตรวจสอบ</strong><br><br>
            <strong>4.  กรณีทรัพย์สินเสียหายจากบุคคลภายนอก ให้ดำเนินการแจ้งความกับเจ้าหน้าที่พนักงานสอบสวนท้องที่ไว้เป็นหลักฐาน</strong><br><br>
            <strong>5.  ให้ความร่วมมือกับเจ้าหน้าที่หรือตัวแทนของบริษัทฯ ในการตรวจสภาพความเสียหาย หรือขอเอกสารประกอบเพิ่มเติม</strong>
          </div>
        </div>

        <div class="box">
          <div class="topic">ขั้นตอนการพิจารณาการเรียกร้องค่าสินไหมทดแทน</div>

          <div class="detail">
              <strong>1.  เมื่อได้รับแจ้งการเรียกร้องค่าสินไหมทดแทน บริษัทจะดำเนินการดังนี้</strong><br>
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1.1  กรณีไม่ต้องสำรวจความเสียหาย บริษัทฯ จะขอเอกสารประกอบการพิจารณาการเรียกร้อง
                          ค่าสินไหมทดแทน (Click เอกสารประกอบการพิจารณา)<br>
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1.2  กรณีต้องสำรวจความเสียหาย บริษัทฯ จะจัดส่งเจ้าหน้าที่เข้าสำรวจความเสียหายตามนัดหมาย
                          และออกหนังสือขอเอกสารประกอบการพิจารณาเรียกร้องค่าสินไหมทดแทน
                          (Click เอกสารประกอบการพิจารณา)<br><br>

              <strong>2.  เมื่อได้รับเอกสารตามข้อ 1. ครบถ้วนแล้ว บริษัทฯ จะพิจารณาการเรียกร้องค่าสินไหมทดแทน
                   และ แจ้งผลการพิจารณาให้ผู้เอาประกันภัย ผู้รับประโยชน์ หรือผู้มีสิทธิเรียกร้องตามกรมธรรม์
                   ประกันภัย ดังนี้</strong><br>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.1  กรณีไม่ต้องสำรวจความเสียหาย บริษัทฯ จะแจ้งผลการพิจารณาให้ทราบภายใน 7-15 วัน
                         นับแต่วันที่ได้รับเอกสารตามข้อ 1. ครบถ้วน<br>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.2  กรณีต้องสำรวจความเสียหาย บริษัทฯ จะแจ้งผลการพิจารณาให้ทราบภายใน 7-15 วัน
                         นับแต่วันที่ได้รับเอกสารตามข้อ 1. ครบถ้วน และรายงานจากเจ้าหน้าที่สำรวจภัย<br><br>

              <strong>3.  บริษัทฯ จะจ่ายค่าสินไหมทดแทนภายใน 15 วัน นับแต่ได้รับหนังสือตกลงรับชดใช้ค่าสินไหมทดแทนที่ลงนามเรียบร้อยแล้ว</strong><br><br>

              <strong>4.  กรณีความเสียหายไม่อยู่ภายใต้ความคุ้มครองตามกรมธรรม์ บริษัทฯ จะแจ้งให้ผู้เอาประกันภัย /
                   ผู้รับประโยชน์ / ผู้มีสิทธิเรียกร้องตามกรมธรรม์ประกันภัยทราบ และออกหนังสือแจ้งผลการ
                   พิจารณาการเรียกร้องค่าสินไหมทดแทน</strong><br/><br/>

              <strong>5.  หากไม่เห็นด้วยกับผลการพิจารณาการเรียกร้องค่าสินไหมทดแทน และไม่สามารถหาข้อยุติได้
                   ผู้เอาประกันภัย / ผู้รับประโยชน์ / ผู้มีสิทธิเรียกร้องตามกรมธรรม์ประกันภัย สามารถดำเนินการ
                   ดังนี้</strong><br/>
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 5.1  ติดต่อ/ส่งเอกสารหรือข้อมูลเพิ่มเติมกลับมายังบริษัทฯ เพื่อทบทวนการพิจารณาจ่ายค่าสินไหม<br>
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 5.2  ติดต่อหน่วยงานรับเรื่องร้องเรียนของบริษัทฯ >> <a href="{{ URL::to('petition') }}">คลิกที่นี่</a>

          </div>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('more-script')
<script>
  $(function(){
    $('.box .topic').on('click', function(){
      if ($(this).parent().hasClass('active')){
        $(this).parent().find('.detail').slideUp();
        $(this).parent().removeClass('active');
      }
      else{
        $('.box.active .detail').slideUp();
        $(this).parent().find('.detail').slideDown();
        $('.box.active').removeClass('active');
        $(this).parent().addClass('active');
      }
    });
  });
</script>
@endsection
