<?php
$lang = session()->get('locale');
$i=1;
?>
@extends('frontend.layouts/main')

@section('title','Career')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/career.css') }}"/>
@endsection

@section('content')
<div id="career">
  <div class="container">
    <div class="headline">{{ trans('career.career') }}</div>

    <div class="content">
      <p>{{ trans('career.description') }}</p>

      <div class="table-responsive">
        <table class="table">
          <thead>
            <tr>
              <th class="text-center">No.</th>
              <th class="col-xs-6">{{ trans('career.position') }}</th>
              <th class="text-center">{{ trans('career.amount') }}</th>
              <th class="text-center">{{ trans('career.position_des') }}</th>
            </tr>
          </thead>

          <tbody>
            @foreach($careers as $career)
              <?php
                $position = $career->name_th;
                $detail = $career->detail_th;
                $property = $career->property_th;
                $qty = $career->qty;
                if($lang == 'en'){
                  if($career->name_en != '') $position = $career->name_en;
                  if($career->detail_en != '') $detail = $career->detail_en;
                  if($career->property_en != '') $property = $career->property_en;
                }
              ?>
            <tr>
              <td class="text-center">{{ $i }}</td>
              <td>
                {{ $position }}

                <div class="item-{{ $career->career_id }}" style="display: none;">
                  {!! '<p>'.Html::decode($detail).'</p>' !!}
                  {!! '<p>'.Html::decode($property).'</p>' !!}
                </div>
              </td>
              <td class="text-center">{{ $qty }}</td>
              <td class="text-center">
                <a href="#" class="more-detail" data-id="{{ $career->career_id }}">
                  <i class="glyphicon glyphicon-plus"></i>
                </a>
              </td>
            </tr>
              <?php $i++; ?>
            @endforeach
          </tbody>
        </table>

        <div class="topic">{{ trans('career.document') }}</div>
        <p>{{ trans('career.document_txt') }}</p>

        <div class="topic">{{ trans('career.apply') }}</div>
        <p>
          {{ trans('career.apply_txt1') }}<br/>
          {{ trans('career.apply_txt2') }}
        </p>
      </div>
    </div>
  </div>
</div>
@endsection
@section('more-script')
  <script>
    $(function(){
      $('.more-detail').on('click',function(e){
        e.preventDefault();
        data_id = $(this).data('id');
//        console.log(data_id);
        $('.item-'+data_id).slideToggle();
      });
    })
  </script>
@endsection