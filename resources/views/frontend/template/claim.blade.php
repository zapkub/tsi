<?php
$lang = session()->get('locale');
?>
@extends('frontend.layouts/main')

@section('title','How to Claim')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/claim.css') }}"/>
@endsection

@section('content')
  <div id="banner"></div>

  <div id="content">
    <div class="container">
      <div class="col-sm-10 col-sm-offset-1">
        <h1 class="headline">ข้อปฏิบัติเมื่อนำรถเข้าซ่อม</h1>

        <div class="box active">
          <div class="topic">กรณีลูกค้าซื้อกรมธรรม์คุ้มครองความเสียหายตัวรถ ประเภทการจัดซ่อมแบบซ่อมศูนย์</div>

          <div class="detail">
            <ol>
              <li>บริษัทฯแนะนำลูกค้านำรถกลับไปซ่อมที่ศูนย์ซ่อม ของโชว์รูมที่ลูกค้าซื้อรถมา</li>
              <li>หากลูกค้าไม่สะดวกนำรถกลับเข้าซ่อมที่ศูนย์ซ่อมของโชว์รูมที่ลูกค้าซื้อรถมา บริษัทฯแนะนำให้ใช้บริการศูนย์ซ่อมที่อยู่ใกล้ลูกค้ามากที่สุด </li>
              <li>เอกสารที่ใช้ในการติดต่อเจ้าหน้าที่ของศูนย์ซ่อม มีดังนี้<br/>
                3.1 ใบรับรองความเสียหาย<br/>
                3.2 สำเนาใบขับขี่<br/>
                3.3 สำเนาทะเบียนรถ
              </li>
              <li>เจ้าหน้าที่ของศูนย์ซ่อมเมื่อได้รับเอกสารทั้งหมด จะออกใบรับรถให้กับทางลูกค้า</li>
              <li>เจ้าหน้าที่ของศูนย์ซ่อม ดำเนินการประเมินราคาค่าซ่อม นำเสนอบริษัทฯ เพื่อขออนุมัติการจัดซ่อม</li>
              <li>เจ้าหน้าที่ประเมินราคาของบริษัทฯ พิจารณาอนุมัติจัดซ่อมให้ศูนย์ซ่อม ดำเนินการจัดซ่อมต่อไป ภายใน 3 วัน</li>
              <li>เมื่อทางศูนย์ซ่อม ดำเนินการซ่อมรถให้กับลูกค้าเสร็จ เมื่อถึงกำหนดวัน เวลาที่ศูนย์ซ่อมนัดหมายรับรถ ให้ลูกค้านำใบรับรถติดต่อศูนย์ซ่อมเพื่อนำรถออก</li>
            </ol>
          </div>
        </div>

        <div class="box">
          <div class="topic">กรณีลูกค้าซื้อกรมธรรม์คุ้มครองความเสียหายตัวรถ ประเภทการจัดซ่อมเป็นซ่อมอู่</div>

          <div class="detail">
            <ol>
              <li>บริษัทฯแนะนำลูกค้านำรถซ่อมที่อู่ในเครือของบริษัทฯ ที่ลูกค้าสะดวกที่สุด(สามารถดูรายชื่ออู่ได้ที่ www.tsi.co.th )</li>
              <li>เอกสารที่ใช้ในการติดต่อเจ้าหน้าที่ของอู่ซ่อม มีดังนี้<br/>
                2.1 ใบรับรองความเสียหาย<br/>
                2.2 สำเนาใบขับขี่<br/>
                2.3 สำเนาทะเบียนรถ
              </li>
              <li>เจ้าหน้าที่ของอู่ซ่อมเมื่อได้รับเอกสารทั้งหมด จะออกใบรับรถให้กับทางลูกค้า</li>
              <li>เจ้าหน้าที่ของอู่ซ่อม ดำเนินการประเมินราคาค่าซ่อม นำเสนอบริษัทฯ เพื่อขออนุมัติการจัดซ่อม</li>
              <li>เจ้าหน้าที่ประเมินราคาของบริษัทฯ พิจารณาอนุมัติจัดซ่อมให้อู่ซ่อม ดำเนินการจัดซ่อมต่อไป ภายใน 3 วัน</li>
              <li>เมื่อทางอู่ซ่อม ดำเนินการซ่อมรถให้กับลูกค้าเสร็จ เมื่อถึงกำหนดวัน เวลาที่อู่ซ่อมนัดหมายรับรถ ให้ลูกค้านำใบรับรถติดต่ออู่ซ่อมเพื่อนำรถออก </li>
            </ol>
          </div>
        </div>

        <div class="box">
          <div class="topic">กรณีลูกค้าประสงค์นำรถไปซ่อมเอง ซึ่งไม่ใช่อู่ในเครือของบริษัทฯ(อู่นอกเครือ)</div>

          <div class="detail">
            <ol>
              <li>เมื่อลูกค้าติดต่ออู่ซ่อมที่จะนำรถเข้าซ่อมได้แล้ว ให้อู่ซ่อมนั้นๆ เร่งดำเนินการจัดทำใบประเมินราคา เพื่อนำเสนอบริษัทฯขออนุมัติจัดซ่อม </li>
              <li>เอกสารที่ใช้ในการนำเสนอบริษัทฯขออนุมัติจัดซ่อม มีดังนี้
                <ul>
                  <li>ใบรับรองความเสียหาย</li>
                  <li>ใบประเมินราคาค่าซ่อม</li>
                  <li>ภาพถ่ายความเสียหาย ทุกรายการ กรุณาถ่ายภาพให้ชัดเจน</li>
                </ul>
              </li>
              <li>ช่องทางการนำเสนอเอกสารเพื่อขออนุมัติจัดซ่อม มีดังนี้
                <ul>
                  <li>ลูกค้าแฟกซ์เอกสารเพื่อนำเสนอขออนุมัติ มาที่ เบอร์ 02-2360318</li>
                  <li>ลูกค้าอีเมล์พร้อมแนบเอกสารเพื่อนำเสนอขออนุมัติ มาที่ claims.ev@tsi.co.th  </li>
                  <li>ลูกค้านำเอกสารเพื่อนำเสนอขออนุมัติ มาที่บริษัทฯด้วยตนเอง</li>
                </ul>

                *** บางกรณีอู่ซ่อมจะดำเนินขั้นตอนนี้แทนลูกค้า เพื่ออำนวยความสะดวกให้
              </li>
              <li>เจ้าหน้าที่ประเมินราคาของบริษัทฯ พิจารณาอนุมัติจัดซ่อมให้อู่ซ่อม ดำเนินการจัดซ่อมต่อไป</li>
              <li>เมื่อทางอู่ซ่อม ดำเนินการซ่อมรถให้กับลูกค้าเสร็จ เมื่อถึงกำหนดวัน เวลาที่อู่ซ่อมนัดหมายรับรถ ให้ลูกค้านำใบรับรถติดต่ออู่ซ่อมเพื่อนำรถออก </li>
              <li>ขั้นตอนการเบิกค่าใช้จ่ายในการจัดซ่อม ให้จัดเตรียมเอกสาร ดังนี้
                <ul>
                  <li>ใบรับรองความเสียหาย(ฉบับจริง)</li>
                  <li>ใบประเมินราคาค่าซ่อม ที่ได้รับการอนุมัติจัดซ่อม โดยมีประทับตราบริษัทฯ</li>
                  <li>สำเนาใบขับขี่</li>
                  <li>สำเนาทะเบียนรถ</li>
                  <li>ใบเสร็จรับเงินฉบับจริง</li>
                  <li>ภาพถ่ายในการจัดซ่อม : ก่อนซ่อม ขณะซ่อม หลังซ่อม</li>
                  <li>สำเนาบัญชีธนาคารของผู้ครอบครองรถ</li>
                </ul>
              </li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('more-script')
<script>
  $(function(){
    $('.box.active .detail').slideDown();

    $('.box .topic').on('click', function(){
      if ($(this).parent().hasClass('active')){
        $(this).parent().find('.detail').slideUp();
        $(this).parent().removeClass('active');
      }
      else{
        $('.box.active .detail').slideUp();
        $(this).parent().find('.detail').slideDown();
        $('.box.active').removeClass('active');
        $(this).parent().addClass('active');
      }
    });
  });
</script>
@endsection
