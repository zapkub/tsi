<?php
$lang = session()->get('locale');
?>
@extends('frontend.layouts/main')

@section('title','Petition')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/product-global.css') }}"/>
@endsection

@section('content')
<div id="main-content">
  <div class="container">
    <div class="headline">
      <div class="headline-sub">{{ 'เรื่องร้องเรียน' }}</div>
    </div>

    <div class="contact-form">
      @if(session()->has('successMsg'))
        <div class="col-sm-offset-2 col-sm-8 alert alert-success text-success text-center">{{ session()->get('successMsg') }}</div>
      @endif

      <div class="col-sm-offset-2 col-sm-8 ">
        <form class="form-horizontal" action="{{ url()->to('petition/submit') }}" method="post" enctype="multipart/form-data">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="form-group">
            <div class="row">
              <div class="col-md-6">
                <label class="radio-inline">
                  <input type="radio" name="subject" value="ร้องเรียนเรื่องสินไหม" checked> &nbsp; ร้องเรียนเรื่องสินไหม
                </label>
              </div>
              <div class="col-md-6">
                <label class="radio-inline">
                  <input type="radio" name="subject" value="ร้องเรียนเรื่องอื่นๆ"> &nbsp; ร้องเรียนเรื่องอื่นๆ
                </label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-6">
                <label>ชื่อ - นามสกุล</label>
                <input type="text" name="name" value="{{ old('name') }}">
                @if(!empty($errors->first('name')))
                  <div class="error text-danger">{{ $errors->first('name') }}</div>
                @endif
              </div>
              <div class="col-md-6">
                <label>เบอร์โทรศัพท์</label>
                <input type="text" name="mobile" value="{{ old('mobile') }}" maxlength="10">
                @if(!empty($errors->first('mobile')))
                  <div class="error text-danger">{{ $errors->first('mobile') }}</div>
                @endif
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-6">
                <label>กรมธรรม์เลขที่/เลขรับแจ้งอุบัติเหตุ</label>
                <input type="text" name="insurance_no" value="{{ old('insurance_no') }}">
                @if(!empty($errors->first('insurance_no')))
                  <div class="error text-danger">{{ $errors->first('insurance_no') }}</div>
                @endif
              </div>
              <div class="col-md-6">
                <label>อีเมล</label>
                <input type="text" name="email" value="{{ old('email') }}">
                @if(!empty($errors->first('email')))
                  <div class="error text-danger">{{ $errors->first('email') }}</div>
                @endif
              </div>
            </div>
          </div>
          <div class="form-group">
            <label>ข้อความ</label>
            <textarea name="msg" rows="5">{{ old('msg') }}</textarea>
            @if(!empty($errors->first('msg')))
              <div class="error text-danger">{{ $errors->first('msg') }}</div>
            @endif
          </div>

          {{--<div class="form-group">--}}
            {{--<label>แนบไฟล์เอกสารหรือรูปภาพ</label>--}}
            {{--<input type="file" name="file_name" value="{{ old('file_name') }}">--}}

          {{--</div>--}}
          <div class="form-group text-center">
            <button type="submit" class="btn btn-buy">ส่งข้อความ</button>
          </div>
        </form>
      </div>
    </div>


  </div>
</div>
@endsection

@section('more-script')

@endsection
