<?php
$lang = session()->get('locale');
?>
@extends('frontend.layouts/main')

@section('title','News')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/news.css') }}"/>
@endsection

@section('content')
<div id="news">
  <div class="container">
    <div class="headline">{{ trans('index.news_activity') }}</div>
    {{--<div class="sub-headline">{{ trans('about.company_name') }}</div>--}}

    <div class="list row">
      @foreach ($news as $key => $news_item)
        <?php
        $news_name = $news_item->name_th;
        $news_des = $news_item->short_description_th;
        $img_path = $news_item->img_path;
        $url_detail = URL::to('news/'.$news_item->news_id);
        if($lang == 'en'){
          if($news_item->name_en != '') $news_name = $news_item->name_en;
          if($news_item->short_description_en != '') $news_des = $news_item->short_description_en;
        }
        if($img_path != '' && file_exists('uploads/news/'.$img_path)){
          $img_path = URL::asset('uploads/news/'.$img_path);
        }else{
          $img_path = 'http://dummyimage.com/300x140/f0f0f0/d9d9d9.jpg&text=TSI';
        }
        ?>
      <div class="col-sm-6 col-md-4">
        <div class="news">
          <a href="{{ $url_detail }}" title="{{ $news_name }}">
            <div class="image" style="background-image: url('{{ $img_path }}')"></div>
          </a>

          <div class="title"><a href="{{ $url_detail }}" title="{{ $news_name }}">{{ $news_name }}</a></div>
          <div class="description">
            {{ $news_des }}... <a href="{{ $url_detail }}" class="read-more">{{ trans('index.read_more') }}</a>
          </div>
        </div>
      </div>
      @if (($key+1)%2==0)
        <div class="clearfix visible-sm-block"></div>
      @endif
      @if (($key+1)%3==0)
        <div class="clearfix visible-md-block visible-lg-block"></div>
      @endif
      @endforeach
    </div>
  </div>
</div>
@endsection
