<?php
$lang = session()->get('locale');
$name = $news->name_th;
$des = $news->description_th;
if($lang == 'en') {
  if($news->name_en != '') $name = $news->name_en;
  if($news->description_en != '') $des = $news->description_en;
}

?>
@extends('frontend.layouts/main')

@section('title','News')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/news.css') }}"/>
@endsection

@section('content')
<div id="news">
  <div class="container">
    <div class="headline">{{ $name }}</div>
    {{--<div class="sub-headline">{{ trans('about.company_name') }}</div>--}}
    <div class="content">
      {!! Html::decode($des) !!}
    </div>

  </div>
</div>
@endsection
