<?php
$lang = session()->get('locale');
?>
@extends('frontend.layouts/main')

@section('title','Contact Us')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/contact.css') }}"/>
@endsection

@section('content')
  <div id="contact">
    <div class="container">
      <div class="headline">ติดต่อเรา</div>
      <div class="tsi-contact row">
        <div class="address col-md-8">
          ที่อยู่ 160 อาคารไทยเศรษฐกิจ ถนนสาทรเหนือ<br>
          เขตบางรัก กรุงเทพฯ 10500
        </div>
        <div class="phone col-md-4">
          โทร 0-2630-9055, 0-2630-9111 (อัตโนมัติ 70 คู่สาย)<br>
          แฟกซ์ 0-2237-4621, 0-2237-4624<br>
          อีเมล <a href="mailto:webmaster@tsi.co.th">webmaster@tsi.co.th</a>
        </div>
      </div>

      <div class="map">
        <iframe frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?key={{ env('MAP_API_KEY') }}&q=บมจ.ไทยเศรษฐกิจประกันภัย" allowfullscreen></iframe>
      </div>

      <table class="table table-detail">
        <thead>
          <tr>
            <th>ติดต่อ ฝ่าย/หน่วยงานต่างๆในบริษัท</th>
            <th class="text-center">รายละเอียด</th>
          </tr>
        </thead>
        <tbody>
          @foreach($departments as $key => $item)
            <tr class="department" data-id="toggle-{{ $key+1 }}">
              <td class="col-md-10">{{ $item->department_name }}</td>
              <td class="col-md-2 text-center plus">+</td>
            </tr>
            <tr class="more-detail" id="toggle-{{ $key+1 }}">
              <td colspan="2">
                {!! $item->detail !!}
              </td>
            </tr>
          @endforeach

                <span class="name">คุณสุรชัย  นาควังสากุล</span><br>
                โทร 0-2630-9055, 0-2630-9111 ต่อ 321<br>
                อีเมล <a href="mailto:surachai.nak@tsi.co.th">surachai.nak@tsi.co.th</a><br><br>

                <span class="name">คุณวิมลรัตน์ นาชะ</span><br>
                โทร.0-2630-9055, 0-2630-9111 ต่อ 320<br>
                อีเมล <a href="mailto:wimonrat.nac@tsi.co.th">wimonrat.nac@tsi.co.th</a>
            </td>
          </tr>

          {{--<tr class="department" data-id="toggle-1">--}}
            {{--<td class="col-md-10">ฝ่ายรับประกันรถยนต์</td>--}}
            {{--<td class="col-md-2 text-center plus">+</td>--}}
          {{--</tr>--}}
          {{--<tr class="more-detail" id="toggle-1">--}}
            {{--<td colspan="2">--}}
                {{--<span class="name">คุณอารยา แสงเงิน</span><br>--}}
                {{--โทร 0-2630-9055, 0-2630-9111 ต่อ 313<br>--}}
                {{--อีเมล <a href="mailto:araya.sae@tsi.co.th">araya.sae@tsi.co.th</a><br><br>--}}

                {{--<span class="name">คุณสุรชัย  นาควังสากุล</span><br>--}}
                {{--โทร 0-2630-9055, 0-2630-9111 ต่อ 321<br>--}}
                {{--อีเมล <a href="mailto:surachai.nak@tsi.co.th">surachai.nak@tsi.co.th</a><br><br>--}}

                {{--<span class="name">คุณวิมลรัตน์ นาชะ</span><br>--}}
                {{--โทร.0-2630-9055, 0-2630-9111 ต่อ 320<br>--}}
                {{--อีเมล <a href="mailto:wimonrat.nac@tsi.co.th">wimonrat.nac@tsi.co.th</a>--}}
            {{--</td>--}}
          {{--</tr>--}}

          {{--<tr class="department" data-id="toggle-2">--}}
            {{--<td class="col-md-10">ฝ่ายสินไหมรถยนต์</td>--}}
            {{--<td class="col-md-2 text-center plus">+</td>--}}
          {{--</tr>--}}
          {{--<tr class="more-detail" id="toggle-2">--}}
            {{--<td colspan="2">--}}
              {{--<span class="name">คุณวรฐ  ชูสวัสดิ์</span><br>--}}
              {{--โทร 0-2630-9055, 0-2630-9111 ต่อ 137<br>--}}
              {{--อีเมล <a href="mailto:varot.chu@tsi.co.th">varot.chu@tsi.co.th</a>--}}
            {{--</td>--}}
          {{--</tr>--}}

          {{--<tr class="department" data-id="toggle-3">--}}
            {{--<td class="col-md-10">ฝ่ายรับประกันอัคคีภัยและเบ็ดเตล็ด</td>--}}
            {{--<td class="col-md-2 text-center plus">+</td>--}}
          {{--</tr>--}}
          {{--<tr class="more-detail" id="toggle-3">--}}
            {{--<td colspan="2">--}}
              {{--<span class="name">คุณพิชิต  เพ่งสุข</span><br>--}}
              {{--โทร 0-2630-9055, 0-2630-9111 ต่อ 307<br>--}}
              {{--อีเมล <a href="mailto:pichit.pen@tsi.co.th">pichit.pen@tsi.co.th</a><br><br>--}}
              {{--<!-- <span class="name">คุณทวีศักดิ์ ใจซื่อ</span><br>--}}
              {{--อีเมล <a href="mailto:thaweesak.jai@tsi.co.th">thaweesak.jai@tsi.co.th</a><br>--}}
              {{--โทร 0-2630-9055, 0-2630-9111 ต่อ 332 -->--}}
            {{--</td>--}}
          {{--</tr>--}}
          {{--<tr class="department" data-id="toggle-4">--}}
            {{--<td class="col-md-10">ฝ่ายรับประกันภัยทางทะเลและขนส่ง</td>--}}
            {{--<td class="col-md-2 text-center plus">+</td>--}}
          {{--</tr>--}}
          {{--<tr class="more-detail" id="toggle-4">--}}
            {{--<td colspan="2">--}}
              {{--<span class="name">คุณชลดา สวัสดิ์ประสิทธิ์</span><br>--}}
              {{--โทร 0-2630-9055, 0-2630-9111 ต่อ 225<br>--}}
              {{--อีเมล <a href="mailto:marine.dep@tsi.co.th">marine.dep@tsi.co.th</a>--}}
            {{--</td>--}}
          {{--</tr>--}}
          {{--<tr class="department" data-id="toggle-5">--}}
            {{--<td class="col-md-10">ฝ่ายกฎหมาย</td>--}}
            {{--<td class="col-md-2 text-center plus">+</td>--}}
          {{--</tr>--}}
          {{--<tr class="more-detail" id="toggle-5">--}}
            {{--<td colspan="2">--}}
              {{--<span class="name">คุณโชติ เนตรสุริวงค์</span><br>--}}
              {{--โทร 0-2630-9055, 0-2630-9111 ต่อ 506<br>--}}
              {{--อีเมล <a href="mailto:chot.net@tsi.co.th">chot.net@tsi.co.th</a>--}}
            {{--</td>--}}
          {{--</tr>--}}
          {{--<tr class="department" data-id="toggle-6">--}}
            {{--<td class="col-md-10">ฝ่ายรับเรื่องร้องเรียนกรณีมีข้อพิพาท</td>--}}
            {{--<td class="col-md-2 text-center plus">+</td>--}}
          {{--</tr>--}}
          {{--<tr class="more-detail" id="toggle-6">--}}
            {{--<td colspan="2">--}}
              {{--<span class="name">คุณดุษฎี หล่อคุณธรรม</span><br>--}}
              {{--โทร 0-2630-9055, 0-2630-9111 ต่อ 521<br>--}}
              {{--อีเมล <a href="mailto:dusadee.law@tsi.co.th">dusadee.law@tsi.co.th</a>--}}
            {{--</td>--}}
          {{--</tr>--}}
          {{--<tr class="department" data-id="toggle-7">--}}
            {{--<td class="col-md-10">ศูนย์รับแจ้งอุบัติเหตุ</td>--}}
            {{--<td class="col-md-2 text-center plus">+</td>--}}
          {{--</tr>--}}
          {{--<tr class="more-detail" id="toggle-7">--}}
            {{--<td colspan="2">--}}
              {{--ศูนย์รับแจ้งอุบัติเหตุ โทร 1352 ตลอด 24 ชั่วโมง--}}
            {{--</td>--}}
          {{--</tr>--}}

          <tr class="department" data-id="toggle-3">
            <td class="col-md-10">ฝ่ายรับประกันอัคคีภัยและเบ็ดเตล็ด</td>
            <td class="col-md-2 text-center plus">+</td>
          </tr>
          <tr class="more-detail" id="toggle-3">
            <td colspan="2">
              <span class="name">คุณพิชิต  เพ่งสุข</span><br>
              โทร 0-2630-9055, 0-2630-9111 ต่อ 307<br>
              อีเมล <a href="mailto:pichit.pen@tsi.co.th">pichit.pen@tsi.co.th</a><br><br>
              <!-- <span class="name">คุณทวีศักดิ์ ใจซื่อ</span><br>
              อีเมล <a href="mailto:thaweesak.jai@tsi.co.th">thaweesak.jai@tsi.co.th</a><br>
              โทร 0-2630-9055, 0-2630-9111 ต่อ 332 -->
            </td>
          </tr>
          <tr class="department" data-id="toggle-4">
            <td class="col-md-10">ฝ่ายรับประกันภัยทางทะเลและขนส่ง</td>
            <td class="col-md-2 text-center plus">+</td>
          </tr>
          <tr class="more-detail" id="toggle-4">
            <td colspan="2">
              <span class="name">คุณชลดา สวัสดิ์ประสิทธิ์</span><br>
              โทร 0-2630-9055, 0-2630-9111 ต่อ 225<br>
              อีเมล <a href="mailto:marine.dep@tsi.co.th">marine.dep@tsi.co.th</a>
            </td>
          </tr>
          <tr class="department" data-id="toggle-5">
            <td class="col-md-10">ฝ่ายกฎหมาย</td>
            <td class="col-md-2 text-center plus">+</td>
          </tr>
          <tr class="more-detail" id="toggle-5">
            <td colspan="2">
              <span class="name">คุณโชติ เนตรสุริวงค์</span><br>
              โทร 0-2630-9055, 0-2630-9111 ต่อ 506<br>
              อีเมล <a href="mailto:chot.net@tsi.co.th">chot.net@tsi.co.th</a>
            </td>
          </tr>
          <tr class="department" data-id="toggle-6">
            <td class="col-md-10">ฝ่ายรับเรื่องร้องเรียนกรณีมีข้อพิพาท</td>
            <td class="col-md-2 text-center plus">+</td>
          </tr>
          <tr class="more-detail" id="toggle-6">
            <td colspan="2">
              <span class="name">คุณดุษฎี หล่อคุณธรรม</span><br>
              โทร 0-2630-9055, 0-2630-9111 ต่อ 521<br>
              อีเมล <a href="mailto:dusadee.law@tsi.co.th">dusadee.law@tsi.co.th</a>
            </td>
          </tr>
          <tr class="department" data-id="toggle-7">
            <td class="col-md-10">ศูนย์รับแจ้งอุบัติเหตุ</td>
            <td class="col-md-2 text-center plus">+</td>
          </tr>
          <tr class="more-detail" id="toggle-7">
            <td colspan="2">
              ศูนย์รับแจ้งอุบัติเหตุ โทร 1352 ตลอด 24 ชั่วโมง
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
@endsection

@section('more-script')
  <script>
    $(function(){
      $('.department').on('click',function(){
        data_id = $(this).data('id');
        console.log(data_id);
        $('#'+data_id).slideToggle();
      });
    })
  </script>
@endsection
