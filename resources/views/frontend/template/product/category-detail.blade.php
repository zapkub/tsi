<?php
$lang = session()->get('locale');
?>
@extends('frontend.layouts/main')

@section('title','Category')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/product-global.css') }}"/>
    {{--<link rel="stylesheet" href="{{ URL::asset('css/front/home.css') }}"/>--}}
@endsection

@section('content')
<div id="banner"@if(!empty($cat->img_name) && file_exists('uploads/category/' . $cat->img_name)) style="background-image: url('{{ URL::asset('uploads/category/' . $cat->img_name) }}');"@endif>
  <div class="promo col-xs-10 col-sm-6">
    <div class="headline-main">{{ $cat['category_name_' . session()->get('locale')] }}</div>
    <div class="headline-sub">
      {!! $cat['description_' . session()->get('locale')] !!}
    </div>
  </div>
</div>
@if ($cat->category_id == 7)
<a href="{{ URL::to('what-to-do-do') }}" id="quick-faq-bar" class="button -default">
  <i class="glyphicon glyphicon-comment"></i> {{ trans('index.car_insurance') }}
</a>
@endif
<div id="product-list">
  <div class="container">
    <div class="headline">{{ trans('navbar.product') }}</div>

    @if ($cat->category_id == 7)

    
    <div class="list">
      <div class="row">
        @foreach ($subcats as $key => $category)
        <?php
        $img_name = $category->s_img_name;

        if (!empty($img_name) && file_exists('uploads/category_s/' . $img_name))
          $img_path = URL::asset('uploads/category_s/' . $category->s_img_name);
        else
          $img_path = 'http://dummyimage.com/600x400/DDDDDD/BBBBBB&text=TSI';
        ?>
          <a href="{{ URL::route('product.category-detail', $category->category_id) }}" class="item col-sm-4">
            <div>
              <div class="product-img">
                <img src="{{ $img_path }}" />

                <div class="icon-bg"></div>
                <img src="{{ URL::asset('images/home-icon-' . (!empty($cat->icon_name)?$cat->icon_name:'car') . '.png') }}" class="icon-img" />
              </div>

              <div class="product-info">
                <div class="product-name-main">{{ $category['category_name_' . session()->get('locale')] }}</div>
              </div>
            </div>
          </a>
          @if (($key+1)%3==0)
        </div>
        <div class="row">
          @endif
        @endforeach
      </div>
    </div>
    @else
    <div class="list">
      <div class="row">
        @foreach ($product_list as $key => $product)
        <?php
        $img_name = $product->s_img_name;

        if (!empty($img_name) && file_exists('uploads/product_s/' . $img_name))
          $img_path = URL::asset('uploads/product_s/' . $product->s_img_name);
        else
          $img_path = 'http://dummyimage.com/600x400/DDDDDD/BBBBBB&text=TSI';
        ?>
          <a href="{{ URL::route('product.product-detail', $product->product_id) }}" class="item col-sm-4">
            <div>
              <div class="product-img">
                <img src="{{ $img_path }}" />

                <div class="icon-bg"></div>
                <img src="{{ URL::asset('images/home-icon-' . (!empty($cat->icon_name)?$cat->icon_name:'car') . '.png') }}" class="icon-img" />
              </div>

              <div class="product-info">
                <div class="product-name-main">{{ $product->Product['product_name_' . session()->get('locale')] }}</div>
              </div>
            </div>
          </a>
          @if (($key+1)%3==0)
        </div>
        <div class="row">
          @endif
        @endforeach
      </div>
    </div>
    @endif
  </div>
</div>

<div id="product-description">
  <div class="container text-center">
    <div class="headline-main">{{ $cat['category_name_' . session()->get('locale')] }}</div>
    <div class="headline-sub col-sm-8 col-sm-offset-2">
      {!! $cat['description_' . session()->get('locale')] !!}
    </div>
  </div>
</div>

<div id="products" class="container">
  <div class="headline">
    <div class="main">Get Insurance for Anything You Need</div>
    <div class="sub">{{ trans('index.select_insurance') }}</div>
  </div>

  <div class="list">
    @foreach ($global_categories as $key => $cat)
    <a href="{{ URL::route('product.category-detail', $cat->category_id) }}" class="product col-xs-6 col-sm-4 col-md-2">
      <div class="icon square button -default">
        <img src="{{ URL::asset('images/home-icon-' . $cat->icon_name . '-hover.png') }}" alt="{{ $cat['category_name_' . session()->get('locale')] }}" class="img-hover" />
        <img src="{{ URL::asset('images/home-icon-' . $cat->icon_name . '.png') }}" alt="{{ $cat['category_name_' . session()->get('locale')] }}" class="img-normal" />
      </div>

      <div class="info">
        <div class="arrow"></div>
        <div class="name">{{ $cat['category_name_' . session()->get('locale')] }}</div>
      </div>
    </a>
    @if (($key+1)%2 == 0)
        <div class="clearfix visible-xs-block"></div>
    @endif
    @if (($key+1)%3 == 0)
        <div class="clearfix visible-sm-block"></div>
    @endif
    @endforeach
  </div>
</div>
@endsection
