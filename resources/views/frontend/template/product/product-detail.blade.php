<?php
$lang = session()->get('locale');
?>
@extends('frontend.layouts/main')

@section('title','Home')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/product-global.css') }}"/>
@endsection

@section('content')
<div id="banner"@if(!empty($product->img_name) && file_exists('uploads/product/' . $product->img_name)) style="background-image: url('{{ URL::asset('uploads/product/' . $product->img_name) }}');"@endif>
  <div class="promo col-xs-10 col-sm-6">
    <div class="headline-main">{{ $product['product_name_' . session()->get('locale')] }}</div>
    <div class="headline-sub">
      {!! $product['short_description_' . session()->get('locale')] !!}
    </div>
  </div>
</div>

<div id="main-content">
  <div class="container">
    <div class="headline">
      <div class="headline-main">"{{ $product['product_name_' . session()->get('locale')] }}"</div>
      <div class="description col-sm-8 col-sm-offset-2">
        {!! $product['description_' . session()->get('locale')] !!}
      </div>
    </div>

    <?php
    $protection = $product['protection_' . $lang];
    $warranty = $product['warranty_' . $lang];
    $privilege = $product['privilege_' . $lang];
    $franchise = $product['franchise_' . $lang];
    $inquiry = $product['inquiry_' . $lang];
    ?>
    <div class="content">
      <div class="option text-center">
        <a href="{{ url()->route('product.buy',$product->product_id) }}" class="button -default">สนใจทำประกัน</a>
      </div>

      @if(!empty($protection))
        <div class="block">
          <div class="topic"><i class="glyphicon glyphicon-chevron-right"></i> รายละเอียดความคุ้มครอง</div>
          <div class="detail">
            {!! $protection !!}
          </div>
        </div>
      @endif

      @if(!empty($warranty))
        <div class="block">
          <div class="topic"><i class="glyphicon glyphicon-chevron-right"></i> เงื่อนไขในการรับประกันภัย</div>
          <div class="detail">
            {!! $warranty !!}
          </div>
        </div>
      @endif
      @if(!empty($privilege))
        <div class="block">
          <div class="topic"><i class="glyphicon glyphicon-chevron-right"></i> สิทธิประโยชน์</div>
          <div class="detail">
            {!! $privilege !!}
          </div>
        </div>
      @endif

      @if(!empty($franchise))
        <div class="block">
          <div class="topic"><i class="glyphicon glyphicon-chevron-right"></i> สิทธิพิเศษ</div>
          <div class="detail">
            {!! $franchise !!}
          </div>
        </div>
      @endif

      @if(!empty($inquiry))
        <div class="block">
          <div class="topic"><i class="glyphicon glyphicon-chevron-right"></i> สอบถามรายละเอียดเพิ่มเติม</div>
          <div class="detail">
            {!! $inquiry !!}
          </div>
        </div>
      @endif
    </div>

    <div class="option text-center">
      <a href="{{ url()->route('product.buy',$product->product_id) }}" class="button -default">สนใจทำประกัน</a>
    </div>
  </div>
</div>
@endsection

@section('more-script')
  <script type="text/javascript">
    $(function(){
      $('.content .block .topic').on('click', function(){
        icon = $(this).find('i');
        detail = $(this).parent().find('.detail');

        detail.toggle();
        if (icon.hasClass('glyphicon-chevron-down'))
          icon.removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-right');
        else
          icon.removeClass('glyphicon-chevron-right').addClass('glyphicon-chevron-down');
      });
    });
  </script>
@endsection
