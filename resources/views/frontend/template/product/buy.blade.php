<?php
$lang = session()->get('locale');
?>
@extends('frontend.layouts/main')

@section('title','Buy Product')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/product-global.css') }}"/>
@endsection

@section('content')
<div id="main-content">
  <div class="container">
    <div class="headline">
      <div class="headline-sub">{{ 'ซื้อ '.$cat['category_name_' . $lang] }}</div>
      <div class="headline-main">"{{ $product['product_name_' . $lang] }}"</div>
    </div>

    <div class="contact-form">
      @if(session()->has('successMsg'))
        <div class="col-sm-offset-2 col-sm-8 col-md-offset-4 col-md-4 alert alert-success text-success text-center">{{ session()->get('successMsg') }}</div>
      @endif
        @if(session()->has('errorMsg'))
          <div class="col-sm-offset-2 col-sm-8 col-md-offset-4 col-md-4 alert alert-danger text-danger text-center">{{ session()->get('errorMsg') }}</div>
        @endif

      <div class="col-sm-offset-2 col-sm-8 col-md-offset-4 col-md-4">
        <form class="form-horizontal" action="{{ url()->route('product.buy.submit') }}" method="post" enctype="multipart/form-data">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" name="product_id" value="{{ $product->product_id }}">
          <div class="form-group">
            <label>ชื่อ - นามสกุล</label>
            <input type="text" name="name" value="{{ old('name') }}">
            @if(!empty($errors->first('name')))
              <div class="error text-danger">{{ $errors->first('name') }}</div>
            @endif
          </div>
          <div class="form-group">
            <label>อีเมล</label>
            <input type="text" name="email" value="{{ old('email') }}">
            @if(!empty($errors->first('email')))
              <div class="error text-danger">{{ $errors->first('email') }}</div>
            @endif
          </div>
          <div class="form-group">
            <label>เบอร์โทรศัพท์</label>
            <input type="text" name="mobile" value="{{ old('mobile') }}">
            @if(!empty($errors->first('mobile')))
              <div class="error text-danger">{{ $errors->first('mobile') }}</div>
            @endif
          </div>
          <div class="form-group">
            <label>ข้อความ</label>
            <textarea rows="6" name="mobile">{{ old('text') }}</textarea>
            @if(!empty($errors->first('text')))
              <div class="error text-danger">{{ $errors->first('text') }}</div>
            @endif
          </div>
          <div class="form-group">
            {{--<label>เบอร์โทรศัพท์</label>--}}
            <div class="g-recaptcha" data-sitekey="{{ env('RE_CAP_SITE') }}"></div>
            @if($errors->first('g-recaptcha-response'))
              <div class="text-danger">{{ $errors->first('g-recaptcha-response') }}</div>
            @endif
          </div>
          <div class="form-group text-center">
            <button type="submit" class="btn btn-buy">ซื้อประกัน</button>
          </div>
        </form>
      </div>
    </div>


  </div>
</div>
@endsection

@section('more-script')
  <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection
