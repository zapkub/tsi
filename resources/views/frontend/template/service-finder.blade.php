@extends('frontend.layouts/main')

@section('title','Home')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/home.css') }}"/>
  <link rel="stylesheet" href="{{ URL::asset('css/front/service.css') }}"/>
@endsection

@section('content')

<div id="service-finder" class="service-finder">

</div>
@endsection

@section('more-script')
    <script src="https://maps.googleapis.com/maps/api/js?sensor=true"></script>
    <script>
      var services = {!! $service !!};
    </script>

    <script src="js/front/service.components.js" ></script>

    <script>
    // API Example
      window.addEventListener('serviceMarkerIsClicked',function(e){
        alert(e.detail.info.garage_name_th);
      });
    </script>

@endsection
