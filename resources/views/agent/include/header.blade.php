<div class="page-header">
    <!-- BEGIN HEADER TOP -->
    <div class="page-header-top" style="background-color: #a8cbfe;">
        <div class="container">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                <a href="{{URL::to('_agent')}}"><img src="{{ URL::asset('images/logo_tsi.png') }}" alt="logo" class="logo-default" height="68"></a>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler"></a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu hidden-xs">
                <ul class="nav navbar-nav pull-right">
                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="javascript:;" >
                            <span class="username username-hide-mobile" style="font-weight:bold;color:#ffffff;">Hello, {{auth()->guard('agent')->user()->firstname}}</span>
                        </a>
                    </li>
                    <li class="droddown dropdown-separator">
                        <span class="separator"></span>
                    </li>
                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="{{URL::to('_agent/logout')}}" >
                            <span class="username username-hide-mobile" style="font-weight:bold;color:#ffffff;"><i class="fa fa-sign-out"></i> Logout</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
    </div>
    <!-- END HEADER TOP -->
    <!-- BEGIN HEADER MENU -->
    <div class="page-header-menu">
        <div class="container">

            <!-- BEGIN MEGA MENU -->
            <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
            <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
            <div class="hor-menu ">
                <ul class="nav navbar-nav">

                    <li><a href="{{ URL::to('_agent/car_insurance/create') }}"><i class="fa fa-file"></i> บันทึกใบคำขอประกันภัยรถยนต์</a></li>

                    <li><a href="{{ URL::to('_agent/car_insurance') }}"><i class="fa fa-file"></i> พิมพ์ใบคำขอเอาประกันภัยรถยนต์</a></li>

                    <li><a href="{{ URL::to('_agent/form_insurance/create') }}"><i class="fa fa-file"></i> บันทึกใบคำขอแก้ไขข้อมูลในตารางกรมธรรม์</a></li>

                    <li><a href="{{ URL::to('_agent/form_insurance') }}"><i class="fa fa-file"></i> พิมพ์ใบคำขอแก้ไขข้อมูลในตารางกรมธรรม์</a></li>


                    <li class="visible-xs-block"><a href="logout"><i class="fa fa-sign-out"></i> Logout</a></li>

                </ul>
            </div>
            <!-- END MEGA MENU -->
        </div>
    </div>
    <!-- END HEADER MENU -->
</div>

