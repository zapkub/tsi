@extends('agent-login')
@section('content')

        <!-- BEGIN LOGO -->
<div class="logo">
    {{--<a href="index.html">--}}
    {{--<img src="../../assets/admin/layout3/img/logo-big.png" alt=""/>--}}
    {{--</a>--}}
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content" style="background-color: #a8cbfe;">
    <!-- BEGIN LOGIN FORM -->
    <form class="login-form" action="{{ URL::to('_agent/login') }}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <h3 class="form-title" style="color:#ffffff;">AGENT</h3>
        {{--<div class="alert alert-danger display-hide">--}}
        {{--<button class="close" data-close="alert"></button>--}}
        {{--<span>--}}
        {{--Enter any username and password. </span>--}}
        {{--</div>--}}
        @if(Session::has('errorMsg'))
            <div class="alert alert-danger">
                <span class="text-danger">{{ Session::get('errorMsg') }}</span>
            </div>
        @endif
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Username</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username or Email" name="username" value=""/>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" value=""/>
        </div>
        <div class="form-actions">
            <button type="submit" class="btn btn-success uppercase">Login</button>
            <label class="remember check">
                <input type="checkbox" name="remember" value="1"/>Remember
            </label>
            {{--<a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a>--}}
        </div>
    </form>
    <!-- END LOGIN FORM -->
    <!-- BEGIN FORGOT PASSWORD FORM -->
    <form class="forget-form" action="index.html" method="post">
        <h3>Forget Password ?</h3>
        <p>
            Enter your e-mail address below to reset your password.
        </p>
        <div class="form-group">
            <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email"/>
        </div>
        <div class="form-actions">
            <button type="button" id="back-btn" class="btn btn-default">Back</button>
            <button type="submit" class="btn btn-success uppercase pull-right">Submit</button>
        </div>

    </form>
    <!-- END FORGOT PASSWORD FORM -->
</div>
@endsection