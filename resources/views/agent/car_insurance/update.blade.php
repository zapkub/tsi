<?php
use App\Http\Controllers\Agent\CarInsuranceController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new CarInsuranceController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}

$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);

?>

<style>
    .radio-inline{
        font-size:16px;
        margin-bottom:5px;
    }
    .border-table{
        border-bottom: solid 1px #000;
        border-top:solid 1px #000;
        margin-bottom: 20px;
    }
    input[type="text"]{
        font-size: 13px;
        font-weight: normal;
        color: #333;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        box-shadow: none;
        transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        -ms-border-radius: 4px;
        -o-border-radius: 4px;
        border-radius: 4px;
        height: 32px;
    }
    .select-box{
        font-size: 16px;
        font-weight: normal;
        color: #333;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        box-shadow: none;
        transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        -ms-border-radius: 4px;
        -o-border-radius: 4px;
        border-radius: 4px;
        height: 32px;
    }


    .t-width{
        width:50px;
    }
    .head{
        background-color: #444d58;
        color:#BCC2CB;
    }
    .f-red{
        color:#b94a48;
    }

    .help-block-error{
        color:red;
        font-size:14px;
        margin-left: 2px;
    }
</style>

@extends('agent')
@section('content')

        <!-- BEGIN SAMPLE TABLE PORTLET-->

<div class="portlet light">

    <div class="portlet-body flip-scroll">

        <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" id="form-car" method="POST" class="form-horizontal">
            <input name="_method" type="hidden" value="{{$method}}">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <div style="font-size:26px;color:red;text-align: center;">* ตัวเลขไม่ต้องใส่เครื่องหมาย Comma (,) เช่น 600000</div>

            <table class="table-condensed flip-content border-table">
                <thead class="flip-content">
                <tr class="head">
                    <td colspan="3" class="text-center"><strong>บริษัท ไทยเศรษฐกิจประกันภัย จำกัด (มหาชน)<br>คำขอเอาประกันภัยรถยนต์</strong></td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><strong>รหัส TSK</strong></td>
                    <td><strong>การประกันภัยรถยนต์</strong></td>
                    <td class="text-right">{{ $mainFn->format_date_th(date('Y-m-d H:i:s'),3) }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>เล่มที่ <input type="text" name="book" value="{{ $book }}" ></td>

                    <?php
                    $db = DB::table('agent')->where('agent_id',auth()->guard('agent')->user()->agent_id)->first();
                    ?>
                    <td class="text-right">ผู้แจ้ง {{ $db->company_name }} {{ $db->agent_no }}</td>
                    <input type="hidden" name="agent_no" id="agent_no" value="{{ $db->agent_no }}">
                    <input type="hidden" name="branch_id" id="branch_id" value="{{ $db->branch_id }}">
                </tr>
                <tr>
                    <td>ประเภทการซ่อม</td>
                    <td>
                        <select name="type_repair" class="select-box" required>
                            <option value="">โปรดระบุ</option>
                            <option value="1" @if($type_repair == 1) selected @endif>ซ่อมอู่</option>
                            <option value="2" @if($type_repair == 2) selected @endif>ซ่อมห้าง</option>
                            <option value="3" @if($type_repair == 3) selected @endif>T - Care</option>
                        </select>
                    </td>
                    <td></td>
                </tr>
                </tbody>
            </table>


            <table class="table-condensed flip-content border-table">

                <thead class="flip-content">
                <tr class="head">
                    <th colspan="4">ผู้ขอเอาประกันภัย</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td width="20%">ชื่อ</td>
                    <td width="14%" class="form-group">
                        <input type="text" name="firstname" value="{{ $firstname }}" required>
                    </td>
                    <td width="14%" class="form-group">
                        <input type="text" name="lastname" value="{{ $lastname }}" required>
                    </td>
                    <td width="50%">
                        <label class="radio-inline"><input type="radio" name="status_insurance" value="0" @if($status_insurance==0) checked @endif> ประกันใหม่</label>
                        <label class="radio-inline"><input type="radio" name="status_insurance" value="1" @if($status_insurance==1) checked @endif> ต่ออายุกรมธรรม์ที่</label>
                        <input type="text" name="extend" value="{{ $extend }}">
                    </td>
                </tr>
                <tr>
                    <td width="20%">ที่อยู่</td>
                    <td width="80%" colspan="3"><textarea class="form-control" name="address">{{ $address }}</textarea></td>
                </tr>
                <tr>
                    <td width="20%">ประเภทการประกันภัย</td>
                    <td width="80%" colspan="3">
                        <label class="radio-inline"><input type="radio" name="type_insurance" value="0" @if($type_insurance==0) checked @endif class="form-control"> ประเภท1</label>
                        <label class="radio-inline"><input type="radio" name="type_insurance" value="1" @if($type_insurance==1) checked @endif class="form-control"> ประเภท2</label>
                        <label class="radio-inline"><input type="radio" name="type_insurance" value="2" @if($type_insurance==2) checked @endif class="form-control"> ประเภท3</label>
                        <label class="radio-inline"><input type="radio" name="type_insurance" value="3" @if($type_insurance==3) checked @endif class="form-control"> ประเภท2+</label>
                        <label class="radio-inline"><input type="radio" name="type_insurance" value="4" @if($type_insurance==4) checked @endif class="form-control"> ประเภท3+</label>
                        <label class="radio-inline"><input type="radio" name="type_insurance" value="5" @if($type_insurance==5) checked @endif class="form-control"> พ.ร.บ</label>
                        <label class="radio-inline"><input type="radio" name="type_insurance" value="6" @if($type_insurance==6) checked @endif class="form-control"> อื่นๆระบุ</label>
                        <input type="text" name="type_insurance_other" value="{{ $type_insurance_other }}">
                    </td>
                </tr>
                <tr>
                    <td width="20%">เลขที่บัตรประชาชน</td>
                    <td width="80%" colspan="3" class="form-group"><input type="text" name="id_card" value="{{ $id_card }}" required></td>
                </tr>
                <tr>
                    <td width="20%">อาชีพ</td>
                    <td width="80%" colspan="3">
                        <label class="radio-inline"><input type="radio" name="occupation" value="0" @if($occupation==0) checked @endif> ข้าราชการ / พนักงานรัฐวิสาหกิจ</label>
                        <label class="radio-inline"><input type="radio" name="occupation" value="1" @if($occupation==1) checked @endif> รับจ้าง</label>
                        <label class="radio-inline"><input type="radio" name="occupation" value="2" @if($occupation==2) checked @endif> ธุรกิจส่วนตัว</label>
                        <label class="radio-inline"><input type="radio" name="occupation" value="3" @if($occupation==3) checked @endif> ไม่ได้ประกอบอาชีพ</label>
                        <label class="radio-inline"><input type="radio" name="occupation" value="4" @if($occupation==4) checked @endif> อื่นๆระบุ</label>
                        <input type="text" name="occupation_other" value="{{ $occupation_other }}">
                    </td>
                </tr>
                </tbody>
            </table>

            <table class="table-condensed flip-content border-table">

                <thead class="flip-content">
                <tr class="head">
                    <th colspan="3">ประเภทการรับประกันภัยที่ต้องการ</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><label class="radio-inline"><input type="radio" name="driver_type" value="0" @if($driver_type==0) checked @endif> ไม่ระบุผู้ขับขี่</label></td>
                    <td></td>
                </tr>
                <tr>
                    <td><label class="radio-inline"><input type="radio" name="driver_type" value="1" @if($driver_type==1) checked @endif> ระบุชื่อผู้ขับขี่ 1.<input type="text" name="driver_name" value="{{ $driver_name }}"></label></td>
                    <?php


                    if(!empty($birthday)){

                        $birthday_1 = explode('-',$birthday);
                        foreach ($birthday_1 as $key => $value){
                            if($key==0){
                                $year = $value;
                            }
                            if($key==1){
                                $month = $value;
                            }
                            if($key==2){
                                $day = $value;
                            }
                        }

                    }else{
                        $year = date('Y')+543;
                        $day = 0;
                        $month = 0;
                    }

                    if(!empty($birthday2)){

                        $birthday_2 = explode('-',$birthday2);
                        foreach ($birthday_2 as $key => $value){
                            if($key==0){
                                $year2 = $value;
                            }
                            if($key==1){
                                $month2 = $value;
                            }
                            if($key==2){
                                $day2 = $value;
                            }
                        }

                    }else{
                        $year2 = date('Y')+543;
                        $day2 = 0;
                        $month2 = 0;
                    }
                    ?>
                    <td>
                        วัน <select name="day" class="select-box">
                            <option value="">Day</option>
                            @for($i=1;$i<=31;$i++)
                                <option value="{{ $i }}" @if($i==$day) selected @endif>{{ $i }}</option>
                            @endfor
                        </select>
                        เดือน <select name="month" class="select-box">
                            <option value="">Month</option>
                            @for($i=1;$i<=12;$i++)
                                <option value="{{ $i }}" @if($i==$month) selected @endif>{{ $i }}</option>
                            @endfor
                        </select>
                        ปีเกิด <select name="year" class="select-box">
                            <option value="">Year</option>
                            @for($i=$year;$i>=$year-100;$i--)
                                <option value="{{ $i }}" @if($i==$year) selected @endif>{{ $i }}</option>
                            @endfor
                        </select>
                    </td>
                    <td>อาชีพ <input type="text" name="driver_occupation" value="{{ $driver_occupation }}"></td>
                </tr>
                <tr>
                    <td>เลขที่ใบขับขี่ <input type="text" name="driver_license" value="{{ $driver_license }}"></td>
                    <td>จังหวัด <input type="text" name="driver_provice" value="{{ $driver_provice }}"></td>
                    <td>โทร <input type="text" name="driver_mobile" value="{{ $driver_mobile }}"></td>
                </tr>
                <tr>
                    <td>ระบุชื่อผู้ขับขี่ 2.<input type="text" name="driver_name2" value="{{ $driver_name2 }}"></td>
                    <td>
                        วัน <select name="day2" class="select-box">
                            <option value="">Day</option>
                            @for($i=1;$i<=31;$i++)
                                <option value="{{ $i }}" @if($i==$day2) selected @endif>{{ $i }}</option>
                            @endfor
                        </select>
                        เดือน <select name="month2" class="select-box">
                            <option value="">Month</option>
                            @for($i=1;$i<=12;$i++)
                                <option value="{{ $i }}" @if($i==$month2) selected @endif>{{ $i }}</option>
                            @endfor
                        </select>
                        ปีเกิด <select name="year2" class="select-box">
                            <option value="">Year</option>
                            @for($i=$year;$i>=$year-100;$i--)
                                <option value="{{ $i }}" @if($i==$year2) selected @endif>{{ $i }}</option>
                            @endfor
                        </select>
                    </td>
                    <td>อาชีพ <input type="text" name="driver_occupation2" value="{{ $driver_occupation2 }}"></td>
                </tr>
                <tr>
                    <td>เลขที่ใบขับขี่ <input type="text" name="driver_license2" value="{{ $driver_license2 }}"></td>
                    <td>จังหวัด <input type="text" name="driver_provice2" value="{{ $driver_provice2 }}"></td>
                    <td>โทร <input type="text" name="driver_mobile2" value="{{ $driver_mobile2 }}"></td>
                </tr>

                </tbody>
            </table>

            <table class="table-condensed flip-content border-table">

                <tr>
                    <td>เลขเครื่องหมายตาม พ.ร.บ. <input type="text" name="number_sign" value="{{ $number_sign }}"></td>
                    <td>บริษัท <input type="text" name="company" value="{{ $company }}"> </td>
                    <td>สิ้นสุดเมื่อ</td>
                    <td>
                        <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
                            <input type="text" name="end_date" class="form-control" value="{{ $end_date }}">
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        การใช้รถยนต์
                        <label class="radio-inline"><input type="radio" name="use_car" value="0" @if($occupation==0) checked @endif> ส่วนบุคคล</label>
                        <label class="radio-inline"><input type="radio" name="use_car" value="1" @if($occupation==1) checked @endif> เพื่อการพาณิชย์</label>
                        <label class="radio-inline"><input type="radio" name="use_car" value="2" @if($occupation==2) checked @endif> เพื่อการพาณิชน์พิเศษ</label>
                        <label class="radio-inline"><input type="radio" name="use_car" value="3" @if($occupation==3) checked @endif> รับจ้างสาธารณะ</label>
                        <label class="radio-inline"><input type="radio" name="use_car" value="4" @if($occupation==4) checked @endif> อื่นๆ ระบุ</label> <input type="text" name="use_car_other" value="{{ $use_car_other }}">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">ผู้รับประโยชน์ <input type="text" name="beneficiary" value="{{ $beneficiary }}"></td>
                    <td></td>
                    <td></td>
                </tr>

            </table>

            <table class="table-condensed flip-content border-table">

                <thead class="flip-content">
                <tr class="head">
                    <th colspan="7">รายการรถยนต์ที่เอาประกัน</th>
                </tr>
                </thead>
                <tbody>
                <tr class="text-center">
                    <td class="f-red">ชื่อรถยนต์ / รุ่น*</td>
                    <td class="f-red">เลขทะเบียน*</td>
                    <td class="f-red">เลขตัวถัง*</td>
                    <td class="f-red">ปี รุ่น*</td>
                    <td>จำนวนที่นั่ง / ขนาด / น้ำหนัก</td>
                </tr>
                <tr>
                    <td class="text-center form-group" ><input type="text" name="name_car" id="name_car" value="{{ $name_car }}" required style="width:100%;"></td>
                    <td class="text-center form-group" ><input type="text" name="registration" value="{{ $registration }}" required style="width:100%;"></td>
                    <td class="text-center form-group" ><input type="text" name="chassis" value="{{ $chassis }}" required style="width:100%;"></td>
                    <td class="text-center form-group" ><input type="text" name="roon" value="{{ $roon }}" required style="width:100%;"></td>
                    <td class="text-center"><input type="text" class="t-width" name="seating" value="{{ $seating }}">/<input type="text" class="t-width" name="size" value="{{ $size }}">/<input type="text" class="t-width" name="weight" value="{{ $weight }}"></td>
                </tr>
                <tr class="text-center">
                    <td>รหัสรถยนต์</td>
                    <td>เลขเครื่องรถยนต์</td>
                    <td>มูลค่าเต็มรวมตกแต่ง</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td class="text-center"><input type="text" name="code_car" value="{{ $code_car }}" style="width:100%;"></td>
                    <td class="text-center"><input type="text" name="engine" value="{{ $engine }}" style="width:100%;"></td>
                    <td class="text-center"><input type="text" name="total_decorate" value="{{ $total_decorate }}" style="width:100%;"></td>
                    <td colspan="2"></td>
                </tr>
                </tbody>
            </table>

            <table class="table-condensed flip-content border-table">

                <thead class="flip-content">
                <tr class="head">
                    <th colspan="4">รายการตกแต่งเปลี่ยนแปลงรถยนต์เพิ่มเติม (โปรดระบุรายละเอียด)</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>1. <input type="text" name="decorate_car" value="{{ $decorate_car }}" style="width: 90%"></td>
                    <td>ราคา <input type="text" name="price" value="{{ $price }}"> บาท</td>
                </tr>
                <tr>
                    <td>2. <input type="text" name="decorate_car2" value="{{ $decorate_car2 }}" style="width: 90%"></td>
                    <td>ราคา <input type="text" name="price2" value="{{ $price2 }}"> บาท</td>
                </tr>
                <tr>
                    <td>3. <input type="text" name="decorate_car3" value="{{ $decorate_car3 }}" style="width: 90%"></td>
                    <td>ราคา <input type="text" name="price3" value="{{ $price3 }}"> บาท</td>
                </tr>
                <tr>
                    <td>อุปกรณ์พิเศษ <label class="radio-inline"><input type="radio" name="equipment" value="0" @if($equipment==0) checked @endif>ไม่มี</label>
                        <label class="radio-inline"><input type="radio" name="equipment" value="1" @if($equipment==1) checked @endif>มีระบุ</label></td>
                    <td><textarea class="form-control" name="equipment_other">{{ $equipment_other }}</textarea></td>
                </tr>
                </tbody>
            </table>


            <table class="table-condensed flip-content border-table">

                <thead class="flip-content">
                <tr class="head">
                    <th>ความรับผิดต่อบุคคลภายนอก</th>
                    <th>รถยนต์เสียหาย สูญหายไฟไหม้</th>
                    <th>ความคุ้มครองตามเอกสารแนบท้าย</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td valign="top">1.ความเสียหายต่อชีวิต ร่างกาย หรือ อนามัย<br>เฉพาะส่วนเกินวงเงินสูงสุดตาม พ.ร.บ. <input type="text" name="person_damage" value="{{ $person_damage }}"> บาท/คน <input type="text" name="person_damage2" value="{{ $person_damage2 }}"> บาท/ครั้ง<br>
                        2.ความเสียหายต่อทรัพย์สิน <input type="text" name="person_damage3" value="{{ $person_damage3 }}"> บาท/ครั้ง<br>
                        2.1ความเสียหายส่วนแรก <input type="text" name="person_damage4" value="{{ $person_damage4 }}"> บาท/ครั้ง<br>
                    </td>
                    <td valign="top">1.ความเสียหายต่อรถยนต์ <input type="text" name="car_damage" value="{{ $car_damage }}"> บาท / ครั้ง<br>
                        1.1.ความเสียหายส่วนแรก <input type="text" name="car_damage2" value="{{ $car_damage2 }}"> บาท / ครั้ง
                        2.รถยนต์สูญหายไฟไหม้ <br><input type="text" name="car_damage3" value="{{ $car_damage3 }}"> บาท<br><br><br><br>

                        <span style="font-size:46px;color:red;margin:50px;">ไม่รวม พ.ร.บ</span>
                    </td>
                    <td valign="top">1.อุบัติเหตุส่วนบุคคล<br>
                        1.1.เสียชีวิต สูญเสียอวัยวะ ทุพพลภาพถาวร<br>
                        ก.ผู้ขับขี่ 1 คน <input type="text" name="document_ptt" value="{{ $document_ptt }}"> บาท<br>
                        ข.ผู้โดยสาร <input type="text" name="document_ptt2" value="{{ $document_ptt2 }}"> คน<br>
                        1.2.ทุพพลภาพชั่วคราว<br>
                        ก.ผู้ขับขี่ 1 คน <input type="text" name="document_ptt3" value="{{ $document_ptt3 }}"> บาท / สัปดาห์<br>
                        ข.ผู้โดยสาร <input type="text" name="document_ptt4" value="{{ $document_ptt4 }}"> คน <input type="text" name="document_ptt5" value="{{ $document_ptt5 }}"> บาท / คน / สัปดาห์<br>
                        2.ค่ารักษาพยาบาล <input type="text" name="document_ptt6" value="{{ $document_ptt6 }}"> บาท / คน<br>
                        3.การประกันตัวผู้ขับขี่ <input type="text" name="document_ptt7" value="{{ $document_ptt7 }}"> บาท / ครั้ง
                    </td>
                </tr>
                <tr>
                    <td>เบี้ยสุทธิ <input type="text" name="net" value="{{ $net }}"> บาท</td>
                    <td>เบี้ยรวมภาษีอากร <input type="text" name="net_tax" value="{{ $net_tax }}"> บาท</td>
                    <td>เบี้ย พ.ร.บ <input type="text" name="net_act" value="{{ $net_act }}"> บาท</td>
                </tr>

                </tbody>
            </table>

            <table class="table-condensed flip-content border-table">

                <thead class="flip-content">
                <tr>
                    <td colspan="2">ข้าพเจ้าขอรับรองว่า คำบอกกล่าวตามรายการข้างบนเป็นความจริงและให้นับถือเป็นส่วนหนึ่งของสัญญาระหว่างข้าพเจ้ากับบริษัท</td>
                </tr>
                <tr>
                    <td>โดยข้าพเจ้ามีความประสงค์ให้กรมธรรม์มีผลบังคับใช้ตั้งแต่วันที่</td>
                    <td>
                        <div class="input-group input-large date-picker input-daterange" data-date="<?php echo date("Y-m-d");?>" data-date-format="yyyy-mm-dd" >
                            <input type="text" class="form-control" name="from_date" value="{{ $from_date }}" required>
												<span class="input-group-addon">
												ถึงวันที่ </span>
                            <input type="text" class="form-control" name="to_date" value="{{ $to_date }}">
                        </div>

                    </td>
                </tr>
                </tbody>
            </table>

            <table class="table-condensed flip-content border-table">
                <tbody>

                <tr>
                    <td colspan="4">ชื่อผู้พิมพ์</td>
                </tr>
                <tr>
                    <td>ชื่อผู้บันทึก</td>
                    <td><input type="text" name="agent" value=""></td>
                    <td>ลายมือชื่อผู้ขอเอาประกัน..........................................................................</td>
                    <td></td>
                </tr>
                <tr>
                    <td class="f-red">ชื่อรหัสตัวแทน*</td>
                    <td><input type="text" name="code_agent" value="" required></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>หมายเหตุ</td>
                    <td><textarea class="form-control" name="note">{{ $note }}</textarea></td>
                    <td>วันที่...................เดือน......................พ.ศ......................</td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td class="f-red">เอกสารฉบับนี้เป็นเพียงการแจ้งขอเอาประกันภัยรถยนต์เท่านั้น<br>มิใช่เอกสารการแสดงการรับชำระเงินค่าเบี้ยประกันภัย</td>
                    <td></td>
                </tr>

                </tbody>
            </table>

            <table class="table-condensed flip-content border-table">
                <tbody>
                <tr>
                    <td colspan="4" class="f-red">คำเตือนของกรมการประกันภัย กระทรวงพาณิชย์<br>
                        ให้ตอบคำถามข้างต้นตามเป็นจริงทุกข้อ มิฉะนั้นบริษัทอาจถือเป็นเหตุปฏิเสธความรับผิดชอบตามสัญญาประกันภัยได้<br>
                        ตามประมวลกฏหมายเพ่งและพาณิชน์มาตรา 865</td>
                </tr>
                @if($method != "PUT")
                    <tr>
                        <td class="text-center"> <input type="checkbox" name="check" id="check" value="1"> * รายละเอียดครบถ้วนถูกต้อง</td>
                    </tr>
                    <tr>
                        <td class="text-center"><input type="submit" class="btn btn-default" value="ส่งใบคำขอเข้าบริษัท ไทยเศรษฐกิจประกันภัย จำกัด(มหาชน)"></td>
                    </tr>
                @endif
                </tbody>
            </table>
            <input type="hidden" name="status" value="0">
        </form>

    </div>
</div>

@endsection

@section('js')
    <script src="{{URL::asset('assets/admin/scripts/validate/insurance_branch.js')}}"></script>

    <script>
        $( "#form-car" ).submit(function( event ) {
            if($("#check").is(':checked') != true){
                alert( "กรุณายืนยันรายละเอียดครบถ้วนถูกต้อง" );
                event.preventDefault();
            }

        });
    </script>
@endsection