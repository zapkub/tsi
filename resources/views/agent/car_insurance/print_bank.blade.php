<?php
use App\Http\Controllers\Agent\CarInsuranceController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new CarInsuranceController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}

$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);
?>

<style>
   #font-page {
        font-size: 20px; font-family: "THSarabunNew";
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }

    @page {
        size: A4;
        margin: 0;
    }
    @media print {
        html, body {
            width: 200mm;
            height: 297mm;
        }
    }

   tr{
       line-height: 18px;
   }

    .head{
        border: solid 1px;
    }
    .f-red{
        color:#b94a48;
    }
</style>

@extends('agent')
@section('content')

        <!-- BEGIN SAMPLE TABLE PORTLET-->
<div class="portlet light" id="font-page">

    <div class="portlet-body flip-scroll">

        <table>
            <thead class="flip-content">
            <tr>
                <td width="10%">
                    <img src="{{ URL::asset('images/logo-blue.png') }}" alt="logo" class="logo-default" height="68;">
                </td>
                <td colspan="6" class="text-center">
                    <strong>บริษัท ไทยเศรษฐกิจประกันภัย จำกัด (มหาชน)<br>คำขอเอาประกันภัยรถยนต์</strong>
                </td>
            </tr>
            </thead>
            </table>

            <table>
                <thead class="flip-content">
            <tr>
                <td></td>
                <td><strong>การประกันภัยรถยนต์</strong></td>
                <td>{{ $mainFn->format_date_th(date('Y-m-d H:i:s'),3) }}</td>
            </tr>
            <?php
            $db = DB::table('agent')->select('company_name','firstname','lastname','agent_no')->where('agent_id',auth()->guard('agent')->user()->agent_id)->first();
            ?>
            <tr>
                <td><strong>รหัส TSK</strong></td>
                <td>
                    เล่มที่ {{ $book }}
                </td>
                <td>ผู้ขอ ชื่อ {{ $db->firstname }} {{ $db->lastname }} กท {{ $db->agent_no }} ( {{ $mainFn->format_date_th($data->created_at,4) }} )</td>
            </tr>
            <tr>
                <td>ประเภทการซ่อม</td>
                <td>@if($type_repair==1) ซ่อมอู่ @elseif($type_repair==2) ซ่อมห้าง @elseif($type_repair==3) T- Care @endif</td>
                <td>@if(!empty($staff_accept)) ผู้ยืนยัน  {{ $staff_accept }} @endif @if($staff_accept_date != '0000-00-00 00:00:00')( {{ $mainFn->format_date_th($staff_accept_date,4) }} ) @endif</td>
            </tr>

            <tr class="head">
                <th colspan="7">ผู้ขอเอาประกันภัย</th>
            </tr>

            <tr>
                <td width="20%">ชื่อ</td>
                <td width="40%">{{ $firstname }} {{ $lastname }}</td>
                <td width="40%" colspan="4">
                    @if($status_insurance==0) <i class="fa fa-check"></i> ประกันใหม่ @endif
                    @if($status_insurance==1) <i class="fa fa-check"></i> ต่ออายุกรมธรรม์ที่ กท {{ $extend }} @endif
                </td>
            </tr>
            <tr>
                <td width="20%">ที่อยู่</td>
                <td width="80%" colspan="6">{{ $address }}</td>
            </tr>
            <tr>
                <td width="20%"></td>
                <td width="80%" colspan="6">
                    @if($type_insurance==0) <i class="fa fa-check"></i> ประเภท1 @endif
                    @if($type_insurance==1) <i class="fa fa-check"></i> ประเภท2 @endif
                    @if($type_insurance==2) <i class="fa fa-check"></i> ประเภท3 @endif
                    @if($type_insurance==3) <i class="fa fa-check"></i> ประเภท2+ @endif
                    @if($type_insurance==4) <i class="fa fa-check"></i> ประเภท3+ @endif
                    @if($type_insurance==5) <i class="fa fa-check"></i> พ.ร.บ @endif
                    @if($type_insurance==6) <i class="fa fa-check"></i> อื่นๆ ระบุ <input type="text" name="type_insurance_other" value="{{ $type_insurance_other }}"> @endif
            </tr>
            <tr>
                <td width="20%">เลขที่บัตรประชาชน</td>
                <td width="80%" colspan="6">{{ $id_card }}</td>
            </tr>
            <tr>
                <td width="20%">อาชีพ</td>
                <td width="80%" colspan="6">
                    @if($occupation==0) <i class="fa fa-check"></i> ข้าราชการ / พนักงานรัฐวิสาหกิจ @endif
                    @if($occupation==1) <i class="fa fa-check"></i> รับจ้าง @endif
                    @if($occupation==2) <i class="fa fa-check"></i> ธุรกิจส่วนตัว @endif
                    @if($occupation==3) <i class="fa fa-check"></i> ไม่ได้ประกอบอาชีพ @endif
                    @if($occupation==4) <i class="fa fa-check"></i> อื่นๆ ระบุ {{ $occupation_other }} @endif
                </td>
            </tr>

            <tr class="head">
                <th colspan="7">ประเภทการรับประกันภัยที่ต้องการ</th>
            </tr>

            <?php
            $year = date('Y');
            $birthday_1 = explode('-',$birthday);
            foreach ($birthday_1 as $key => $value){
                if($key==0){
                    $year = $value;
                }
                if($key==1){
                    $month = $value;
                }
                if($key==2){
                    $day = $value;
                }
            }

            $birthday_2 = explode('-',$birthday2);
            foreach ($birthday_2 as $key => $value){
                if($key==0){
                    $year2 = $value;
                }
                if($key==1){
                    $month2 = $value;
                }
                if($key==2){
                    $day2 = $value;
                }
            }
            ?>
            @if($type_insurance==0)
                <tr>
                    <td width="20%" colspan="7"><i class="fa fa-check"></i> ไม่ระบุผู้ขับขี่</td>
                </tr>
            @endif

            @if($type_insurance==1)
                <tr>
                    <td width="20%" ><i class="fa fa-check"></i> ระบุชื่อผู้ขับขี่ 1.{{ $driver_name }}</td>
                    <td width="40%" >
                        วัน {{ $day }}
                        เดือน {{ $month }}
                        ปีเกิด {{ $year }}
                    </td>
                    <td width="40%" colspan="5">อาชีพ {{ $driver_occupation }}</td>
                </tr>
                <tr>
                    <td width="20%" >เลขที่ใบขับขี่ {{ $driver_license }}</td>
                    <td width="40%" >จังหวัด {{ $driver_provice }}</td>
                    <td width="40%" colspan="5">โทร {{ $driver_mobile }}</td>
                </tr>
                <tr>
                    <td width="20%" >ระบุชื่อผู้ขับขี่ 2.{{ $driver_name2 }}</td>
                    <td width="40%" >
                        วัน {{ $day2 }}
                        เดือน {{ $month2 }}
                        ปีเกิด {{ $year2 }}
                    </td>
                    <td width="40%" colspan="5">อาชีพ {{ $driver_occupation2 }}</td>
                </tr>
                <tr>
                    <td width="20%" >เลขที่ใบขับขี่ {{ $driver_license2 }}</td>
                    <td width="40%" >จังหวัด {{ $driver_provice2 }}</td>
                    <td width="40%" colspan="5">โทร {{ $driver_mobile2 }}</td>
                </tr>
            @endif

            <tr>
                <td width="20%">เลขเครื่องหมายตาม พ.ร.บ. {{ $number_sign }}</td>
                <td width="40%">บริษัท {{ $company }}</td>
                <td width="40%">สิ้นสุดเมื่อ {{ $mainFn->format_date_th($end_date,2) }}</td>
            </tr>
            <tr>
                <td width="20%">การใช้รถยนต์        @if($use_car==0) <i class="fa fa-check"></i>
                    ส่วนบุคคล           @elseif($use_car==1) <i class="fa fa-check"></i>
                    เพื่อการพาณิชน์       @elseif($use_car==2) <i class="fa fa-check"></i>
                    เพื่อการพาณิชน์พิเศษ   @elseif($use_car==3) <i class="fa fa-check"></i>
                    รับจ้างสาธารณะ      @elseif($use_car==4) <i class="fa fa-check"></i>
                    อื่นๆ               @elseif($use_car==6) <i class="fa fa-check"></i> ระบุ {{ $use_car_other }} @endif</td>
                <td width="40%" colspan="6">ผู้รับประโยชน์ {{ $beneficiary }}</td>
            </tr>



            </tbody>
        </table>


        <table>
            <thead class="flip-content">
            <tr class="head">
                <th colspan="7">รายการรถยนต์ที่เอาประกัน :</th>
            </tr>
            <tr class="text-left">
                <td width="20%">ชื่อรถยนต์ / รุ่น</td>
                <td width="15%">เลขทะเบียน</td>
                <td width="15%">เลขตัวถัง</td>
                <td width="15%">ปี รุ่น</td>
                <td width="20%">จำนวนที่นั่ง / ขนาด / น้ำหนัก</td>
                <td width="20%" colspan="2">มูลค่าเต็มรวมตกแต่ง</td>
            </tr>
            <tr>
                <td align="left">{{ $name_car }}</td>
                <td align="left">{{ $registration }}</td>
                <td align="left">{{ $chassis }}</td>
                <td align="left">{{ $roon }}</td>
                <td class="text-left">
                    @if(empty($seating) and empty($size) and empty($weight))
                        <?php $s = $seating ?>

                    @else

                        <?php $s = $seating."/".$size."/".$weight ?>

                    @endif
                    {{ $s }}
                </td>
                <td align="left">{{ $total_decorate }}</td>
            </tr>
            <tr class="text-left">
                <td>รหัสรถยนต์</td>
                <td>เลขเครื่องรถยนต์</td>
                <td colspan="5"></td>
            </tr>
            <tr>
                <td align="left">{{ $code_car }}</td>
                <td align="left">{{ $engine }}</td>
                <td colspan="5"></td>
            </tr>

            </tbody>
        </table>

        <table>

            <tr class="head">
                <th colspan="7">รายการตกแต่งเปลี่ยนแปลงรถยนต์เพิ่มเติม (โปรดระบุรายละเอียด)</th>
            </tr>

            <tr>
                <td width="60%">1. {{ $decorate_car }}</td>
                <td width="10%">ราคา</td>
                <td width="10%">{{ $price }}</td>
                <td width="10%">บาท</td>
            </tr>
            <tr>
                <td width="60%">2. {{ $decorate_car2 }}</td>
                <td width="10%">ราคา</td>
                <td width="10%">{{ $price2 }}</td>
                <td width="10%">บาท</td>
            </tr>
            <tr>
                <td width="60%">3. {{ $decorate_car3 }}</td>
                <td width="10%">ราคา</td>
                <td width="10%">{{ $price3 }}</td>
                <td width="10%">บาท</td>
            </tr>
            <tr>
                <td>อุปกรณ์พิเศษ @if($equipment==0) <i class="fa fa-check"></i> @endif
                    ไม่มี @if($equipment==1) <i class="fa fa-check"></i>
                    มีระบุ @endif</td>
                @if($equipment==1) <td>{{ $equipment_other }}</td> @endif
                <td></td>
            </tr>
            </tbody>
        </table>

        <table>
            <tr class="head" style="line-height: ">
                <td width="35%" style="border-right:solid 1px;"><strong>ความรับผิดต่อบุคคลภายนอก</strong></td>
                <td width="35%" style="border-right:solid 1px;"><strong>รถยนต์เสียหาย สูญหายไฟไหม้</strong></td>
                <td width="35%" style="border-right:solid 1px;" colspan="2"><strong>ความคุ้มครองตามเอกสารแนบท้าย</strong></td>
            </tr>

            <tr>
                <td valign="top" width="35%" style="border:solid 1px;">1.ความเสียหายต่อชีวิต ร่างกาย หรือ อนามัย<br>เฉพาะส่วนเกินวงเงินสูงสุดตาม พ.ร.บ. {{ $person_damage }} บาท/คน {{ $person_damage2 }} บาท/ครั้ง<br>
                    2.ความเสียหายต่อทรัพย์สิน {{ $person_damage3 }} บาท/ครั้ง<br>
                    2.1ความเสียหายส่วนแรก {{ $person_damage4 }} บาท/ครั้ง
                </td>
                <td valign="top" width="30%" style="border:solid 1px;">1.ความเสียหายต่อรถยนต์ {{ $car_damage }} บาท / ครั้ง<br>
                    1.1.ความเสียหายส่วนแรก {{ $car_damage2 }} บาท / ครั้ง<br>
                    2.รถยนต์สูญหายไฟไหม้ {{ $car_damage3 }} บาท<br>
<br>
                    <span style="font-size:36px;color:red;">ไม่รวม พ.ร.บ</span>
                </td>
                <td valign="top" width="35%" style="border:solid 1px;" colspan="2">1.อุบัติเหตุส่วนบุคคล<br>
                    1.1.เสียชีวิต สูญเสียอวัยวะ ทุพพลภาพถาวร<br>
                    ก.ผู้ขับขี่ 1 คน {{ $document_ptt }} บาท<br>
                    ข.ผู้โดยสาร {{ $document_ptt2 }} คน<br>
                    1.2.ทุพพลภาพชั่วคราว<br>
                    ก.ผู้ขับขี่ 1 คน {{ $document_ptt3 }} บาท / สัปดาห์<br>
                    ข.ผู้โดยสาร {{ $document_ptt4 }} คน {{ $document_ptt5 }} บาท / คน / สัปดาห์<br>
                    2.ค่ารักษาพยาบาล {{ $document_ptt6 }} บาท / คน<br>
                    3.การประกันตัวผู้ขับขี่ {{ $document_ptt7 }} บาท / ครั้ง
                </td>
            </tr>
            <tr>
                <td width="35%" style="border:solid 1px;">เบี้ยสุทธิ {{ $net }} บาท</td>
                <td width="35%" style="border:solid 1px;">เบี้ยรวมภาษีอากร {{ $net_tax }} บาท</td>
                <td colspan="2" width="35%" style="border:solid 1px;">เบี้ย พ.ร.บ {{ $net_act }} บาท</td>
            </tr>

            <tr>
                <td colspan="2">ข้าพเจ้าขอรับรองว่า คำบอกกล่าวตามรายการข้างบนเป็นความจริงและให้นับถือเป็นส่วนหนึ่งของสัญญาระหว่างข้าพเจ้ากับบริษัท</td>
            </tr>
            <tr>
                <td colspan="2">โดยข้าพเจ้ามีความประสงค์ให้กรมธรรม์มีผลบังคับใช้ตั้งแต่วันที่
                    {{ $mainFn->format_date_th($from_date,2) }} ถึงวันที่ {{ $mainFn->format_date_th($to_date,2) }}
                </td>
            </tr>

            <tr>
                <td>ลงชื่อผู้พิมพ์</td>
                <td colspan="3">{{ $db->firstname }} {{ $db->lastname }}</td>
            </tr>
            <tr>
                <td>ชื่อตัวแทน</td>
                <td>{{ $agent }}</td>
                <td>ลายมือชื่อผู้ขอเอาประกัน..........................................................................</td>
                <td></td>
            </tr>
            <tr>
                <td class="f-red">ชื่อรหัสตัวแทน* กท</td>
                <td>{{ $code_agent }}</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>หมายเหตุ</td>
                <td>{{ $note }}</td>
                <td>วันที่...................เดือน......................พ.ศ......................</td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td class="f-red">เอกสารฉบับนี้เป็นเพียงการแจ้งขอเอาประกันภัยรถยนต์เท่านั้น<br>มิใช่เอกสารการแสดงการรับชำระเงินค่าเบี้ยประกันภัย</td>
                <td></td>
            </tr>

            <tr>
                <td colspan="4" class="f-red">คำเตือนของกรมการประกันภัย กระทรวงพาณิชย์<br>
                    ให้ตอบคำถามข้างต้นตามเป็นจริงทุกข้อ มิฉะนั้นบริษัทอาจถือเป็นเหตุปฏิเสธความรับผิดชอบตามสัญญาประกันภัยได้<br>
                    ตามประมวลกฏหมายเพ่งและพาณิชน์มาตรา 865</td>
            </tr>

            </tbody>
        </table>

    </div>
</div>
<!-- END SAMPLE TABLE PORTLET-->
<script>
       $(document).ready(function(){
                window.print();
        });
</script>

@endsection