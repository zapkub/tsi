<?php
use App\Http\Controllers\Agent\FormInsuranceController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new FormInsuranceController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$service = App\Model\Service::whereNull('deleted_at')->get();

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}

$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);
$car_insurance_id = input::get('car_insurance');

$db = DB::table('agent')->select('company_name','agent_no','firstname','lastname')->where('agent_id',auth()->guard('agent')->user()->agent_id)->first();

        if($car_insurance_id!=''){
            $car_insurance_db = DB::table('car_insurance')->select('branch_id','no','book','firstname','lastname','name_car','registration','agent','code_agent','note')->where('car_insurance_id',$car_insurance_id)->first();
            $no = $car_insurance_db->no;
        }else{
            $no = '';
        }
?>
<style>
    .f-red{
        color:#b94a48;
    }
</style>
@extends('agent')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{ $txt_manage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" class="form-horizontal">
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                    <input type="hidden" name="code_agent"  class="form-control" value="{{ $db->agent_no }}">

                                    <div class="portlet box yellow">
                                        <div class="portlet-title">
                                            <div class="text-center" style="font-size:18px;">
                                                    บริษัท ไทยเศรษฐกิจประกันภัย จำกัด (มหาชน)<br>
                                                    คำร้องขอแก้ไขข้อความในตารางกรมธรรม์
                                            </div>
                                        </div>
                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->
                                            <div class="form-body">
                                                <div class="form-group">

                                                    <div class="col-md-1 text-right">
                                                        <label class="control-label">รหัส TSK</label>
                                                    </div>

                                                    <div class="col-md-8">
                                                        <label class="control-label"> การประกันภัยรถยนต์</label>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <label class="control-label">{{ $mainFn->format_date_th(date("Y-m-d H:i:s"),3) }}</label>
                                                    </div>

                                                    </div>
                                                </div>

                                            <div class="form-body">
                                                <div class="form-group">

                                                    <div class="col-md-1 text-right">

                                                        <label class="control-label">เล่มที่</label>
                                                    </div>

                                                    <div class="col-md-1">
                                                        <input type="text" name="book" class="form-control" value="@if(!empty($car_insurance_id)) {{ $car_insurance_db->book }} @endif">
                                                    </div>

                                                    {{--<div class="col-md-1 text-right">--}}
                                                        {{--<label class="control-label">เลขที่</label>--}}
                                                    {{--</div>--}}

                                                    {{--<div class="col-md-1">--}}
                                                        {{--<input type="text" name="no" class="form-control" value="{{ $no }}">--}}
                                                    {{--</div>--}}

                                                    {{--<div class="col-md-1">--}}
                                                        {{--<label class="control-label">ในใบแก้ไข</label>--}}
                                                    {{--</div>--}}

                                                    <?php
//                                                    $db = DB::table('agent')->select('company_name','agent_no')->where('agent_id',auth()->guard('agent')->user()->agent_id)->first();
                                                    ?>
                                                    <div class="col-md-4"><label class="control-label">ชื่อ {{ $db->firstname }} {{ $db->lastname }} กท {{ $db->agent_no }} ผู้บันทึกสลักหลัง</label></div>
                                                    <input type="hidden" name="agent_no" id="agent_no" value="{{ $db->agent_no }}">

                                                </div>

                                            </div>
                                        </div>
                                    </div>


                                    <div class="portlet box yellow">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-file-o"></i>รายละเอียดคำร้อง
                                            </div>
                                        </div>
                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->
                                                <div class="form-body">
                                                    <div class="form-group">

                                                        <div class="col-md-3 text-right">
                                                            <div class="radio-list">
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="status_endorse" id="status_endorse" value="1" onclick="chk_tt(this.value)" required> สลักหลัง</label>
                                                                <label class="radio-inline">
                                                                     <input type="radio" name="status_endorse" id="status_endorse" value="0" onclick="chk_tt(this.value)" required> ยกเลิก</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 text-left">
                                                            <label class="control-label">กรมธรรม์เลขที่ / ใบคำขอเลขที่ *</label>
                                                            </div>
                                                        <div class="col-md-2 text-left">
                                                            <?php
//                                                            $car_insurance_db2 = DB::table('car_insurance')->select('no','car_insurance_id')->whereNull('deleted_at')->where('agent_id',auth()->guard('agent')->user()->agent_id)->get();
                                                            ?>

                                                            {{--<select name="request_no" id="request_no" class="form-control">--}}
                                                                {{--<option value="">โปรดระบุ</option>--}}
                                                                {{--@foreach($car_insurance_db2 as $value)--}}

                                                                    {{--<option value="{{ $value->no }}" @if($no==$value->no) selected @endif>{{ $value->no }}</option>--}}
                                                                {{--@endforeach--}}

                                                            {{--</select>--}}

                                                            <input type="text" name="request_no" id="request_no" class="form-control">

                                                        </div>
                                                        <div class="col-md-4">
                                                            <label class="control-label">(หากแก้ไขข้อมูลในใบคำขอออนไลน์ให้ใส่เลขที่ใบคำขอนั้น)</label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">ชื่อผู้เอาประกัน</label>
                                                        <div class="col-md-3">
                                                            <input type="text" name="firstname" class="form-control"  value="@if(!empty($car_insurance_id)){{ $car_insurance_db->firstname }}@endif">
                                                        </div>

                                                        <div class="col-md-1">
                                                            นามสกุล
                                                            </div>

                                                        <div class="col-md-3">
                                                            <div class="col-md-12">
                                                                <input type="text" name="lastname" class="form-control"  value="@if(!empty($car_insurance_id)){{ $car_insurance_db->lastname }}@endif">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">วันคุ้มครอง</label>
                                                        <div class="col-md-2">
                                                            <div class="input-group input-normall date-picker input-daterange" data-date="<?php echo date("Y-m-d");?>" data-date-format="yyyy-mm-dd">
                                                                <input type="text" class="form-control" name="date_coverage" value="{{ $date_coverage }}">

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">ชื่อรถยนต์</label>
                                                        <div class="col-md-3">
                                                            <input type="text" name="car_name" class="form-control" value="@if(!empty($car_insurance_id)) {{ $car_insurance_db->name_car }} @endif">
                                                        </div>

                                                        <label class="col-md-2 control-label">เลขที่ทะเบียน</label>
                                                        <div class="col-md-3">
                                                            <input type="text" name="license_plate" class="form-control" value="@if(!empty($car_insurance_id)){{ $car_insurance_db->registration }}@endif" >
                                                        </div>
                                                    </div>

                                                    <div class="form-group" style="display:none;" id="div_detail">
                                                        <label class="col-md-3 control-label">รายละเอียดการเปลี่ยนแปลง</label>
                                                        <div class="col-md-4">
                                                            <textarea name="detail" id="detail" class="form-control" rows="3">{{ $detail }}</textarea>
                                                        </div>
                                                    </div>

                                                    <div class="form-group" style="display:none;" id="div_reason">
                                                        <label class="col-md-3 control-label">สาเหตุการยกเลิก</label>
                                                        <div class="col-md-4">
                                                            <textarea name="reason" id="reason" class="form-control" rows="3" required>{{ $reason }}</textarea>
                                                        </div>
                                                    </div>

                                                </div>
                                        </div>
                                    </div>

                                    <div class="portlet box yellow">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-child"></i>ลงชื่อผู้แก้ไข
                                            </div>
                                        </div>
                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">ลงชื่อผู้แก้ไข</label>
                                                    <div class="col-md-4">
                                                        <input type="text" name="name_agent" class="form-control" value="">
                                                    </div>
                                                </div>

                                                {{--<div class="form-group">--}}
                                                    {{--<label class="col-md-3 control-label">ชื่อตัวแทน</label>--}}
                                                    {{--<div class="col-md-4">--}}
                                                        {{--<input type="text" name="name_agent" class="form-control" value="@if(!empty($car_insurance_id)){{ $car_insurance_db->agent }}@endif">--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}

                                                {{--<div class="form-group">--}}
                                                    {{--<label class="col-md-3 control-label">ชื่อรหัสตัวแทน* กท</label>--}}
                                                    {{--<div class="col-md-4">--}}
                                                        {{--<input type="text" name="code_agent"  class="form-control" value="@if(!empty($car_insurance_id)){{ $car_insurance_db->code_agent }}@endif">--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">หมายเหตุ</label>
                                                    <div class="col-md-4">
                                                        <textarea name="note"  class="form-control" rows="3">@if(!empty($car_insurance_id)){{ $car_insurance_db->note }}@endif</textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-actions fluid">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <input name="check_data" type="checkbox" value="1" required> * รายละเอียดครบถ้วนถูกต้อง
                                                    </div>

                                                    <div class="col-md-offset-3 col-md-9">
                                                        <input type="hidden" name="strParam" value="{{$strParam}}">
                                                        <br>
                                                        <button type="submit" class="btn green">ส่งคำร้องเข้าบริษัท ไทยเศรษฐกิจประกันภัย จำกัด(มหาชน)</button>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
@endsection

<script>
    function chk_tt(value){
        if(value==1){
            $('#detail').attr('required','true');
            $('#div_detail').show();
            $('#reason').removeAttr('required');
            $('#div_reason').hide();
        }else if(value==0){
            $('#reason').attr('required','true');
            $('#div_reason').show();
            $('#detail').removeAttr('required');
            $('#div_detail').hide();
        }
    }
</script>