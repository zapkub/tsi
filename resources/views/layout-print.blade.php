<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Print</title>
    <style>
        body{
            font-family: "MS Sans Serif",thonburi,ayudhaya,Cordin New, CordiaUPC, Sans-Serif;
        }
    </style>
</head>
<body onload="window.print()">
@yield('content')
</body>
</html>
