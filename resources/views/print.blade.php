<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <title>Dashboard</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <link rel="shortcut icon" href="{{ url()->asset('images/favicon.ico') }}" />
    <!-- BEGIN GLOBAL MANDATORY STYLES 1-->
    {{--<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">--}}
    {{--<link href="{{ URL::asset('lib/bkksol_backend_dependencies/src/font-awesome.min.css') }}" rel="stylesheet" type="text/css">--}}
    {{--<link href="{{ URL::asset('lib/bkksol_backend_dependencies/src/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css">--}}
    <link href="{{ URL::asset('lib/bkksol_backend_dependencies/src/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    {{--<link href="{{ URL::asset('lib/bkksol_backend_dependencies/src/uniform.default.css') }}" rel="stylesheet" type="text/css">--}}
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE LEVEL STYLES -->
    {{--<link href="{{ URL::asset('lib/bkksol_backend_dependencies/src/jquery.minicolors.css')}}" rel="stylesheet" type="text/css"/>--}}
    <!-- END PAGE LEVEL STYLES -->

    <!-- BEGIN THEME STYLES -->
    <link href="{{ URL::asset('lib/bkksol_backend_dependencies/src/components-rounded.css') }}" id="style_components" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('lib/bkksol_backend_dependencies/src/plugins.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('assets/admin/layout3/css/layout.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('assets/admin/layout3/css/themes/default.css') }}" rel="stylesheet" type="text/css" id="style_color">
    <link href="{{ URL::asset('assets/admin/layout3/css/custom.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('lib/bkksol_backend_dependencies/src/mainStyle.css') }}" rel="stylesheet" type="text/css">
    {{--<link href="{{ URL::asset('css/legacy/change-status.css') }}" rel="stylesheet" type="text/css">--}}
    <!-- END THEME STYLES -->


    <link rel="stylesheet" type="text/css" media="screen" href="{{URL::asset('js/jquery-ui/jquery-ui.css')}}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{URL::asset('js/jquery-ui/jquery-ui.theme.css')}}">

    @yield('css')

    <script src="{{ URL::asset('lib/bkksol_backend_dependencies/src/jquery.min.js') }}" type="text/javascript"></script>

    <link rel="shortcut icon" href="{{ URL::asset('favicon.ico') }}"/>

    <style>
        body{
            font-family: "MS Sans Serif",thonburi,ayudhaya,Cordin New, CordiaUPC;
            /*font-size: 10pt; */
            /*line-height : 14pt*/
        }
    </style>

</head>
<body class="page-header-menu-fixed">
<!-- BEGIN CONTENT -->
@yield('content')

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="{{ URL::asset('assets/global/plugins/respond.min.js') }}"></script>
<script src="{{ URL::asset('assets/global/plugins/excanvas.min.js') }}"></script>
<![endif]-->

{{--17-11-58--}}
<script src="{{ URL::asset('assets/admin/scripts/page.js') }}"></script>
<script src="{{ URL::asset('js/jquery-ui/jquery-ui.min.js') }}"></script>


<script src="{{ URL::asset('lib/bkksol_backend_dependencies/src/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('lib/bkksol_backend_dependencies/src/jquery-migrate.min.js') }}" type="text/javascript"></script>

<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="{{ URL::asset('lib/bkksol_backend_dependencies/src/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('lib/bkksol_backend_dependencies/src/bootstrap.min.js') }}" type="text/javascript"></script>

<script src="{{ URL::asset('lib/bkksol_backend_dependencies/src/mainScript.js') }}" type="text/javascript"></script>

<!-- END CORE PLUGINS -->
<script src="{{ URL::asset('lib/bkksol_backend_dependencies/src/metronic.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/admin/layout3/scripts/layout.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/admin/layout3/scripts/demo.js') }}" type="text/javascript"></script>

@yield('js')

<script>
    jQuery(document).ready(function() {
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
    });
</script>
<!-- END JAVASCRIPTS -->

</body>
</html>
