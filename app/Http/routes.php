<?php
use App\Http\Middleware\Cors;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//Route::get('/',function(){
//    return view('frontend.index');
//});
Route::get('/',[
    'as'=>'home',
    'uses'=>'Frontend\MainController@main'
]);
Route::get('map', 'Frontend\MapController@getAll');
Route::get('lang/{locale}','Frontend\MainController@lang');
Route::get('data','Frontend\DataController@getIndex');
Route::controller('download','Frontend\DownloadController');

Route::group(['prefix' => 'service'],function(){
    Route::get('/','Frontend\ServiceController@main');
    Route::get('available-area/{type}','Frontend\ServiceController@getArea');
    Route::get('available-province/{geo_id}/{type}','Frontend\ServiceController@getProvince');
    Route::get('available-district/{province_id}/{type}','Frontend\ServiceController@getDistrict');
    Route::get('available-service/{district_id}/{type}','Frontend\ServiceController@getServiceListByDistrictIdAndType');
    Route::get('available-brand','Frontend\ServiceController@getBrand');
    Route::get('available-service-brand/{district_id}/{brand_id}/{type}','Frontend\ServiceController@getServiceListByBrand');
});



//Route::group(['prefix' => 'service-finder'],function(){
//    //route to view
//    Route::get('/',[
//        'as'=>'service',
//        'uses'=>'Frontend\ServiceFinderController@main'
//    ]);
//    //end view
//    // Data provider api
//    Route::get('/available-area/{type}',[
//        'as'=>'area',
//        'uses'=>'Frontend\ServiceFinderController@getArea'
//    ]);
//    Route::get('/available-province/{geo_id}/{type}',[
//        'as'=>'province',
//        'uses'=>'Frontend\ServiceFinderController@getProvince'
//    ]);
//    Route::get('/available-district/{province_id}/{type}',[
//        'as'=>'district',
//        'uses'=>'Frontend\ServiceFinderController@getdistrict'
//    ]);
//
//    Route::get('/available-service/{service_id}/{type}',[
//        'as'=>'service-list',
//        'uses'=>'Frontend\ServiceFinderController@getServiceListByDistrictIdAndType'
//    ]);
//    // End data provider api
//});

Route::group(['prefix'=>'about_us'],function() {
    Route::get('/',function(){
        return redirect()->to('about_us/history');
    });
    Route::get('history',[
        'as'=>'about_us.history',
        'uses'=>'Frontend\AboutUsController@history'
    ]);

    Route::get('vision',[
        'as'=>'about_us.vision',
        'uses'=>'Frontend\AboutUsController@vision'
    ]);

    Route::get('manager',[
        'as'=>'about_us.manager',
        'uses'=>'Frontend\AboutUsController@manager'
    ]);

    Route::get('executive_committee',[
        'as'=>'about_us.executive_committee',
        'uses'=>'Frontend\AboutUsController@executive_committee'
    ]);

    Route::get('annual_report',[
        'as'=>'about_us.annual_report',
        'uses'=>'Frontend\AboutUsController@annual_report'
    ]);

    Route::get('overall_operation',[
        'as'=>'about_us.overall_operation',
        'uses'=>'Frontend\AboutUsController@overall_operation'
    ]);
});


Route::get('news','Frontend\NewsController@index');
Route::get('news/{id}','Frontend\NewsController@detail');
Route::get('what-to-do', [
  // 'as'=>'what-to-do',
  // 'uses'=>'Frontend\ProductDataController@getProductData'

  function(){
    return view('frontend.template.what-to-do');}
]);
Route::get('what-to-do-do', [
  'as'=>'what-to-do-do',
  'uses'=>'Frontend\ProductDataController@getProductData'

  // function(){
    // return view('frontend.template.what-to-do');}
]);
Route::get('claim',function(){
    return view('frontend.template.claim');
});
Route::get('compensation',function(){
    return view('frontend.template.compensation');
});
Route::group(['prefix'=>'motor_claims'],function() {
  Route::get('/',[
      'as'=>'claims.motor_claims',
      'uses'=>'Frontend\ClaimsController@getIndex'
  ]);
  Route::get('1/{id}',[
      'as'=>'claims.motor_1_claims',
      'uses'=>'Frontend\ClaimsController@getMotorAccident'
  ]);
  Route::get('2/{id}',[
      'as'=>'claims.motor_2_claims',
      'uses'=>'Frontend\ClaimsController@getMotorFix'
  ]);
});
Route::group(['prefix'=>'other_claims'],function() {
  Route::get('/',[
      'as'=>'claims.other_claims',
      'uses'=>'Frontend\ClaimsController@getIndex'
  ]);
  Route::get('{id}',[
      'as'=>'claims.other_1_claims',
      'uses'=>'Frontend\ClaimsController@getOtherClaim1'
  ]);
});


// Route::get('motor_claims',[
//     'as'=>'claims.motor_claims',
//     'uses'=>'Frontend\ClaimsController@getIndex'
// ]);
// Route::get('motor_1_claims',[
//     'as'=>'claims.motor_1_claims',
//     'uses'=>'Frontend\ClaimsController@getMotorAccident'
// ]);
// Route::get('motor_2_claims',[
//     'as'=>'claims.motor_2_claims',
//     'uses'=>'Frontend\ClaimsController@getMotorFix'
// ]);
// Route::get('motor_3_claims',[
//     'as'=>'claims.motor_3_claims',
//     'uses'=>'Frontend\ClaimsController@getMotorPayClaim'
// ]);
Route::get('other_claims',[
    'as'=>'claims.other_claims',
    'uses'=>'Frontend\ClaimsController@getOther'
]);
Route::get('other_1_claims',[
    'as'=>'claims.other_1_claims',
    'uses'=>'Frontend\ClaimsController@getOtherClaim1'
]);
Route::get('other_2_claims',[
    'as'=>'claims.other_2_claims',
    'uses'=>'Frontend\ClaimsController@getOtherClaim2'
]);
Route::get('other_3_claims',[
    'as'=>'claims.other_3_claims',
    'uses'=>'Frontend\ClaimsController@getOtherClaim3'
]);
Route::group(['prefix'=>'career'],function() {
    Route::get('/',[
        'as'=>'career.list',
        'uses'=>'Frontend\CareerController@index'
    ]);

    Route::get('{id}',[
        'as'=>'career.detail',
        'uses'=>function(){
            return view('frontend.template.career.career-detail');
        }
    ]);
});

Route::get('about',function(){
    return view('frontend.about');
});
Route::get('product/cat/{cat_id}',[
  'as'=>'product.category-detail',
  'uses'=>'Frontend\ProductController@getCategoryDetail'
]);
Route::get('product/detail/{product_id}',[
  'as'=>'product.product-detail',
  'uses'=>'Frontend\ProductController@getProductDetail'
]);
Route::get('product/buy/{product_id}',[
    'as'=>'product.buy',
    'uses'=>'Frontend\ProductController@getBuy'
]);
Route::post('product/buy/submit',[
    'as'=>'product.buy.submit',
    'uses'=>'Frontend\ProductController@postSubmit'
]);

Route::get('promotion',function(){
    return view('frontend.promotion');
});
Route::get('member',function(){
    return view('frontend.member');
});

Route::get('signin', function(){
    return view('signin');
});
Route::controller('contact', 'Frontend\ContactController');
Route::controller('petition', 'Frontend\PetitionController');

Route::group(array('middleware' => ['cors', 'web'], 'prefix' => 'api'),function() {
  Route::resource('province', 'API\ProvinceController');
  Route::resource('incident', 'API\NotifyController');
  Route::resource('surveyor', 'API\SurveyorGPSController');
  Route::resource('token', 'API\TokenController');
  Route::post('login', 'API\LoginController@postLogin');

  Route::get('result/{sid}', 'Frontend\ResultController@index');
});


/*Route::get('/','Frontend\HomeController@getIndex' );*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['prefix'=>'address'],function() {
    Route::get('region','AddressController@region');
    Route::get('province/{geo_id}','AddressController@province');
    Route::get('district/{province_id}','AddressController@district');
    Route::get('zipcode/{district_id}','AddressController@zipcode');
});


Route::group(['middleware' => ['web']], function () {
    //
});

Route::get('test-print',function(){
   return view('layout-print');
});
Route::group(['prefix'=>'_admin'],function() {

    // Route::get('/',['middleware'=>'admin',function(){
    //     return view('backend.index');
    // }]);
    Route::get('/',['middleware'=>'admin',
                    'uses'=>'Backend\TelephoneController@index'
    ]);
    Route::get('login','Backend\AuthController@getLogin');
    Route::post('login','Backend\AuthController@postLogin');
    Route::get('logout','Backend\AuthController@getLogout');

    Route::resource('admin', 'Backend\AdminController');
    Route::resource('admin_role', 'Backend\AdminRoleController');
    Route::resource('page', 'Backend\PageController');
    Route::resource('permission', 'Backend\PermissionController');

    Route::resource('banner','Backend\BannerController');
    Route::resource('new_page', 'Backend\NewPageController');
    Route::resource('surveyor', 'Backend\SurveyorController');
    Route::resource('staff', 'Backend\StaffController');
    Route::resource('service', 'Backend\ServiceController');
    Route::resource('hospital', 'Backend\HospitalController');
    Route::resource('garage', 'Backend\GarageController');
    Route::resource('brand', 'Backend\BrandController');
    Route::resource('agent', 'Backend\AgentController');
    Route::resource('compensation', 'Backend\CompensationController');
    Route::resource('compensation_detail', 'Backend\CompensationDetailController');
    Route::resource('car_insurance_branch', 'Backend\CarInsuranceBranchController');
    Route::resource('car_insurance_branch_s', 'Backend\CarInsuranceBranchSController');
    Route::resource('car_insurance_main', 'Backend\CarInsuranceMainController');
    Route::resource('category', 'Backend\CategoryController');
    Route::resource('repair', 'Backend\RepairController');
    Route::resource('product_category', 'Backend\ProductCategoryController');
    Route::resource('product', 'Backend\ProductController');
    Route::resource('car_insurance', 'Backend\CarInsuranceController');
    Route::resource('form_insurance', 'Backend\FormInsuranceController');
    Route::resource('company_insurance', 'Backend\CompanyInsuranceController');
    Route::resource('insurance', 'Backend\InsuranceController');
    Route::resource('branch_insurance', 'Backend\BranchInsuranceController');

    Route::resource('history', 'Backend\HistoryController');
    Route::resource('vision_and_mission', 'Backend\VisionAndMissionController');
    Route::resource('position', 'Backend\PositionController');
    Route::resource('executive_committee', 'Backend\ExecutiveCommitteeController');
    Route::resource('yearbook', 'Backend\YearBookController');
    Route::resource('financial', 'Backend\FinancialController');

    Route::resource('career', 'Backend\CareerController');
    Route::resource('contact', 'Backend\ContactController');
    Route::resource('contact_msg', 'Backend\ContactMsgController');
    Route::resource('petition', 'Backend\PetitionController');
    Route::resource('amg', 'Backend\AmgController');
    Route::resource('csr', 'Backend\CsrController');
    Route::resource('csr_img','Backend\CsrImgController');
    Route::resource('news', 'Backend\NewsController');
    Route::resource('intro_page', 'Backend\IntroPageController');
    Route::resource('call_center', 'Backend\CallCenterController');
    Route::resource('send_case', 'Backend\SendCaseController');
    Route::resource('car_insurance_case', 'Backend\CarInsuranceCaseController');
    Route::resource('send_insurance', 'Backend\SendInsuranceController');
    Route::resource('car_insurance_agent', 'Backend\CarInsuranceAgentController');
    Route::resource('form_insurance_agent', 'Backend\FormInsuranceAgentController');

    Route::resource('banner_footer', 'Backend\BannerFooterController');
    Route::resource('banner_claim', 'Backend\BannerClaimController');
    Route::resource('telephone', 'Backend\TelephoneController');

    Route::resource('product_data', 'Backend\ProductDataController');
    Route::resource('document_within_category', 'Backend\DocumentWithInCategoryController');
    Route::resource('document_within', 'Backend\DocumentWithInController');

    Route::resource('banner_insurance', 'Backend\BannerInsuranceController');
    Route::resource('document_category', 'Backend\DocumentCategoryController');
    Route::resource('document', 'Backend\DocumentController');
    Route::resource('branch', 'Backend\BranchController');

    Route::resource('insurance_branch', 'Backend\InsuranceBranchController');
    Route::resource('form_branch', 'Backend\FormBranchController');

    Route::resource('edit_profile', 'Backend\EditProfileController');

    Route::controller('sequence','Backend\SequenceController');
    Route::controller('gps','Backend\GpsController');
});


Route::group(['prefix'=>'_agent'],function() {

//    Route::get('/',function(){
//        return view('agent.index');
//    });

    Route::get('/',['middleware'=>'agent',function(){
        return view('agent.index');
    }]);

//    Route::get('/',['middleware'=>'agent',function(){ 
//        return view('agent.index'); 
//    }]);


    Route::get('login','Backend\AuthController@getLoginAgent');
    Route::post('login','Backend\AuthController@postLoginAgent');
    Route::get('logout','Backend\AuthController@getLogoutAgent');

    Route::resource('car_insurance', 'Agent\CarInsuranceController');
    Route::resource('form_insurance', 'Agent\FormInsuranceController');
    Route::resource('edit_profile', 'Agent\EditProfileController');

});

Route::post('/api-active','Backend\ApiController@changeActive');
Route::post('/api-change','Backend\ApiController@change_status');
Route::post('/api-check-user','Backend\ApiController@check_user');
