<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Model\Region;
use App\Model\Zipcode;
use App\Model\Province;
use App\Model\District;
use App\Model\Amphur;

class AddressController extends Controller
{
    public function __construct(){

    }
    public function region(){
        $data = Region::select('geo_id as id','name_th as name')
            ->orderBy('name_th','asc')
            ->get();
        return $data;
    }
    public function province($geo_id){
        $data = Province::select('province_id as id','province_name_th as name')
            ->where('geo_id',$geo_id)
            ->orderBy('province_name_th','asc')
            ->get();
        return $data;
    }
    public function district($province_id){
        $data = Amphur::select('district_id as id','name')
            ->where('province_id',$province_id)
            ->orderBy('name','asc')
            ->get();
        return $data;
    }
    public function zipcode($district_id){
        $data = Amphur::select('district_id as id','zip as name')
            ->where('district_id',$district_id)
            ->orderBy('zip','asc')
            ->first();
        return $data;
    }
}
