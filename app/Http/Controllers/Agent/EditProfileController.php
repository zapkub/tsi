<?php
namespace App\Http\Controllers\Agent;

use Illuminate\Http\Request;
use App\Model\Agent;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;
use Hash;

class EditProfileController extends Controller
{
    public $model = 'App\Model\Agent';
    public $titlePage = 'แก้ไขข้อมูลส่วนตัว';
    public $tbName = 'agent';
    public $pkField = 'agent_id';
    public $fieldList = array('company_name','agent_no','firstname','lastname','email','username','password','branch_id');
    public $a_search = array('');
    public $path = '_agent/edit_profile';
    public $page = 'edit_profile';
    public $viewPath = 'agent/edit_profile';

    public function __construct()
    {
        $this->middleware('agent');
    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        if(auth()->guard('agent')->user()->agent_id == $id) {
            $objFn = new MainFunction();

            $url_to = $this->path . '/' . $id;
            $method = 'PUT';
            $txt_manage = "Update";
            Session::put('referUrl', URL::previous());

            $model = $this->model;
            $data = $model::find($id);
            return view($this->viewPath . '/update', compact('data', 'url_to', 'method', 'txt_manage'));
        }
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id )
    {
        $objFn = new MainFunction();

        $model = $this->model;
        $data = $model::find($id);

        $strParam = $request->strParam;

        if(!empty($request->new_password)){
            $request->password = Hash::make($request->new_password);
        }

        $objFn->db_update($data,$this->pkField,$request,$this->fieldList);

        return Redirect::Back();
    }

}

