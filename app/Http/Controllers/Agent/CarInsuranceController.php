<?php
namespace App\Http\Controllers\Agent;

use Illuminate\Http\Request;
use App\Model\CarInsurance;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class CarInsuranceController extends Controller
{
    public $model = 'App\Model\CarInsurance';
    public $titlePage = 'CarInsurance';
    public $tbName = 'car_insurance';
    public $pkField = 'car_insurance_id';
    public $fieldList = array('agent_id','branch_id','type_repair','status','no','book','firstname','lastname','address','status_insurance','extend','type_insurance','type_insurance_other','id_card','occupation','occupation_other','driver_type','driver_name','birthday','driver_type','driver_occupation','driver_license','driver_provice','driver_mobile','driver_name2','birthday2','driver_occupation2','driver_license2','driver_provice2','driver_mobile2','number_sign','company','end_date','use_car','use_car_other','beneficiary','name_car','registration','chassis','roon','seating','size','weight','total_decorate','code_car','engine','decorate_car','price','decorate_car2','price2','decorate_car3','price3','equipment','equipment_other','person_damage','person_damage2','person_damage3','person_damage4','car_damage','car_damage2','car_damage3','document_ptt','document_ptt2','document_ptt3','document_ptt4','document_ptt5','document_ptt6','document_ptt7','net','net_tax','net_act','from_date','to_date','agent','code_agent','note','flag','staff_accept','staff_accept_date','reason_c');
    public $a_search = array('no');
    public $path = '_agent/car_insurance';
    public $page = 'car_insurance';
    public $viewPath = 'agent/car_insurance';

    public function __construct()
    {
        $this->middleware('agent');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {
        $agent_id = auth()->guard('agent')->user()->agent_id;
        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if(empty($orderBy)) $orderBy = $this->pkField;
        $sortBy = Input::get('sortBy');
        if(empty($sortBy)) $sortBy = 'desc';

        $search = Input::get('search');
        $model = $this->model;
        $data = $model::whereNull('deleted_at')->where('status','0')->where('agent_id',$agent_id);

        if(!empty($search))
        {
            $data = $data->where(function ($query) use($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field, 'like', '%'.$search.'%');
                }
            });
        }
        $countData = $data->count();
        $data = $data 
            ->orderBy($orderBy,$sortBy)
            ->paginate($perPage);
        $data->setPath($this->path);
        $data->lastPage();

        $data2 = $model::whereNull('deleted_at')->where('status','1')->where('agent_id',$agent_id);

        if(!empty($search))
        {
            $data2 = $data2->where(function ($query) use($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field, 'like', '%'.$search.'%');
                }
            });
        }
        $countData2 = $data2->count();
        $data2 = $data2
            ->orderBy($orderBy,$sortBy)
            ->paginate($perPage);
        $data2->setPath($this->path);
        $data2->lastPage();

        $data3 = $model::whereNull('deleted_at')->where('status','2')->where('agent_id',$agent_id);

        if(!empty($search))
        {
            $data3 = $data3->where(function ($query) use($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field, 'like', '%'.$search.'%');
                }
            });
        }
        $countData3 = $data3->count();
        $data3 = $data3
            ->orderBy($orderBy,$sortBy)
            ->paginate($perPage);
        $data3->setPath($this->path);
        $data3->lastPage();

        return view($this->viewPath.'/index',compact('data','data2','data3','countData','countData2','countData3'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";

        return view($this->viewPath.'/update',compact('url_to','method','txt_manage'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {

        $objFn = new MainFunction();
        $model = $this->model;
        $data = new $model;

        $request->birthday = $request->year."-".$request->month."-".$request->day;
        $request->birthday2 = $request->year2."-".$request->month2."-".$request->day2;
        
        $request->staff_accept = '';
        $request->staff_accept_date = '';

//        return $request->birthday;

        $request->agent_id = auth()->guard('agent')->user()->agent_id;
        $request->flag = 1;
        $lastNo = "";
        $request->reason_c = "";

        $queryLast = DB::table('car_insurance')->select('no')->orderBy('car_insurance_id','desc')->where('flag',1);
        $countData = $queryLast->count();
        $queryLast = $queryLast->first();

        if($countData > 0){
            $lastNo = $queryLast->no;
        }

//        $currentYear = substr(date('Y')+543,2,2);
//        $lastYear = substr($lastNo,-4);

        if ($countData == 0){
            $newRandomNo = '0001';
        }else{
            $lastNoF = substr($lastNo,-4);

            $newRandomNo = $lastNoF+1;
            $newRandomNo = str_pad($newRandomNo,4,"0",STR_PAD_LEFT);

//            if($currentYear > $lastYear){
//                $newRandomNo = "0001";
//            }else {
//                $newRandomNo = $lastNoF+1;
//                $newRandomNo = str_pad($newRandomNo,4,"0",STR_PAD_LEFT);
//            }
        }

//        if($request->flag == 2){
//            $branch_db = DB::table('branch')->where('branch_id',$request->branch_id)->first();
//            $request->no = $branch_db->code."/".$currentYear.date('m').$newRandomNo;
//        }else{
//            $request->no = $request->agent_no."/".$newRandomNo;
//        }

        $request->no = $request->agent_no."/".$newRandomNo;


        $objFn->db_add($data,$this->pkField,$request,$this->fieldList);

        return Redirect::to('_agent/car_insurance');
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {
        $txt_manage = "Print";
        Session::put('referUrl',URL::previous());

        $model = $this->model;
        $data = $model::find($id);

        return view($this->viewPath.'/print',compact('data','txt_manage'));

    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl',URL::previous());

        $model = $this->model;
        $data = $model::find($id);
        return view($this->viewPath.'/update',compact('data','url_to','method','txt_manage'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id )
    {
        $objFn = new MainFunction();
        $model = $this->model;
        $data = $model::find($id);

        $request->detail = e($request->detail);

        $objFn->db_update($data,$this->pkField,$request,$this->fieldList);
        return Redirect::Back();
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl',URL::previous());
        $model = $this->model;
        $model::find($id)->delete();

        return Redirect::to(Session::get('referUrl'));
    }
}

