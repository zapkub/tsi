<?php
namespace App\Http\Controllers\Agent;

use Illuminate\Http\Request;
use App\Model\FormInsurance;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Validator;
use Session;
use Config;
use Storage;

class FormInsuranceController extends Controller
{
    public $model = 'App\Model\FormInsurance';
    public $titlePage = 'FormInsurance';
    public $tbName = 'form_insurance';
    public $pkField = 'form_insurance_id';
    public $fieldList = array('status_endorse','agent_id','no','book','request_no','firstname','lastname','car_name','license_plate','date_coverage','detail','reason','note','name_agent','code_agent','flag','reason_c');
    public $a_search = array('no','request_no');
    public $path = '_agent/form_insurance';
    public $page = 'form_insurance'; 
    public $viewPath = 'agent/form_insurance';

    public function __construct()
    {
        $this->middleware('agent');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {
        $agent_id = auth()->guard('agent')->user()->agent_id;
        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if(empty($orderBy)) $orderBy = $this->pkField;
        $sortBy = Input::get('sortBy');
        if(empty($sortBy)) $sortBy = 'desc';

        $search = Input::get('search');
        $model = $this->model;
        $data = $model::whereNull('deleted_at')->where('status','0')->where('agent_id',$agent_id);

        if(!empty($search))
        {
            $data = $data->where(function ($query) use($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field, 'like', '%'.$search.'%');
                }
            });
        }
        $countData = $data->count();
        $data = $data
            ->orderBy($orderBy,$sortBy)
            ->paginate($perPage);
        $data->setPath($this->path);
        $data->lastPage();

        $data2 = $model::whereNull('deleted_at')->where('status','1')->where('agent_id',$agent_id);

        if(!empty($search))
        {
            $data2 = $data2->where(function ($query) use($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field, 'like', '%'.$search.'%');
                }
            });
        }
        $countData2 = $data2->count();
        $data2 = $data2
            ->orderBy($orderBy,$sortBy)
            ->paginate($perPage);
        $data2->setPath($this->path);
        $data2->lastPage();

        $data3 = $model::whereNull('deleted_at')->where('status','2')->where('agent_id',$agent_id);

        if(!empty($search))
        {
            $data3 = $data3->where(function ($query) use($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field, 'like', '%'.$search.'%');
                }
            });
        }
        $countData3 = $data3->count();
        $data3 = $data3
            ->orderBy($orderBy,$sortBy)
            ->paginate($perPage);
        $data3->setPath($this->path);
        $data3->lastPage();

        return view($this->viewPath.'/index',compact('data','data2','data3','countData','countData2','countData3'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "ส่งคำร้องเข้าฝ่ายรับประกันรถยนต์สำนักงานใหญ่";

        return view($this->viewPath.'/update',compact('url_to','method','txt_manage'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $objFn = new MainFunction();
        $model = $this->model;
        $data = new $model;

        $request->agent_id = auth()->guard('agent')->user()->agent_id;
        $request->detail = e($request->detail);
        $request->flag = 1;
        $request->reason_c = "";

        if($request->status_endorse==1){
            $TT = "EN";
        }else{
            $TT = "CN";
        }

        $lastNo = "";

        $queryLast = DB::table('form_insurance')->select('no')->orderBy('form_insurance_id','desc');
        $countData = $queryLast->count();
        $queryLast = $queryLast->first();

        if($countData > 0){
            $lastNo = $queryLast->no;
        }

        if ($countData == 0){
            $newRandomNo = '0001';
        }else {
            $lastNoF = substr($lastNo, -4);

            $newRandomNo = $lastNoF + 1;
            $newRandomNo = str_pad($newRandomNo, 4, "0", STR_PAD_LEFT);
        }

        $request->no = $request->agent_no."/".$TT.$newRandomNo;

        $id = $objFn->db_add($data,$this->pkField,$request,$this->fieldList);

        return Redirect::to('_agent/form_insurance');
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {
        $txt_manage = "Print";
        Session::put('referUrl',URL::previous());

        $model = $this->model;
        $data = $model::find($id); 

        return view($this->viewPath.'/print',compact('data','txt_manage'));
    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl',URL::previous());

        $model = $this->model;
        $data = $model::find($id);

        return view($this->viewPath.'/update',compact('data','url_to','method','txt_manage'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id )
    {
        $objFn = new MainFunction();
        $model = $this->model;

        return Redirect::to('_agent/car_insurance');
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl',URL::previous());
        $model = $this->model;
        $model::find($id)->delete();

        return Redirect::to(Session::get('referUrl'));
    }
}

