<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Province;

class ProvinceController extends Controller {

    public function index(){
        $provinces = Province::get();

        return response()->json($provinces);
    }

}
