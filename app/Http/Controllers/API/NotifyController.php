<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Notify;

class NotifyController extends Controller {

  public function store(Request $request){
      $plate_number = $request->plate_number;
      $plate_province_id = $request->plate_province_id;
      $mobile = $request->mobile;
      $lat = $request->lat;
      $lng = $request->lng;

      $result = Notify::insert([
          'plate_number' => $plate_number,
          'plate_province_id' => $plate_province_id,
          'mobile' => $mobile,
          'lat' => $lat,
          'lng' => $lng
      ]);

      return ($result == 1)?'true':'false';
  }

}
