<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Input;

use App\Model\Surveyor;

class LoginController extends Controller {

    public function postLogin(Request $request){
        $username = $request->username;
        $password = $request->password;

        $surveyor = Surveyor::where('username', $username)->where('password', $password)->first();
        $result['success'] = (count($surveyor) > 0)?'1':'0';
        $result['surveyor'] = $surveyor;

        return response()->json($result);
    }

}
