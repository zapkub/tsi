<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Input;

use App\Model\Surveyor;

class SurveyorGPSController extends Controller {

    public function update(Request $request, $surveyor_id){
        $lat = $request->lat;
        $lng = $request->lng;

        $result = Surveyor::where('surveyor_id', $surveyor_id)
                ->update([
                  'latitude' => $lat,
                  'longitude' => $lng
                ]);

        return ($result == 1)?'true':'false';
    }

}
