<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TokenController extends Controller {

    public function index(){
      // $encrypter = app('Illuminate\Encryption\Encrypter');
      // $encrypted_token = $encrypter->encrypt(csrf_token());
        // return $encrypted_token;
      return csrf_token();
    }

}
