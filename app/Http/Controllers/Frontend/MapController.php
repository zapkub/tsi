<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Surveyor;

class MapController extends Controller {

    public function getAll(){
        $surveyors = Surveyor::where('latitude', '!=', '')
                            ->where('longitude', '!=', '')
                            ->get();

        return view('frontend.map', compact('surveyors'));
    }

}
