<?php
namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\History;
use App\Model\VisionAndMission;
use App\Model\ExecutiveCommittee;
use App\Model\YearBook;
use App\Model\Financial;

class AboutUsController extends Controller {
    public function history(){
        // ประวัติบริษัท
        $history = History::first();
        return view('frontend.template.about_us.history',compact('history'));
    }
    public function vision(){
        // วิสัยทัศน์
        $vision = VisionAndMission::first();
        return view('frontend.template.about_us.vision',compact('vision'));
    }
    public function manager(){
        $vice_manager = ExecutiveCommittee::select('user_title.title as title_th','user_title.title_en','executive_committee.*','position.name_th as position_th','position.name_en as position_en')
                          ->leftJoin('user_title','user_title.user_title_id','=','executive_committee.user_title_id')
                          ->leftJoin('position','position.position_id','=','executive_committee.position_id')
                        //   ->where('executive_committee.flag','2')
                          ->where('sequence', '1')
                          ->first();

        $assistant_manager = ExecutiveCommittee::select('user_title.title as title_th','user_title.title_en','executive_committee.*','position.name_th as position_th','position.name_en as position_en')
                              ->leftJoin('user_title','user_title.user_title_id','=','executive_committee.user_title_id')
                              ->leftJoin('position','position.position_id','=','executive_committee.position_id')
                            //   ->where('executive_committee.flag','2')
                              ->where('sequence', '2')
                              ->first();

        // คณะผู้บริหาร
        $managers = ExecutiveCommittee::select('user_title.title as title_th','user_title.title_en','executive_committee.*','position.name_th as position_th','position.name_en as position_en')
            ->leftJoin('user_title','user_title.user_title_id','=','executive_committee.user_title_id')
            ->leftJoin('position','position.position_id','=','executive_committee.position_id')
            // ->where('executive_committee.flag','2')
//            ->where('sequence', '>', '3')
            ->orderBy('executive_committee.sequence','asc')
            ->get();

        return view('frontend.template.about_us.manager',compact('vice_manager', 'assistant_manager', 'managers'));
    }
    public function executive_committee(){
        // คณะกรรมการ
        $committees = ExecutiveCommittee::orderBy('executive_committee.sequence','asc')
                        ->get();
        return view('frontend.template.about_us.executive_committee',compact('committees'));
    }
    public function annual_report(){
        // รายงานประจำปี
        $reports = YearBook::orderBy('yearbook_id','desc')->get();

        return view('frontend.template.about_us.annual_report',compact('reports'));
    }
    public function overall_operation(){
        // ฐานะการเงินและผลการดำเนินงาน
        $financials = Financial::orderBy('financial_id','desc')->get();
        $items = array();

        foreach ($financials as $financial){
          $items[$financial->financial_years][$financial->quarter] = $financial;
        }

        return view('frontend.template.about_us.overall_operation',compact('items'));
    }




}
