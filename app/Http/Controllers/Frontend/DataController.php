<?php 

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Garage;

class DataController extends Controller {
    public function getIndex(){
        $garages = Garage::select('garage_id','share_location')->get();
        foreach($garages as $garage){
            $garage_id = $garage->garage_id;
            $location = $garage->share_location;
            if($location != ''){
                $str = $location;
                $nStr = str_replace(array('http://maps.google.com/maps?q=','ttp://maps.google.com/maps?q='),"",$str);
                $a_explode = explode(',',$nStr);
                $la = trim($a_explode[0]);
                $lo = trim($a_explode[1]);
                $objGarage = Garage::find($garage_id);
                $objGarage->latitude = $la;
                $objGarage->longitude = $lo;
                $objGarage->save();
                echo $nStr.'<br>';
            }
        }
    }

}