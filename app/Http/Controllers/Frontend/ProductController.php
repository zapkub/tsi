<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Category;
use App\Model\ProductCategory;
use App\Model\Product;
use App\Model\ContactMsg;
use ReCaptcha\ReCaptcha;

use Validator;
use Mail;
use Input;

class ProductController extends Controller {

    public function getCategoryDetail($cat_id){
      $cat = Category::find($cat_id);

      $subcats = Category::where('parent_category_id', $cat_id)->orderBy('category_id', 'asc')->get();

      $product_list = ProductCategory::join('product', 'product_category.product_id', '=', 'product.product_id')
                                    ->where('category_id', $cat_id)
                                    ->whereNull('product_category.deleted_at')
                                    ->whereNull('product.deleted_at')
                                    ->get();

      return view('frontend.template.product.category-detail', compact('cat', 'subcats', 'product_list'));
    }

    public function getProductDetail($product_id){
      $product = Product::find($product_id);

      return view('frontend.template.product.product-detail', compact('product'));
    }
    public function getBuy($product_id){
        $product = Product::find($product_id);
        $cat = Category::find($product->ProductCategory[0]->category_id);

        return view('frontend.template.product.buy', compact('product', 'cat'));
    }
    public function postSubmit(Request $request){
        $product_id = $request->product_id;
        $getCaptcha = Input::get('g-recaptcha-response');

        if(empty($request->product_id)){
            return redirect()->to('/');
        }
        $product = Product::find($product_id);
        $product_name = $product->product_name_th;

        $category_id = $product->ProductCategory[0]->category_id;
        $cat = Category::find($category_id);
        $cat_name = $cat->category_name_th;

        $validator = Validator::make($request->all(),
            [
                'name' => 'required',
                'email' => 'required|email',
                'mobile' => 'required',
                'text' => 'required',
                "g-recaptcha-response" => 'required',
            ],
            [
                'name.required' => 'กรุณากรอก ชื่อ - นามสกุล',
                'email.required' => 'กรุณากรอกอีเมล',
                'email.email' => 'กรุณาตรวจสอบรูปแบบอีเมล',
                'mobile.required' => 'กรุณากรอกเบอร์โทรศัพท์',
                'text.required' => 'กรุณากรอกข้อความ',
                "g-recaptcha-response.required" => "โปรดยืนยันว่าคุณไม่ใช่โปรแกรมอัตโนมัติ"
            ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        if($this->checkReCaptcha($getCaptcha) == true) {
            $input = $request->all();
            $msg = 'คุณ'.$input['name'].'<br>';
            $msg .= 'เบอร์โทรศัพท์ :: '.$input['mobile'].'<br>';
            $msg .= 'อีเมล :: '.$input['email'].'<br>';
            $msg .= 'สนใจซื้อ '.$cat_name.' '.$product_name;
            $msg .= 'ข้อความ <br>'.$input['text'];

            $input['msg'] = $msg;
            $input['subject'] = 'ซื้อประกัน';
            ContactMsg::create($input);
            // ส่งเมล
            $name = $request->name;
            $email = $request->email;
            if($category_id == '5'){
                $to_mail = 'uw.non2@tsi.co.th';
            } else if($category_id == '1' || $category_id == '2' || $category_id == '3' || $category_id == '4'){
                $to_mail = 'uw.non1@tsi.co.th';
            }else{
                $to_mail = 'uw.motor@tsi.co.th';
            }
            Mail::send('emails.buy-product',['msg'=>$msg],function($m) use ($email,$name,$to_mail){
                $m->from(env('MAIL_CONTACT'), 'TSI System');
                $m->to($to_mail, 'Webmaster TSI');
                $m->replyTo($email, $name);
                $m->bcc(['napat@bangkoksolutions.com'], 'Webmaster BKKSol');
                $m->subject('ซื้อประกัน');
            });

            return redirect()->back()->with('successMsg','คำสั่งซื้อถูกบันทึกแล้ว เจ้าหน้าที่จะติดต่อกลับภายใน 1-2 วัน ทำการ');
        }else{
            return redirect()->back()->with('errorMsg','คุณกรอกแคปช่าไม่ถูกต้องค่ะ');
        }
    }

    public function checkReCaptcha($getCaptcha){
        $remoteip = $_SERVER['REMOTE_ADDR'];
        $secret = env('RE_CAP_SECRET');

        $recaptcha = new ReCaptcha($secret);
        $resp = $recaptcha->verify($getCaptcha,$remoteip);
        if($resp->isSuccess()){
            return true;
        }else {
            return false;
        }

    }
}
