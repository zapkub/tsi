<?php 

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\ContactMsg;

use Validator;
use Mail;

class PetitionController extends Controller {

    public function getIndex(){
        return view('frontend.template.petition');
    }

    public  function postSubmit(Request $request){
        $validator = Validator::make($request->all(),
            [
                'name' => 'required',
                'email' => 'required|email',
                'mobile' => 'required',
//                'insurance_no' => 'required|numeric',
                'msg' => 'required'
            ],
            [
                'name.required' => 'กรุณากรอก ชื่อ - นามสกุล',
                'email.required' => 'กรุณากรอกอีเมล',
                'email.email' => 'กรุณาตรวจสอบรูปแบบอีเมล',
                'mobile.required' => 'กรุณากรอกเบอร์โทรศัพท์',
//                'insurance_no.required' => 'กรุณากรอกเลขกรมธรรม์ หรือ เลขรับแจ้งอุบัติเหตุ',
                'msg.required' => 'กรุณากรอกข้อความ',
//                'insurance_no.numeric' => 'เลขกรมธรรม์เป็นตัวเลขเท่านั้น',
            ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $input = $request->all();
        ContactMsg::create($input);
        // ส่งเมล
        $name = $input['name'];
        $email = $input['email'];
        $subject = $input['subject'];

        $msg = 'คุณ'.$name.'<br>';
        $msg .= 'เบอร์โทรศัพท์ :: '.$input['mobile'].'<br>';
        $msg .= 'อีเมล :: '.$email.'<br>';
        $msg .= 'เลขกรมธรรม์ :: '.$input['insurance_no'].'<br>';
        $msg .= $input['msg'];

        $to_mails = ['varot.chu@tsi.co.th', 'dusadee.law@tsi.co.th', 'chot.net@tsi.co.th', 'complainunit@tsi.co.th'];
        Mail::send('emails.buy-product',['msg'=>$msg],function($m) use ($email,$name,$subject,$to_mails){
            $m->from(env('MAIL_CONTACT'), 'TSI System');
            $m->to($to_mails, 'Webmaster TSI');
            $m->replyTo($email, $name);
//            $m->bcc(['charuwan@bangkoksolutions.com','napat@bangkoksolutions.com'], 'Webmaster BKKSol');
            $m->subject($subject);
        });

        return redirect()->back()->with('successMsg','บันทึกข้อมูลเรียบร้อยแล้ว เจ้าหน้าที่จะติดต่อกลับภายใน 1-2 วัน ทำการ');
    }
}