<?php
namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Compensation;
use App\Model\CompensationDetail;

class ClaimsController extends Controller {

    public function getIndex(){
      // $motor = Compensation::join('compensation_detail', 'compensation_detail.compensation_id', '=', 'compensation.compensation_id');
      $motor = Compensation::where('type', 1)->get();

      return view('frontend.template.claims.motor_claims', compact('motor'));
    }
    public function getOther(){
      $other = Compensation::where('type', 2)->get();
      return view('frontend.template.claims.other_claims', compact('other'));
    }
    public function getMotorAccident($id){
      $motor = CompensationDetail::where('compensation_id', $id)->orderBy('sequence','asc')->get();
      return view('frontend.template.claims.motor_1_claims', compact('motor'));
    }
    public function getMotorFix($id){
        $compensation = Compensation::where('compensation_id', $id)->first();
        $motor = CompensationDetail::where('compensation_id', $id)->orderBy('sequence','asc')->get();
      return view('frontend.template.claims.motor_2_claims', compact('motor', 'compensation'));
    }
    public function getMotorPayClaim(){

      return view('frontend.template.claims.motor_3_claims', compact(''));
    }
    public function getOtherClaim1($id){
      $compensation = Compensation::where('compensation_id', $id)->first();
      $motor = CompensationDetail::where('compensation_id', $id)->orderBy('sequence','asc')->get();
      return view('frontend.template.claims.other_1_claims', compact('motor', 'compensation'));
    }
    public function getOtherClaim2(){

      return view('frontend.template.claims.other_2_claims', compact(''));
    }
    public function getOtherClaim3(){

      return view('frontend.template.claims.other_3_claims', compact(''));
    }



}
