<?php 

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\News;

class NewsController extends Controller {

    public function index(){
        $news = News::orderBy('news_id','desc')->get();
        return view('frontend.template.news.news-list',compact('news'));
    }

    public function detail($id){
        $news = News::find($id);
        return view('frontend.template.news.news-detail',compact('news'));
    }

}