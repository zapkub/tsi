<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Region;
use App\Model\Province;
use App\Model\Garage;
use App\Model\Service;
use App\Model\District;
use App\Model\Brand;
use DB;

class ServiceType {
  const SERVICE_TYPE_GARAGE = 'garage';
  const SERVICE_TYPE_OFFICIAL = 'officialGarage';
  const SERVICE_TYPE_SERVICE = 'service';
  const SERVICE_TYPE_BRAND = 'brand';
}
class ServiceController extends Controller {
    public function main() {
        // ดึงข้อมูลภาค จังหวัด เขต ใส่ ไปใน blade แทน dump
        $products[] = array('name' => 'อู่ซ่อม', 'type' => ServiceType::SERVICE_TYPE_GARAGE);
        $products[] = array('name' => 'ศูนย์ซ่อม', 'type' => ServiceType::SERVICE_TYPE_OFFICIAL);
        $products[] = array('name' => 'ศูนย์บริการทีเอสไอ', 'type' => ServiceType::SERVICE_TYPE_SERVICE);
        $services = $products;
        return view('frontend.template.service', compact('services'));

    }

    // @param -
    // return area
    public function getArea($type) {
        $lang = session()->get('locale');
        // Get regions
        if($type == ServiceType::SERVICE_TYPE_SERVICE){
            $regions = Service::select('region.region_id as id','region.name_'.$lang.' as name', DB::raw('count(service.region_id) as count'))
                ->join('region','region.region_id','=','service.region_id')
                ->groupBy('service.region_id')
                ->get();
        }else{
            $regions = Garage::select('region.region_id as id','region.name_'.$lang.' as name', DB::raw('count(garage.region_id) as count'))
                ->join('region','region.region_id','=','garage.region_id')->groupBy('garage.region_id');

            if($type == ServiceType::SERVICE_TYPE_GARAGE){
                $regions = $regions->where('garage.gtype_id','!=','7')->get();
            }else if($type == ServiceType::SERVICE_TYPE_OFFICIAL){
                $regions = $regions->where('garage.gtype_id','7')->get();
            }
        }
        return ['area'=>$regions,'latlong' => '13.7245206,100.6331099'];
    }

    //end new method

    /**
     * @param area_id
     * @return array of province in area
     */
    public function getProvince($region_id,$type) {
        // เอา province มาจาก area id ?
        $lang = session()->get('locale');

        // Get Province
        if($type == ServiceType::SERVICE_TYPE_SERVICE){
            $provinces = Service::select('service.province_id as id','province_name_'.$lang.' as name','region_id', DB::raw('count(service.province_id) as count'))
                ->join('a_province','a_province.province_id','=','service.province_id')
                ->where('service.region_id',$region_id)
                ->groupBy('service.province_id')
                ->get();
        }else{
            $provinces = Garage::select('garage.province_id as id','province_name_'.$lang.' as name','region_id','gtype_id', DB::raw('count(garage.province_id) as count'))
                ->join('a_province','a_province.province_id','=','garage.province_id')
                ->where('garage.region_id',$region_id)
                ->groupBy('garage.province_id');
            if($type == ServiceType::SERVICE_TYPE_GARAGE){
                $provinces = $provinces->where('garage.gtype_id','!=','7')->get();
            }else if($type == ServiceType::SERVICE_TYPE_OFFICIAL){
                $provinces = $provinces->where('garage.gtype_id','7')->get();
            }
        }
        return ['province' => $provinces];
    }

    // @param province_id
    // @return array of districts in province
    public function getDistrict($province_id,$type) {
        $lang = session()->get('locale');
        $result = [];
        // Get Province
        if($type == ServiceType::SERVICE_TYPE_SERVICE){
            $districts = Service::select('service.district_id as id','district.name',DB::raw('count(service.district_id) as count'),'service.province_id')
                ->join('district','district.district_id','=','service.district_id')
                ->where('service.province_id',$province_id)
                ->groupBy('service.district_id')
                ->get();
        }else{
            $districts = Garage::select('garage.district_id as id','district.name',DB::raw('count(garage.district_id) as count','province_id'))
                ->join('district','district.district_id','=','garage.district_id')
                ->where('garage.province_id',$province_id)
                ->groupBy('garage.district_id');
            if($type == ServiceType::SERVICE_TYPE_GARAGE){
                $districts = $districts->where('garage.gtype_id','!=','7')->get();
            }else if($type == ServiceType::SERVICE_TYPE_OFFICIAL){
                $districts = $districts->where('garage.gtype_id','7')->get();
            }
        }


        if($type == ServiceType::SERVICE_TYPE_GARAGE){
            $result = Garage::select('garage.*','garage_name_'.$lang.' as name','district.name as district_name')->where('garage.province_id',$province_id)->where('gtype_id','!=','7')
                ->join('district','district.district_id','=','garage.district_id')
                ->get();
        }else if($type == ServiceType::SERVICE_TYPE_OFFICIAL){
            $result = Garage::select('garage.*','garage_name_'.$lang.' as name','district.name as district_name', 'brand.img_name as img_name')->where('garage.province_id',$province_id)->where('gtype_id','=','7')
                ->join('district','district.district_id','=','garage.district_id')
                ->join('brand','brand.brand_id','=','garage.brand_id')
                ->get();
        }else if($type == ServiceType::SERVICE_TYPE_SERVICE){
            $result = Service::select('service.*','branch_name_'.$lang.' as name','district.name as district_name')->where('service.province_id',$province_id)
                ->join('district','district.district_id','=','service.district_id')
                ->get();
        }

        $data = Province::select('region.name_'.$lang .' as region_name','a_province.province_name_'.$lang .' as province_name')
            ->leftJoin('region','region.geo_id','=','a_province.geo_id')
            ->where('a_province.province_id', $province_id)
            ->first();
        return ['district' => $districts,'province_id'=>$province_id,'service'=>$result, 'region_name'=>$data->region_name,'province_name'=>$data->province_name];
    }

    // API SHOULD FETCH DATA AND SEND ONLY WHAT VIEWS NEED
    public function getServiceListByDistrictIdAndType ($district_id,$type){
      $lang = session()->get('locale');
      $result = [];

      if($type == ServiceType::SERVICE_TYPE_GARAGE){
        $result = Garage::select('*','garage_name_'.$lang.' as name')->where('district_id',$district_id)->where('gtype_id','!=','7')->get();
      }else if($type == ServiceType::SERVICE_TYPE_OFFICIAL){
        $result = Garage::select('*','garage_name_'.$lang.' as name')->where('district_id',$district_id)
        ->join('brand','brand.brand_id','=','garage.brand_id')
        ->where('gtype_id','=','7')->get();
      }else if($type == ServiceType::SERVICE_TYPE_SERVICE){
        $result = Service::select('*','branch_name_'.$lang.' as name')->where('district_id',$district_id)->get();
      }

        $data = District::select('region.name_'.$lang .' as region_name','a_province.province_name_'.$lang .' as province_name','district.name as district_name')
            ->join('a_province','district.province_id','=','a_province.province_id')

            ->leftJoin('region','region.geo_id','=','a_province.geo_id')
            ->where('district.district_id', $district_id)
            ->first();


        if(count($result) > 0){
            foreach($result as $item){
                $item['district_name'] = $data->district_name;
            }
        }
      return ['district_id'=>$district_id,'service'=>$result,'region_name'=>$data->region_name,'province_name'=>$data->province_name,'district_name'=>$data->district_name];
    }
    public function getBrand(){
      $lang = session()->get('locale');
      $result = [];
      $result = Brand::select('*','name_'.$lang.' as name')->get();
      return $result;

    }
    public function getServiceListByBrand($district_id,$brand_id,$type){
      $lang = session()->get('locale');
      $result = [];
      $result =  Garage::select('*','garage_name_'.$lang.' as name')->where('district_id',$district_id)
      ->join('brand','brand.brand_id','=','garage.brand_id')
      ->where('gtype_id','=','7')
      ->where('garage.brand_id',$brand_id)
      ->get();
      // return ['service'=>$result];
      $data = District::select('region.name_'.$lang .' as region_name','a_province.province_name_'.$lang .' as province_name','district.name as district_name')
          ->join('a_province','district.province_id','=','a_province.province_id')

          ->leftJoin('region','region.geo_id','=','a_province.geo_id')
          ->where('district.district_id', $district_id)
          ->first();
      return ['district_id'=>$district_id,'service'=>$result,'region_name'=>$data->region_name,'province_name'=>$data->province_name,'district_name'=>$data->district_name];
    }
     public function index(){
        echo 'hi';

     }
}
