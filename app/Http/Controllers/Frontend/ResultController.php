<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Surveyor;

class ResultController extends Controller {

    public function index($sid){
        $surveyors = Surveyor::get();

        foreach ($surveyors as $surveyor){
            echo $surveyor->surveyor_id . '. ' . $surveyor->contact_name . ' (' . $surveyor->username . ') - ' . $surveyor->latitude . ' / ' . $surveyor->longitude . ' (' . $surveyor->updated_at . ')<br>';
        }
    }

}
