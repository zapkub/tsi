<?php 

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Model\Document;
use App\Model\DocumentCategory;

use Validator;
use Mail;

class DownloadController extends Controller {

    public function getIndex(){
        $lang = session()->get('locale');
        $objFn = new MainFunction();
        $docs = DocumentCategory::select('document_category_id','document_category_name_'.$lang.' as category_name')
            ->orderBy('sequence','asc')
            ->get();

        foreach($docs as $doc){
            $data = Document::select('document_id','document_name_'.$lang.' as document_name','file_name','document_date')->where('document_category_id',$doc->document_category_id)->orderBy('document_date','desc')->get();
            $doc->documents = $data;
        }

        return view('frontend.template.download',compact('docs','objFn'));
    }

}