<?php 

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Career;

class CareerController extends Controller {

    public function index(){
        $careers = Career::where('status','1')->orderBy('sequence','asc')->get();
        return view('frontend.template.career.career-list',compact('careers'));
    }
}