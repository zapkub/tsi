<?php 

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\ContactDepartment;

class ContactController extends Controller {

    public function getIndex(){

        $departments = ContactDepartment::orderby('sequence','asc')->get();
        return view('frontend.template.contact-us',compact('departments'));
    }
}