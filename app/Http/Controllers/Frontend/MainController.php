<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Banner;
use App\Model\IntroPage;
use App\Model\News;
use App\Model\BannerFooter;

class MainController extends Controller {

    public function main(){
        $banners = Banner::where('status', '1')->orderBy('sequence','asc')->get();

        $intro_banners = IntroPage::where('start_date', '<=', date('Y-m-d'))
                                  ->where('end_date', '>=', date('Y-m-d'))
                                  ->where('status', '1')
                                  ->take(5)->get();

        $news = News::orderBy('news_id','desc')->take(3)->get();

        $banners_footer = BannerFooter::where('status','1')->orderBy('sequence','asc')->take(4)->get();

        return view('frontend.template.index',compact('banners', 'intro_banners', 'news','banners_footer'));
    }
    public function lang($locale){
      session()->put('locale',$locale);

      return back();
    }

}
