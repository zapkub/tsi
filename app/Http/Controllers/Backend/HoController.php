<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\Blog;
use App\Http\Controllers\Controller;
use Cache;

class HoController extends Controller
{
    public $model = 'App\Model\History';
    public $titlePage = 'History';
    public $tbName = 'history';
    public $pkField = 'history_id';
    public $fieldList = array('detail_th','detail_en');
    public $a_search = array('detail_th');
    public $path = '_admin/history';
    public $page = 'history';
    public $viewPath = 'backend/history';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function getIndex()
    {

        $model = $this->model;
        $data = $model::get(); 

        Cache::put('data',$data,1);
        $value = Cache::get('data');
        return $value;

        return view($this->viewPath . '/index', compact('data'));
    }

    // ----------------------------------------- View Add Page

}

