<?php
namespace App\Http\Controllers\Backend;
use App\Model\ExecutiveCommittee;
use App\Model\Career;
use App\Model\CompensationDetail;
use App\Model\BannerFooter;
use App\Model\DocumentCategory;
use Illuminate\Http\Request;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class SequenceController extends Controller
{

    public function __construct()
    {
//        $this->middleware('auth');
    }

    public function postExecutivecommittee(Request $request)
    {

        $executive_committee_id = $request->executive_committee_id;
        $new_sorting = $request->new_sorting;
        $old_sorting = $request->old_sorting;

        $data = ExecutiveCommittee::where('sequence', $new_sorting)->first();

        if (count($data) == 0) {

            $change_data2 = ExecutiveCommittee::find($executive_committee_id);
            $change_data2->sequence = $new_sorting;
            $change_data2->save();
            return redirect()->back();

        }

        $change_data = ExecutiveCommittee::find($data->executive_committee_id);
        $change_data->sequence = $old_sorting;
        $change_data->save();

        $change_data2 = ExecutiveCommittee::find($executive_committee_id);
        $change_data2->sequence = $new_sorting;
        $change_data2->save();


        return redirect()->back();


    }


    public function postCareer(Request $request)
    {

        $career_id = $request->career_id;
        $new_sorting = $request->new_sorting;
        $old_sorting = $request->old_sorting;

        $data = Career::where('sequence', $new_sorting)->first();

        if (count($data) == 0) {

            $change_data2 = Career::find($career_id);
            $change_data2->sequence = $new_sorting;
            $change_data2->save();
            return redirect()->back();

        }

        $change_data = Career::find($data->career_id);
        $change_data->sequence = $old_sorting;
        $change_data->save();

        $change_data2 = Career::find($career_id);
        $change_data2->sequence = $new_sorting;
        $change_data2->save();

        return redirect()->back();


    }

    public function postBanner(Request $request)
    {

        $banner_footer_id = $request->banner_footer_id;
        $new_sorting = $request->new_sorting;
        $old_sorting = $request->old_sorting;

        $data = BannerFooter::where('sequence', $new_sorting)->first();

        if (count($data) == 0) {

            $change_data2 = BannerFooter::find($banner_footer_id);
            $change_data2->sequence = $new_sorting;
            $change_data2->save();
            return redirect()->back();

        }

        $change_data = BannerFooter::find($data->banner_footer_id);
        $change_data->sequence = $old_sorting;
        $change_data->save();

        $change_data2 = BannerFooter::find($banner_footer_id);
        $change_data2->sequence = $new_sorting;
        $change_data2->save();

        return redirect()->back();


    }

    public function postDocument(Request $request)
    {

        $document_category_id = $request->document_category_id;
        $new_sorting = $request->new_sorting;
        $old_sorting = $request->old_sorting;

        $data = DocumentCategory::where('sequence', $new_sorting)->first();

        if (count($data) == 0) {

            $change_data2 = DocumentCategory::find($document_category_id);
            $change_data2->sequence = $new_sorting;
            $change_data2->save();
            return redirect()->back();

        }

        $change_data = DocumentCategory::find($data->document_category_id);
        $change_data->sequence = $old_sorting;
        $change_data->save();

        $change_data2 = DocumentCategory::find($document_category_id);
        $change_data2->sequence = $new_sorting;
        $change_data2->save();

        return redirect()->back();


    }

    public function postCompensationdetail(Request $request)
    {

        $compensation_detail_id = $request->compensation_detail_id;
        $new_sorting = $request->new_sorting;
        $old_sorting = $request->old_sorting;

        $data = CompensationDetail::where('sequence', $new_sorting)->first();

        if (count($data) == 0) {

            $change_data2 = CompensationDetail::find($compensation_detail_id);
            $change_data2->sequence = $new_sorting;
            $change_data2->save();
            return redirect()->back();

        }

        $change_data = CompensationDetail::find($data->compensation_detail_id);
        $change_data->sequence = $old_sorting;
        $change_data->save();

        $change_data2 = CompensationDetail::find($compensation_detail_id);
        $change_data2->sequence = $new_sorting;
        $change_data2->save();

        return redirect()->back();


    }

}
