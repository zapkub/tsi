<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\FormInsurance;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Validator;
use Session;
use Config;
use Storage;

class FormInsuranceStaffController extends Controller
{
    public $model = 'App\Model\FormInsurance';
    public $titlePage = 'FormInsuranceStaff';
    public $tbName = 'form_insurance';
    public $pkField = 'form_insurance_id';
    public $fieldList = array('no','book','status','request_no','firstname','lastname','car_named','license_plate','date_coverage','car_name','date_coverage','license_plate','detail','reason','note','status','name_agent','code_agent');
    public $a_search = array('firstname');
    public $path = '_admin/form_insurance_staff';
    public $page = 'form_insurance_staff';
    public $viewPath = 'backend/form_insurance_staff';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {
        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if(empty($orderBy)) $orderBy = $this->pkField;
        $sortBy = Input::get('sortBy');
        if(empty($sortBy)) $sortBy = 'desc';

        $search = Input::get('search');
        $model = $this->model;
        $data = $model::whereNull('deleted_at')->where('status','0');

        if(!empty($search))
        {
            $data = $data->where(function ($query) use($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field, 'like', '%'.$search.'%');
                }
            });
        }
        $countData = $data->count();
        $data = $data
            ->orderBy($orderBy,$sortBy)
            ->paginate($perPage);
        $data->setPath($this->page);
        $data->lastPage();

        $data2 = $model::whereNull('deleted_at')->where('status','1');

        if(!empty($search))
        {
            $data2 = $data2->where(function ($query) use($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field, 'like', '%'.$search.'%');
                }
            });
        }
        $countData2 = $data2->count();
        $data2 = $data2
            ->orderBy($orderBy,$sortBy)
            ->paginate($perPage);
        $data2->setPath($this->page);
        $data2->lastPage();

        $data3 = $model::whereNull('deleted_at')->where('status','2');

        if(!empty($search))
        {
            $data3 = $data3->where(function ($query) use($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field, 'like', '%'.$search.'%');
                }
            });
        }
        $countData3 = $data3->count();
        $data3 = $data3
            ->orderBy($orderBy,$sortBy)
            ->paginate($perPage);
        $data3->setPath($this->page);
        $data3->lastPage();

        return view($this->viewPath.'/index',compact('data','data2','data3','countData','countData2','countData3'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "ส่งคำร้องเข้าฝ่ายรับประกันรถยนต์สำนักงานใหญ่";

        return view($this->viewPath.'/update',compact('url_to','method','txt_manage'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $objFn = new MainFunction();
        $model = $this->model;
        $data = new $model;

        $validator = Validator::make(Input::all(),
            [
                'check_data' => 'required',
            ],
            [
                'check_data.required' => 'กรุณายืนยันข้อมูล',
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $request->detail = e($request->detail);

        $id = $objFn->db_add($data,$this->pkField,$request,$this->fieldList);

        return Redirect::to('_admin/car_insurance');
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {

    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl',URL::previous());

        $model = $this->model;
        $data = $model::find($id);

        return view($this->viewPath.'/update',compact('data','url_to','method','txt_manage'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id )
    {
        $objFn = new MainFunction();
        $model = $this->model;
        $updated_at = date('Y-m-d H:i:s');

        $data = DB::table('car_insurance')
            ->where('car_insurance_id', $id)
            ->update(['approve' => 1 , 'updated_at' => $updated_at]);

        return Redirect::to('_admin/car_insurance');
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl',URL::previous());
        $model = $this->model;
        $model::find($id)->delete();

        return Redirect::to(Session::get('referUrl'));
    }
}

