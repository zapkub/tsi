<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\FormInsurance;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Validator;
use Session;
use Config;
use Storage;

class FormInsuranceAgentController extends Controller
{
    public $model = 'App\Model\FormInsurance';
    public $titlePage = 'ยืนยันสลักหลัง (ตัวแทน)';
    public $tbName = 'form_insurance';
    public $pkField = 'form_insurance_id';
    public $fieldList = array('status_endorse','branch_id','agent_id','no','book','request_no','firstname','lastname','car_name','license_plate','date_coverage','detail','reason','note','name_agent','code_agent','staff_accept','reason_c');
    public $a_search = array('no','request_no');
    public $path = '_admin/form_insurance_agent';
    public $page = 'form_insurance_agent';
    public $viewPath = 'backend/form_insurance_agent';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {
        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if(empty($orderBy)) $orderBy = $this->pkField;
        $sortBy = Input::get('sortBy');
        if(empty($sortBy)) $sortBy = 'desc';
        $status = Input::get('status');
        $search = Input::get('search');
        $model = $this->model;
        $data = $model::select('form_insurance.status_endorse','form_insurance.detail','form_insurance.reason','form_insurance.request_no','form_insurance.form_insurance_id','form_insurance.no','form_insurance.firstname','form_insurance.lastname','form_insurance.license_plate','form_insurance.created_at','form_insurance.code_agent','form_insurance.branch_id')->leftjoin('agent','agent.agent_id','=','form_insurance.agent_id')->whereNull('form_insurance.deleted_at')->where('form_insurance.status',$status)->where('agent.flag','1');

        if(!empty($search))
        {
            $data = $data->where(function ($query) use($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field, 'like', '%'.$search.'%');
                }
            });
        }
        $countData = $data->count();
        $data = $data
            ->orderBy($orderBy,$sortBy)
            ->paginate($perPage);
        $data->setPath($this->path);
        $data->lastPage();

        return view($this->viewPath.'/index',compact('data','countData','status'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "ส่งคำร้องเข้าฝ่ายรับประกันรถยนต์สำนักงานใหญ่";

        return view($this->viewPath.'/update',compact('url_to','method','txt_manage'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $objFn = new MainFunction();
        $model = $this->model;
        $data = new $model;

        $id = $objFn->db_add($data,$this->pkField,$request,$this->fieldList);

        return Redirect::to('_agent/form_insurance');
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {
        $txt_manage = "Print";
        Session::put('referUrl',URL::previous());

        $model = $this->model;
        $data = $model::find($id);

        return view($this->viewPath.'/print',compact('data','txt_manage'));
    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl',URL::previous());

        $model = $this->model;
        $data = $model::find($id);

        return view($this->viewPath.'/update',compact('data','url_to','method','txt_manage'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id )
    {
        $objFn = new MainFunction();
        $model = $this->model;
        $data = $model::find($id);

        $strParam = $request->strParam;

        $db = DB::table('admin')->select('firstname','lastname')->where('admin_id',auth()->guard('admin')->user()->admin_id)->first();
        $fullname = $db->firstname." ".$db->lastname;

        if(!empty($request->check)){
            DB::table('form_insurance')->where('form_insurance_id',$id)->update(['status' => '1' , 'staff_accept' => $fullname]);
        }else{
            DB::table('form_insurance')->where('form_insurance_id',$id)->update(['status' => '2' , 'staff_accept' => $fullname , 'reason_c' => $request->reason_c]);
        }

        return Redirect::to($this->path.'?'.$strParam);
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl',URL::previous());
        $model = $this->model;
        $model::find($id)->delete();

        return Redirect::to(Session::get('referUrl'));
    }
}

