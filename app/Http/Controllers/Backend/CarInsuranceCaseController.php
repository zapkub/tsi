<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\CarInsuranceCase;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class CarInsuranceCaseController extends Controller
{
    public $model = 'App\Model\CarInsuranceCase';
    public $titlePage = 'CarInsuranceCase';
    public $tbName = 'car_insurance_case';
    public $pkField = 'car_insurance_case_id';
    public $fieldList = array('surveyor_id','gps','case_date','status');
    public $a_search = array('');
    public $path = '_admin/car_insurance_case';
    public $page = 'car_insurance_case';
    public $viewPath = 'backend/car_insurance_case';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {
        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if(empty($orderBy)) $orderBy = $this->pkField;
        $sortBy = Input::get('sortBy');
        if(empty($sortBy)) $sortBy = 'desc';

        $search = Input::get('search');
        $model = $this->model;
        $data = $model::whereNull('deleted_at');

        if(!empty($search))
        {
            $data = $data->where(function ($query) use($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field, 'like', '%'.$search.'%');
                }
            });
        }
        $countData = $data->count();
        $data = $data
            ->orderBy($orderBy,$sortBy)
            ->paginate($perPage);
        $data->setPath($this->path);
        $data->lastPage();

        return view($this->viewPath.'/index',compact('data','data2','countData','countData2'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";

        return view($this->viewPath.'/update',compact('url_to','method','txt_manage'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $objFn = new MainFunction();
        $model = $this->model;
        $data = new $model;

        $request->detail = e($request->detail);

        $id = $objFn->db_add($data,$this->pkField,$request,$this->fieldList);

        return Redirect::to($this->path);
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {
        $url_to = $this->path;
        $txt_manage = "Print";
        $model = $this->model;
        $data = $model::find($id);

        return view($this->viewPath.'/print_form',compact('data','txt_manage','url_to'));
    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl',URL::previous());

        $model = $this->model;
        $data = $model::find($id);
        return view($this->viewPath.'/update',compact('data','url_to','method','txt_manage'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id )
    {
        $objFn = new MainFunction();
        $model = $this->model;
        $data = $model::find($id);

        $request->detail = e($request->detail);

        $objFn->db_update($data,$this->pkField,$request,$this->fieldList);
        return Redirect::Back();
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl',URL::previous());
        $model = $this->model;
        $model::find($id)->delete();

        return Redirect::to(Session::get('referUrl'));
    }
}

