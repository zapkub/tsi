<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\CompensationDetail;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class CompensationDetailController extends Controller
{
    public $model = 'App\Model\CompensationDetail';
    public $titlePage = 'CompensationDetail';
    public $tbName = 'compensation_detail';
    public $pkField = 'compensation_detail_id';
    public $fieldList = array('compensation_id','title_th','title_en','detail_th','detail_en','sequence');
    public $a_search = array('title_th','title_th');
    public $path = '_admin/compensation_detail';
    public $page = 'compensation_detail';
    public $viewPath = 'backend/compensation_detail';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {
        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if(empty($orderBy)) $orderBy = $this->pkField;
        $sortBy = Input::get('sortBy');
        if(empty($sortBy)) $sortBy = 'desc';

        $search = Input::get('search');
        $cid = Input::get('cid');
        $model = $this->model;


        $data = new $model;
        if(!empty($cid)){
            $data = $data->where('compensation_id',$cid);
        }
        if(!empty($search))
        {
            $data = $data->where(function ($query) use($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field, 'like', '%'.$search.'%');
                }
            });
        }
        $countData = $data->count();
        $data = $data
            ->orderBy($orderBy,$sortBy)
            ->paginate($perPage);
        $data->setPath($this->page);
        $data->lastPage();


        return view($this->viewPath.'/index',compact('data','countData'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";

        // $db = DB::table('compensation')->select('compensation')->Where('compensation_id',input::get('cid'))->first();
        // $compensation = $db->compensation_id;

        return view($this->viewPath.'/update',compact('url_to','method','txt_manage','compensation'));
        // return view($this->viewPath.'/update',compact('url_to','method','txt_manage'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $objFn = new MainFunction();
        $model = $this->model;
        $obj = $model::orderBy('sequence','desc')->first();
        $request->sequence = $obj->sequence+1;

        $data = new $model;
        $request->compensation_id = e($request->compensation_id);
        $request->title_th = e($request->title_th);
        $request->title_en = e($request->detail_en);
        $request->detail_th = e($request->detail_th);
        $request->detail_en = e($request->detail_en);
        $id = $objFn->db_add($data,$this->pkField,$request,$this->fieldList);
        return Redirect::to($this->path."?cid=".input::get('compensation_id'));
        // return Redirect::to($this->path);
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {

    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl',URL::previous());

        $model = $this->model;
        $data = $model::find($id);
        return view($this->viewPath.'/update',compact('data','url_to','method','txt_manage'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id )
    {
        $objFn = new MainFunction();
        $strParam = $request->strParam;

        // $request->title_th = e($request->title_th);
        // $request->title_en = e($request->title_en);
        // $request->detail_th = e($request->detail_th);
        // $request->detail_en = e($request->detail_en);

        $model = $this->model;
        $data = $model::find($id);
        $id = $objFn->db_update($data,$this->pkField,$request,$this->fieldList);
        return Redirect::to($this->path.'?'.$strParam);
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl',URL::previous());
        $model = $this->model;
        $model::find($id)->delete();

        return Redirect::to(Session::get('referUrl'));
    }
}
