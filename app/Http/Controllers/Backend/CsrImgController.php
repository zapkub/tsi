<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\CsrImg;
use App\Model\Csr;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class CsrImgController extends Controller
{
    public $model = 'App\Model\CsrImg';
    public $titlePage = 'CsrImg';
    public $tbName = 'csr_img';
    public $pkField = 'csr_img_id';
    public $fieldList = array('csr_id','img_path');
    public $a_search = array('');
    public $path = '_admin/csr_img';
    public $page = 'csr_img';
    public $viewPath = 'backend/csr_img';

    public function __construct()
    {
      //  $this->middleware('auth');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {

        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if(empty($orderBy)) $orderBy = 'sequence';
        $sortBy = Input::get('sortBy');
        if(empty($sortBy)) $sortBy = 'asc';

        $search = Input::get('search');
        $model = $this->model;
        $csr_id = Input::get('csr_id');

        $data = new $model;
        $data = $model::whereNull('deleted_at')->where('csr_id',$csr_id);

        if(!empty($search))
        {
            $data = $data->where(function ($query) use($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field, 'like', '%'.$search.'%');
                }
            });
        }
        $countData = $data->count();
        $data = $data
            ->orderBy($orderBy,$sortBy)
            ->paginate($perPage);
        $data->setPath($this->path);
        $data->lastPage();

        return view($this->viewPath.'/index',compact('data','countData','csr'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";

        return view($this->viewPath.'/update',compact('url_to','method','txt_manage'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $objFn = new MainFunction();

        $strParam = $request->strParam;
        $model = $this->model;
        $data = new $model;
        $id = $objFn->db_add($data,$this->pkField,$request,$this->fieldList);

        if (Input::hasFile('img_path')) {
            $photo = $request->file('img_path');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $path = public_path('uploads/'.$this->tbName);           // set path
            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;               // set filename
            $destinationPath = $path;
            $objFn->img_full_resize($photo, $destinationPath, $filename);   //
            $objFn->image_resize($photo, $destinationPath,100, $filename);   // resize image
            $data = $model::find($id);
            $data->img_path = $filename;
            $data->save();
        }

        $check_sequence = $model::where('csr_id',$request->csr_id)->orderBy('sequence','desc')->first();
        $data->sequence = $check_sequence->sequence+1;
        $data->save();

        return Redirect::to($this->path.'?'.$strParam);
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {

    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl',URL::previous());

        $model = $this->model;
        $data = $model::find($id);
        return view($this->viewPath.'/update',compact('data','url_to','method','txt_manage'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id )
    {
        $objFn = new MainFunction();
        $strParam = $request->strParam;

        $model = $this->model;
        $data = $model::find($id);
        $id = $objFn->db_update($data,$this->pkField,$request,$this->fieldList);
        if (Input::hasFile('img_path')) {
            $photo = $request->file('img_path');                     // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $old_name = $data->img_path;                            // get old name
            $path = public_path('uploads/'.$this->tbName);           // set path
            $objFn->del_storage($path,$old_name);                   // delete old picture in storage
            $objFn->del_storage($path.'/100',$old_name);                   // delete old picture in storage
            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;               // set filename
            $destinationPath = $path;
            $objFn->img_full_resize($photo, $destinationPath, $filename);   // resize image
            $objFn->image_resize($photo, $destinationPath,100, $filename);   // resize image
            $data = $model::find($id);
            $data->img_path = $filename;
            $data->save();
        }
        return Redirect::to($this->path.'?'.$strParam);
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl',URL::previous());
        $model = $this->model;
        $model::find($id)->delete();

        return Redirect::to(Session::get('referUrl'));
    }
}

