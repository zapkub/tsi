<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\Orders;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;
use Mail;

class DashboardController extends Controller
{
    public $model = 'App\Model\Orders';
    public $titlePage = 'Orders';
    public $tbName = 'orders';
    public $pkField = 'orders_id';
    public $fieldList = array('orders_no','orders_date','user_id', 'billing_address_id', 'billing_address', 'shipping_address_id', 'shipping_address', 'payment_method_id','paysbuy_method', 'payment_by','payment_date', 'total_price','discount_price','shipping_price','total_point','status_id','tracking_no');
    public $a_search = array('');
    public $path = '_admin/orders';
    public $page = 'orders';
    public $viewPath = 'backend';

    public function __construct()
    {
        $this->middleware('agent');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {
        return view($this->viewPath . '/index');
    }

}
