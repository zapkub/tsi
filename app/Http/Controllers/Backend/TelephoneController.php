<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\Telephone;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class TelephoneController extends Controller
{
    public $model = 'App\Model\Telephone';
    public $titlePage = 'ข้อมูลรายชื่อและที่ตั้งของบริษัทประกันวินาศภัย';
    public $tbName = 'telephone';
    public $pkField = 'telephone_id';
    public $fieldList = array('name','floor','department','telephone','fax');
    public $a_search = array('name');
    public $path = '_admin/';
    public $page = 'telephone';
    public $viewPath = 'backend/';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {

        $objFn = new MainFunction();

        $page_id = DB::table('page')->where('page_name','=',$this->titlePage)->first()->page_id;
        $permission = $objFn->permission($page_id,'r');
        if ($permission == '404') { return view('errors/404'); }
        else if ($permission == '403') { return view('errors/403'); }

        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if(empty($orderBy)) $orderBy = $this->pkField;
        $sortBy = Input::get('sortBy');
        if(empty($sortBy)) $sortBy = 'desc';

        $search = Input::get('search');
        $department = Input::get('departments');
        $floor = Input::get('floor');


        $model = $this->model;
        $data = new $model;

        if(!empty($search))
        {
            $data = $data->where(function ($query) use($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field, 'like', '%'.$search.'%');
                }
            });
        }

        if(!empty($department)){
            $data = $data->where('department', 'like', '%'.$department.'%');
        }
        if(!empty($floor)){
            $data = $data->where('floor', $floor);
        }

        //return $objFn->loop_category2($data->get());
        $countData = $data->count();
        $data = $data
            ->orderBy($orderBy,$sortBy)
            ->paginate($perPage);
        $data->setPath($this->page);
        $data->lastPage();


        return view('backend.index',compact('data','countData','permission'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
          // return 'Ok';
        $objFn = new MainFunction();
//
//         /* Check Permission */
        $page_id = DB::table('page')->where('page_name','=',$this->titlePage)->first()->page_id;
        $permission = $objFn->permission($page_id,'c');
        if ($permission == '404') { return view('errors/404'); }
        else if ($permission == '403') { return view('errors/403'); }
//
        $url_to = $this->path.'telephone';
        $method = 'POST';
        $txt_manage = "Add";
        $model = $this->model;
//
        $telephone = $model::get();
//         $telephone_id = $telephone[0]->telephone_id;
// //
//         $telephone_id = Input::get('telephone_id');
        return view($this->viewPath.'telephone'.'/update',compact('url_to','method','txt_manage','telephone','telephone_id'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $objFn = new MainFunction();

        /* Check Permission */
        $page_id = DB::table('page')->where('page_name','=',$this->titlePage)->first()->page_id;
        $permission = $objFn->permission($page_id,'c');
        if ($permission == '404') { return view('errors/404'); }
        else if ($permission == '403') { return view('errors/403'); }

        $model = $this->model;
        $data = new $model;
        $id = $objFn->db_add($data,$this->pkField,$request,$this->fieldList);


        return Redirect::to($this->path);
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {

    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {

        $objFn = new MainFunction();
        //
        // /* Check Permission */
        $page_id = DB::table('page')->where('page_name','=',$this->titlePage)->first()->page_id;
        $permission = $objFn->permission($page_id,'u');
        if ($permission == '404') { return view('errors/404'); }
        else if ($permission == '403') { return view('errors/403'); }
        //
        $url_to = $this->path.'telephone'.'/'.$id;
        // return $url_to;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl',URL::previous());
        //
        $model = $this->model;
        $data = $model::find($id);
        $telephone_id = $data->telephone_id;
        // //
        $telephone = $model::all();
        // //
        // return view($this->viewPath.'telephone'.'/update',compact('data','url_to','method','txt_manage','category','category_id'));
        return view($this->viewPath.'telephone'.'/update',compact('data','url_to','method','txt_manage','telephone','telephone_id'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id )
    {
        $objFn = new MainFunction();

        /* Check Permission */
        $page_id = DB::table('page')->where('page_name','=',$this->titlePage)->first()->page_id;
        $permission = $objFn->permission($page_id,'u');
        if ($permission == '404') { return view('errors/404'); }
        else if ($permission == '403') { return view('errors/403'); }

        $strParam = $request->strParam;

        $model = $this->model;
        $data = $model::find($id);
        $objFn->db_update($data,$this->pkField,$request,$this->fieldList);



        return Redirect::to($this->path);
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl',URL::previous());
        $objFn = new MainFunction();

        /* Check Permission */
        $page_id = DB::table('page')->where('page_name','=',$this->titlePage)->first()->page_id;
        $permission = $objFn->permission($page_id,'d');
        if ($permission == '404') { return view('errors/404'); }
        else if ($permission == '403') { return view('errors/403'); }

        $model = $this->model;
        $model::find($id)->delete();

        return Redirect::to(Session::get('referUrl'));
    }


}
