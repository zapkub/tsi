<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class DocumentController extends Controller
{
    public $model = 'App\Model\Document';
    public $titlePage = 'Document';
    public $tbName = 'document';
    public $pkField = 'document_id';
    public $fieldList = array('document_category_id','document_name_th','document_name_en','file_name','document_date');
    public $a_search = array('document_name_th','document_name_en');
    public $path = '_admin/document';
    public $page = 'document';
    public $viewPath = 'backend/document';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {
        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if(empty($orderBy)) $orderBy = $this->pkField;
        $sortBy = Input::get('sortBy');
        if(empty($sortBy)) $sortBy = 'desc';

        $search = Input::get('search');

        $model = $this->model;

        $data = $model::where('document_category_id',Input::get('document_category_id'));

        if(!empty($search))
        {
            $data = $data->where(function ($query) use($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field, 'like', '%'.$search.'%');
                }
            });
        }
        $countData = $data->count();
        $data = $data
            ->orderBy($orderBy,$sortBy)
            ->paginate($perPage);
        $data->setPath($this->page);
        $data->lastPage();


        return view($this->viewPath.'/index',compact('data','countData'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";

        return view($this->viewPath.'/update',compact('url_to','method','txt_manage'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $objFn = new MainFunction();
        $model = $this->model;
        $data = new $model;
        $strParam = $request->strParam;

        $id = $objFn->db_add($data,$this->pkField,$request,$this->fieldList);

        if (Input::hasFile('file_name')) {
            $photo = $request->file('file_name');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $old_name = $data->pdf_path;                             // get old name
            $path = public_path('uploads/document');

            $extension = $photo->getClientOriginalExtension();      // get extension

            if($extension == 'pdf' || $extension == 'docx' || $extension == 'xlsx') {
                $filename = $new_name . "." . $extension;               // set filename
                $destinationPath = $path;

                Input::file('file_name')->move($destinationPath,$filename);

                $data = $model::find($id);
                $data->file_name = $filename;
                $data->save();
            }
        }

        return Redirect::to($this->path.'?'.$strParam);
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {

    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl',URL::previous());

        $model = $this->model;
        $data = $model::find($id);
        return view($this->viewPath.'/update',compact('data','url_to','method','txt_manage'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id )
    {
        $objFn = new MainFunction();
        $strParam = $request->strParam;
        $model = $this->model;
        $data = $model::find($id);

        $objFn->db_update($data,$this->pkField,$request,$this->fieldList);

        if (Input::hasFile('file_name')) {
            $photo = $request->file('file_name');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $old_name = $data->file_name;                             // get old name
            $path = public_path('uploads/document');

            $extension = $photo->getClientOriginalExtension();      // get extension

            if($extension == 'pdf' || $extension == 'docx' || $extension == 'xlsx') {
                $filename = $new_name . "." . $extension;               // set filename
                $destinationPath = $path;

                Input::file('file_name')->move($destinationPath,$filename);

                $data = $model::find($id);
                $data->file_name = $filename;
                $data->save();
            }
        }

        return Redirect::to($this->path.'?'.$strParam);
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl',URL::previous());
        $model = $this->model;
        $model::find($id)->delete();

        return Redirect::to(Session::get('referUrl'));
    }
}
