<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\Amg;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class AmgController extends Controller
{
    public $model = 'App\Model\Amg';
    public $titlePage = 'Amg';
    public $tbName = 'amg';
    public $pkField = 'amg_id';
    public $fieldList = array('name_th','name_en','detail_th','detail_en','img_path','img_path_en','pdf_path','pdf_path_en');
    public $a_search = array('');
    public $path = '_admin/amg';
    public $page = 'amg';
    public $viewPath = 'backend/amg';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {
        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if(empty($orderBy)) $orderBy = $this->pkField;
        $sortBy = Input::get('sortBy');
        if(empty($sortBy)) $sortBy = 'desc';

        $search = Input::get('search');
        $model = $this->model;
        $data = new $model;

        if(!empty($search))
        {
            $data = $data->where(function ($query) use($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field, 'like', '%'.$search.'%');
                }
            });
        }
        $countData = $data->count();
        $data = $data
            ->orderBy($orderBy,$sortBy)
            ->paginate($perPage);
        $data->setPath($this->path);
        $data->lastPage();


        return view($this->viewPath.'/index',compact('data','countData'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";

        return view($this->viewPath.'/update',compact('url_to','method','txt_manage'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $objFn = new MainFunction();
        $model = $this->model;
        $data = new $model;

        $request->detail_th = e($request->detail_th);
        $request->detail_en = e($request->detail_en);

        $id = $objFn->db_add($data,$this->pkField,$request,$this->fieldList);

        if (Input::hasFile('pdf_path')) {
            $photo = $request->file('pdf_path');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $old_name = $data->pdf_path;                             // get old name
            $path = public_path('uploads/amg_pdf');

            $extension = $photo->getClientOriginalExtension();      // get extension

            if($extension == 'pdf') {
                $filename = $new_name . "." . $extension;               // set filename
                $destinationPath = $path;

                Input::file('pdf_path')->move($destinationPath,$filename);

                $data = $model::find($id);
                $data->pdf_path = $filename;
                $data->save();
            }
        }

        if (Input::hasFile('pdf_path_en')) {
            $photo = $request->file('pdf_path_en');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $old_name = $data->pdf_path_en;                             // get old name
            $path = public_path('uploads/amg_pdf_en');

            $extension = $photo->getClientOriginalExtension();      // get extension

            if($extension == 'pdf') {
                $filename = $new_name . "." . $extension;               // set filename
                $destinationPath = $path;

                Input::file('pdf_path_en')->move($destinationPath,$filename);

                $data = $model::find($id);
                $data->pdf_path_en = $filename;
                $data->save();
            }
        }

        if (Input::hasFile('img_path')) {
            $photo = $request->file('img_path');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $path = public_path('uploads/amg');           // set path
            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;               // set filename
            $destinationPath = $path;
            $objFn->img_full_resize($photo, $destinationPath, $filename);   //
            $objFn->image_resize($photo, $destinationPath,100, $filename);   // resize image
            $data = $model::find($id);
            $data->img_path = $filename;
            $data->save();
        }

        if (Input::hasFile('img_path_en')) {
            $photo = $request->file('img_path_en');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $path = public_path('uploads/amg_en');           // set path
            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;               // set filename
            $destinationPath = $path;
            $objFn->img_full_resize($photo, $destinationPath, $filename);   //
            $objFn->image_resize($photo, $destinationPath,100, $filename);   // resize image
            $data = $model::find($id);
            $data->img_path_en = $filename;
            $data->save();
        }

        return Redirect::to($this->path);
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {

    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl',URL::previous());

        $model = $this->model;
        $data = $model::find($id);
        return view($this->viewPath.'/update',compact('data','url_to','method','txt_manage'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id )
    {
        $objFn = new MainFunction();
        $model = $this->model;
        $data = $model::find($id);

        $request->detail_th = e($request->detail_th);
        $request->detail_en = e($request->detail_en);

        $objFn->db_update($data,$this->pkField,$request,$this->fieldList);

        if (Input::hasFile('pdf_path')) {
            $photo = $request->file('pdf_path');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $old_name = $data->pdf_path;                             // get old name
            $path = public_path('uploads/amg_pdf');

            $extension = $photo->getClientOriginalExtension();      // get extension

            if($extension == 'pdf') {
                $filename = $new_name . "." . $extension;               // set filename
                $destinationPath = $path;

                Input::file('pdf_path')->move($destinationPath,$filename);

                $data = $model::find($id);
                $data->pdf_path = $filename;
                $data->save();
            }
        }

        if (Input::hasFile('pdf_path_en')) {
            $photo = $request->file('pdf_path_en');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $old_name = $data->pdf_path_en;                             // get old name
            $path = public_path('uploads/amg_pdf_en');

            $extension = $photo->getClientOriginalExtension();      // get extension

            if($extension == 'pdf') {
                $filename = $new_name . "." . $extension;               // set filename
                $destinationPath = $path;

                Input::file('pdf_path_en')->move($destinationPath,$filename);

                $data = $model::find($id);
                $data->pdf_path_en = $filename;
                $data->save();
            }
        }

        if (Input::hasFile('img_path')) {
            $photo = $request->file('img_path');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $old_name = $data->img_path;                            // get old name
            $path = public_path('uploads/amg');           // set path
            $objFn->del_storage($path,$old_name);                   // delete old picture in storage
            $objFn->del_storage($path.'/100',$old_name);                   // delete old picture in storage
            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;               // set filename
            $destinationPath = $path;
            $objFn->img_full_resize($photo, $destinationPath, $filename);   // resize image
            $objFn->image_resize($photo, $destinationPath,100, $filename);   // resize image
            $data = $model::find($id);
            $data->img_path = $filename;
            $data->save();
        }

        if (Input::hasFile('img_path_en')) {
            $photo = $request->file('img_path_en');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $old_name = $data->img_path_en;                            // get old name
            $path = public_path('uploads/amg_en');           // set path
            $objFn->del_storage($path,$old_name);                   // delete old picture in storage
            $objFn->del_storage($path.'/100',$old_name);                   // delete old picture in storage
            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;               // set filename
            $destinationPath = $path;
            $objFn->img_full_resize($photo, $destinationPath, $filename);   // resize image
            $objFn->image_resize($photo, $destinationPath,100, $filename);   // resize image
            $data = $model::find($id);
            $data->img_path_en = $filename;
            $data->save();
        }

        return Redirect::Back();
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl',URL::previous());
        $model = $this->model;
        $model::find($id)->delete();

        return Redirect::to(Session::get('referUrl'));
    }
}

