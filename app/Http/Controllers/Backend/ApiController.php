<?php
namespace App\Http\Controllers\Backend;

use App\Model\ArticleCategory;
use App\Model\MomyArticleCategory;
use Illuminate\Http\Request;
use App\Model\Article;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Response;
use Validator;

class ApiController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function changeActive()
    {

        $rules = array('pk_field'=>'required','v_pk_field'=>'required','change_field'=>'required','value'=>'required','tb_name'=>'required');
        $validator = Validator::make(Input::all(),$rules);
        if($validator->fails())
        {
            return Response::json(array('success'=>false,'text'=>'Validate Error'));
        } else {


            if(Input::get('value') == '1')
            {
                $Obj = DB::table(Input::get('tb_name'))
                    ->where(Input::get('pk_field'),Input::get('v_pk_field'))
                    ->update(array(
                        Input::get('change_field') => 0,
                        'updated_at' => date('Y-m-d H:i:s')

                    ));

                echo "0";
            }else{

                $Obj = DB::table(Input::get('tb_name'))
                    ->where(Input::get('pk_field'),Input::get('v_pk_field'))
                    ->update(array(
                        Input::get('change_field') => 1,
                        'updated_at' => date('Y-m-d H:i:s')

                    ));

                echo "1";
            }
        }
    }

    public function changeReport()
    {

        $rules = array('pk_field'=>'required','v_pk_field'=>'required','change_field'=>'required','value'=>'required','tb_name'=>'required');
        $validator = Validator::make(Input::all(),$rules);
        if($validator->fails())
        {
            return Response::json(array('success'=>false,'text'=>'Validate Error'));
        } else {


            if(Input::get('value') == 'N')
            {
                $Obj = DB::table(Input::get('tb_name'))
                    ->where(Input::get('pk_field'),Input::get('v_pk_field'))
                    ->update(array(
                        Input::get('change_field') => 'Y',
                        'updated_at' => date('Y-m-d H:i:s')

                    ));

                echo "Y";
            }else{

                $Obj = DB::table(Input::get('tb_name'))
                    ->where(Input::get('pk_field'),Input::get('v_pk_field'))
                    ->update(array(
                        Input::get('change_field') => 'N',
                        'updated_at' => date('Y-m-d H:i:s')

                    ));

                echo "N";
            }
        }
    }
    public function changeCategory($type,$main_cate)
    {

        if($type == 'article'){
            $category = ArticleCategory::where('MAIN_CATEGORY_ID',$main_cate)->get();
        }elseif($type == 'momy_article'){
            $category = MomyArticleCategory::where('MAIN_CATEGORY_ID',$main_cate)->get();
        }
        $str = '';
        foreach($category as $field)
        {
            $str .= '<option value='.$field->CATEGORY_ID.'>'.$field->NAME.'</option>';
        }

        return $str;
    }


    public function getAmphur($province_id)
    {
        $data = DB::table('amphur')->where('PROVINCE_ID',$province_id)->orderBy('AMPHUR_NAME','asc')->get();
        $str[0] = '<option value="">กรุณาเลือกอำเภอ</option>';
        foreach($data as $field){
            $str[0] .= '<option value='.$field->AMPHUR_ID.'>'.trim($field->AMPHUR_NAME," ").'</option>';
        }
        $str[1]=$province_id;
        return $str;
    }
    public function getAmphur2($amphur_id,$province_id)
    {
        $data = DB::table('amphur')->where('PROVINCE_ID',$province_id)->orderBy('AMPHUR_NAME','asc')->get();
        $str[0] = '<option value="">กรุณาเลือกอำเภอ</option>';
        foreach($data as $field){
            $str[0] .= '<option value='.$field->AMPHUR_ID;
            if($field->AMPHUR_ID==$amphur_id)
                $str[0] .= ' selected ';
            $str[0] .= '>'.trim($field->AMPHUR_NAME," ").'</option>';
        }
        $str[1] = $amphur_id;
        return $str;
    }




    public function getProvince($province_id)
    {
        $province = DB::table('province')->orderBy('PROVINCE_NAME','asc')->get();
        $str = '<option value="">กรุณาเลือกจังหวัด</option>';
        foreach($province as $field){
            $str .= '<option value='.$field->PROVINCE_ID;
            if($field->PROVINCE_ID==$province_id)
                $str .= ' selected ';
            $str .= '>'.trim($field->PROVINCE_NAME," ").'</option>';
        }



        return $str;
    }
    
}

