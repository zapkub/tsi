<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\CarInsurance;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input; 
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class CarInsuranceBranchController extends Controller
{
    public $model = 'App\Model\CarInsurance';
    public $model2 = 'App\Model\FormInsurance';
    public $titlePage = 'การตรวจยืนยันใบคำขอออนไลน์';
    public $tbName = 'car_insurance';
    public $pkField = 'car_insurance_id';
    public $fieldList = array('agent_id','branch_id','type_repair','status','no','book','firstname','lastname','address','status_insurance','extend','type_insurance','type_insurance_other','id_card','occupation','occupation_other','driver_name','birthday','driver_occupation','driver_license','driver_provice','driver_mobile','driver_name2','birthday2','driver_occupation2','driver_license2','driver_provice2','driver_mobile2','number_sign','company','end_date','use_car','use_car_other','beneficiary','name_car','registration','chassis','roon','seating','size','weight','total_decorate','code_car','engine','decorate_car','price','decorate_car2','price2','decorate_car3','price3','equipment','equipment_other','person_damage','person_damage2','person_damage3','person_damage4','car_damage','car_damage2','car_damage3','document_ptt','document_ptt2','document_ptt3','document_ptt4','document_ptt5','document_ptt6','document_ptt7','net','net_tax','net_act','from_date','to_date','agent','code_agent','note','reason_c');
    public $a_search = array('no');
    public $path = '_admin/car_insurance_branch';
    public $page = 'car_insurance_branch';
    public $viewPath = 'backend/car_insurance_branch';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {
        $objFn = new MainFunction();

        /* Check Permission */
        $page_id = DB::table('page')->where('page_name','=',$this->titlePage)->first()->page_id;
        $permission = $objFn->permission($page_id,'r');
        if ($permission == '404') { return view('errors/404'); }
        else if ($permission == '403') { return view('errors/403'); }

        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if(empty($orderBy)) $orderBy = $this->pkField;
        $sortBy = Input::get('sortBy');
        if(empty($sortBy)) $sortBy = 'desc';

        $search = Input::get('search');
        $model = $this->model;
        $model2 = $this->model2;
//        $data = $model::whereNull('deleted_at');
        $data = $model::select('car_insurance.branch_id','car_insurance.car_insurance_id','branch.name_th','branch.code',DB::raw('SUM(net) as total_net'),DB::raw('SUM(net_tax) as total_net_tax'),DB::raw('SUM(net_act) as total_net_act'))->leftjoin('branch','branch.branch_id','=','car_insurance.branch_id')->leftjoin('agent','agent.agent_id','=','car_insurance.agent_id')->whereNull('car_insurance.deleted_at')->where('car_insurance.status','1')->where('agent.flag','2')->groupBy('car_insurance.branch_id');

        $count_1 = $model::leftjoin('agent','agent.agent_id','=','car_insurance.agent_id')->whereNull('car_insurance.deleted_at')->where('car_insurance.status','0')->where('agent.flag','2')->count();
        $count_2 = $model::leftjoin('agent','agent.agent_id','=','car_insurance.agent_id')->whereNull('car_insurance.deleted_at')->where('car_insurance.status','1')->where('agent.flag','2')->count();
        $count_3 = $model::leftjoin('agent','agent.agent_id','=','car_insurance.agent_id')->whereNull('car_insurance.deleted_at')->where('car_insurance.status','2')->where('agent.flag','2')->count();

        $count_s_1 = $model2::leftjoin('agent','agent.agent_id','=','form_insurance.agent_id')->whereNull('form_insurance.deleted_at')->where('form_insurance.status','0')->where('agent.flag','2')->count();
        $count_s_2 = $model2::leftjoin('agent','agent.agent_id','=','form_insurance.agent_id')->whereNull('form_insurance.deleted_at')->where('form_insurance.status','1')->where('agent.flag','2')->count();
        $count_s_3 = $model2::leftjoin('agent','agent.agent_id','=','form_insurance.agent_id')->whereNull('form_insurance.deleted_at')->where('form_insurance.status','2')->where('agent.flag','2')->count();

        if(!empty($search))
        {
            $data = $data->where(function ($query) use($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field, 'like', '%'.$search.'%');
                }
            });
        }
        if(!empty(Input::get('from_date')) && !empty(Input::get('to_date')) )
            $data = $data->whereBetween('car_insurance.created_at',[Input::get('from_date').' 00:00:00',Input::get('to_date').' 00:00:00']);


        $countData = $data->count();
        $data = $data
            ->orderBy($orderBy,$sortBy)
            ->paginate($perPage);
        $data->setPath($this->path);
        $data->lastPage();

        return view($this->viewPath.'/index',compact('data','countData','count_1','count_2','count_3','count_s_1','count_s_2','count_s_3','permission'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $objFn = new MainFunction();

        /* Check Permission */
        $page_id = DB::table('page')->where('page_name','=',$this->titlePage)->first()->page_id;
        $permission = $objFn->permission($page_id,'c');
        if ($permission == '404') { return view('errors/404'); }
        else if ($permission == '403') { return view('errors/403'); }

        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";

        return view($this->viewPath.'/update',compact('url_to','method','txt_manage'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $objFn = new MainFunction();

        /* Check Permission */
        $page_id = DB::table('page')->where('page_name','=',$this->titlePage)->first()->page_id;
        $permission = $objFn->permission($page_id,'c');
        if ($permission == '404') { return view('errors/404'); }
        else if ($permission == '403') { return view('errors/403'); }

        $model = $this->model;
        $data = new $model;

        $request->birthday = $request->year." ".$request->month." ".$request->day;
        $request->birthday2 = $request->year2." ".$request->month2." ".$request->day2;

        $request->agent_id = auth()->guard('agent')->user()->agent_id;
        $request->code_agent = "กท".$request->code_agent;

        $last_no = DB::table('car_insurance')->orderBy('no','desc')->first();
        $no_e = explode('/',$last_no->no)[1]+1;

        if($no_e==""){
            $no = "0001";
        }else{
            $no = sprintf("%04d",$no_e);
        }

        $request->no = $request->agent_no."/".$no;
        $objFn->db_add($data,$this->pkField,$request,$this->fieldList);

        return Redirect::to($this->path);
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {
        $objFn = new MainFunction();

        /* Check Permission */
        $page_id = DB::table('page')->where('page_name','=',$this->titlePage)->first()->page_id;
        $permission = $objFn->permission($page_id,'r');
        if ($permission == '404') { return view('errors/404'); }
        else if ($permission == '403') { return view('errors/403'); }

        $txt_manage = "Print";
        Session::put('referUrl',URL::previous());

        $model = $this->model;
        $data = $model::find($id);

        return view($this->viewPath.'/print',compact('data','txt_manage'));

    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $objFn = new MainFunction();

        /* Check Permission */
        $page_id = DB::table('page')->where('page_name','=',$this->titlePage)->first()->page_id;
        $permission = $objFn->permission($page_id,'c');
        if ($permission == '404') { return view('errors/404'); }
        else if ($permission == '403') { return view('errors/403'); }

        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl',URL::previous());

        $model = $this->model;
        $data = $model::find($id);
        return view($this->viewPath.'/update',compact('data','url_to','method','txt_manage'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id )
    {
        $objFn = new MainFunction();

        /* Check Permission */
        $page_id = DB::table('page')->where('page_name','=',$this->titlePage)->first()->page_id;
        $permission = $objFn->permission($page_id,'c');
        if ($permission == '404') { return view('errors/404'); }
        else if ($permission == '403') { return view('errors/403'); }

        $model = $this->model;
        $data = $model::find($id);

        $strParam = $request->strParam;

        $db = DB::table('admin')->select('firstname','lastname')->where('admin_id',auth()->guard('admin')->user()->admin_id)->first();
        $fullname = $db->firstname." ".$db->lastname;

        if(!empty($request->check)){
            DB::table('car_insurance')->where('car_insurance_id',$id)->update(['status' => '1' , 'staff_accept' => $fullname]);
        }else{
            DB::table('car_insurance')->where('car_insurance_id',$id)->update(['status' => '2' , 'staff_accept' => $fullname , 'reason_c' => $request->reason_c]);
        }

        return Redirect::to($this->path.'?'.$strParam);
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl',URL::previous());

        $objFn = new MainFunction();

        /* Check Permission */
        $page_id = DB::table('page')->where('page_name','=',$this->titlePage)->first()->page_id;
        $permission = $objFn->permission($page_id,'d');
        if ($permission == '404') { return view('errors/404'); }
        else if ($permission == '403') { return view('errors/403'); }

        $model = $this->model;
        $model::find($id)->delete();

        return Redirect::to(Session::get('referUrl'));
    }

}

