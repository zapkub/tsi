<?php
namespace App\Http\Controllers\Backend;

use App\Model\Product;


use Illuminate\Http\Request;
use App\Model\NewPage;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class ChangeStatusController extends Controller
{

    public function __construct()
    {
//        $this->middleware('admin');
    }


    public function postProduct(Request $request)
    {

        $product_id = $request->product_id;
        $new_status = $request->new_status;

        $data = Product::where('status', $new_status)->first();

            $change_data2 = Product::find($product_id);
            $change_data2->status = $new_status;
            $change_data2->save();




        return redirect()->back();


    }
}