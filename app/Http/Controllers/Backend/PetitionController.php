<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\ContactMsg;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Mail;
use Storage;

class PetitionController extends Controller
{
    public $model = 'App\Model\ContactMsg';
    public $titlePage = 'Petition';
    public $tbName = 'contact_msg';
    public $pkField = 'contact_msg_id';
    public $fieldList = array('name','email','msg','mobile');
    public $a_search = array('name','email');
    public $path = '_admin/petition';
    public $page = 'petition';
    public $viewPath = 'backend/petition';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {
        $objFn = new MainFunction();

        /* Check Permission */
        $page_id = DB::table('page')->where('page_name','=',$this->titlePage)->first()->page_id;
        $permission = $objFn->permission($page_id,'r');
        if ($permission == '404') { return view('errors/404'); }
        else if ($permission == '403') { return view('errors/403'); }

        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if(empty($orderBy)) $orderBy = $this->pkField;
        $sortBy = Input::get('sortBy');
        if(empty($sortBy)) $sortBy = 'desc';

        $search = Input::get('search');

        $model = $this->model;

        $data = $model::where('parent_id',0);
        $data = $data->whereIN('subject',["ร้องเรียนเรื่องอื่นๆ","ร้องเรียนเรื่องสินไหม"]);
        if(!empty($search))
        {
            $data = $data->where(function ($query) use($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field, 'like', '%'.$search.'%');
                }
            });
        }
        $countData = $data->count();
        $data = $data
            ->orderBy($orderBy,$sortBy)
            ->paginate($perPage);
        $data->setPath($this->page);
        $data->lastPage();

//        $reply_time = $model::select("created_at")->where("parent_id",15)->get();
//        return $reply_time;



        return view($this->viewPath.'/index',compact('data','countData','permission'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $objFn = new MainFunction();

        /* Check Permission */
        $page_id = DB::table('page')->where('page_name','=',$this->titlePage)->first()->page_id;
        $permission = $objFn->permission($page_id,'c');
        if ($permission == '404') { return view('errors/404'); }
        else if ($permission == '403') { return view('errors/403'); }

        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Reply";
        $id = Input::get('id');
        $use_msg = ContactMsg::select("msg","name","email","mobile")->where('contact_msg_id',$id)->get();


        $a = json_decode($use_msg);
        $a_use_msg = $a[0]->msg;
        $a_use_name = $a[0]->name;
        $a_use_email = $a[0]->email;
        $a_use_mobile = $a[0]->mobile;

        return view($this->viewPath.'/update',compact('url_to','method','txt_manage','a_use_msg','a_use_name','a_use_email','a_use_mobile'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $objFn = new MainFunction();

        /* Check Permission */
        $page_id = DB::table('page')->where('page_name','=',$this->titlePage)->first()->page_id;
        $permission = $objFn->permission($page_id,'c');
        if ($permission == '404') { return view('errors/404'); }
        else if ($permission == '403') { return view('errors/403'); }

        $model = $this->model;
        $data = new $model;
        $strParam = $request->strParam;
        $id = Input::get('id');
        $objFn->db_add($data,$this->pkField,$request,$this->fieldList);

        $data->parent_id = $id;
        $data->status = 1;
        $data->save();

        $data2 = $model::find($id);
        $data2->status = 1;
        $data2->save();





        Mail::send('email.contact', ['data' => $data2, 'msg' => $data->msg],  function($message) use ($data2,$data)
        {
            $message->from($data->email , $data->name);
            $message->to( $data2->email , $data2->name)->subject('Welcome!');
        });

        return Redirect::to($this->path);
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {
        $objFn = new MainFunction();

        /* Check Permission */
        $page_id = DB::table('page')->where('page_name','=',$this->titlePage)->first()->page_id;
        $permission = $objFn->permission($page_id,'r');
        if ($permission == '404') { return view('errors/404'); }
        else if ($permission == '403') { return view('errors/403'); }

        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = "Review";
        Session::put('referUrl',URL::previous());

        $model = $this->model;
        $data = $model::find($id);
        $contact_massage = ContactMsg::where('contact_msg_id',$id)->get();
        $reply_massage = ContactMsg::where('parent_id',$id)->get();

        $c = json_decode($contact_massage);
        $c_name = $c[0]->name;
        $c_email = $c[0]->email;
        $c_mobile = $c[0]->mobile;
        $c_msg = $c[0]->msg;

        $r = json_decode($reply_massage);
        $r_name = $r[0]->name;
        $r_email = $r[0]->email;
        $r_mobile = $r[0]->mobile;
        $r_msg = $r[0]->msg;
        $r_time_send = $r[0]->created_at;

//return $c_name.'<br>'.$c_email.'<br>'.$c_msg.'<br>'.$r_name.'<br>'.$r_email.'<br>'.$r_msg.'<br>'.$r_time_send;

        return view($this->viewPath.'/review',compact('data','url_to','method','txt_manage','review_massage','c_name','c_email','c_msg','c_mobile','r_name','r_email','r_msg','r_mobile','r_time_send'));


    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $objFn = new MainFunction();

        /* Check Permission */
        $page_id = DB::table('page')->where('page_name','=',$this->titlePage)->first()->page_id;
        $permission = $objFn->permission($page_id,'u');
        if ($permission == '404') { return view('errors/404'); }
        else if ($permission == '403') { return view('errors/403'); }

        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl',URL::previous());

        $model = $this->model;
        $data = $model::find($id);

        return view($this->viewPath.'/update',compact('data','url_to','method','txt_manage'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id )
    {
        $objFn = new MainFunction();

        /* Check Permission */
        $page_id = DB::table('page')->where('page_name','=',$this->titlePage)->first()->page_id;
        $permission = $objFn->permission($page_id,'u');
        if ($permission == '404') { return view('errors/404'); }
        else if ($permission == '403') { return view('errors/403'); }

        $strParam = $request->strParam;

        $model = $this->model;
        $data = $model::find($id);

        $objFn->db_update($data,$this->pkField,$request,$this->fieldList);



        return Redirect::to($this->path);
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl',URL::previous());
        
        $objFn = new MainFunction();

        /* Check Permission */
        $page_id = DB::table('page')->where('page_name','=',$this->titlePage)->first()->page_id;
        $permission = $objFn->permission($page_id,'d');
        if ($permission == '404') { return view('errors/404'); }
        else if ($permission == '403') { return view('errors/403'); }

        $model = $this->model;

        $model::find($id)->delete();

        return Redirect::to(Session::get('referUrl'));
    }
//---------------------------------------
    /*public function getReply(){

        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";
        $id = Input::get('id');
        $use_msg = ContactMsg::select('msg')->where('contact_msg_id',$id)->first();
        return view($this->viewPath.'/update',compact('url_to','method','txt_manage','use_msg'));
    }*/

}

