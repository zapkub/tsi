<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\News;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class NewsController extends Controller
{
    public $model = 'App\Model\News';
    public $titlePage = 'News';
    public $tbName = 'news';
    public $pkField = 'news_id';
    public $fieldList = array('name_th','name_en','short_descripion_th','short_descripion_en','description_th','description_en','img_path','img_path_en','pdf_path','pdf_path_en','news_date');
    public $a_search = array('name_th','name_en');
    public $path = '_admin/news';
    public $page = 'news';
    public $viewPath = 'backend/news';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {
        $objFn = new MainFunction();

        /* Check Permission */
        $page_id = DB::table('page')->where('page_name','=',$this->titlePage)->first()->page_id;
        $permission = $objFn->permission($page_id,'r');
        if ($permission == '404') { return view('errors/404'); }
        else if ($permission == '403') { return view('errors/403'); }

        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if(empty($orderBy)) $orderBy = $this->pkField;
        $sortBy = Input::get('sortBy');
        if(empty($sortBy)) $sortBy = 'desc';

        $search = Input::get('search');
        $model = $this->model;
        $data = new $model;

        if(!empty($search))
        {
            $data = $data->where(function ($query) use($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field, 'like', '%'.$search.'%');
                }
            });
        }
        $countData = $data->count();
        $data = $data
            ->orderBy($orderBy,$sortBy)
            ->paginate($perPage);
        $data->setPath($this->page);
        $data->lastPage();


        return view($this->viewPath.'/index',compact('data','countData','permission'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $objFn = new MainFunction();

        /* Check Permission */
        $page_id = DB::table('page')->where('page_name','=',$this->titlePage)->first()->page_id;
        $permission = $objFn->permission($page_id,'c');
        if ($permission == '404') { return view('errors/404'); }
        else if ($permission == '403') { return view('errors/403'); }

        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";

        return view($this->viewPath.'/update',compact('url_to','method','txt_manage'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $objFn = new MainFunction();

        /* Check Permission */
        $page_id = DB::table('page')->where('page_name','=',$this->titlePage)->first()->page_id;
        $permission = $objFn->permission($page_id,'c');
        if ($permission == '404') { return view('errors/404'); }
        else if ($permission == '403') { return view('errors/403'); }

        $model = $this->model;
        $data = new $model;
        $id = $objFn->db_add($data,$this->pkField,$request,$this->fieldList);

        if (Input::hasFile('pdf_path')) {
            $photo = $request->file('pdf_path');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $old_name = $data->pdf_path;                             // get old name
            $path = public_path('uploads/news_pdf');

            $extension = $photo->getClientOriginalExtension();      // get extension

            if($extension == 'pdf') {
                $filename = $new_name . "." . $extension;               // set filename
                $destinationPath = $path;

                Input::file('pdf_path')->move($destinationPath,$filename);

                $data = $model::find($id);
                $data->pdf_path = $filename;
                $data->save();
            }
        }

        if (Input::hasFile('pdf_path_en')) {
            $photo = $request->file('pdf_path_en');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $old_name = $data->pdf_path_en;                             // get old name
            $path = public_path('uploads/news_pdf_en');

            $extension = $photo->getClientOriginalExtension();      // get extension

            if($extension == 'pdf') {
                $filename = $new_name . "." . $extension;               // set filename
                $destinationPath = $path;

                Input::file('pdf_path_en')->move($destinationPath,$filename);

                $data = $model::find($id);
                $data->pdf_path_en = $filename;
                $data->save();
            }
        }

        if (Input::hasFile('img_path')) {
            $photo = $request->file('img_path');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $path = public_path('uploads/news');           // set path
            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;               // set filename
            $destinationPath = $path;
            $objFn->img_full_resize($photo, $destinationPath, $filename);   //
            $objFn->image_resize($photo, $destinationPath,100, $filename);   // resize image
            $data = $model::find($id);
            $data->img_path = $filename;
            $data->save();
        }

        if (Input::hasFile('img_path_en')) {
            $photo = $request->file('img_path_en');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $path = public_path('uploads/news_en');           // set path
            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;               // set filename
            $destinationPath = $path;
            $objFn->img_full_resize($photo, $destinationPath, $filename);   //
            $objFn->image_resize($photo, $destinationPath,100, $filename);   // resize image
            $data = $model::find($id);
            $data->img_path_en = $filename;
            $data->save();
        }

        return Redirect::to($this->path);
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {

    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $objFn = new MainFunction();

        /* Check Permission */
        $page_id = DB::table('page')->where('page_name','=',$this->titlePage)->first()->page_id;
        $permission = $objFn->permission($page_id,'u');
        if ($permission == '404') { return view('errors/404'); }
        else if ($permission == '403') { return view('errors/403'); }

        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl',URL::previous());

        $model = $this->model;
        $data = $model::find($id);
        return view($this->viewPath.'/update',compact('data','url_to','method','txt_manage'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id )
    {
        $objFn = new MainFunction();

        /* Check Permission */
        $page_id = DB::table('page')->where('page_name','=',$this->titlePage)->first()->page_id;
        $permission = $objFn->permission($page_id,'u');
        if ($permission == '404') { return view('errors/404'); }
        else if ($permission == '403') { return view('errors/403'); }

        $strParam = $request->strParam;
        $model = $this->model;
        $data = $model::find($id);

        $objFn->db_update($data,$this->pkField,$request,$this->fieldList);

        if (Input::hasFile('pdf_path')) {
            $photo = $request->file('pdf_path');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $old_name = $data->pdf_path;                             // get old name
            $path = public_path('uploads/news_pdf');

            $extension = $photo->getClientOriginalExtension();      // get extension

            if($extension == 'pdf') {
                $filename = $new_name . "." . $extension;               // set filename
                $destinationPath = $path;

                Input::file('pdf_path')->move($destinationPath,$filename);

                $data = $model::find($id);
                $data->pdf_path = $filename;
                $data->save();
            }
        }

        if (Input::hasFile('pdf_path_en')) {
            $photo = $request->file('pdf_path_en');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $old_name = $data->pdf_path_en;                             // get old name
            $path = public_path('uploads/news_pdf_en');

            $extension = $photo->getClientOriginalExtension();      // get extension

            if($extension == 'pdf') {
                $filename = $new_name . "." . $extension;               // set filename
                $destinationPath = $path;

                Input::file('pdf_path_en')->move($destinationPath,$filename);

                $data = $model::find($id);
                $data->pdf_path_en = $filename;
                $data->save();
            }
        }

        if (Input::hasFile('img_path')) {
            $photo = $request->file('img_path');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $old_name = $data->img_path;                            // get old name
            $path = public_path('uploads/news');           // set path
            $objFn->del_storage($path,$old_name);                   // delete old picture in storage
            $objFn->del_storage($path.'/100',$old_name);                   // delete old picture in storage
            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;               // set filename
            $destinationPath = $path;
            $objFn->img_full_resize($photo, $destinationPath, $filename);   // resize image
            $objFn->image_resize($photo, $destinationPath,100, $filename);   // resize image
            $data = $model::find($id);
            $data->img_path = $filename;
            $data->save();
        }

        if (Input::hasFile('img_path_en')) {
            $photo = $request->file('img_path_en');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $old_name = $data->img_path_en;                            // get old name
            $path = public_path('uploads/news_en');           // set path
            $objFn->del_storage($path,$old_name);                   // delete old picture in storage
            $objFn->del_storage($path.'/100',$old_name);                   // delete old picture in storage
            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;               // set filename
            $destinationPath = $path;
            $objFn->img_full_resize($photo, $destinationPath, $filename);   // resize image
            $objFn->image_resize($photo, $destinationPath,100, $filename);   // resize image
            $data = $model::find($id);
            $data->img_path_en = $filename;
            $data->save();
        }

        return Redirect::to($this->path.'?'.$strParam);
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl',URL::previous());

        $objFn = new MainFunction();

        /* Check Permission */
        $page_id = DB::table('page')->where('page_name','=',$this->titlePage)->first()->page_id;
        $permission = $objFn->permission($page_id,'d');
        if ($permission == '404') { return view('errors/404'); }
        else if ($permission == '403') { return view('errors/403'); }

        $model = $this->model;
        $model::find($id)->delete();

        return Redirect::to(Session::get('referUrl'));
    }
}

