<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\Category;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class CategoryController extends Controller
{
    public $model = 'App\Model\Category';
    public $titlePage = 'Category';
    public $tbName = 'category';
    public $pkField = 'category_id';
    public $fieldList = array('category_name_th','category_name_en','parent_category_id','icon_name','img_name','s_img_name','description_th','description_en');
    public $a_search = array('category_name_th','category_name_en');
    public $path = '_admin/category';
    public $page = 'category';
    public $viewPath = 'backend/category';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {
        $objFn = new MainFunction();

        /* Check Permission */
        $page_id = DB::table('page')->where('page_name','=',$this->titlePage)->first()->page_id;
        $permission = $objFn->permission($page_id,'r');
        if ($permission == '404') { return view('errors/404'); }
        else if ($permission == '403') { return view('errors/403'); }

        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if(empty($orderBy)) $orderBy = $this->pkField;
        $sortBy = Input::get('sortBy');
        if(empty($sortBy)) $sortBy = 'desc';

        $search = Input::get('search');
        $model = $this->model;
        $data = new $model;

        if(!empty($search))
        {
            $data = $data->where(function ($query) use($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field, 'like', '%'.$search.'%');
                }
            });
        }

        //return $objFn->loop_category2($data->get());
        $countData = $data->count();
        $data = $data
            ->orderBy($orderBy,$sortBy)
            ->paginate($perPage);
        $data->setPath($this->page);
        $data->lastPage();


        return view($this->viewPath.'/index',compact('data','countData','permission'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $objFn = new MainFunction();

        /* Check Permission */
        $page_id = DB::table('page')->where('page_name','=',$this->titlePage)->first()->page_id;
        $permission = $objFn->permission($page_id,'c');
        if ($permission == '404') { return view('errors/404'); }
        else if ($permission == '403') { return view('errors/403'); }

        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";
        $model = $this->model;

        $category = $model::get();
//        $category_id = $category[0]->category_id;

        $category_id = Input::get('category_id');
        return view($this->viewPath.'/update',compact('url_to','method','txt_manage','category','category_id'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $objFn = new MainFunction();

        /* Check Permission */
        $page_id = DB::table('page')->where('page_name','=',$this->titlePage)->first()->page_id;
        $permission = $objFn->permission($page_id,'c');
        if ($permission == '404') { return view('errors/404'); }
        else if ($permission == '403') { return view('errors/403'); }

        $model = $this->model;
        $data = new $model;
        $id = $objFn->db_add($data,$this->pkField,$request,$this->fieldList);



        if (Input::hasFile('img_name')) {
            $photo = $request->file('img_name');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $path = public_path('uploads/category');           // set path
            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;               // set filename
            $destinationPath = $path;
            $objFn->img_full_resize($photo, $destinationPath, $filename);   //
            $objFn->image_resize($photo, $destinationPath,100, $filename);   // resize image
            $data = $model::find($id);
            $data->img_name = $filename;
            $data->save();
        }

        if (Input::hasFile('s_img_name')) {
            $photo = $request->file('s_img_name');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $path = public_path('uploads/category_s');           // set path
            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;               // set filename
            $destinationPath = $path;
            $objFn->img_full_resize($photo, $destinationPath, $filename);   //
            $objFn->image_resize($photo, $destinationPath,100, $filename);   // resize image
            $data = $model::find($id);
            $data->s_img_name = $filename;
            $data->save();
        }

        return Redirect::to($this->path);
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {

    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $objFn = new MainFunction();

        /* Check Permission */
        $page_id = DB::table('page')->where('page_name','=',$this->titlePage)->first()->page_id;
        $permission = $objFn->permission($page_id,'u');
        if ($permission == '404') { return view('errors/404'); }
        else if ($permission == '403') { return view('errors/403'); }

        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl',URL::previous());

        $model = $this->model;
        $data = $model::find($id);
        $category_id = $data->parent_category_id;

        $category = $model::all();

        return view($this->viewPath.'/update',compact('data','url_to','method','txt_manage','category','category_id'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id )
    {
        $objFn = new MainFunction();

        /* Check Permission */
        $page_id = DB::table('page')->where('page_name','=',$this->titlePage)->first()->page_id;
        $permission = $objFn->permission($page_id,'u');
        if ($permission == '404') { return view('errors/404'); }
        else if ($permission == '403') { return view('errors/403'); }

        $strParam = $request->strParam;

        $model = $this->model;
        $data = $model::find($id);
        $objFn->db_update($data,$this->pkField,$request,$this->fieldList);

        if (Input::hasFile('img_name')) {
            $photo = $request->file('img_name');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $old_name = $data->img_name;                            // get old name
            $path = public_path('uploads/category');           // set path
            $objFn->del_storage($path,$old_name);                   // delete old picture in storage
            $objFn->del_storage($path.'/100',$old_name);                   // delete old picture in storage
            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;               // set filename
            $destinationPath = $path;
            $objFn->img_full_resize($photo, $destinationPath, $filename);   // resize image
            $objFn->image_resize($photo, $destinationPath,100, $filename);   // resize image
            $data = $model::find($id);
            $data->img_name = $filename;
            $data->save();
        }

        if (Input::hasFile('s_img_name')) {
            $photo = $request->file('s_img_name');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $old_name = $data->s_img_name;                            // get old name
            $path = public_path('uploads/category_s');           // set path
            $objFn->del_storage($path,$old_name);                   // delete old picture in storage
            $objFn->del_storage($path.'/100',$old_name);                   // delete old picture in storage
            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;               // set filename
            $destinationPath = $path;
            $objFn->img_full_resize($photo, $destinationPath, $filename);   // resize image
            $objFn->image_resize($photo, $destinationPath,100, $filename);   // resize image
            $data = $model::find($id);
            $data->s_img_name = $filename;
            $data->save();
        }

        return Redirect::to($this->path.'?'.$strParam);
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl',URL::previous());
        $objFn = new MainFunction();

        /* Check Permission */
        $page_id = DB::table('page')->where('page_name','=',$this->titlePage)->first()->page_id;
        $permission = $objFn->permission($page_id,'d');
        if ($permission == '404') { return view('errors/404'); }
        else if ($permission == '403') { return view('errors/403'); }

        $model = $this->model;

        $data = $model::select("img_name","s_img_name")
            ->where("category_id",$id)
            ->first();

        $path = public_path('uploads/category/');           //// set path
        $path_100 = public_path('uploads/category/100/');           //// set path

        $path_s = public_path('uploads/category_s/');
        $path_s_100 = public_path('uploads/category_s/100/');

        $file = "$data->img_name";
        $file_s = "$data->s_img_name";
//        if($file != '') {
//            unlink($path.$file);                    // delete image in public path
//            unlink($path_100.$file);                  // delete resize image
//        }
//
//        if($file_s != '') {
//            unlink($path_s.$file);                    // delete image in public path
//            unlink($path_s_100.$file_s);                  // delete resize image
//        }

        $model::find($id)->delete();

        return Redirect::to(Session::get('referUrl'));
    }


}

