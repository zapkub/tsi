<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\Users;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Admin;
use App\Model\Agent;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Auth;
use Validator;
use Cookie;

class AuthController extends Controller
{

    protected $redirectPath = '_admin/';
    protected $loginPath = '_admin/login';

    public function __construct()
    {

    }
    public function getLogin()
    {
        if(!auth()->guard('admin')->check()) {
            return view('backend.login');
        }else{
            return redirect()->to('_admin');
        }
    }

    public function getLoginAgent()
    {
        if(!auth()->guard('agent')->check()) {
            return view('agent.login');
        }else{
            return redirect()->to('_agent');
        }
    }

    public function postLogin(Request $request)
    {
        $username = $request->username;
        $password = $request->password;
        $remember = $request->remember;

        // Rules Validate Login
        $validator = Validator::make($request->all(),
            [
                'username' => 'required',
                'password' => 'required'
            ],
            [
                'username.required' => 'กรุณากรอกบัญชีผู้ใช้งาน',
                'password.required' => 'กรุณากรอกรหัสผ่าน'
            ]
        );

        // Check Validate
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }else{
            $auth = auth()->guard('admin');
            if ($auth->attempt(['username' => $username, 'password' => $password],$remember)) {
                Cookie::queue('c_admin_id',$auth->user()->admin_id,525600);

                return redirect()->to('_admin/');
            }else{
                return redirect()->back()->with('errorMsg','ชื่อผู้ใช้งานไม่ถูกต้อง');
            }
        }
    }
    public function postLoginAgent(Request $request)
    {
        $username = $request->username;
        $password = $request->password;
        $remember = $request->remember;

        // Rules Validate Login
        $validator = Validator::make($request->all(),
            [
                'username' => 'required',
                'password' => 'required'
            ],
            [
                'username.required' => 'กรุณากรอกบัญชีผู้ใช้งาน',
                'password.required' => 'กรุณากรอกรหัสผ่าน'
            ]
        );

        // Check Validate
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }else{
            $auth = auth()->guard('agent');
            if ($auth->attempt(['username' => $username, 'password' => $password],$remember)) {
                Cookie::queue('c_agent_id',$auth->user()->agent_id,525600);

                return redirect()->to('_agent/');
            }else{
                return redirect()->back()->with('errorMsg','ชื่อผู้ใช้งานไม่ถูกต้อง');
            }
        }
    }

    public function getLogout(){
        Session::flush();
        Auth::logout();

        return Redirect::to('/_admin/login');
    }
    public function getLogoutAgent(){
        Session::flush();
        Auth::logout();

        return Redirect::to('/_agent/login');
    }
}

