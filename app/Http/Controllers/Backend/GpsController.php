<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\Contact;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Gps;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;


class GpsController extends Controller
{
//    public function __construct()
//    {
//        $this->middleware('admin');
//    }

    // ----------------------------------------- Show All List Page
    public function getIndex()
    {

        return view('backend.gps.index');
    }
    public function postSave(Request $request){
        $data = new Gps();
        $data->gps = $request->gps;
        $data->save();

        return redirect()->to('_admin/gps');
    }

}

