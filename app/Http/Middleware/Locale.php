<?php 

namespace App\Http\Middleware;

use Closure;
use App;
use Config;

class Locale {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {

       

        $lang = session()->get('locale') ? session()->get('locale') : Config::get('app.locale');
        session()->put('locale',$lang);  
        App::setLocale($lang);
        return $next($request);
    }
}