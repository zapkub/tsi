<?php

namespace App\Http\Middleware;

use Closure;
use Cookie;

class StaffMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $c_staff_id = Cookie::get('c_staff_id');

        if(!auth()->guard('staff')->check() && empty($c_staff_id)) {
            return redirect()->to('_staff/login');
        }elseif(auth()->guard('staff')->check()){
            return $next($request);
        }
        if(!empty($c_staff_id)){
            $auth = auth()->guard('staff');
            if ($auth->attempt(['staff_id' => $c_staff_id])) {
                return $next($request);
            }
        }

    }
}