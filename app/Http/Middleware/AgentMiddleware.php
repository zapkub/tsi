<?php

namespace App\Http\Middleware;

use Closure;
use Cookie;

class AgentMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $c_agent_id = Cookie::get('c_agent_id');

        if(!auth()->guard('agent')->check() && empty($c_agent_id)) {
            return redirect()->to('_agent/login');
        }elseif(auth()->guard('agent')->check()){
            return $next($request);
        }
        if(!empty($c_agent_id)){
            $auth = auth()->guard('agent');
            if ($auth->attempt(['agent_id' => $c_agent_id])) {
                return $next($request);
            }
        }

    }
}