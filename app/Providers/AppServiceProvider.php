<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Model\Category;
use App\Model\BannerClaim;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $categories = Category::where('parent_category_id', 0)->take(6)->get();
        $bannerClaim = BannerClaim::get();

        view()->share(['global_categories'=> $categories, 'bannerClaim' => $bannerClaim]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
