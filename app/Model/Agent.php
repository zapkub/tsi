<?php

namespace App\Model;

//use Kbwebs\MultiAuth\PasswordResets\CanResetPassword;
//use Kbwebs\MultiAuth\PasswordResets\Contracts\CanResetPassword as CanResetPasswordContract;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Agent extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'agent';
    protected $primaryKey = 'agent_id';
    public $timestamps = true;

    protected $fillable = [
        'name', 'email', 'password','username'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
