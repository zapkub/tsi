<?php

namespace App\Model;

//use Kbwebs\MultiAuth\PasswordResets\CanResetPassword;
//use Kbwebs\MultiAuth\PasswordResets\Contracts\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

// class Admin extends Model
// {
//     use SoftDeletes;
//     protected $table = 'admin';
//     protected $primaryKey = 'admin_id';
//     public $timestamps = true;
//     protected $dates = ['deleted_at'];

//     public function UserTitle(){
//             return $this->belongsTo('App\Model\UserTitle','user_title_id');
//     }

// }


class Admin extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'admin';
    protected $primaryKey = 'admin_id';
    public $timestamps = true;
    protected $fillable = [
        'name', 'email', 'password','username'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ]; 
    

   
} 

?>