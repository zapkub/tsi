<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Manufacturer extends Model
{
    use SoftDeletes;
    protected $table = 'manufacturer';
    protected $primaryKey = 'manufacturer_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function Product(){
        return $this->hasMany('App\Model\Product','manufacturer_id');
    }

}

?>