<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StaticPage extends Model
{
    use SoftDeletes;
    protected $table = 'static_page';
    protected $primaryKey = 'static_page_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}

?>