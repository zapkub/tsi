<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Csr extends Model
{
    use SoftDeletes;
    protected $table = 'csr';
    protected $primaryKey = 'csr_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function CsrImg()
    {
        return $this->hasMany('App\Model\CsrImg','csr_id');
    }


    public function img_first($csr_id){
        $data =  CsrImg::where('csr_id',$csr_id)->orderby('sequence','asc')->first();
        if(count($data)==1){
            $data = $data->img_path;
        }else{
            $data = "";
        }
        return $data;

    }




}

?>