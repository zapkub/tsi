<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gtype extends Model
{
    use SoftDeletes;
    protected $table = 'gtype';
    protected $primaryKey = 'gtype_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>