<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class History extends Model
{
    use SoftDeletes;
    protected $table = 'history';
    protected $primaryKey = 'history_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>