<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Staff extends Model
{
    use SoftDeletes;
    protected $table = 'staff';
    protected $primaryKey = 'staff_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>