<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BannerFooter extends Model
{
    use SoftDeletes;
    protected $table = 'banner_footer';
    protected $primaryKey = 'banner_footer_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}

?>