<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Document extends Model
{
    use SoftDeletes;
    protected $table = 'document';
    protected $primaryKey = 'document_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>