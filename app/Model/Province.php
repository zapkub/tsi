<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Province extends Model
{
    use SoftDeletes;
    protected $table = 'a_province';
    protected $primaryKey = 'province_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>
