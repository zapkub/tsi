<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GarageType extends Model
{
    use SoftDeletes;
    protected $table = 'garage_type';
    protected $primaryKey = 'garage_type_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>