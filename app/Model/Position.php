<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Position extends Model
{
    use SoftDeletes;
    protected $table = 'position';
    protected $primaryKey = 'position_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>