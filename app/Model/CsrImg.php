<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CsrImg extends Model
{
    use SoftDeletes;
    protected $table = 'csr_img';
    protected $primaryKey = 'csr_img_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
?>