<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Region extends Model
{
    use SoftDeletes;
    protected $table = 'region';
    protected $primaryKey = 'region_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    
}

?>
