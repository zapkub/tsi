<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CarInsurance extends Model
{
    use SoftDeletes;
    protected $table = 'car_insurance';
    protected $primaryKey = 'car_insurance_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function Service()
    {
        return $this->hasMany('App\Model\Service','service_id');
    }

    public function Branch(){
        return $this->belongsTo('App\Model\Branch' ,'branch_id');
    }

}

?>