<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Discount extends Model
{
    use SoftDeletes;
    protected $table = 'discount';
    protected $primaryKey = 'discount_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];


    public function Product(){
        return $this->belongsTo('App\Model\Product' ,'product_id');
    }


}

?>