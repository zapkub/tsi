<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CarInsuranceCase extends Model
{
    use SoftDeletes;
    protected $table = 'car_insurance_case';
    protected $primaryKey = 'car_insurance_case_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}

?>