<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Geography extends Model
{
    use SoftDeletes;
    protected $table = 'a_geography';
    protected $primaryKey = 'geo_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}

?>