<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContactDepartment extends Model
{
    use SoftDeletes;
    protected $table = 'contact_department';
    protected $primaryKey = 'contact_department_id';
    public $fillable = ['department_name','detail','sequence'];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>