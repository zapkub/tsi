<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notify extends Model
{
    use SoftDeletes;
    protected $table = 'notify';
    protected $primaryKey = 'notify_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>
