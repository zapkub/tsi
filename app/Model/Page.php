<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Model
{
    use SoftDeletes;
    protected $table = 'page';
    protected $primaryKey = 'page_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}

?>
