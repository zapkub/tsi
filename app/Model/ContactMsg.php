<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContactMsg extends Model
{
    use SoftDeletes;
    protected $table = 'contact_msg';
    protected $primaryKey = 'contact_msg_id';
    protected $fillable = ['subject','name','email','mobile','insurance_no','msg'];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function ReplyTime($id)
    {
//return $id;


        $reply_time = $this->select("created_at")->where("parent_id",$id)->first();
        $a_reply_time = $reply_time->created_at;
        return $a_reply_time;
//        return $reply_time;
    }



}


?>