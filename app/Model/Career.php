<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Career extends Model
{
    use SoftDeletes;
    protected $table = 'career';
    protected $primaryKey = 'career_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}

?>