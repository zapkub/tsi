<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AboutUs extends Model
{
    use SoftDeletes;
    protected $table = 'about_us';
    protected $primaryKey = 'about_us_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>