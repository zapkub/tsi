<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Title extends Model
{
    use SoftDeletes;
    protected $table = 'title';
    protected $primaryKey = 'title_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>