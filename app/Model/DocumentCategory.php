<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DocumentCategory extends Model
{
    use SoftDeletes;
    protected $table = 'document_category';
    protected $primaryKey = 'document_category_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>