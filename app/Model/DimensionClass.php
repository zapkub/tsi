<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DimensionClass extends Model
{
    use SoftDeletes;
    protected $table = 'dimension_class';
    protected $primaryKey = 'dimension_class_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function Product(){
        return $this->hasMany('App\Model\Product','dimension_class_id');
    }

}

?>