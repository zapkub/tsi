<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gps extends Model
{
    use SoftDeletes;
    protected $table = 'gps';
    protected $primaryKey = 'gps_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>