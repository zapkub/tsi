<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserTitle extends Model
{
    use SoftDeletes;
    protected $table = 'user_title';
    protected $primaryKey = 'user_title_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>