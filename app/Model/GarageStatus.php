<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GarageStatus extends Model
{
    use SoftDeletes;
    protected $table = 'garage_status';
    protected $primaryKey = 'garage_status_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>