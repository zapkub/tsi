<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class ProductData extends Model
{
    use SoftDeletes;
    protected $table = 'product_data';
    protected $primaryKey = 'product_data_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>
