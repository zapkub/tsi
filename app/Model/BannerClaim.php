<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BannerClaim extends Model
{
    use SoftDeletes;
    protected $table = 'banner_claim';
    protected $primaryKey = 'banner_claim_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}

?>
