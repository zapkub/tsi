<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IntroPage extends Model
{
    use SoftDeletes;
    protected $table = 'intro_page';
    protected $primaryKey = 'intro_page_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>