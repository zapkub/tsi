<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FormInsurance extends Model
{
    use SoftDeletes;
    protected $table = 'form_insurance';
    protected $primaryKey = 'form_insurance_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function Branch(){
        return $this->belongsTo('App\Model\Branch' ,'branch_id');
    }
}

?>