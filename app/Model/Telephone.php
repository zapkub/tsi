<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Telephone extends Model
{
    use SoftDeletes;
    protected $table = 'telephone';
    protected $primaryKey = 'telephone_id';
    protected $fillable = ['name','floor','department','telephone','fax'];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}


?>
