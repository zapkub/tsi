<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Amg extends Model
{
    use SoftDeletes;
    protected $table = 'amg';
    protected $primaryKey = 'amg_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>