<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class District extends Model
{
    use SoftDeletes;
    protected $table = 'district';
    protected $primaryKey = 'district_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>
