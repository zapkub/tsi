<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Surveyor extends Model
{
    use SoftDeletes;
    protected $table = 'surveyor';
    protected $primaryKey = 'surveyor_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function Region(){
        return $this->belongsTo('App\Model\Region','region_id');
    }

    public function Province(){
        return $this->belongsTo('App\Model\Province','province_id');
    }

    public function District(){
        return $this->belongsTo('App\Model\District','district_id');
    }
}

?>