<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class YearBook extends Model
{
    use SoftDeletes;
    protected $table = 'yearbook';
    protected $primaryKey = 'yearbook_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>