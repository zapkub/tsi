<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Branch extends Model
{
    use SoftDeletes;
    protected $table = 'branch';
    protected $primaryKey = 'branch_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
 
?>