<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attribute extends Model
{
    use SoftDeletes;
    protected $table = 'attribute';
    protected $primaryKey = 'attribute_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];


}

?>