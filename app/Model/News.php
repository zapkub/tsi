<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
    use SoftDeletes;
    protected $table = 'news';
    protected $primaryKey = 'news_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}

?>