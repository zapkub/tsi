<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductTag extends Model
{
    use SoftDeletes;
    protected $table = 'product_tag';
    protected $primaryKey = 'product_tag_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];


    public function Product(){
        return $this->belongsTo('App\Model\Product' ,'product_id');
    }

    public function Tag(){
        return $this->belongsTo('App\Model\Tag' ,'tag_id');
    }


}


?>