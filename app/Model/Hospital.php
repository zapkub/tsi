<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hospital extends Model
{
    use SoftDeletes;
    protected $table = 'hospital';
    protected $primaryKey = 'hospital_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>