<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExecutiveCommittee extends Model
{
    use SoftDeletes;
    protected $table = 'executive_committee';
    protected $primaryKey = 'executive_committee_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function Position(){
        return $this->hasMany('App\Model\Position','position_id');
    }
 
}

?>