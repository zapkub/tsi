<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Compensation extends Model
{
    use SoftDeletes;
    protected $table = 'compensation';
    protected $primaryKey = 'compensation_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}

?>
