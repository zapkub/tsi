<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Garage extends Model
{
    use SoftDeletes;
    protected $table = 'garage';
    protected $primaryKey = 'garage_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function Title(){
            return $this->belongsTo('App\Model\Title','title_id');
    }

    public function Gtype(){
        return $this->belongsTo('App\Model\Gtype','gtype_id');
    }

    public function GarageType(){
        return $this->belongsTo('App\Model\GarageType','garage_type_id');
    }

    public function Region(){
        return $this->belongsTo('App\Model\Region','region_id');
    }

    public function Province(){
        return $this->belongsTo('App\Model\Province','province_id');
    }

    public function District(){
        return $this->belongsTo('App\Model\District','district_id');
    }



}

?>