<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brand extends Model
{
    use SoftDeletes;
    protected $table = 'brand';
    protected $primaryKey = 'brand_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>