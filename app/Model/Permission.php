<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permission extends Model
{
    use SoftDeletes;
    protected $table = 'permission';
    protected $primaryKey = 'permission_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}

?>
