<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompensationDetail extends Model
{
    use SoftDeletes;
    protected $table = 'compensation_detail';
    protected $primaryKey = 'compensation_detail_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}

?>
