<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VisionAndMission extends Model
{
    use SoftDeletes;
    protected $table = 'vision_and_mission';
    protected $primaryKey = 'vision_and_mission_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>