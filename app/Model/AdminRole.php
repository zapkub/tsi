<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdminRole extends Model
{
    use SoftDeletes;
    protected $table = 'admin_role';
    protected $primaryKey = 'admin_role_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}

?>
