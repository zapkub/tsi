var elixir = require('laravel-elixir');
var livereload = require('gulp-livereload');
var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var babelify = require("babelify");
var source = require('vinyl-source-stream');
const mocha = require('gulp-mocha');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
 gulp.task('test', () => {
    return gulp.src('tests/service-finder.spec.js', {read: false})
        // gulp-mocha needs filepaths so you can't have any plugins before it
        .pipe(mocha({reporter: 'nyan'}));
});

// Add support JSX Browserify
elixir.extend('reactifyBrowserifyElixir', function(inputFile, inputFileName, outputDirectory) {

  gulp.task('browserify_and_reactify', function() {
    var b = browserify();
    b.transform(babelify, {
      presets: ["es2015", "react"]
    });
    b.add('resources/assets/js/' + inputFile);
    return b.bundle()
      .pipe(source(inputFileName))
      .pipe(gulp.dest(outputDirectory));
  });
  return gulp.start('browserify_and_reactify');
});
// Add seperate sass compiler for each page extra styles
gulp.task('multiple-sass', function() {


  return gulp.src('resources/assets/sass/front/*.scss')
    .pipe(sass())
    .pipe(autoprefixer())
    .pipe(gulp.dest('public/css/front'));
});

//main elxir task
elixir(function(mix) {

  elixir.config.js.browserify.transformers.push({
    name: 'aliasify',
    options: {}
  });


  mix.task('multiple-sass', 'resources/assets/sass/front/*.scss');
  mix.sass('resources/assets/sass/front/global/*.scss', 'public/css/front/app.css')
    .browserify('front/main.js', 'public/js/main.js')
    .browserify('front/service.components.jsx','public/js/front/service.components.js')
    .browserSync({
      proxy: 'http://localhost/tsi/public' // edit proxy server for development here
    });
});
