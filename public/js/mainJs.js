$(document).ready(function(){
    $('.icon-menu-xs').on('click',function(){
       $('.toggle-menu').slideToggle();
    });
    $('.toggle-icon').on('click',function(){
        var dataId = $(this).attr('data-id');
        var slide = $('#'+dataId);

        $('.toggle-icon').css({
            "-ms-transform" : 'rotate(0deg)', /* IE 9 */
            "-webkit-transform": 'rotate(0deg)', /* Chrome, Safari, Opera */
            "transform" : 'rotate(0deg)',
        });
        $('.toggle-submenu').slideUp();

        if (slide.is(':visible')) {
            $(this).css({
                "-ms-transform" : 'rotate(0deg)', /* IE 9 */
                "-webkit-transform": 'rotate(0deg)', /* Chrome, Safari, Opera */
                "transform" : 'rotate(0deg)',
            });
            slide.slideUp();
        } else {
            $(this).css({
                "-ms-transform" : 'rotate(90deg)', /* IE 9 */
                "-webkit-transform": 'rotate(90deg)', /* Chrome, Safari, Opera */
                "transform" : 'rotate(90deg)',
            });
            slide.slideDown();
        }
    });
});