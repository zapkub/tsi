

$("#bt_preview").click(function(){
    window.open('', 'formpopup', 'width=1024,height=700,resizeable,scrollbars');
    $("#frm_data").attr("action", 'page.php');
    $("#frm_data").attr("target", 'formpopup');
    $('#frm_data').submit();
});

<!-- Chk Upload -->
$(document).ready(function(){
    $("#remove_img").click(function(){
        var r = confirm('Are you sure you want to delete');
        if(r == true){
            $("#img_path2").removeAttr('src');
            $("#upload").show();
            $("#remove_img").hide();
            $("#img_path,#img_type").val('');
        }else{
            return false;
        }

    });
});

$(function () {
    $("#upload").on("click",function(e){
        var objFile= $("<input>",{
            "class":"file_upload",
            "type":"file",
            "multiple":"true",
            "name":"img_name",
            "style":"display:none",
            change: function(e){
                var files = this.files
                showThumbnail(files)
                $("#upload").hide();
                $("#remove_img").show();
                $("#img_name").val('');
            }
        });
        $(this).before(objFile);
        $(".file_upload:last").show().click().hide();
        e.preventDefault();
    });

    function showThumbnail(files){

        //    $("#thumbnail").html("");
        for(var i=0;i<files.length;i++){
            var file = files[i]
            var imageType = /image.*/
            if(!file.type.match(imageType)){
                var i = confirm("สกุลไฟล์ไม่ถูกต้อง");
                if(i==true || i==false){
                    exit();
                }
                continue;
            }


            //var image = document.createElement("img");
            var image = document.getElementById("img_path2");
            var thumbnail = document.getElementById("thumbnail");
            image.file = file;
            thumbnail.appendChild(image)

            var reader = new FileReader()
            reader.onload = (function(aImg){
                return function(e){
                    aImg.src = e.target.result;
                };
            }(image))

            var ret = reader.readAsDataURL(file);
            var canvas = document.createElement("canvas");
            ctx = canvas.getContext("2d");
            image.onload= function(){
                ctx.drawImage(image,100,100)
            }
        } // end for loop

    } // end showThumbnail
});
<!-- End Upload -->



<!-- Chk Upload -->
$(document).ready(function(){
    $("#remove_img1").click(function(){
        var r = confirm('Are you sure you want to delete');
        if(r == true){
            $("#img_path4").removeAttr('src');
            $("#upload1").show();
            $("#remove_img1").hide();
            $("#img_path1,#img_type1").val('');
        }else{
            return false;
        }

    });
});

$(function () {
    $("#upload1").on("click",function(e){
        var objFile= $("<input>",{
            "class":"file_upload1",
            "type":"file",
            "multiple":"true",
            "name":"s_img_name",
            "style":"display:none",
            change: function(e){
                var files = this.files
                showThumbnail1(files)
                $("#upload1").hide();
                $("#remove_img1").show();
                $("#s_img_name").val('');
            }
        });
        $(this).before(objFile);
        $(".file_upload1:last").show().click().hide();
        e.preventDefault();
    });

    function showThumbnail1(files){

        //    $("#thumbnail").html("");
        for(var i=0;i<files.length;i++){
            var file = files[i]
            var imageType = /image.*/
            if(!file.type.match(imageType)){
                var i = confirm("สกุลไฟล์ไม่ถูกต้อง");
                if(i==true || i==false){
                    exit();
                }
                continue;
            }


            //var image = document.createElement("img");
            var image = document.getElementById("img_path4");
            var thumbnail = document.getElementById("thumbnail1");
            image.file = file;
            thumbnail.appendChild(image)

            var reader = new FileReader()
            reader.onload = (function(aImg){
                return function(e){
                    aImg.src = e.target.result;
                };
            }(image))

            var ret = reader.readAsDataURL(file);
            var canvas = document.createElement("canvas");
            ctx = canvas.getContext("2d");
            image.onload= function(){
                ctx.drawImage(image,100,100)
            }
        } // end for loop

    } // end showThumbnail
});
<!-- End Upload -->