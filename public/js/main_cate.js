$(document).ready(function($){
    $('#main_cate_change').on('change', function() {

        var type = $(this).data('type');
        var id = this.value;
        console.log(type);

        $.ajax({
            type : "GET",
            url : '/article-category/'+type+'/'+id,
            success: function(data){
                $('#cate_change').empty().append(data);

            }
        });
         // or $(this).val()
    });
});