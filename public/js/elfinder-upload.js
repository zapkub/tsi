

$("#bt_preview").click(function(){
    window.open('', 'formpopup', 'width=1024,height=700,resizeable,scrollbars');
    $("#frm_data").attr("action", 'page.php');
    $("#frm_data").attr("target", 'formpopup');
    $('#frm_data').submit();
});

<!-- Chk Upload -->
$(document).ready(function(){
    $("#remove_img").click(function(){
        var r = confirm('Are you sure you want to delete');
        if(r == true){
            $("#img_path2").removeAttr('src');
            $("#upload").show();
            $("#remove_img").hide();
            $("#img_path,#img_type").val('');
        }else{
            return false;
        }

    });
});

$(function () {
    $("#upload").on("click",function(e){
        var objFile= $("<input>",{
            "class":"file_upload",
            "type":"file",
            "multiple":"true",
            "name":"img_path",
            "style":"display:none",
            change: function(e){
                var files = this.files
                showThumbnail(files)
                $("#upload").hide();
                $("#remove_img").show();
                $("#img_type").val(1);
                $("#img_path").val('');
            }
        });
        $(this).before(objFile);
        $(".file_upload:last").show().click().hide();
        e.preventDefault();
    });

    function showThumbnail(files){

        //    $("#thumbnail").html("");
        for(var i=0;i<files.length;i++){
            var file = files[i]
            var imageType = /image.*/
            if(!file.type.match(imageType)){
                var i = confirm("สกุลไฟล์ไม่ถูกต้อง");
                if(i==true || i==false){
                    exit();
                }
                continue;
            }


            //var image = document.createElement("img");
            var image = document.getElementById("img_path2");
            var thumbnail = document.getElementById("thumbnail");
            image.file = file;
            thumbnail.appendChild(image)

            var reader = new FileReader()
            reader.onload = (function(aImg){
                return function(e){
                    aImg.src = e.target.result;
                };
            }(image))

            var ret = reader.readAsDataURL(file);
            var canvas = document.createElement("canvas");
            ctx = canvas.getContext("2d");
            image.onload= function(){
                ctx.drawImage(image,100,100)
            }
        } // end for loop

    } // end showThumbnail
});
<!-- End Upload -->
<!-- Start Upload 2 -->
$(function () {
    $("#upload2").on("click",function(e){
        var objFile= $("<input>",{
            "class":"file_upload",
            "type":"file",
            "multiple":"true",
            "name":"img_path_1",
            "style":"display:none",
            change: function(e){
                var files = this.files
                showThumbnail2(files)
                $("#upload2").hide();
                $("#remove_img2").show();
                $("#img_type").val(1);
                $("#img_path2").val('');
            }
        });
        $(this).before(objFile);
        $(".file_upload:last").show().click().hide();
        e.preventDefault();
    });

    function showThumbnail2(files){

        //    $("#thumbnail").html("");
        for(var i=0;i<files.length;i++){
            var file = files[i]
            var imageType = /image.*/
            if(!file.type.match(imageType)){
                var i = confirm("สกุลไฟล์ไม่ถูกต้อง");
                if(i==true || i==false){
                    exit();
                }
                continue;
            }


            //var image = document.createElement("img");
            var image = document.getElementById("img_path4");
            var thumbnail = document.getElementById("thumbnail2");
            image.file = file;
            thumbnail.appendChild(image)

            var reader = new FileReader()
            reader.onload = (function(aImg){
                return function(e){
                    aImg.src = e.target.result;
                };
            }(image))

            var ret = reader.readAsDataURL(file);
            var canvas = document.createElement("canvas");
            ctx = canvas.getContext("2d");
            image.onload= function(){
                ctx.drawImage(image,100,100)
            }
        } // end for loop

    } // end showThumbnail
});
<!-- End Upload 2-->

<!-- Chk File Manager -->

function chk_file_manager(e){
    if(e==1){
        $("#elfinder_group").hide('slow');
        $("#file_manager").val(0);
    }else{
        $("#elfinder_group").show('slow');
        $("#file_manager").val(1);
    }
}
<!-- End Chk File Manager -->

<!-- File Manager -->

function getUrlParam(paramName) {
    var reParam = new RegExp('(?:[\?&]|&amp;)' + paramName + '=([^&]+)', 'i') ;
    var match = window.location.search.match(reParam) ;
    return (match && match.length > 1) ? match[1] : '' ;
}

//$().ready(function() {
//    var url_txt = 'http://'+window.location.hostname +'/tools/elfinder/php/connector.php' ;
//    var file_manager = $('#elfinder').elfinder({
//        url : url_txt,  // connector URL (REQUIRED)
//
//        // lang: 'ru',             // language (OPTIONAL)
//        getFileCallback: function(url) { // editor callback
//            //$('#IMG_PATH').val('<?=$_SERVER["SERVER_NAME"]?>'+ url.substr(9));
//            $('#img_path').val(url)
//            $('#img_type').val('2');
//            $('#remove_img,#upload').show();
//            $('#img_path2').attr('src', url);
//            get_images_file();
//            //FileBrowserDialogue.mySubmit(url); // pass selected file path to TinyMCE
//        }
//    }).elfinder('instance');
//});

<!-- End File Manager -->



<!-- Chk Upload -->
$(document).ready(function(){
    $("#remove_img1").click(function(){
        var r = confirm('Are you sure you want to delete');
        if(r == true){
            $("#img_path3").removeAttr('src');
            $("#upload1").show();
            $("#remove_img1").hide();
            $("#img_path1,#img_type1").val('');
        }else{
            return false;
        }

    });
});

$(function () {
    $("#upload1").on("click",function(e){
        var objFile= $("<input>",{
            "class":"file_upload1",
            "type":"file",
            "multiple":"true",
            "name":"img_path1",
            "style":"display:none",
            change: function(e){
                var files = this.files
                showThumbnail1(files)
                $("#upload1").hide();
                $("#remove_img1").show();
                $("#img_type1").val(1);
                $("#img_path1").val('');
            }
        });
        $(this).before(objFile);
        $(".file_upload1:last").show().click().hide();
        e.preventDefault();
    });

    function showThumbnail1(files){

        //    $("#thumbnail").html("");
        for(var i=0;i<files.length;i++){
            var file = files[i]
            var imageType = /image.*/
            if(!file.type.match(imageType)){
                var i = confirm("สกุลไฟล์ไม่ถูกต้อง");
                if(i==true || i==false){
                    exit();
                }
                continue;
            }


            //var image = document.createElement("img");
            var image = document.getElementById("img_path3");
            var thumbnail = document.getElementById("thumbnail1");
            image.file = file;
            thumbnail.appendChild(image)

            var reader = new FileReader()
            reader.onload = (function(aImg){
                return function(e){
                    aImg.src = e.target.result;
                };
            }(image))

            var ret = reader.readAsDataURL(file);
            var canvas = document.createElement("canvas");
            ctx = canvas.getContext("2d");
            image.onload= function(){
                ctx.drawImage(image,100,100)
            }
        } // end for loop

    } // end showThumbnail
});
<!-- End Upload -->

<!-- Chk File Manager -->

function chk_file_manager(e){
    if(e==1){
        $("#elfinder_group").hide('slow');
        $("#file_manager").val(0);
    }else{
        $("#elfinder_group").show('slow');
        $("#file_manager").val(1);
    }
}
<!-- End Chk File Manager -->

<!-- File Manager -->

function getUrlParam(paramName) {
    var reParam = new RegExp('(?:[\?&]|&amp;)' + paramName + '=([^&]+)', 'i') ;
    var match = window.location.search.match(reParam) ;
    return (match && match.length > 1) ? match[1] : '' ;
}

//$().ready(function() {
//    var url_txt = 'http://'+window.location.hostname +'/tools/elfinder/php/connector.php' ;
//    var file_manager = $('#elfinder').elfinder({
//        url : url_txt,  // connector URL (REQUIRED)
//
//        // lang: 'ru',             // language (OPTIONAL)
//        getFileCallback: function(url) { // editor callback
//            //$('#IMG_PATH').val('<?=$_SERVER["SERVER_NAME"]?>'+ url.substr(9));
//            $('#img_path').val(url)
//            $('#img_type').val('2');
//            $('#remove_img,#upload').show();
//            $('#img_path2').attr('src', url);
//            get_images_file();
//            //FileBrowserDialogue.mySubmit(url); // pass selected file path to TinyMCE
//        }
//    }).elfinder('instance');
//});

<!-- End File Manager -->


$(document).ready(function(){
    $("#remove_pdf").click(function(){
        var r = confirm('Are you sure you want to delete');
        if(r == true){
            $("#upload_pdf").show();
            $("#remove_pdf,#pdf_name").hide();
            $("#pdf_path").val('');
        }else{
            return false;
        }

    });
});

$(document).ready(function(){
    $("#remove_pdf1").click(function(){
        var r = confirm('Are you sure you want to delete');
        if(r == true){
            $("#upload_pdf1").show();
            $("#remove_pdf1,#pdf_name1").hide();
            $("#pdf_path_en").val('');
        }else{
            return false;
        }

    });
});
