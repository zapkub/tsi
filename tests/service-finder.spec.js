var supertest = require('supertest');
var should = require('should');

const api = supertest.agent("http://localhost:3000/tsi/public/service-finder/");


describe("service finder api testing", function() {
    this.timeout(3000);
    it("should work", function(done) {
        api
            .get('available-area/garage')
            .end(function(err, res) {
                if(err)
                {
                  done(new Error("api 404 , make sure that api is running on http://localhost:3000/tsi/public/service-finder"))
                }else {
                  done();
                }

            })
    })
    it("/available-area/garage must return array of object scheme {id:int,name:string,count:int}", function(done) {
      api
          .get('available-area/garage')
          .end(function(err, res) {
              res.body.should.have.property("area").be.an.Array();
              if(res.body.area.length > 0)
              {
                const sample = res.body.area[0];
                sample.should.have.properties("name","id","count");
                sample.name.should.be.String();
                sample.id.should.be.Number();
                sample.count.should.be.Number();
              }
              done();
          })
    })
    it("/available-province/2/garage must return have province and array of object scheme {id:int,name:string,count:int}", function(done) {
      api
          .get('available-province/2/garage')
          .end(function(err, res) {
              res.body.should.have.property("province").be.an.Array();
              if(res.body.province.length > 0)
              {
                const sample = res.body.province[0];
                sample.should.have.properties("name","id","count");
                sample.name.should.be.String();
                sample.id.should.be.Number();
                sample.count.should.be.Number();
                  done();
              }
              else{
                done(new Error("no item found"))
              }

          })
    })

    it("/available-province/1/service must return have province and array of object scheme {id:int,name:string,count:int}", function(done) {
      api
          .get('available-province/1/service')
          .end(function(err, res) {
              res.body.should.have.property("province").be.an.Array();

              if(res.body.province.length > 0)
              {
                const sample = res.body.province[0];
                sample.should.have.properties("name","id","count");
                sample.name.should.be.String();
                sample.id.should.be.Number();
                sample.count.should.be.Number();
                done();
              }
              else{
                done(new Error("no item found"))
              }

          })
    })

    it("/available-province/2/officialGarage must return have province and array of object scheme {id:int,name:string,count:int}", function(done) {
      api
          .get('available-province/2/officialGarage')
          .end(function(err, res) {
              res.body.should.have.property("province").be.an.Array();
              if(res.body.province.length > 0)
              {
                const sample = res.body.province[0];
                sample.should.have.properties("name","id","count");
                sample.name.should.be.String();
                sample.id.should.be.Number();
                sample.count.should.be.Number();
                done();
              }else{
                done(new Error("no item found"))
              }

          })
    })


    // ENDPOINT SERVICE LIST
    it("get service by district_ID must return correct object",function(done){
      api
        .get('available-service/1801/service')
          .end(function(err,res){
            res.body.should.have.property("service").be.an.Array();
            (res.body.service.length).should.be.above(0);
            res.body.service[0].should.have.properties("name","address1","address2","zipcode","phone","fax")
            done();
          })
    })
    it(" get garage by district_ID must return correct object",function(done){
      api
        .get('available-service/2304/garage')
          .end(function(err,res){
            res.body.should.have.property("service").be.an.Array();
            (res.body.service.length).should.be.above(0);
            res.body.service[0].should.have.properties("name","address1","address2","zipcode","phone","fax")
            done();
          })
    })
    it(" get official Garage  by District_ID must return correct object",function(done){
      api
        .get('available-service/1301/officialGarage')
          .end(function(err,res){
            res.body.should.have.property("service").be.an.Array();
            (res.body.service.length).should.be.above(0);
            res.body.service[0].should.have.properties("name","address1","address2","zipcode","phone","fax")
            done();
          })
    })

})


const apiEN = supertest.agent("http://localhost:3000/tsi/public/");

describe("service-finder in EN",function(){

  it("must change to EN locale",function(done){
    apiEN
      .get("lang/en")
      .end(function(err,res){
        if(err){
          done(new Error(err))
        }else{
          done();
        }

      })
  })

  it(" get official Garage by District_ID must return correct object",function(done){
    api
      .get('available-service/1301/officialGarage')
        .end(function(err,res){
          res.body.should.have.property("service").be.an.Array();
          (res.body.service.length).should.be.above(0);
          res.body.service[0].should.have.properties("name","address1","address2","zipcode","phone","fax")
          done();
        })
  })

})
